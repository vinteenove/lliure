# lliure - Plataforma de Aplicações

## Introdução

O **lliure** é uma plataforma de aplicações modular que funciona como um sistema operacional online. Diferente de um CMS tradicional, o lliure foi projetado para ser altamente extensível, permitindo o desenvolvimento de diversas aplicações como ERPs, lojas virtuais, CRMs e muito mais.

Atualmente, estamos na versão **11**, que passou por uma reformulação significativa, dividindo o lliure em múltiplos pacotes. O repositório principal agora contém o **"panel"**, que utiliza o **Composer** para carregar e gerenciar os demais pacotes necessários.

---

## Instalação

### Requisitos

Para rodar o lliure corretamente, você precisará de:

- **PHP 8.1+**
- **MySQL**
- **Web server** (Apache recomendado)
- **mod_rewrite ativo** ([documentação](http://httpd.apache.org/docs/current/mod/mod_rewrite.html))
- **Composer** instalado ([como instalar](https://getcomposer.org/download/))

### Passo a passo

1. **Baixe o repositório principal (`panel`)**
   
   ```bash
   git clone https://bitbucket.org/vinteenove/lliure.git panel
   cd panel
   ```

2. **Instale as dependências via Composer**

   ```bash
   composer install
   ```

2. **Configuração**
 Acesse `http://seuservidor.com/panel`, onde você será automaticamente redirecionado para a página de instalação. Preencha as informações necessárias (Host, Nome do banco, Usuário e Senha).

Duas coisas necessárias para que a instalação ocorra corretamente:

- Crie uma pasta chamada **uploads** paralelamente à pasta `panel`.
- Defina as permissões adequadas com o comando:
  
  ```bash
  chmod 755 uploads panel/etc
  ```

Se tudo ocorrer bem, você receberá uma mensagem de sucesso. O login e senha padrão são **dev**.

---

## Estrutura do lliure

Agora, o lliure é composto por múltiplos pacotes carregados via Composer. Por isso, é importante fazer a atualização regular dos pacotes:
   
```bash
composer update
```

Cada um desses pacotes pode ser atualizado e gerenciado de forma independente.

---

## Desenvolvimento de Aplicativos

O lliure permite que você desenvolva seus próprios aplicativos de forma modular. A estrutura principal contém:

- **`usr/`**: Componentes do núcleo do sistema.
- **`opt/`**: Aplicações internas (login, painel, etc.).
- **`api/`**: APIs para integração e comunicação entre módulos.
- **`app/`**: Aplicações externas desenvolvidas pelos usuários.

### Criando um novo aplicativo

1. Crie uma nova pasta dentro de `app/` com o nome do seu aplicativo.
2. Estruture seu aplicativo seguindo os padrões do lliure.
3. Acesse seu app com:

   ```txt
   {{seu.site}}/panel/app={{pasta_do_seu_app}}
   ```

---

## Rotas no lliure

O lliure utiliza um sistema de rotas estruturado para facilitar o desenvolvimento:

```txt
{{seu.site}}/panel/[wli/|nli/][os/|onserver/|oc/|onclient/]opt|api|app=$MODULO$
```

- `wli/` ou `nli/`: Define acesso privado ou público.
- `os/`, `onserver/`, `oc/`, `onclient/`: Define como o módulo será carregado (completo, parcial ou via AJAX).
- `opt|api|app`: Especifica se é um módulo interno, API ou aplicativo externo.
- `$MODULO$`: O nome do módulo a ser acessado.

---

## Afinal de contas, o que é o lliure?

### O que o lliure não é!

O lliure, diferente do que muitos pensam, **não é um CMS** e **também não é uma loja virtual**. Ou seja, o lliure não serve exclusivamente para "fazer sites". Sim, no início ele foi desenvolvido para esse propósito, mas ao longo do tempo percebeu-se que ele poderia ser muito mais.

Muitas pessoas perguntam por que usar lliure ao invés de WordPress. A resposta pode ser comparada à famosa música "[ela era de leão e ele tinha 16](http://www.vagalume.com.br/legiao-urbana/eduardo-e-monica.html)". São propostas completamente diferentes.

### O que é o lliure, então?

O lliure é classificado como um **WAP** (*Web Application Platform*), ou seja, uma plataforma de aplicações que pode ser comparada a um sistema operacional online.

O lliure também é altamente dependente de seus aplicativos, assim como qualquer SO depende de seus programas. Ele fornece APIs para facilitar o desenvolvimento e ajudar na padronização das aplicações.

### O que posso fazer com ele?

Como dito acima, sendo comparado a um SO, é possível fazer praticamente tudo:

- Administrar uma loja virtual
- Criar um blog
- Gerenciar o ERP de uma empresa
- Controlar fluxo de caixa
- Gerenciar pedidos

Desde que o *Garoto que Programa* esteja com disposição para trabalhar nas madrugadas, é possível desenvolver inúmeros aplicativos, além dos que já estão disponíveis no repositório.

---

## Licença

O lliure é um software open-source disponível sob a licença **MIT**.

Para mais informações, acesse: [lliure.com.br](http://lliure.com.br)
