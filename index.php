<?php

/**
 * Iniciação do lliure
 *
 * @version alpha
 * @package lliure
 * @contato Entre em contato com o desenvolvedor <lliure@lliure.com.br> http://www.lliure.com.br/
 * @Licença //mit-license.org/ MIT License
 */

/** Configurações de charset, time out e display errors */
header('Content-Type: text/html; charset=UTF-8');

set_time_limit(0);
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);

/** Carregamento o autoload */
require_once(__DIR__ . "/vendor/autoload.php");

use Helpers\HttpHelper;
use Lliure\Http\Message\ServerRequest;
use Lliure\Http\Message\Uri;
use LliurePanel\Lliure;
use Opt\SessionFix\SessionFix;
use Opt\LoadRoutines\LoadRoutines;
use Router\Dispatcher;

// HTTP Method Spoofing
$requestMethod = strtoupper($_POST['_method'] ?? $_SERVER['REQUEST_METHOD'] ?? 'GET');
unset($_POST['_method']);

$serverRequest = new ServerRequest(
	$requestMethod,
	Uri::fromParts(HttpHelper::parseFriendlyUri(HttpHelper::getPageUri())),
	apache_request_headers(),
	new \Lliure\Http\Message\Stream(fopen('php://input', 'r'))
);

$pipeLine = new Dispatcher();

$pipeLine->middleware(new SessionFix(sha1(__DIR__)));

$pipeLine->middleware(Lliure::bootstrap(__DIR__));

$pipeLine->middleware(new LoadRoutines);

$pipeLine->middleware(Lliure::isInstalled());

$pipeLine->middleware(Lliure::dispatcher());

$response = $pipeLine->handle($serverRequest);

Lliure::emit($response);