<?php
use LliurePanel\ll;
use Persona\Persona;

\Jquery\Jquery::build();

Persona::add('api/midias/css/jquery.jcrop.min.css',  'css');
Persona::add('api/midias/css/estilo.css',            'css');

Persona::add('api/midias/js/jquery.color.js',        'js');
Persona::add('api/midias/js/jquery.jcrop.js',        'js');
Persona::add('api/midias/js/script.js',              'js');