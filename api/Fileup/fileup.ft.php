<div id="fileUpModalCrop" class="modal fade">
    <div class="modal-dialog" style="width: auto; margin: 15px;">
        <div class="modal-content" style="border-radius: 0;">
            <div class="modal-body" style="border-radius: 0;">
                <div class="fileUpModal-body">

                    <img id="fileUpModalCropImage">

                    <div id="fileUpModalCropFunctions" class="btn-group btn-group-lg">
                        <button id="fileUpModalCropFunctions-histPrev" type="button" class="btn btn-bg-defalt" title="Voltar">
                            <i class="fa fa-chevron-left" aria-hidden="true"></i>
                        </button>
                        <button id="fileUpModalCropFunctions-histNext" type="button" class="btn btn-bg-defalt" title="Avançar" disabled="disabled">
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </button>
                        <button id="fileUpModalCropFunctions-dragMode" data-mode="crop" type="button" class="btn btn-bg-defalt" title="cortar ou mover">
                            <i class="fa fa-arrows" aria-hidden="true"></i>
                            <i class="fa fa-crop" aria-hidden="true"></i>
                        </button>
                        <button id="fileUpModalCropFunctions-zoomPlus" type="button" class="btn btn-bg-defalt" title="menos zoon">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                        </button>
                        <button id="fileUpModalCropFunctions-zoomLess" type="button" class="btn btn-bg-defalt" title="mais zoom">
                            <i class="fa fa-search-minus" aria-hidden="true"></i>
                        </button>
                        <button type="button" class="btn btn-bg-danger" data-dismiss="modal" title="confirmar">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <button id="fileUpModalCropFunctions-cortar" type="button" class="btn btn-bg-success" title="cortar">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    (function($){
        $(function(){

            var historizar = function(atualizar){

                var histo = [];
                var index = false;

                this.next = function(item){
                    if(typeof item == 'undefined'){
                        if((index + 1) <= histo.length){
                            ++index; ret();
                            return histo[index];
                        }
                        return false;

                    }else{
                        if(index === false)
                            index = 0;
                        else
                            index++;
                        histo.length = index;
                        histo[index] = item;
                        index = (histo.length - 1);
                        ret();
                    }
                };

                this.prev = function(){
                    if((index - 1) >= 0){
                        --index; ret();
                        return histo[index];
                    }
                    return false;
                };

                this.reset = function(){
                    histo = [];
                    index = false;
                };

                var ret = function(){
                    atualizar.call(this, !(index === 0), !(index === (histo.length - 1)));
                }
            };
            

            /** @var cropper Cropper */
            var cropper = null;

            $('#fileUpModalCrop').on('show.bs.modal', function(e){
                var $img = $('#fileUpModalCropImage');
                $img.attr('src', '');

            }).on('shown.bs.modal', function(e){

                historico.reset();
                var bas = fileUp.targetCrop;

                var src = bas.find('.fileUpThumb').attr('data-original');
                var $img = $('#fileUpModalCropImage');
                $img.attr('src', src);

                var confs = {
                    viewMode: 1,
                    autoCropArea: .9,
                    ready: function(){
                        historico.next({crop: cropper.getData(), canvas: cropper.getCanvasData()});
                    },
                    zoom: function(){
                        historico.next({crop: cropper.getData(), canvas: cropper.getCanvasData()});
                    },
                    cropend: function(){
                        historico.next({crop: cropper.getData(), canvas: cropper.getCanvasData()});
                    }
                };

                var asp = bas.find('.fileUpThumb').attr('data-crop');

                if(!!asp){
                    asp = asp.split('-');
                    confs.aspectRatio = (parseInt(asp[0]) / parseInt(asp[1]));}

                var corte = bas.find(':input[name="fileUp[crop][]"]').val();
                if(!!corte){
                    corte = corte.split('-');
                    corte = {
                        x      : Math.floor(corte[2]),
                        y      : Math.floor(corte[3]),
                        width  : Math.floor(corte[4]),
                        height : Math.floor(corte[5]),
                        rotate : 0,
                        scaleX : 1,
                        scaleY : 1
                    };
                    confs.data = corte;
                }

                cropper = new Cropper($img[0], confs);
                $('#fileUpModalCropFunctions-dragMode').attr('data-mode', 'crop');

            }).on('hidden.bs.modal', function(e){
                cropper.destroy();
            });

            $('#fileUpModalCropFunctions-cortar').click(function(){
                var bas = fileUp.targetCrop;
                var c = cropper.getData();

                var corte = [];
                var $corte = bas.find('[data-attr="corte"]');

                if($corte.length == 0){
                    bas.find('.fileUpThumb-data').append('<div data-attr="corte"></div>');
                    $corte = bas.find('[data-attr="corte"]');}

                var asp = bas.find('.fileUpThumb').attr('data-crop');
                if(!!asp){
                    asp = asp.split('-');
                    corte.push((asp[0]), (asp[1]));
                }else
                    corte.push(Math.floor(c.width), Math.floor(c.height));
                corte.push(Math.floor(c.x), Math.floor(c.y), Math.floor(c.width), Math.floor(c.height), 'm');

                var thumb = {width: corte[0], height: corte[1]};

                corte = corte.join('-');
                $corte.html('<strong>corte:</strong> ' + corte);

                console.log(corte);
                bas.find(':input[name="fileUp[crop][]"]').val(corte);

                bas.find('img.amostra').attr('src', cropper.getCroppedCanvas(thumb).toDataURL());

                $('#fileUpModalCrop').modal('hide');
            });

            $('#fileUpModalCropFunctions-zoomPlus').click(function(){
                cropper.zoom(0.1);
            });

            $('#fileUpModalCropFunctions-zoomLess').click(function(){
                cropper.zoom(-0.1);
            });

            $('#fileUpModalCropFunctions-dragMode').click(function(){
                var mode = (($(this).attr('data-mode') === 'crop')? 'move': 'crop');
                $(this).attr('data-mode', mode);
                cropper.setDragMode(mode);
            });


            var $histPrev = $('#fileUpModalCropFunctions-histPrev').click(function(){
                if(!$(this).prop('disabled')){
                    var v = historico.prev();
                    if (!!v) {
                        cropper.setData(v.crop);
                        cropper.setCanvasData(v.canvas);
                    }
                }
            });

            var $histNext = $('#fileUpModalCropFunctions-histNext').click(function(){
                if(!$(this).prop('disabled')){
                    var v = historico.next();
                    if (!!v){
                        cropper.setData(v.crop);
                        cropper.setCanvasData(v.canvas);
                    }
                }
            });

            var historico = new historizar(function(a, b){
                $histPrev.prop('disabled', !a);
                $histNext.prop('disabled', !b);
            });

        });
    })(jQuery);
</script>