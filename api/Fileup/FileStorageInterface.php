<?php

namespace Api\Fileup;

use Psr\Http\Message\StreamInterface;

interface FileStorageInterface
{

	public function fileExists(string $name): bool;
	public function fileGet($name, string $mode): StreamInterface;
	public function fileSet(string $name, $value): void;
	public function fileUnset(string $name): void;
	public function fileMoveTo(string $name, string $to): void;
	public function fileMoveFrom(string $name, string $from): void;

}