<?php
/**
*
* API Fileup - lliure WAP
*
* @Versão 8.0
* @Pacote lliure
* @Entre em contato com o desenvolvedor <lliure@lliure.com.br> http://www.lliure.com.br/
* @Licença http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

namespace Api\Fileup;

use Helpers\HttpHelper;
use Lliure\Http\Message\Stream;
use Persona\Persona;
use Api\Fileup\FileStorageInterface;
use Psr\Http\Message\StreamInterface;

/*
	no formulario use assim:
	<form  enctype="multipart/form-data" ...

	$file = new \Api\Fileup\Fileup(); 					//inicia a classe
	$file->titulo = 'Imagem'; 				//titulo da Label
	$file->rotulo = 'Selecionar imagem'; 	// texto do botão
	$file->registro = $dados['imagem'];
	$file->campo = 'imagem'; 				//campo do banco de dados (no retorno no formulario ele irá retornar um $_POST com essa chave, no caso do exemplo $_POST['imagem'])
	$file->extencao = 'png jpg'; 			//extenções permitidas para o upload, se deixar em branco será aceita todas
	$file->form(); 				 			// executa a classe

	OU

	$file = new fileup(array(
		'label' => 'Imagem',
		'name' => 'imagem',
		'value' => $dados['imagem'],
		'accept' => 'png jpg',
		'button' => 'Selecionar imagem'
	));
	echo $file;

	No retorno no formulario use assim:

 	$file = new \Api\Fileup\Fileup(); 											// incia a classe
	$file->diretorio = '../../../uploads/porta_niquel/ofertas/';	// pasta para o upload (lembre-se que o caminho é apartir do arquivo onde estiver sedo execultado)
	$file->up(); // executa a classe
*/

class Fileup implements FileStorageInterface{

    const FILE = 'file', IMAGE_SM = 'image-sm', IMAGE_LG = 'image-lg';
	
	public                         $campo,     $name;
	public                         $titulo,    $label     = null;
	public                         $registro,  $value;
	public                         $extencao,  $accept    = '*';
	public                         $rotulo,    $button    = 'Upload';
	public                         $diretorio, $directory = null;
	public                         $disabled              = false;
	public                         $crop                  = false;
	public                         $mode                  = fileup::FILE;
	protected FileStorageInterface $storager;

	public function __construct(array $attrs = array()){
		$this->campo		=& $this->name;
		$this->titulo		=& $this->label;
		$this->registro		=& $this->value;
		$this->extencao		=& $this->accept;
		$this->rotulo		=& $this->button;
		$this->diretorio	=& $this->directory;

		[
			$this->name,
			$this->label,
			$this->value,
			$this->accept,
			$this->button,
			$this->directory,
			$this->disabled,
			$this->crop,
			$this->mode
        ] = array_values(array_merge(array(
            'name'      => 'file[]',
            'label'     => null,
            'value'     => '',
            'accept'    => '*',
            'button'    => 'Upload',
            'directory' => null,
            'disabled'  => false,
            'crop'      => false,
            'mode'      => fileup::FILE,
		), $attrs));

        $this->storager = $this;
	}

    public function setStorage(FileStorageInterface $storage){
        $this->storager = $storage;
    }

	static public function make(array $attrs = array()){
		return new self($attrs);
	}

    public function __toString(){

        if(!is_array($this->name))
            $this->name = array($this->name);

        $total_names = count($this->name);

        if(!is_array($this->label))
            $this->label = array_fill(0, $total_names, $this->label);

        if(!is_array($this->value))
            $this->value = array_fill(0, $total_names, $this->value);

        if(!is_array($this->button))
            $this->button = array_fill(0, $total_names, $this->button);


        if(!empty($this->accept)){

            if((!is_array($this->accept)))
                $this->accept = array_fill(0, $total_names, $this->accept);

            foreach($this->accept as $k => $r){
                $f = array();
                foreach(($a = preg_split('/[, ]/sim', $r)) as $t){

                    if (empty($t) || $t == '*')
                        continue;

                    if (preg_match('/\\//sim', $t))
                        $f[] = $t;

                    elseif (!preg_match('/^[.]/sim', $t))
                        $f[] = '.' . $t;

                    else
                        $f[] = $t;

                }
                $this->accept[$k] = implode(',', $f);
            }
        }

        $disabled = (($this->disabled)? ' disabled="disabled"': '');

        $ret = '';
        foreach($this->name as $chave => $name){
            @list($name, $corte) = array_reverse(explode('/', $this->value[$chave])); ob_start(); ?>
            <?php if(!empty($this->label[$chave])) echo '<div class="form-group"><label>' . $this->label[$chave] . '</label>'; ?>
            <div class="fileUpBloco">
                <input type="file" name="fileUp[file][]"<?php echo (empty($this->accept[$chave])? '': ' accept="' . $this->accept[$chave] . '"') . $disabled; ?>>
                <input type="hidden" name="fileUp[del][]" value="0"<?php echo $disabled; ?>>
                <input type="hidden" name="fileUp[name][]" value="<?php echo $this->name[$chave]; ?>"<?php echo $disabled; ?>>
                <input type="hidden" name="fileUp[regant][]" value="<?php echo $name; ?>"<?php echo $disabled; ?>>
                <?php switch($this->mode){ case self::FILE: ?>
                    <div class="btn-toolbar">
                        <div class="input-group flex-fill">
                            <div class="input-group-prepend">
                                <button class="btn btn-default fileUpBloco-btn-up" type="button" style="border-right: none;"<?php echo $disabled; ?>>
                                    <i class="fa fa-upload"></i><span class="hidden-xs"> <?php echo $this->button[$chave]; ?></span>
                                </button>
                            </div>
                            <input type="text" class="form-control fileUpBloco-input" value="<?php echo $name; ?>" readonly="readonly"<?php echo $disabled; ?>>
                        </div>
                        
                        <div class="btn-group ml-1">
							<?php if (!empty($this->directory)){ ?>
                                <a href="<?php echo $this->directory. $name; ?>" class="btn btn-default fileUpBloco-btn-ver" title="Ver arquivo" target="_blank" <?php echo ((empty($name) || !!$disabled)? ' disabled="disabled"': ''); ?>>
                                    <i class="fa fa-eye" aria-hidden="true" style="line-height: inherit;"></i>
                                </a>
							<?php } ?>
                            <button class="btn btn-default fileUpBloco-btn-del" type="button" title="Deletar arquivo"<?php echo $disabled; ?>>
                                <i class="fa fa-trash" style="line-height: inherit;"><i class="fa fa-check"></i></i>
                            </button>
                        </div>
                    </div>
                <?php break; case self::IMAGE_LG: ?>
                    <input type="hidden" name="fileUp[crop][]" value="<?php echo $corte; ?>"<?php echo $disabled; ?>>
                    <div class="fileUpThumb fileUp-<?php echo self::IMAGE_LG; ?>" data-original="<?php echo ($this->directory . $name); ?>" data-crop="<?php echo $this->crop; ?>">
                        <div class="form-control" readonly="readonly">
                            <div class="fileUpThumb-image">
                                <img class="img-responsive amostra" src="<?php echo ((!empty($this->value[$chave]))? ($this->directory . $this->value[$chave]): ''); ?>">
                            </div>
                            <div class="fileUpThumb-infos">
                                <div class="fileUpThumb-name"><?php echo $name; ?></div>
                                <div class="fileUpThumb-data">
                                    <?php echo self::atributos(array_merge(
                                        ((!!$this->crop)? ['tamanho-esperado' => '<strong>tamanho esperado:</strong> ' . $this->crop]: []),
                                        ((!!$corte)? ['corte' => '<strong>corte:</strong> ' . $corte]: [])
                                    )); ?>
                                </div>
                            </div>
                            <div class="fileUpThumb-botons">
                                <div class="btn-group" role="group">
                                    <button class="btn btn-bg-defalt fileUpBloco-btn-up" type="button" title="<?php echo $this->button[$chave]; ?>"<?php echo $disabled; ?>>
                                        <i class="fa fa-upload"></i>
                                    </button>
                                    <button class="btn btn-bg-defalt fileUpBloco-btn-crop" type="button" title="Cortar"<?php echo $disabled; ?>>
                                        <i class="fa fa-crop"></i>
                                    </button>
                                    <button class="btn btn-bg-danger fileUpBloco-btn-del" type="button" title="Deletar"<?php echo $disabled; ?>>
                                        <i class="fa fa-trash"  style="line-height: inherit;"><i class="fa fa-check"></i></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php break; case self::IMAGE_SM: ?>
                    <input type="hidden" name="fileUp[crop][]" value="<?php echo $corte; ?>"<?php echo $disabled; ?>>
                    <div class="fileUpThumb fileUp-<?php echo self::IMAGE_SM; ?>" data-original="<?php echo ($this->directory . $name); ?>" data-crop="<?php echo $this->crop; ?>">
                        <div class="fileUpFile input-group">
                            <div class="form-control" readonly="readonly" style="float: none;">
                                <div class="fileUpFile-inputDiv">
                                    <div class="fileUpFile-inputDiv-image">
                                        <img class="img-responsive amostra" src="<?php echo ((!empty($this->value[$chave]))? ($this->directory . $this->value[$chave]): ''); ?>" style="display: inline-block">
                                    </div>
                                    <div class="fileUpFile-inputDiv-infos">
                                        <div class="fileUpThumb-name"><?php echo $name; ?></div>
                                        <div class="fileUpThumb-data">
                                            <?php echo self::atributos(array_merge(
                                                ((!!$this->crop)? ['tamanho-esperado' => '<strong>tamanho esperado:</strong> ' . $this->crop]: []),
                                                ((!!$corte)? ['corte' => '<strong>corte:</strong> ' . $corte]: [])
                                            )); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="input-group-btn">
                                <button class="btn btn-default fileUpBloco-btn-up btn-block" type="button"<?php echo $disabled; ?>>
                                    <i class="fa fa-upload"></i><span class="hidden-xs"> <?php echo $this->button[$chave]; ?></span>
                                </button><br>
                                <button class="btn btn-default fileUpBloco-btn-crop btn-block" type="button"<?php echo $disabled; ?>>
                                    <i class="fa fa-crop"></i><span class="hidden-xs"> Cortar</span>
                                </button><br>
                                <button class="btn btn-default fileUpBloco-btn-del btn-block" type="button" title="Deletar arquivo"<?php echo $disabled; ?>>
                                    <i class="fa fa-trash"  style="line-height: inherit;"><i class="fa fa-check"></i></i><span class="hidden-xs"> Deletar</span>
                                </button>
                            </span>
                        </div>
                    </div>
                    <?php
                    break;
                } ?>
            </div>
            <?php

            if(!empty($this->label[$chave])) {
                echo '</div>';
            }

            $ret .= ob_get_clean();
        }
        return $ret;
    }

    private static function atributos(array $itens){
	    $r = [];
        foreach($itens as $k => $v)
            $r[] = '<div data-attr="' . $k . '">' . $v . '</div> ';
        return implode('', $r);
	}

	public function form(){
		echo $this;
	}

	public function up(){

        if(isset($_POST['fileUp']) && !isset($_FILES['fileUp']['name'])){
            echo 'Arquivo não enviado. verifique se o formulario de origem está setado como <strong>enctype="multipart/form-data"</strong> <br/>';
            unset($_POST['fileUp']);
            return false;
        }

        $campos = [];

        if(isset($_POST['fileUp'])){
            foreach($_POST['fileUp']['name'] as $chave => $name){
		
				$campoName = $_POST['fileUp']['name'][$chave];
				$key = (isset($campos[$campoName])? count($campos[$campoName]): 0);
				$crop = ((isset($_POST['fileUp']['crop'][$chave]) && !empty($_POST['fileUp']['crop'][$chave]))? $_POST['fileUp']['crop'][$chave] . '/': '');
				$campos[$campoName][$key] = $crop . $_POST['fileUp']['regant'][$chave];
		
				if($_POST['fileUp']['del'][$chave] == '1'){
					$this->storager->fileUnset($_POST['fileUp']['regant'][$chave]);
					$campos[$campoName][$key] = null;
				}
		
				if($_FILES['fileUp']['error']['file'][$chave] == 0){
					$imagemNome = self::NomeUnico($_FILES['fileUp']['name']['file'][$chave]);
			
					if(!empty($_POST['fileUp']['regant'][$chave])){
						$this->storager->fileUnset($_POST['fileUp']['regant'][$chave]);
					}
			
					if($_POST['fileUp']['del'][$chave] != '1'){
						//move_uploaded_file($_FILES['fileUp']['tmp_name']['file'][$chave], $this->directory . $imagemNome);
				
						$this->storager->fileMoveFrom($imagemNome, $_FILES['fileUp']['tmp_name']['file'][$chave]);
						$campos[$campoName][$key] = $crop . $imagemNome;
				
					}else{
						$campos[$campoName][$key] = null;
					}
				}
			}
		}
        unset($_POST['fileUp']);

        foreach($campos as $campo => $values) foreach($values as $k => $file) if(!empty($file)){
            parse_str(($campo . '=' . rawurlencode($file)), $unidade);
            $_POST = self::arrayMergeRecursive($_POST, $unidade);
        }
	}

	private static function arrayMergeRecursive(){
        $recurse = function(&$array, &$merge) use (&$recurse){
            if(!is_array($merge)){
                $array = $merge;
                return $array;}

            foreach($merge as $k => $v){

                if(!key_exists($k, $array) || (key_exists($k, $array) && !is_array($array[$k]))){
                    $array[$k] = $v; continue;}

                if(is_array($v)){
                    $recurse($array[$k], $v); continue;}

                if(!is_array($v) && $k === 0)
                     $array[  ] = $v;
                else $array[$k] = $v;

            }return $array;
        };

        $args = func_get_args();
        $array = array_shift($args);

        if(!is_array($array)) return $array;

        foreach ($args as $arg) if(is_array($arg))
            $array = $recurse($array, $arg);
        return $array;
    }

	private static function NomeUnico($arquivo){
		$imagemNome = explode('.', $arquivo);
		$extenc = array_pop($imagemNome);
		$imagemNome = join(".", $imagemNome);
		$imagemNome = jf_urlformat($imagemNome);
		$imagemNome = $imagemNome.'_'.substr(md5(time()), rand(0, 20), 8).'.'.$extenc;
		$imagemNome = strtolower($imagemNome);
		return $imagemNome;
	}

	static function build(){
        \Jquery\Jquery::build();
        \Cropperjs\Cropperjs::build();
        \Api\Vigile\Vigile::build();
        Persona::add(__DIR__. '/fileup.css', 'css', 5);
        Persona::add(__DIR__. '/fileup.js', 'js:footer', 5);
        Persona::add(__DIR__. '/fileup.ft.php', 'php:footer', 5);
    }


	public function fileExists(string $name): bool{
        return file_exists($this->directory . $name);
	}

	public function fileGet($name, string $mode): StreamInterface{
        return new Stream(fopen($this->directory . $name, $mode));
	}
	
	public function fileSet(string $name, $value): void{
        file_put_contents($this->directory . $name, $value);
	}

	public function fileUnset(string $name): void{
		@unlink($this->directory . $name);
	}
	
	public function fileMoveTo(string $name, string $to): void{
        @rename($this->directory . $name, $to);
	}

	public function fileMoveFrom(string $name, string $from, int $permissions = 0644): void{
        @rename($from, $this->directory . $name);
		@chmod($this->directory . $name, $permissions);
	}
}