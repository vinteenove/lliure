<?php

/**
 * Class Vigile
 *
 * classe para gerenciar mensagens de retorno. mesagens estas que são gravadas em sessão, e seram mostrada em uma proxima tela.
 *
 * Carrega resposta para a Saida em Main, isto é, mensagens flutuantes na tela.
 * Vigile::alert('teste main');
 * Vigile::success('teste 2 main');
 * Vigile::info('teste 3 main');
 * Vigile::warning('teste 4 main');
 * Vigile::danger('teste 5 main');
 *
 * Para saidas onde em um local, é precisso 3 psaaos.
 *
 * 1º criar a clase para alimentar o local. utilile a seguinte função fabrica:
 * $vigile = Vigile::location('top');
 *
 * 2º alimete o local com mensagens de retono.
 * $vigile->alert('teste');
 * $vigile->success('teste 2');
 * $vigile->info('teste 3');
 * $vigile->warning('teste 4');
 * $vigile->danger('teste 5');
 *
 * 3º escolha uma dos pois layouts de saida popup ou callout.
 * Vigile::popup('top');
 * Vigile::callout('top');
 *
 */

namespace Api\Vigile;

use Jquery\Jquery;
use Persona\Persona;
use Persona\PersonaBuilder;

class Vigile implements PersonaBuilder{
    public static function build():void{
        Jquery::build();

        Persona::style(__DIR__. '/vigile.css', Persona::COMPONENT_PRIORITY);
        Persona::script(__DIR__. '/vigile.js',  Persona::COMPONENT_PRIORITY, Persona::HEADER_LOCATION);
        Persona::call(__NAMESPACE__ . '\Vigile::script', Persona::COMPONENT_PRIORITY, Persona::FOOTER_LOCATION);
    }

    private function __construct(){}

    public static function location($location){
        return new VigileLocation($location);
    }

    private static function set($type, $msg, array $argus){
        $local = ([
            ((isset($argus[0]))? $argus[0]: 1),
            ((isset($argus[1]))? $argus[1]: 0),
        ]);
        $opt = array_diff_key($argus, [0, 1]);
        $_SESSION['ll']['vigile']['main'][] = [
            'type'  => $type,
            'msg'   => $msg,
            'local' => $local,
            'opt'   => $opt,
        ];
    }

    public static function alert($msg, array $argus = []){
        self::set('alert', $msg, $argus);
    }

    public static function success($msg, array $argus = []){
        self::set('success', $msg, $argus);
    }

    public static function info($msg, array $argus = []){
        self::set('info', $msg, $argus);
    }

    public static function warning($msg, array $argus = []){
        self::set('warning', $msg, $argus);
    }

    public static function danger($msg, array $argus = []){
        self::set('danger', $msg, $argus);
    }

    public static function popup($location){
        ?>
        <div id="<?php echo $location; ?>">
            <?php if(isset($_SESSION['ll']['vigile']['locations'][$location])) foreach($_SESSION['ll']['vigile']['locations'][$location] as $k => $alert){
                self::popupContent($alert);
            }
            unset($_SESSION['ll']['vigile']['locations'][$location]); ?>
        </div>
    <?php }

    protected static function popupContent($alert){ ?>
        <div class="alert alert-<?php echo $alert['type']; ?> alert-dismissible">
            <?php if(!isset($alert['opt']['clouse']) || $alert['opt']['clouse'] != false){ ?>
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
            <?php } ?>
            <?php echo((is_array($alert['msg']))? ((isset($alert['msg']['msg']))? $alert['msg']['msg']: ''): $alert['msg']); ?>
        </div>
    <?php }


    public static function callout($location){ ?>
        <div id="<?php echo $location; ?>">
            <?php if(isset($_SESSION['ll']['vigile']['locations'][$location])) foreach($_SESSION['ll']['vigile']['locations'][$location] as $k => $alert){
                self::calloutContent($alert);
            }
            unset($_SESSION['ll']['vigile']['locations'][$location]); ?>
        </div>
    <?php }

    protected static function calloutContent($alert){ ?>
        <div class="callout callout-fade callout-<?php echo $alert['type']; ?>">
            <?php if(!isset($alert['opt']['clouse']) || $alert['opt']['clouse'] != false){ ?>
                <button type="button" class="close" data-dismiss="callout">
                    <span aria-hidden="true">&times;</span>
                </button>
            <?php } ?>
            <?php echo((is_array($alert['msg']))? ((isset($alert['msg']['msg']))? $alert['msg']['msg']: ''): $alert['msg']); ?>
        </div>
    <?php }

    public static function script(){
        if(isset($_SESSION['ll']['vigile']) && !empty($_SESSION['ll']['vigile'])){ ?>
            <!-- Vigile -->
            <script type="text/javascript">
                (function ($) {
                    $(function () {<?php if(isset($_SESSION['ll']['vigile']['main'])) foreach($_SESSION['ll']['vigile']['main'] as $k => $v){ ?>

                        Vigile(<?php echo((!empty($v['opt']))? json_encode($v['opt']): ''); ?>).<?php echo $v['type']; ?>('<?php echo $v['msg'] ?>', <?php echo json_encode($v['local']); ?>);

                        <?php }unset($_SESSION['ll']['vigile']['main']); ?>
                    });
                })(jQuery);
            </script>
        <?php }
    }

}