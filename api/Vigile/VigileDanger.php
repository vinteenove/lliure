<?php

namespace Api\Vigile;

class VigileDanger extends VigileException{
    public function __construct($message = "", $code = 0, Throwable $previous = null){
        parent::__construct(self::danger, $message, $code, $previous);
    }
}