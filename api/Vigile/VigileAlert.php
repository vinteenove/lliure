<?php

namespace Api\Vigile;

class VigileAlert extends VigileException{
    public function __construct($message = "", $code = 0, Throwable $previous = null){
        parent::__construct(self::alert, $message, $code, $previous);
    }
}