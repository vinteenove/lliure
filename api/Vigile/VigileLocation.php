<?php

namespace Api\Vigile;

use IteratorAggregate;
use LliurePanel\ll;
use ArrayIterator;

class VigileLocation implements IteratorAggregate{

    private $location = '';
    private $data = [];

    public function __construct($location = null){
        if(!isset($_SESSION['ll']['vigile']['locations'])) {
            $_SESSION['ll']['vigile']['locations'] = [];
        }

        if(!!$location && !isset($_SESSION['ll']['vigile']['locations'])) {
            $_SESSION['ll']['vigile']['locations'][$location] = [];
        }

        if(!!$location){
            $this->location = $location;
        }

        $this->data =& $_SESSION['ll']['vigile']['locations'][$this->location];
    }


    /**
     * @return ArrayIterator|Traversable
     */
    public function getIterator(){
        return new ArrayIterator(ll::ato($this->data));
    }

    public function alert($msg, $argus = []){
        $this->set('alert', $msg, $argus);
    }

    public function success($msg, $argus = []){
        $this->set('success', $msg, $argus);
    }

    public function info($msg, $argus = []){
        $this->set('info', $msg, $argus);
    }

    public function warning($msg, $argus = []){
        $this->set('warning', $msg, $argus);
    }

    public function danger($msg, $argus = []){
        $this->set('danger', $msg, $argus);
    }

    final protected function set($type, $msg, $argus){
        $this->data[] = [
            'type' => $type,
            'msg' => $msg,
            'opt' => $argus,
        ];
    }

    final public function clear(){
        $this->data = [];
    }

    final public function getClear(){
        list($return, $this->data) = [$this->data, []];
        return $return;
    }
}
