<?php

namespace Api\Vigile;

class VigileInfo extends VigileException{
    public function __construct($message = "", $code = 0, Throwable $previous = null){
        parent::__construct(self::info, $message, $code, $previous);
    }
}