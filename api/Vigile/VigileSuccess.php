<?php

namespace Api\Vigile;

class VigileSuccess extends VigileException{
    public function __construct($message = "", $code = 0, Throwable $previous = null){
        parent::__construct(self::success, $message, $code, $previous);
    }
}