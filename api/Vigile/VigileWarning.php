<?php

namespace Api\Vigile;

class VigileWarning extends VigileException{
    public function __construct($message = "", $code = 0, Throwable $previous = null){
        parent::__construct(self::warning, $message, $code, $previous);
    }
}