<?php

namespace Api\Vigile;

use Exception;

class VigileException extends Exception{

    const
        success = 'success',
        info = 'info',
        warning = 'warning',
        danger = 'danger',
        alert = 'alert';

    protected $type = self::success;

    public function __construct($type, $message = "", $code = 0, Throwable $previous = null){
        $this->type = $type;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function getType(){
        return $this->type;
    }
}