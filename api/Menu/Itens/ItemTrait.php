<?php

namespace Api\Menu\Itens;

use LliurePanel\ll;

trait ItemTrait{
    public function draw(){
        list($attrs, $href, $icone, $nome) = $this->prepare();
        
        ?><li <?php echo \Helpers\HtmlHelper::attr($attrs); ?>>
            <a href="<?php echo $href; ?>" class="nav-link<?php echo (($this->active)? ' active': ''); ?>">
                <?php if($icone){ ?><i class="<?php echo $icone; ?>"></i><?php } ?>
                <p><?php echo $nome; ?></p>
            </a>
        </li><?php
    }
}