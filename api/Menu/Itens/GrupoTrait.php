<?php

namespace Api\Menu\Itens;

use LliurePanel\ll;

trait GrupoTrait{
    public function draw(){
        list($attrs, $href, $icone, $nome) = $this->prepare();

        if($this->myNivel() <= 0){ ?>
            <?php if(!empty($nome)){ ?>
                <li <?php echo \Helpers\HtmlHelper::attr($attrs); ?>><?php echo $nome; ?></li>
            <?php } ?>
            <?php foreach($this->children as $iten) $iten->draw(); ?>
        <?php }else{ ?>
            <li <?php echo \Helpers\HtmlHelper::attr($attrs); ?>>
                <a href="<?php echo $href; ?>" class="nav-link<?php echo (($this->active)? ' active': ''); ?>">
                    <i class="<?php echo $icone; ?>"></i>
                    <p>
                        <?php echo $nome; ?>
                        <?php if(!empty($this->children)){ ?>
                            <i class="right fas fa-angle-left"></i>
                        <?php } ?>
                    </p>
                </a>
                <?php if(!empty($this->children)){ ?>
                    <ul class="nav nav-treeview">
                        <?php foreach($this->children as $iten) $iten->draw(); ?>
                    </ul>
                <?php } ?>
            </li>
        <?php }
    }
}