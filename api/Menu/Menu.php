<?php

namespace Api\Menu;

use LliurePanel\ll;

abstract class Menu{
    /* @var $father $this */

    protected $father;
    protected $active = false;
    protected $type = '';
    protected $folder = '';
    protected $attrs = [];
    protected $children = [];

    public function __construct($pasta, ...$params){
        $attrs = $itens = [];
        if(func_num_args() == 2){
            list($attrs) = $params;
        }elseif(func_num_args() > 2){
            list($attrs, $itens) = $params;
        }

        if(is_array($attrs) && isset($attrs[0], $attrs[1])){
            $attrs['fa'] = $attrs[0];
            $attrs['nome'] = $attrs[1];
            unset($attrs[0], $attrs[1]);
        }

        $base = [
            'fa' => '',
            'nome' => '',
            'href' => false,
            'query' => false,
            'class' => false,
        ];

        if(is_array($itens)) foreach($itens as $item){
            if($item instanceof self){
                $item->father = $this;
            }
        }

        $this->folder = $pasta;
        $this->attrs = array_merge($base, $attrs);
        $this->type = basename(\Helpers\HttpHelper::path(get_called_class()));
        $this->children = $itens;
    }

    public function prepare(){
        global $_ll;

        $attrs = $this->attrs;
        $nome = $attrs['nome'];
        $icone = $this->ico();
        $href = $attrs['href'];
        $query = $attrs['query'];
        $class = $attrs['query'];
        unset($attrs['fa'], $attrs['nome'], $attrs['href'], $attrs['query'], $attrs['class']);


        $href = (!!$href)? $href: (($q = ((is_string($query))? $query: ((is_array($query))? http_build_query($query): null)) && !empty($q))? '&'.$q: '');
        $attrs['class'] = (($this->myNivel() <= 0)? 'nav-header': 'nav-item') . ((!empty($this->children))? ' has-treeview': '') . (($class)? ' ' . $class: '');

        return [$attrs, $href, $icone, $nome];
    }

    abstract public function draw();

    protected function ico(){
        return (($this->attrs['fa'])? 'nav-icon fa ' . $this->attrs['fa']: '');
    }

    protected function myNivel(){
        return (($this->type == 'Grupo' && $this->father == null)? 0: 1 + (($this->father != null)? $this->father->myNivel(): 0));
    }

    protected function path(){
        return (($this->father != null)? $this->father->path() . '>': '') . $this->folder;
    }

}