<?php


namespace Api\Jfbox;
use Persona\Persona;
use LliurePanel\ll;

class Jfbox
{
     static function build(){
        \Jquery\Jquery::build();

        Persona::add(__DIR__. '/jfbox.css',       'css');
        Persona::add(__DIR__. '/jquery.jfbox.js', 'js');
     }
}