<?php
use LliurePanel\ll;
use Persona\Persona;

\Jquery\Jquery::build();
\Bootstrap\Bootstrap::build();

Persona::add(__DIR__. '/css/bootstrap-datepicker3.css', 'css', 2);

Persona::add(__DIR__. '/js/bootstrap-datepicker.min.js', 'js', 2);
Persona::add(__DIR__. '/locales/bootstrap-datepicker.pt-br.min.js', 'js', 2);
Persona::add(__DIR__. '/js/bootstrap-datepicker-default.js', 'js', 2);