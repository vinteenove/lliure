<?php

namespace Api\Aplimo;

class Apm {
    public ?array $query;
    public string $home;
    public string $onserver;
    public string $onclient;
}