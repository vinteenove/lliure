<?php

namespace Api\Aplimo\NewSideBar;

class Link extends \Menu\Itens\Link implements AplimoSideBarInterface{
    use AplimoSideBar;

    public function __construct(string $key, string $label, array $attrs = []){
        $fa = ((isset($attrs['fa']))? $attrs['fa']: '');
        $mark = ((isset($attrs['mark']))? $attrs['mark']: '');
        $home = ((isset($attrs['home']))? $attrs['home']: '');
        $href = ((isset($attrs['href']))? $attrs['href']: '');
        $query = ((isset($attrs['query']))? $attrs['query']: '');
        $destiny = ((isset($attrs['destiny']))? $attrs['destiny']: '');
        unset($attrs['fa'], $attrs['nome'], $attrs['mark'], $attrs['href'], $attrs['query'], $attrs['destiny']);
        if($fa) $this->ico($fa);
        if($mark) $this->mark($mark);
        if($home) $this->home($home);
        if($query) $this->query($query);
        if($destiny) $this->destiny($destiny);
        parent::__construct($key, $label, $href, $attrs);
    }

    public function __toString(): string{
        $icone = $this->getIco();
        $this->label = '<i class="' . $icone . '"></i><p>' . $this->label . '</p>';
        $this->href = $this->getHref();
        return parent::__toString();
    }

    public function isPage(): bool{
        return true;
    }
}