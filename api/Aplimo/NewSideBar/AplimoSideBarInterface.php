<?php

namespace Api\Aplimo\NewSideBar;

use LliurePanel\ll;
use Menu\MenuIteratorInterface;
use Helpers\HttpHelper;

interface AplimoSideBarInterface
{

    /**
     * @param array|string
     * @return AplimoSideBarInterface
     */
    public function mark($mark): AplimoSideBarInterface;

    /**
     * @param string
     * @return AplimoSideBarInterface
     */
    public function ico(string $faClass): AplimoSideBarInterface;

    /**
     * @param bool $isHome
     * @return AplimoSideBarInterface
     */
    public function home(bool $isHome): AplimoSideBarInterface;

    /**
     * @param string $query
     * @return AplimoSideBarInterface
     */
    public function query(string $query): AplimoSideBarInterface;

    /**
     * @param string $destiny
     * @return AplimoSideBarInterface
     */
    public function destiny(string $destiny): AplimoSideBarInterface;

    /**
     * @return int
     */
    public function getLevel(): int;

    public function getIco(): string;

    /**
     * @return string
     */
    public function getPath(): string;

    public function getQuery();

    public function getHref(): string;

    /**
     * @return string
     */
    public function getDestiny(): string;


    public function isHome(): bool;

    public function isPage(): bool;

    public function isTarget(array $query): bool;
}