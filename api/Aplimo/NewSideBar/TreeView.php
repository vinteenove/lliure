<?php

namespace Api\Aplimo\NewSideBar;

class TreeView extends \Menu\Itens\TreeView implements AplimoSideBarInterface{
    use AplimoSideBar;

    public function __construct(string $key, string $label, array $attrs = []){

        $fa = ((isset($attrs['fa']))? $attrs['fa']: '');
        $mark = ((isset($attrs['mark']))? $attrs['mark']: '');
        $home = ((isset($attrs['home']))? $attrs['home']: '');
        unset($attrs['fa'], $attrs['nome'], $attrs['mark']);

        if($fa) $this->ico($fa);
        if($mark) $this->mark($mark);
        if($home) $this->home($home);

        parent::__construct($key, $label, $attrs);
    }

    public function __toString(): string{
        if($this->children()){
            foreach($this->children() as $child){
                $this->subMenu .= $child;
            }
        }

        if($this->getLevel() <= 0){
            return $this->template()->render('Header', get_object_vars($this));
        }else{
            $this->label = '<i class="' . $this->getIco() . '"></i> <p>' . $this->label . '<i class="fas fa-angle-left right"></i></p>';
            return $this->template()->render('TreeView', get_object_vars($this));
        }
    }

    public function isPage(): bool{
        return false;
    }
}