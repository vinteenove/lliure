<?php

namespace Api\Aplimo\NewSideBar;

use LliurePanel\ll;
use Menu\MenuIteratorInterface;
use Helpers\HttpHelper;

trait AplimoSideBar
{
    protected ?array $mark = null;
    protected string $ico = '';
    protected bool $home = false;
    protected string $query = '';
    protected ?string $destiny = null;

    /**
     * @param array|string
     * @return AplimoSideBarInterface
     */
    public function mark($mark): AplimoSideBarInterface
    {
        if(is_string($mark)) $mark = explode(',', $mark);
        if(is_array($mark)) $this->mark = $mark;
        return $this;
    }

    /**
     * @param string
     * @return AplimoSideBarInterface
     */
    public function ico(string $faClass): AplimoSideBarInterface
    {
        $this->ico = $faClass;
        return $this;
    }

    /**
     * @param bool $isHome
     * @return AplimoSideBarInterface
     */
    public function home(bool $isHome): AplimoSideBarInterface
    {
        $this->home = $isHome;
        return $this;
    }

    /**
     * @param string $query
     * @return AplimoSideBarInterface
     */
    public function query(string $query): AplimoSideBarInterface
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @param string $destiny
     * @return AplimoSideBarInterface
     */
    public function destiny(string $destiny): AplimoSideBarInterface
    {
        $this->destiny = $destiny;
        return $this;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        $level = -1;
        $current = $this;
        do{
            $level++;
            $current = $current->parent();
        }while($current instanceof MenuIteratorInterface);
        return $level;
    }

    public function getIco(): string
    {
        if($this->ico) return "nav-icon {$this->ico}";
        switch($this->getLevel()){
            case 1:  return 'nav-icon fas fa-circle';
            case 2:  return 'nav-icon far fa-circle';
            default: return 'nav-icon far fa-dot-circle';
        }
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        $path = '';
        $current = $this;
        do{
            $path = $current->getKey() . ((empty($path))? '': '>') . $path;
            $current = $current->parent();
        } while ($current instanceof MenuIteratorInterface);
        return $path;
    }

    public function getQuery()
    {
        list($p, $q) = $t = array_values(array_merge(['path' => '', 'query' => ''], parse_url($this->query)));

        $query = $path = [];
        HttpHelper::parse_get(explode('/', $p), $path);
        parse_str($q, $query);

        $query = array_merge($path, $query);

        $query = HttpHelper::buildQuery($query);
        return $query;
    }

    public function getHref(): string
    {
        global $_ll;
        if(isset($this->href) && !empty($this->href)) return $this->href;
        return ll::$data->app->home . 'apm=' . $this->getPath() . '/' . $this->getQuery();
    }

    /**
     * @return string
     */
    public function getDestiny(): string
    {
        if($this->destiny !== null){
            return $this->destiny;
        }
        return $this->getPath();
    }

    /**
     * @return bool
     */
    public function isHome(): bool{
        return $this->home;
    }

    /**
     * @return array
     */
    public function getMark(): array{
        if($this->mark !== null){
            return $this->mark;
        }
        return ['apm' => $this->getPath()];
    }

    /**
     * @param array $query
     * @return bool
     */
    public function isTarget(array $query): bool{
        $marks = $this->getMark();
        $path = $this->getPath();

        foreach($marks as $k => $m){
            if((is_numeric($k) && $path == $m) || (is_string($k) && isset($query[$k]) && ($query[$k] == $path || $query[$k] == $m))){
                return true;
            }
        }

        return false;
    }

}