<?php

namespace Api\Aplimo;

use Api\Aplimo\NewSideBar\AplimoSideBarInterface;
use Helpers\HttpHelper;
use Menu\Itens\BrandText;
use Menu\Itens\Breadcrumb;
use Menu\Itens\BreadcrumbLink;
use Menu\Itens\Button;
use Menu\Itens\Link;
use Menu\Itens\Menu;
use Menu\Itens\PushMenu;
use Menu\Itens\Search;
use Menu\MenuData;
use Menu\MenuParentInterface;
use Menu\MenuTemplateInterface;
use Persona\PersonaBuilder;
use Persona\Persona;
use ShortTag\ShortTag;
use ClassBody\ClassBody;
use LliurePanel\ll;

/**
 *
 * API Aplimo - lliure
 *
 * @Versão 7
 * @Pacote lliure
 * @Entre  em contato com o desenvolvedor <jfrasson@vinteenove.com.br> http://www.lliure.com.br/
 * @Licença //mit-license.org/ MIT License
 *
 */

/*

Documentação
// O nome Aplimo origina-se da junção das palavras aplikajó e temo


## Utilização

	$aplimo = new aplimo();
	$aplimo->nome = 'Aplimo';

	$aplimo->menu('Página incial', 'home');

	$aplimo->sub_menu('Configuração');
	$aplimo->sub_menu_item('Clientes', 'clientes');
	$aplimo->sub_menu_item('Contratos', 'contato');

	$aplimo->menu('Faturas', 'faturas');
	$aplimo->menu('Audiéncias', 'audiencias');
	$aplimo->menu('Consultas', 'consultas');

	$aplimo->monta();


	**************    Explicação    **************

	Iniciando a classe
	$aplimo = new aplimo();

	Define o nome do aplicativo (que vai aparecer na barra superiror)
	$aplimo->nome = 'Teste';

	Define um link no menu
	$aplimo->menu('Página incial', 'home');

	Define inicio de um sub-menu
	$aplimo->sub_menu('Configuração');

	Define os links do sub-menu criado
	$aplimo->sub_menu_item('Clientes', 'clientes');




	***********	Exemplo HC Menu

	$this->hc_menu_item('a', array('url' => $_ll[$_ll['operation_type']]['home'].'&apm=jogo&sapm=estadio', 'texto' => 'Listar todos'));
*/

class Aplimo
{

    public static function build(): void{
        \Jquery\Jquery::build();
        \FontAwesome\FontAwesome::build();

        Persona::style(__DIR__ . '/aplimo.css', Persona::COMPONENT_PRIORITY);
        Persona::script(__DIR__ . '/aplimo.js', Persona::COMPONENT_PRIORITY);
        Persona::call('\Api\Aplimo\Aplimo::loadJS', Persona::COMPONENT_PRIORITY);
    }


    private $smalt = null;
    private static string $js = '';

    public string $nome = '';
    private static ?string $basePath = null;

    private $pagePath = false;
    private $pageFile = false;

    public MenuTemplateInterface $sidebarMenu;
    public MenuTemplateInterface $hdMenuLeft;
    public MenuTemplateInterface $hdMenuRight;

    public Apm $apm;
    public Wli $wli;
    public ?MenuData $sidbarTarget = null;

    /**
     * Aplimo constructor.
     * @param bool   $botaoHome
     * @param string $basePath
     */
    function __construct(bool $botaoHome, string $basePath)
    {
        global $_ll;
        self::basePath($basePath);

        $this->sidebarMenu = new \Menu\Itens\Sidebar(\Menu\Itens\Sidebar::NavBar, [
            'class'          => "nav nav-pills nav-sidebar flex-column nav-child-indent",
            'data-widget'    => "treeview",
            'role'           => "menu",
            'data-accordion' => "false",
        ]);
        $this->hdMenuLeft = new Menu(Menu::NavBar);
        $this->hdMenuRight = new Menu(Menu::NavBar, ['class' => 'ml-auto']);
        $this->apm = new Apm();
        $this->wli = new Wli();
    }

    static function basePath(?string $pasta = null): string
    {
        if (!!$pasta){
            self::$basePath = $pasta;
        }
        return self::$basePath;
    }

	private static function searchPageByActivation($item): ?MenuData{
		if($item instanceof MenuData){
            if($item->isActive()){
                return $item;
            }
		}

		if($item instanceof MenuParentInterface){
			if($item->children()){
				foreach($item->children() as $child){
                    if($active = self::searchPageByActivation($child)){
                        return $active;
                    }
				}
			}
		}

		return null;
	}
	
	/**
     * @param $url
     * @return string
     */
    static function realUrl($url)
    {
        return \Helpers\HttpHelper::path(str_replace(['>', '<', '..//'], ['/', '/../', '../'], $url), '>');
    }

    private static function searchPageByQuery($query, $haystack)
    {

        if($haystack instanceof AplimoSideBarInterface){
            if($haystack->isTarget($query)){
                return $haystack;
            }
        }

        if($haystack instanceof MenuParentInterface){
            if($haystack->children()){
                foreach($haystack->children() as $child){
                    $page = self::searchPageByQuery($query, $child);
                    if($page){
                        return $page;
                    }
                }
            }
        }

        return null;
    }

    private static function searchFirstPage($haystack): ?AplimoSideBarInterface
    {

        if($haystack instanceof AplimoSideBarInterface){
            if($haystack->isPage()){
                return $haystack;
            }
        }

        if($haystack instanceof MenuParentInterface){
            if($haystack->children()){
                foreach($haystack->children() as $child){
                    $page = self::searchFirstPage($child);
                    if($page){
                        return $page;
                    }
                }
            }
        }

        return null;
    }

    function header()
    {
        global $_ll, $mainMenuLeft;
        ll::$data->titulo = strip_tags($this->nome) . " | " . ll::$data->titulo;

        $this->sidbarTarget = self::searchPageByQuery($_GET, $this->sidebarMenu);
        if(!$this->sidbarTarget){
            if(isset($_GET['apm'])){
                $key = $_GET['apm'];
                $pagePath = explode('>', self::realUrl($key));
                $pageFile = array_pop($pagePath);
                $pagePath = implode('/', [...$pagePath, $pageFile]);
                $pageFileX = self::$basePath . "{$pagePath}/{$pageFile}.x.php";
                $pageFileHD = self::$basePath . "{$pagePath}/{$pageFile}.hd.php";
                $pageFileOS = self::$basePath . "{$pagePath}/{$pageFile}.os.php";
                $pageFileOC = self::$basePath . "{$pagePath}/{$pageFile}.oc.php";
                if(file_exists($pageFileX) || file_exists($pageFileHD) || file_exists($pageFileOS) || file_exists($pageFileOC)){
                    $sidebarMenu = new \Menu\Itens\Sidebar(\Menu\Itens\Sidebar::NavBar);
                    $sidebarMenu->children($this->sidbarTarget = new \Api\Aplimo\NewSideBar\Link($key, 'destiny'));
                }
            }
        }
        if(!$this->sidbarTarget){
            $this->sidbarTarget = self::searchFirstPage($this->sidebarMenu);
        }

        $path = null;
        $apm = null;
        if($this->sidbarTarget){
            $path = $this->sidbarTarget->getDestiny();
            $apm = $this->sidbarTarget->getPath();
        }

        $baseUri = strtolower(basename(self::$basePath));
        $this->pagePath = self::realUrl($path);
        $this->pagePath = explode('>', $this->pagePath);
        $this->pageFile = array_pop($this->pagePath);
        $this->pagePath = implode('/', [...$this->pagePath, $this->pageFile]);

        $this->apm->home = $baseUri . '/apm=' . $apm;
        $this->apm->onserver = $baseUri . '/os/apm=' . $apm;
        $this->apm->onclient = $baseUri . '/oc/apm=' . $apm;

        $this->wli->hd = self::$basePath . "{$this->pagePath}/{$this->pageFile}.hd.php";
        $this->wli->os = self::$basePath . "{$this->pagePath}/{$this->pageFile}.os.php";
        $this->wli->oc = self::$basePath . "{$this->pagePath}/{$this->pageFile}.oc.php";
        $this->wli->x = self::$basePath . "{$this->pagePath}/{$this->pageFile}.x.php";

        //$mainMneuBradImage = $mainMenuLeft->children()->find('mainMneuBradImage');
        //$mainMneuBradImage->before(new PushMenu('aplimoPushMenu', 'PushMenu'));
        //$mainMneuBradImage->after(new Link('aplimoNameApp', $this->nome, $_ll[$_ll['operation_type']]['home']));

        if (file_exists($this->wli->hd)){
            require_once $this->wli->hd;
        }

        ll::on('prepend.content', function (){
			if(!self::searchPageByActivation($this->sidebarMenu)){
			    $this->sidbarTarget->active(true);
			} ?>
            <aside class="aplimo-sidebar main-sidebar sidebar-dark-white">
                <div class="sidebar">
                    <div class="aplimo-sidebar-padding">
                        <nav class="mt-4">
                            <?php echo $this->sidebarMenu; ?>
                        </nav>
                    </div>
                </div>
            </aside>
        <?php });

        ll::on('ClassBody', function (ClassBody $e) {
            $e->removeClass(['sidebar-collapse']);
        });
    }

    function onserver()
    {

        if ($this->pagePath === false){
            die(json_encode(array('erro' => 'Comando aplimo::header() ainda não foi executado')));
        }

        if (file_exists($this->wli->os)){
            return require_once $this->wli->os;
        }

        $name = basename($this->wli->os);
        die(json_encode(array('erro' => "Arquivo {$name} não encontrado na pagina requisitada.")));
    }

    function onclient()
    {

        if ($this->pagePath === false){
            die('Comando aplimo::header() ainda não foi executado');
        }
        
        if (file_exists($this->wli->oc)){
            return require_once $this->wli->oc;
        }

        $name = basename($this->wli->oc);
        die(json_encode(array('erro' => "Arquivo {$name} não encontrado na pagina requisitada.")));
    }

    /**
     * @param callable|null $content
     */
    public function monta(?callable $content = null)
    {

        if ($this->pagePath === false) {
            echo('Comando aplimo::header() ainda não foi executado');
            return;
        }

        if (file_exists($this->wli->x) || is_callable($content)) { ?>

            <div class="content-header">
                <div class="container-fluid">
                    <?php if($this->hdMenuLeft->children() || $this->hdMenuRight->children()) { ?>
                        <nav id="apm-h-menu" class="navbar navbar-expand navbar-white navbar-light ml-0">
                            <?php echo $this->hdMenuLeft; ?>
                            <?php echo $this->hdMenuRight; ?>
                        </nav>
                    <?php } ?>
                </div>
            </div>

            <div class="content">
                <div class="container-fluid">
                    <?php if (is_callable($content)){
                        call_user_func($content);
                    }
                    elseif (file_exists($this->wli->x)){
                        require_once $this->wli->x;
                    } ?>
                </div>
            </div>

        <?php } else {
            $name = basename($this->wli->x);
            echo("Arquivo {$name} não encontrado para pagina requisitada.");
        }
    }

    public static function loadJS(){ ?>
        <!-- aplimo Load scripts -->
        <script>
            <?php echo self::$js; ?>
        </script>
    <?php }















    /** @deprecated */
    function menu($titulo, $url, $fa = null)
    {

        /* $array = array(
            'titu'     => $titulo,
            'link'     => $url,
            'class'    => '',
            'basePath' => self::$basePath,
        );
        if($fa != null) $array['fa'] = $fa;
        $this->menu[] = $array; */

        self::menuNovo(array(
            self::menuItem($url, ((!!$fa) ? [substr($fa, 3), $titulo] : $titulo)),
        ));
    }

    /** @deprecated */
    function sub_menu($titulo, $folder, $fa = null)
    {
        self::menuNovo(array(
            (self::menuSubGrupo($folder, ((!!$fa) ? [substr($fa, 3), $titulo] : $titulo))),
        ));
        $this->smalt = $folder;
    }

    /** @deprecated */
    function sub_menu_item($titulo, $url, $mark = null)
    {
        self::menuNovo(array(
            self::menuSubGrupo($this->smalt, array(
                self::menuItem($url, $titulo, ((!!$mark) ? array('mark' => ((is_string($mark)) ? explode(',', $mark) : $mark)) : array())),
            )),
        ));
    }

    /** @deprecated */
    function hc_menu($texto, $mod, $tipo = 'a', $orientacao = null, $class = null, $compl = null)
    {
        $name = null;

        switch ($tipo) {
            case 'a':
            case 'botao':
                $data['url'] = $mod;

            break;
            case 'input':
                $data['name'] = $class;
                $data['url'] = $compl;

            case 'botao_js':
                $data['js'] = $mod;

            break;
        }

        $data['texto'] = $texto;
        $data['align'] = $orientacao;
        $data['adjunct'] = $compl;
        $data['class'] = $class;

        //$data = json_encode($data, true);
        $this->hc_menu_item($tipo, $data);
    }

    /**
     * @deprecated
     * Exemplo de utlização
     * $this->hc_menu_item('a', '{"texto": "teste", "url": "http://google.com"}');
     *
     * $this->hc_menu_item('a', array("texto" => "teste", "url" => "http://google.com"));
     *
     * $type: passe o tipo do menu pode ser
     *               a: link comum
     *        botao_js: para um botão com ação javascrip
     *           input: para criar um input
     */
    function hc_menu_item($type = 'a', $data = null)
    {

        if (!is_array($data)) {
            $data = utf8_encode($data);
            $data = json_decode($data, true);
        }

        $item = array(
            'tipo' => $type,
            'texto' => $data['texto'],
            'url' => isset($data['url']) ? $data['url'] : null,
            'align' => isset($data['align']) ? $data['align'] : 'right',
            'class' => isset($data['class']) ? $data['class'] : null,
            'adjunct' => isset($data['adjunct']) ? $data['adjunct'] : '',
            'name' => isset($data['name']) ? $data['name'] : null,
            'js' => isset($data['js']) ? $data['js'] : null,
        );

        //$tmp_menu = array_keys($this->hc_menu);
        //return array_shift($tmp_menu);

        switch ($item['tipo']) {
            case 'a':

                if ($item['align'] == 'right') self::hdMenuRight(array(
                    self::hdMenuA($item['texto'], $item['url'], array('class' => $item['class']))
                )); else self::hdMenuLeft(array(
                    self::hdMenuA($item['texto'], $item['url'], array('class' => $item['class']))
                ));

            break;
            case 'botao':
            case 'botao_js':
                self::$js .= $item['js'];

                $attrs = ShortTag::Explode("[{$item['adjunct']}]");
                $attrs['class'] = ((isset($attrs['class'])) ? $attrs['class'] . ' ' : '') . $item['class'];

                if ($item['align'] == 'right') self::hdMenuRight(array(
                    self::hdMenuButton($item['texto'], $attrs)
                )); else self::hdMenuLeft(array(
                    self::hdMenuButton($item['texto'], $attrs)
                ));

            break;
            case 'input':
                self::$js .= $item['js'];
                $input = self::hdMenuForm($item['texto'], $item['url'], array(
                    'form' => array(
                        'class' => $item['class'],
                    ),
                    'input' => array(
                        'name' => $item['name'],
                        'value' => (isset($_GET[$item['name']]) ? $_GET[$item['name']] : ''),
                    ),
                ));
                if ($item['align'] == 'right') self::hdMenuRight(array($input)); else self::hdMenuLeft(array($input));
            break;
        }
    }



    /**
     * @deprecated use $this->sidebarMenu->children();
     *
     *  $aplimo = new aplimo();
     *  $aplimo->menuNovo(array(
     *
     *      $aplimo->menuGrupo('paginas', 'Menu', array(
     *          $aplimo->menuSubGrupo('jogo', ['trophy', 'Jogos'], array(
     *              $aplimo->menuItem('estadio', 'Ginásios'),
     *              $aplimo->menuItem('equipe', 'Equipes'),
     *              $aplimo->menuItem('rodada', 'Rodadas'),
     *              $aplimo->menuItem('categorias', 'Categorias'),
     *          )),
     *          $aplimo->menuItem('artilharia', ['futbol-o', 'Artilharia']),
     *      )),
     *
     *      $aplimo->menuGrupo('anexo', ['paperclip', 'Anexos'], array(
     *          $aplimo->menuItem('regras', 'Regras', ['modo' => 'onserver', 'url' => ['teste' => 'teste']]),
     *          $aplimo->menuItem('outros', 'Outros'),
     *      )),
     *
     *      $aplimo->menuGrupo('teste', 'Teste', array(
     *          $aplimo->menuItem('teste1', 'Teste 1'),
     *          $aplimo->menuSubGrupo('teste2', 'Teste 2', array(
     *              $aplimo->menuItem('teste2-1', 'Teste 2.1'),
     *              $aplimo->menuItem('teste2-2', 'Teste 2.2'),
     *              $aplimo->menuSubGrupo('teste2-3',   'Teste 2.3', array(
     *                  $aplimo->menuItem('teste2-3-1', 'Teste 2.3.1'),
     *                  $aplimo->menuItem('teste2-3-2', 'Teste 2.3.2'),
     *                  $aplimo->menuItem('teste2-3-3', 'Teste 2.3.3'),
     *                  $aplimo->menuItem('teste2-3-4', 'Teste 2.3.4'),
     *              )),
     *              $aplimo->menuSubGrupo('teste2-4',   'Teste 2.4', array(
     *                  $aplimo->menuItem('teste2-4-1', 'Teste 2.4.1'),
     *                  $aplimo->menuItem('teste2-4-2', 'Teste 2.4.2'),
     *                  $aplimo->menuItem('teste2-4-3', 'Teste 2.4.3'),
     *                  $aplimo->menuItem('teste2-4-4', 'Teste 2.4.4'),
     *              )),
     *          )),
     *          $aplimo->menuItem('teste3', 'Teste 3'),
     *          $aplimo->menuItem('teste4', 'Teste 4'),
     *      )),
     *
     *  ));
     *
     * @param array $itens Lista de itens para o menu lateral
     */
    public function menuNovo(array $itens)
    {
        foreach($itens as $iten) if($iten instanceof MenuData) $this->sidebarMenu->children($iten);
    }

    /**
     * @deprecated use \Api\Aplimo\NewSideBar\TreeView()
     *
     * @param            $pasta
     * @param            $attrs
     * @param array|null $itens
     * @return NewSideBar\TreeView
     */
    public function menuSubGrupo($pasta, $attrs, array $itens = null)
    {
        if ($itens === null)
            [$itens, $attrs] = [$attrs, []];

        if (is_array($attrs) && isset($attrs[0], $attrs[1])){
            [$attrs['fa'], $attrs['nome']] = $attrs;
            unset($attrs[0], $attrs[1]);
        }
        if (!is_array($attrs))
            $attrs = ['nome' => $attrs];

        $nome = ((isset($attrs['nome']))? $attrs['nome']: '');
        $treeView = new \Api\Aplimo\NewSideBar\TreeView($pasta, $nome, $attrs);
        foreach($itens as $iten) if($iten instanceof MenuData) $treeView->children($iten);
        return $treeView;
    }

    /**
     * @deprecated use \Api\Aplimo\NewSideBar\TreeView()
     * @param            $pasta
     * @param            $attrs
     * @param array|null $itens
     * @return NewSideBar\TreeView
     */
    public function menuGrupo($pasta, $attrs, array $itens = null)
    {
        return self::menuSubGrupo($pasta, $attrs, $itens);
    }

    /**
     * @deprecated use \Api\Aplimo\NewSideBar\Link()
     *
     * @param      $pasta
     * @param null $attrs
     * @return NewSideBar\Link
     */
    public function menuItem($pasta, $attrs = null)
    {
        if (is_array($attrs) && isset($attrs[0], $attrs[1])){
            [$attrs['fa'], $attrs['nome']] = $attrs;
            unset($attrs[0], $attrs[1]);
        }
        if (!is_array($attrs) && !empty($attrs))
            $attrs = ['nome' => $attrs];

        $nome = ((isset($attrs['nome']))? $attrs['nome']: '');
        return new \Api\Aplimo\NewSideBar\Link($pasta, $nome, $attrs);
    }

    /**
     * @deprecated use \Api\Aplimo\NewSideBar\TreeView()
     * @param            $pasta
     * @param            $attrs
     * @param array|null $itens
     * @return NewSideBar\TreeView
     */
    public function menuItemGrupo($pasta, $attrs, array $itens = null)
    {
        return self::menuSubGrupo($pasta, $attrs, $itens);
    }




    /**
     * @deprecated use $aplimo->hdMenuRight->children();
     *
     * Acrescenta item no menu do header no app a direita.
     *
     * $this->hdMenuRigth(array(
     *     $this->hdMenuA('texto', 'url', array('class' => 'teste', 'data-teste' => 'teste')),
     *     $this->hdMenuButton('botao', array('id' => 'btm-teste-danger', 'class' => 'btn-danger')),
     *     $this->hdMenuForm('placeholder', 'url', array('button' => array('html' => 'texto')))
     * ));
     *
     * @param array $itens
     */
    public function hdMenuRight(array $itens)
    {
        foreach($itens as $iten) $this->hdMenuRight->children($iten);
    }

    /**
     * @deprecated use $aplimo->hdMenuLeft->children();
     *
     * Acrescenta item no menu do header no app a esquerda.
     *
     * $this->hdMenuLeft(array(
     *     $this->hdMenuA('texto', 'url', array('class' => 'teste', 'data-teste' => 'teste')),
     *     $this->hdMenuButton('botao', array('id' => 'btm-teste-danger', 'class' => 'btn-danger')),
     *     $this->hdMenuForm('placeholder', 'url', array('button' => array('html' => 'texto')))
     * ));
     *
     * @param array $itens
     */
    public function hdMenuLeft(array $itens)
    {
        foreach($itens as $iten) $this->hdMenuLeft->children($iten);
    }

    /**
     * @deprecated use \Menu\Itens\Link();
     */
    public function hdMenuA($texto, $url, array $attrs = array())
    {
        /*$item['type'] = 'a';
        $attrs['class'] = ('btn btn-default navbar-btn' . ((isset($attrs['class'])) ? ' ' . $attrs['class'] : ''));
        $item['a'] = array_merge(array(
            'class' => '',
            'href' => $url,
            'html' => $texto,
        ), $attrs);*/
        //return $item;
        $attrs['class'] = 'btn btn-default' . ((empty($attrs['class']))? '': ' ' . $attrs['class']);
        return new Link('link', $texto, $url, $attrs);
    }

    /**
     * @deprecated use new \Menu\Itens\Button();
     */
    public function hdMenuButton($texto, array $attrs = array())
    {
        $attrs['class'] = 'btn btn-default navbar-btn' . ((isset($attrs['class'])) ? ' ' . $attrs['class'] : '');
        return new Button('button', $texto, 'button', $attrs);
    }

    /**
     * @deprecated use new \Menu\Itens\Search();
     */
    public function hdMenuForm($placeholder, $url, array $attrs = array())
    {
        return new Search('form', $placeholder, $url, $attrs);
    }

    /**
     * @deprecated use new \Menu\Itens\Breadcrumb();
     * @param array $links
     * @return Breadcrumb
     */
    public function hdMenuBreadcrumbs(array $links = [])
    {
        $breadcrumd = new Breadcrumb('Breadcrumb', 'Breadcrumb');
        if(!empty($links)){
            [$endLink, $endLabel] = [end($links), key($links)];
            array_pop($links);

            foreach($links as $label => $link)
                $breadcrumd->children(new BreadcrumbLink('BreadcrumbLink', $label,$link));

            if($endLink)
                $breadcrumd->children(new BreadcrumbLink('BreadcrumbLink', $endLabel, $endLink));
        }
        return $breadcrumd;
    }

    /**
     * @deprecated use \Menu\Itens\BrandText();
     */
    public function hdMenuTitle($title)
    {
        return new BrandText('BrandText', $title);
    }
}