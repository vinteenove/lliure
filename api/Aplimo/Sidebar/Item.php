<?php

namespace Api\Aplimo\Sidebar;

use Api\Aplimo\Sidebar;
use Api\Menu\Itens\ItemTrait;

class Item extends Sidebar {
    use ItemTrait;
}