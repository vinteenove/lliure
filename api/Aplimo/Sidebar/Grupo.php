<?php

namespace Api\Aplimo\Sidebar;

use Api\Aplimo\Sidebar;
use Api\Menu\Itens\GrupoTrait;

class Grupo extends Sidebar {
    use GrupoTrait;
}