<?php

namespace Api\Aplimo\Sidebar;

use Api\Aplimo\Sidebar;
use Api\Menu\Itens\ItemGrupoTrait;

class ItemGrupo extends Sidebar {
    use ItemGrupoTrait;
}