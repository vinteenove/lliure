<?php

namespace Api\Aplimo;

use Api\Menu\Menu;
use LliurePanel\ll;

abstract class Sidebar extends Menu{
    /* @var $father $this */

    public function __construct($pasta, ...$params){
        parent::__construct($pasta, ...$params);

        $this->attrs = array_merge([
            'mark' => false,
            'home' => false,
        ], $this->attrs);
    }

    protected function ico(){
        switch($this->myNivel()){
            case 1:
                return 'nav-icon ' . (($this->attrs['fa'])? $this->attrs['fa']: 'fas fa-circle');
            break;
            case 2:
                return 'nav-icon ' . (($this->attrs['fa'])? $this->attrs['fa']: 'far fa-circle');
            break;
            default:
                return 'nav-icon ' . (($this->attrs['fa'])? $this->attrs['fa']: 'far fa-dot-circle');
            break;
        }
    }

    public function prepare(){
        global $_ll;

        $query = $this->attrs['query'];

        list($attrs, $href, $icone, $nome) = parent::prepare();
        unset($attrs['mark'], $attrs['home']);

        $href =  ((!!$href)? $href: ((!!$this->attrs['href'])? $this->attrs['href']: ($_ll[$_ll['operation_type']]['home'] . '&apm=' . $this->path()) . (($q = ((is_string($query))? $query: ((is_array($query))? http_build_query($query): null)) && !empty($q))? '&' . $q: '')));

        return [$attrs, $href, $icone, $nome];
    }

    private function standardKey($prefUrl = false, $home = false, $page = false){
        switch($this->type){
            case 'Item': case 'ItemGrupo':
                $page = ((!!$page)? $page: ((gettype($this->folder) == "string")? ((!!$prefUrl)? $prefUrl. '>': '') . $this->folder: false));
                $home = ((!$home && (isset($this->attrs['home']) && is_string($this->attrs['home'])))? $this->attrs['home']: $home);

            if($this->type == 'Item') break;
            case 'Grupo':
                $page = ((!!$page)? $page: self::searchStandardKey($this->children, ((!!$prefUrl)? $prefUrl. '>': ''). $this->folder));
                $home = ((!!$home)? $home: self::searchStandardKey($this->children, ((!!$prefUrl)? $prefUrl. '>': ''). $this->folder, ((!$home)? null: $home)));
            break;
        }
        return [$home, $page];
    }

    public static function searchStandardKey(array $itens, $prefUrl = false, $home = false, $page = false){
        /* @var $item self */
        foreach($itens as $item)
            list($home, $page) = $item->standardKey($prefUrl, $home, $page);
        return (($home === null)? false: ((!$home)? $page: $home));
    }

    public function activatingByPath($path){
        $ret = false;
        switch($this->type){
            case 'Item':
            case 'ItemGrupo':
                if(!empty($this->folder) && $this->path() == $path){
                    $ret = $this->active = true;
                }
                if(!$ret && (!!$this->attrs['mark']) && ((is_array($this->attrs['mark']) && !empty($marks = $this->attrs['mark'])) || (is_string($this->attrs['mark']) && !!($marks = explode(',', $this->attrs['mark']))))) foreach($marks as $k => $m){
                    if(is_numeric($k) && $path == $m){
                        $ret = $this->active = true;
                    }elseif(is_string($k) && isset($_GET[$k]) && ($_GET[$k] == $this->path() || $_GET[$k] == $m)){
                        $ret = $this->active = true;
                    }
                }
            if($this->type == 'Item'){
                break;
            }
            case 'Grupo':
                if(!$ret && (self::enableByPath($this->children, $path))){
                    $ret = $this->active = true;
                }
            break;
        }
        return $ret;
    }

    public static function enableByPath(array $itens, $path){
        $enabled = false;
        $path = str_replace(['/', '\\'], '>', $path);
        /* @var $item self */
        foreach($itens as $item){
            $activating = $item->activatingByPath($path);
            $enabled = ($enabled || $activating);
        }
        return $enabled;
    }
}