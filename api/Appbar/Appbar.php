<?php


namespace Api\Appbar;

use LliurePanel\ll;
use Menu\Itens\Menu;
use Menu\MenuParentInterface;
use Persona\Persona;

require_once __DIR__ . '/inicio.php';

class Appbar
{

    static function build(){
        \Jquery\Jquery::build();
        Persona::add(__DIR__. '/estilo.css', 'css');
    }

    private static int $indexCorret = 0;
    private int $id = 0;
    private string $nome;
    private string $home;
    private MenuParentInterface $menuLeft;
    private MenuParentInterface $menuRight;

    public function __construct(string $nome, string $home){
        $this->id = self::$indexCorret++;
        $this->nome = $nome;
        $this->home = $home;
        $this->menuLeft = new Menu(Menu::NavBar);
        $this->menuRight = new Menu(Menu::NavBar);
    }

    public function menuLeft(): MenuParentInterface{
        return $this->menuLeft;
    }

    public function menuRight(): MenuParentInterface{
        return $this->menuRight;
    }

    public function view(){
        ob_start(); ?>
            <header>
                <nav class="navbar navbar-expand-md navbar-white navbar-light">
                    <a class="navbar-brand" href="<?php echo $this->home; ?>"><?php echo $this->nome; ?></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#AppbarNavBarCollapse-<?php echo $this->id; ?>" aria-controls="AppBarNavBarToggle" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="AppbarNavBarCollapse-<?php echo $this->id; ?>">
                        <ul class="navbar-nav">
                            <?php echo $this->menuLeft; ?>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <?php echo $this->menuRight; ?>
                        </ul>
                    </div>
                </nav>
            </header>
        <?php return ob_get_clean();
    }

    public function __toString(){
        return $this->view();
    }

}