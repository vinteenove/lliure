<?php

namespace Api\Select2;

use Persona\Persona;
use Persona\PersonaBuilder;

class Select2 implements PersonaBuilder {

    public static function build(): void{
        \Jquery\Jquery::build();

        Persona::style(__DIR__ . '/../../vendor/select2/select2/dist/css/select2.css', Persona::COMPONENT_PRIORITY);
        Persona::style(__DIR__ . '/../../vendor/ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css', Persona::COMPONENT_PRIORITY);
        Persona::style(__DIR__ . '/css/personalisation.css', (Persona::COMPONENT_PRIORITY + 1));

        Persona::script(__DIR__ . '/../../vendor/select2/select2/dist/js/select2.min.js', Persona::COMPONENT_PRIORITY);
        Persona::script(__DIR__ . '/../../vendor/select2/select2/dist/js/i18n/pt-BR.js', Persona::COMPONENT_PRIORITY);
        Persona::script(__DIR__ . '/js/personalisation.js', (Persona::COMPONENT_PRIORITY + 1));
    }
}