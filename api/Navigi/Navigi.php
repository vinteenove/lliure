<?php

namespace Api\Navigi;

use Helpers\HttpHelper;
use LliurePanel\ll;
use LliurePanel\Routable;
use LliurePanel\RouterLegacyTrait;
use Persona\Persona;
use Router\Router;
use Opt\SessionFix\SessionFix;
use ShortTag\ShortTag;

/**
*
* API navigi - lliure
*
* @Versão 8.0
* @Pacote lliure
* @Entre em contato com o desenvolvedor <jomadee@glliure.com.br> http://www.lliure.com.br/
* @Licença http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/*	***	Documentação da função ***
	
	Para iniciar a classe
	$navegador = new \Api\Navigi\Navigi();
	
	Define a tabela da consulta
	$navegador->tabela = PREFIXO.'tabela ';

	Definendo a query para consulta
	$navegador->query = 'select * from '.$navegador->tabela;

	Define como será a exibição. por ser "lista" ou "icone"
	$navegador->exibicao = 'icone';

	Define as cofigurações da navegação
	$navegador->config = (array) $config;

	Para rodar a classe
	$navegador->monta();

	###
	Opção "config", essa é a mais complicada pois é nela que difinimos como vai ser o icone o clique duplo e a divercidade da busca...

	Assim ficaria uma configuração mais simples, no caso apenas vamos direcionar o duplo clique  para uma página (visualizar) que tenha o id "X" (por padrão o jfnav pesquisa o campo "id" e adiciona no final da url)
	$navegador->config = array(
				'link' => '?app=meuapp&pagina=visualizar&id='
				);

#	Para alterar o icone padrão adicione 'ico' => 'img/meuico.png' onde o endereço começa a contar apartir da raiz do aplicativo
	exemplo da utilização
	$navegador->config = array(
				'link' => '?app=meuapp&pagina=visualizar&id=',
				'ico' => 'img/meuico.png'
				);

#	Alterando a coluna de exibição (por padrão se chama nome), desta forma vamos definir que a coluna que vamos consultar será "cor" ao invés de "nome"
		$navegador->config = array(
				'link' => '?app=meuapp&pagina=visualizar&id=',
				'coluna' => 'cor'
				);

#	Exemplo de montagem do config com tabelas anexadas, isso é usado para quando a coluna principal estiver em outra tabela (um exemplo é quando utilizamos multidiomas), o "as_id" nada mais é que o id da FK para quando for fazer o rename realizar na tabela correta e com o id correto
	$navegador->config = array(
			'link' => '?app=meuapp&pagina=visualizar&id=',
			'ico' => 'img/meuico.png',

			'tabela' => PREFIXO.'meuapp_dados'
			'as_id' => 'campo_id'
			);
	
#	Consultando mais de um tipo de registro
	para este fim você tera que usar da mesma forma que a de cima porem dentro de arrays, e usar o parametro 'configSel' para definir o campo que diferencia um do outro, e os indices dos array de configuração será a diferença

	$navegador->configSel = 'tipo';
	$navegador->config['produto'] =  array (
	 			'link' => '?app=meuapp&pagina=visualizar&id=',
				'ico' => 'img/meuico.png'
				);
	
	$navegador->config['categoria'] =  array (
				'link' => '?app=meuapp&p=categoria&id=',
				'ico' => 'img/outroico.png'
				);
	
#	Para habilitar a função "apagar" passe como "true" o paramentro 'delete'
	$navegador->delete = true;
	
#	Para habilitar a função "renomear" passe como "true" o paramentro 'rename'
	$navigi->rename = true;
	
#	Trabalhando com botões auxiliares
	use 'ico' para definir o icone do botao
		'link' para definir o link ao clicar
		'modal' em caso de abertura de modal, sendo "Largura X Altura" ex: 250x100, para que fique automatico use a palavra "auto"
	
	ex:
	'botao' => array(
				array('ico' => ll::$data->app->pasta.'img/box.png',
				'link' => ll::$data->app->sen_html.'&apm=produtos&sapm=produto&ac=estoque&id=#ID',
				'modal' => '300xauto')
			)

#	Alterando os nomes das etiquetas
	$navigi->etiqueta = array(
								'id' => 'Pedido',								
								'coluna' => 'Data'
							);
	//1 lembrando que essas são as duas padrões utilizadas pelo sistema, caso adicione mais, as mesmo serão carregadas no modo lista com seus respectivos conteudos
	
	//2 utilize um array com o arg 0 com o nome e o arg 1 com a medida da coluna caso necessário
		ex: 'usuario' => array('nome','50px');

# Pesquisa
	para instanciar uma pesquisa utilize
	$navigi->pesquisa = 'Id:int,Numero:str';
	
	por padrao todos são strings
	$navigi->pesquisa = 'Id,Numero';
		
		
#	Exemplo de utilização simples *************
	
	$navigi = new \Api\Navigi\Navigi();
	$navigi->tabela = PREFIXO.'app';
	$navigi->query = 'select * from '.$navigi->tabela.' order by nome asc' ;
	$navigi->delete = true;
	$navigi->rename = true;
	$navigi->config = array(
		'ico' => ll::$data->app->pasta.'imagens/sys/app.png',
		'link' => '?app=meuapp&ac=editar&id='           
		);								
	$navigi->monta();
	
*/


class Navigi implements Routable
{

    use RouterLegacyTrait;

    const
        ICONE = 'icone',
	    LISTA = 'lista';

	/** @var string $query query em mysql */
	var $query;
	private $queryTotal;
    private $queryLista;

	/** @var string $rest url onde serão feitas as consultas */
	var $rest = 'navigi/os';

	/** @var bool|string String com as configurações coluna:tipo separados por espaço referentes a pesquisa */
	var $pesquisa = false;

	/** @var $placeholder string Texto que aparecera dentro do campo de pesquisa do navigi */
	var $placeholder = 'Pesquisa';

	/** @var string $tabela Nome da tabela onde esta acontecendo as consultas. */
	var $tabela;

	/** @var array $config configurações: ico ou fa; link; tabela; as_id, botao: array() */
	var $config;

	/** @var bool $debug Quando em true mastra dados relevantas para verificação. */
	var $debug = false;

	/** @var string $exibicao Define o modelo de listagem, opçoes: 'icone', 'lista' */
	var $exibicao = self::ICONE;

	/** @var bool $delete Quando true libera o botão de deleção na listagem */
	var $delete = false;	

	/** @var bool $rename Qaundo true libera o botão de rename na listagem */
	var $rename = false;	

	/** @var bool|string $configSel nome do campo que faz a diferenciação dos dados listados */
	var $configSel = false; 	

	/** @var bool|int $paginacao Quando >= 1 uma paginação aparecera */
	var $paginacao = false;

    /** @var array $etiqueta ex: ['id' => 'codigo' [, 'coluna' => 'valor']] */
	var $etiqueta = null;

	/** @var array $order um array contendo as definições padão do order. Ex: ['id' => 'DESC', 'coluna' => 'ASC'] */
	var $order = [];

	var $cell = null;

	private $colunas = array();
	private $campos = array();
	private $ordens = array();
	private static $filtros = array();

	private $whereBy = [];
	private $orderBy = [];

	public function monta($echo = false){
        global $_ll;

        /** Retro compatibilidade para verções antigas*/
        if(isset($this->config['campo'])){
            $this->configSel = $this->config['campo'];
            unset($this->config['campo']);
        }

        /** AJUSTA O PADRAO PARA CONFIGURACAO DE BOTOES */
        if(isset($this->config['botao']) && !isset($this->config['botao'][0]) && isset($this->config['botao']['link']))
            $this->config['botao'] = [0 => $this->config['botao']];

        if($this->configSel === false)
            $this->config = [$this->config];

        $filtros['coluna'] = $filtros['id'] = [];
        foreach($this->config as $chave => $valor){
            $this->config[$chave]['coluna'] = (isset($this->config[$chave]['coluna'])? $this->config[$chave]['coluna']: 'nome');
            $this->config[$chave]['as_id'] = (isset($this->config[$chave]['as_id'])? $this->config[$chave]['as_id']: 'id');
            $this->config[$chave]['id'] = (isset($this->config[$chave]['id'])? $this->config[$chave]['id']: $this->config[$chave]['as_id']);
            $this->config[$chave]['tabela'] = (isset($this->config[$chave]['tabela'])? $this->config[$chave]['tabela']: $this->tabela);

            $filtros['id'][] = $this->config[$chave]['as_id'];
            $filtros['coluna'][] = $this->config[$chave]['coluna'];
        }

        /** CONFIGURA A ETIQUETA*/
        $this->colunas = ['id' => [], 'coluna' => []];

        if(!isset($this->etiqueta['id']))
            $this->etiqueta['id'] = 'Cod.';

        if(!isset($this->etiqueta['coluna']))
            $this->etiqueta['coluna'] = '';

        foreach($this->etiqueta as $chave => $valor){
            if(!is_array($valor))
                $this->etiqueta[$chave] = $valor = ['label' => $valor, 'width' => 'auto'];

            elseif(is_array($valor) && isset($valor[0], $valor[1]))
                $this->etiqueta[$chave] = $valor = array_diff_key(array_merge(['label' => $valor[0], 'width' => $valor[1]], $valor), ['', '']);

            if(isset($filtros[$chave], $valor['filter']) && !isset($valor['filter']['col']))
                $this->etiqueta[$chave]['filter']['col'] = $valor['filter']['col'] = $filtros[$chave];

            $this->etiqueta[$chave]['order'] = $valor['order'] = ((key_exists('order', $valor))? $valor['order']:
                ((isset($valor['filter']['type']))? true: false));

            if(isset($filtros[$chave]) && (!!$valor['order']))
                $this->etiqueta[$chave]['order'] = $valor['order'] = $filtros[$chave];

            $this->colunas[$chave] = $valor;
            if($chave != 'id' && $chave != 'coluna') $this->cell[$chave] = $valor;
        }

        if(isset($_GET['nvgS']) && !empty($_GET['nvgS']) && is_array($_GET['nvgS'])) foreach($this->colunas as $key => $valor){
            if(!!($a = $this->filterFirter($key, $valor))){
                if(!empty($this->whereBy)) $this->whereBy[] = 'AND';
                $this->whereBy[] = '(';
                foreach($a as $i) $this->whereBy[] = $i;
                $this->whereBy[] = ')';
            }
        }


        if($this->pesquisa != false && isset($_GET['nvgP']) && !empty($_GET['nvgP'])){
            $termos = explode(' ', base64_decode($_GET['nvgP']));

            // fitra os valores vazios
            foreach($termos as $chave => $valor)
                if(!empty($valor)) $busca[] = $valor;

            $pesCam = [];
            foreach(explode(',', $this->pesquisa) as $key => $value){
                @list($k, $v) = explode(':', $value, 2);
                $pesCam[$k] = $v;}

            $pesquisaBilb = function($campos, $valor){
                $r = [];
                foreach($campos as $col => $type){
                    if(!empty($r)) $r[] = 'OR';
                    switch($type){
                        default: case 'str':
                            $r[] = [$col, 'like', '%' . $valor . '%'];
                        break;
                        case 'int':
                            $r[] = [$col => $valor];
                        break;
                    }
                }
                return $r;
            };

            foreach($busca as $chave => $valor){
                if(!empty($this->whereBy)) $this->whereBy[] = 'AND';
                $this->whereBy[] = '(';
                foreach($pesquisaBilb($pesCam, $valor) as $i) $this->whereBy[] = $i;
                $this->whereBy[] = ')';
            }
        }



        $where = '';
        if(!empty($this->whereBy)) $where = ' WHERE ' . self::buildWhere($this->whereBy);

        if(isset($_GET['nvgO']) && is_array($_GET['nvgO'])){
            foreach($_GET['nvgO'] as $k => $v) if (!!($q = $this->filterOrder($k, ((isset($this->colunas[$k]))? $this->colunas[$k]: [])))) foreach($q as $o){

                list($col, $mod) = each($o);
                $this->orderBy[$col] = $mod;

            }
        }elseif(isset($this->order) && !!$this->order) foreach($this->order as $c => $m)
            if(strtoupper($m) == 'ASC' || strtoupper($m) == 'DESC') foreach(((isset($filtros[$c]))? $filtros[$c]: [$c]) as $cf)
                $this->orderBy[$cf] = strtoupper($m);

        $order = '';
        if(!empty($this->orderBy)) $order = ' ORDER BY ' . self::buildOrder($this->orderBy);


        $limit = '';
        $this->paginacao = ((is_numeric($this->paginacao) && $this->paginacao >= 1)? ['itens' => $this->paginacao]: false);
        if($this->paginacao !== false){

            $pagina = 1;
            if(isset($_GET['nvgPG']) && !empty($_GET['nvgPG'])) $pagina = $_GET['nvgPG'];

            $get = $_GET;
            unset($get['nvgPG']);
            $url = ll::$data->app->home . ((!empty($get))? '/' . HttpHelper::buildQuery($get): '');

            $this->paginacao = array_merge($k = [

                'url'    => $url,
                'pagina' => $pagina,
                'itens'  => 20,
                'range'  => 5,
                'start'  => '<i class="fa fa-angle-double-left"></i>',
                'prev'   => '<i class="fa fa-angle-left"></i>',
                'next'   => '<i class="fa fa-angle-right"></i>',
                'end'    => '<i class="fa fa-angle-double-right"></i>',

            ], array_intersect_key($this->paginacao, $k));

            $limit = ' LIMIT '. (($this->paginacao['pagina'] - 1) * $this->paginacao['itens']). ', '. ($this->paginacao['itens']);
        }

        $this->query = ((!empty($where) || !empty($order))? 'SELECT qry.* FROM (' . $this->query . ') AS qry'. $where. $order: $this->query);
        $this->queryTotal = ('SELECT COUNT(*) as total FROM (' . $this->query . ') qry');
        $this->queryLista = $this->query . $limit;


        /** caso tenha botoes na configuracao muda a exibicao para lista*/
        if(isset($this->config['botao']))
            $this->exibicao = 'lista';

        $navigi = [
            'tabela'     => $this->tabela,
            'query'      => $this->query,
            'queryTotal' => $this->queryTotal,
            'queryLista' => $this->queryLista,
            'whereBy'    => $this->whereBy,
            'orderBy'    => $this->orderBy,
            'debug'      => $this->debug,
            'delete'     => $this->delete,
            'rename'     => $this->rename,
            'paginacao'  => $this->paginacao,
            'exibicao'   => $this->exibicao,
            'configSel'  => $this->configSel,
            'config'     => $this->config,
            'etiqueta'   => $this->etiqueta,
            'cell'       => $this->cell,
            'order'      => $this->order,
        ];

        $ico = false;
        if($this->configSel != false){
            $ico = $this->config;
            $ico = array_pop($ico);
            $ico = (isset($ico['ico']) || isset($ico['fa'])? true: false);
        }

        if($ico) array_unshift($this->colunas, ['class' => 'navigi_ico', 'order' => false]);
        if($this->debug == true) echo '<pre>' . htmlspecialchars(print_r($navigi, true)) . '</pre>';
        
        $encriptado = jf_encode(SessionFix::getSessionId(), serialize($navigi));


        $inpts = $buttons = $hidens = [];
        $i = 0;
        foreach(((isset($_GET['nvgS']))? $_GET['nvgS']: []) as $k => $v) $inpts["nvgS[{$k}]"] = $v;
        foreach(((isset($_GET['nvgO']))? $_GET['nvgO']: []) as $k => $v) $buttons["nvgO[{$k}]"] = $v;
        foreach(((isset($_GET['nvgS']))? $_GET['nvgS']: []) as $k => $v) $hidens["nvgS[{$k}]"] = $v;
        foreach(((isset($_GET['nvgO']))? $_GET['nvgO']: []) as $k => $v) $hidens["nvgO[{$k}]"] = $i++ . ":$v";

        ob_start(); ?>
        <div id="navigi" class="navigi_loading dataTables_wrapper dt-bootstrap4" data-exibicao="<?php echo $this->exibicao; ?>" rest="<?php echo $this->rest; ?>" token="<?php echo $encriptado; ?>">
            <?php if(!!$this->pesquisa){ ?>
                <form action="<?php echo $this->rest . '/nvgAC=pesquisa'; ?>" method="post" style="width: 100%;">
                    <input type="hidden" name="url" value="<?php echo htmlspecialchars((ll::$data->url->endereco . ll::$data->app->home. '&' . HttpHelper::buildQuery( array_diff_key($_GET, ['nvgP' => '', 'nvgPG' => '']) ))); ?>">
                    <div class="row">
                        <div class="col-lg-6"></div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="pesquisa" placeholder="<?php echo $this->placeholder; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="fa fa-search" style="line-height: inherit"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <?php if(isset($_GET['nvgP'])){ $pesquisa = base64_decode($_GET['nvgP']); ?>
                <div class="alert alert-light" style="width: 100%;">
                    <a href="<?php echo(ll::$data->url->endereco . ll::$data->app->home . '&' . HttpHelper::buildQuery(array_diff_key($_GET, ['nvgP' => '', 'nvgPG' => '']))); ?>" class="close"><span aria-hidden="true">&times;</span></a>
                    Resultado da busca por: <strong><?php echo $pesquisa; ?></strong>
                </div>
                <?php } ?>
            <?php } ?>
            <?php if($this->exibicao == 'lista'){ ?>
                <table class="table navigi_list">
                    <thead class="id-form-filter">
                        <tr>
                            <?php foreach($this->colunas as $key => $valor){ ?>
                                <th <?php echo self::montaAttr( $attrs = array_merge(
                                    ((isset($valor['class']))? ['class' => $valor['class'] . ' table-th-filter']: ['class' => 'table-th-filter']),
                                    ((isset($valor['width']) || isset($valor[1]))? ['style' => 'width: '. ((isset($valor['width']))? $valor['width']: $valor[1])]: [])
                                )); ?>>
                                    <?php echo $this->filters($key, $valor, $inpts, $buttons); ?>
                                </th>
                            <?php } ?>
                            <th class="navigi_botoes"><?php echo (($this->filterHas($this->colunas))? $this->filterForm($hidens): ''); ?></th>
                        </tr>
                    </thead>
                    <tbody class="navigi_areaIcones"></tbody>
                </table>
            <?php }elseif($this->exibicao == 'icone'){ ?>
                <div class="navigi_areaIcones"></div>
            <?php } ?>
            <div class="form-group">
                <div class="navigi_paginacao"></div>
            </div>
        </div>
        <?php $r = ob_get_clean();
        if(!$echo) echo $r;
        else return $r;
        return '';
    }

    /**
     * @param string $chave
     * @return string
     */
    private function getConfigColuna($chave){
	    return (($chave != 'coluna')? $chave: ((isset($this->config[$chave]['coluna']))? $this->config[$chave]['coluna']: ((isset($this->config[0]['coluna']))? $this->config[0]['coluna']: $chave)));
    }

    private function filterHas(array $colunas){
        $has = false;
        foreach($colunas as $coluna){
            $filter = ((isset($coluna['filter']))? $coluna['filter']: []);
            if(!isset($coluna['order'])) $coluna['order'] = ((isset($coluna['filter']['type']))? true: false);

            $has = ((isset($filter['type']) || (isset($coluna['order']) && (!!$coluna['order'])))? true: $has);
        }
        return $has;
    }

    private function filters($campo, $coluna, $inpts, $buttons){

        $r = '';
        $filter = ((isset($coluna['filter']))? $coluna['filter']: []);
        if(!isset($coluna['order'])) $coluna['order'] = ((isset($coluna['filter']['type']))? true: false);

        if(isset($filter['type']) || (isset($coluna['order']) && (!!$coluna['order']))){
            $r .= '<div class="input-group" style="width: 100%;">';

            if(isset($filter['type'])){
                self::$filtros[$filter['type']] = $filter['type'];
                switch($filter['type']){
                    default:
                    case 'text':
                    case 'number':
                        $this->campos[$campo] = $campo;
                        $r .=
                            '<input class="form-control" ' . self::montaAttr($filter) . ' ' . self::montaInput('nvgS[' . $campo . ']', $inpts) . '>';
                    break;
                    case 'telefone':
                        $this->campos[$campo] = $campo;
                        $r .=
                            '<input class="form-control" '. self::montaAttr($filter). ' data-mask="telefone" '. self::montaInput('nvgS['.$campo.']', $inpts). '>';
                    break;
                    case 'select':
                        $this->campos[$campo] = $campo;
                        $r .=
                            '<select class="form-control"'. self::montaInput('nvgS['.$campo.']', $inpts). '>'.
                                self::montaSelect('nvgS['.$campo.']', $inpts, ((isset($filter['options']))? $filter['options']: array()), true).
                            '</select>';
                    break;
                    case 'dateRange':
                        $this->campos[$campo.'-S'] = $campo.'-S';
                        $this->campos[$campo.'-E'] = $campo.'-E';
                        $r = '<div class="input-group input-daterange">';
                        $r .=
                        '<input type="text" class="form-control input-daterange-start" '. self::montaInput('nvgS['.$campo.'-S]', $inpts) .' placeholder="De:">'.
                        '<span class="input-group-addon" style="width: 0 !important; min-width: 0;"></span>'.
                        '<input type="text" class="form-control input-daterange-end" '. self::montaInput('nvgS['.$campo.'-E]', $inpts) .' placeholder="Ate:">';
                    break;
                }
            }
            $r .= '<label>'. ((isset($coluna['label']))? $coluna['label']: ((isset($coluna[0]))? $coluna[0]: '')). '</label>';

            if($coluna['order']){
                $this->ordens[$campo] = $campo; $r .=
                '<span class="input-group-btn">'.
                    '<button class="btn btn-default" type="button" '. self::montaInput('nvgO['.$campo.']', $buttons). '>'.
                        '<i class="fa fa-sort"></i>'.
                        '<i class="fa fa-sort-asc"></i>'.
                        '<i class="fa fa-sort-desc"></i>'.
                    '</button>'.
                '</span>';}

            $r .= '</div>';

	    }else{
            $r .= '<label>'. ((isset($coluna['label']))? $coluna['label']: ((isset($coluna[0]))? $coluna[0]: '')). '</label>';
        }

	    return $r;
    }

    private function filterForm($hidens){
	    global $_ll;

	    $get = $_GET;
	    unset($get['nvgPG'], $get['nvgS'], $get['nvgO']);

        $r =
        '<form action="' . $this->rest . '/nvgAC=pesquisa" method="post" style="margin: 0;">'.
            '<input name="url" value="'. ll::$data->url->endereco. ll::$data->app->home. '&' . http_build_query($get) .'" type="hidden">';

            foreach($this->campos as $campo) $r .=
                '<input type="hidden" class="form-control"'. self::montaInput('nvgS['. $campo. ']', $hidens). '>';

            foreach($this->ordens as $campo) $r .=
                '<input type="hidden" class="form-control"'. self::montaInput('nvgO['. $campo. ']', $hidens). '>';

            $r .=
            '<div class="btn-group">'.
                '<button type="submit" class="btn btn-default btn-form-filter-submit" name="filter"><i class="fa fa-search" aria-hidden="true"></i></button>'.
                '<button type="submit" class="btn btn-default btn-form-filter-submit"><i class="fa fa-eraser" aria-hidden="true"></i></button>'.
            '</div>'.
        '</form>';

        return $r;
    }

    private function filterFirter($campo, $coluna){
        $filter = ((isset($coluna['filter']))? $coluna['filter']: []);
        $r = [];

        if(isset($filter['type'])) foreach(((isset($filter['col']))? ((is_array($filter['col']))? $filter['col']: [$filter['col']]): [$campo]) as $col){
            if(!empty($r)) $r[] = 'OR';

            switch($filter['type']){
                default;
                    if(isset($_GET['nvgS'][$campo]))
                        $r[] = [$col, 'LIKE', '%' . ($_GET['nvgS'][$campo]) . '%'];

                break;
                case 'number':
                case 'select':
                    if(isset($_GET['nvgS'][$campo]))
                        $r[] = [$col => $_GET['nvgS'][$campo]];

                break;
                case 'dateRange':
                    $d = [];
                    $format = ((isset($filter['format']))? $filter['format']: 'Y-m-d H:i:s');

                    if(isset($_GET['nvgS'][$campo . '-S']))
                        $d[] = [$col, '>=', date_create_from_format('d/m/Y H:i:s', $_GET['nvgS'][$campo . '-S'] . ' 00:00:00')->format($format)];

                    if(isset($_GET['nvgS'][$campo . '-E']))
                        $d[] = [$col, '<=', date_create_from_format('d/m/Y H:i:s', $_GET['nvgS'][$campo . '-E'] . ' 23:59:59')->format($format)];

                    if(count($d) == 2){
                        $r[] = '(';
                        $r[] = $d[0];
                        $r[] = 'AND';
                        $r[] = $d[1];
                        $r[] = ')';

                    }elseif(count($d) == 1)
                        $r[] = $d[0];

                break;
            }
        }

        return $r;
    }

    private function filterOrder($campo, $coluna){
        $r = [];
        foreach(((isset($coluna['order']) && !!$coluna['order'])? ((is_array($coluna['order']))? $coluna['order']: ((is_string($coluna['order']))? [$coluna['order']]: [$campo])): []) as $o)
            $r[] = [$o => ($_GET['nvgO'][$campo])];
        return $r;
    }

    public static function filterScripts(){ ?>
        <script type="text/javascript">
            ;(function ($){

                $('.navigi_list .id-form-filter').on('keyup change', 'input, select', function(){
                    var value = '', $self = $(this);
                    $self.attr('value', (value = $self.val()));
                    $self.closest('.id-form-filter').find('input[type="hidden"][name="' + this.name + '"]').val(value);
                });

                $('.navigi_list .id-form-filter button[name]').click(function(){
                    var value = '', $self = $(this), pos = [], atu = 0;
                    $self.val(value = (($self.val() == '')? 'ASC': (($self.val() == 'ASC')? 'DESC': '')));

                    var $o = $('input[type="hidden"][name^="nvgO"').each(function (i, e){
                        var v = [], $this = $(e);
                        if($this.val() != "" && (v = $this.val().split(':')).length > 1) pos[v[0]] = e.name;
                    });
                    if(!(pos.indexOf(this.name) + 1)) pos.push(this.name);
                    if(value == '' && !!((atu = pos.indexOf(this.name)) + 1)) pos = (pos.slice(0, atu).concat(pos.slice(atu + 1)));
                    $o.each(function (i, e){
                        if(!(pos.indexOf(e.name) + 1)) $(this).val(''); else{
                            var v = $(this).val().split(':');
                            $(this).val(pos.indexOf(e.name) + ':' + ((!!v[1])? v[1]: value));
                        }
                    });
                    if(value != '' && !!(pos.indexOf(this.name) + 1))
                        $self.closest('.id-form-filter').find('input[type="hidden"][name="' + this.name + '"]').val(pos.indexOf(this.name) + ':' + value);
                });

                <?php foreach(self::$filtros as $filtro) switch($filtro) {case 'dateRange': ?>

                    $('.navigi_list .input-daterange').each(function (i, e){
                        var $dateStart = $(e).find('.input-daterange-start');
                        var $dateEnd   = $(e).find('.input-daterange-end');
                        $(e).datepicker({
                            inputs: $('.input-daterange-start, .input-daterange-end'),
                            clearBtn: true,
                            beforeShowDay: function (e){
                                var oDatS = $dateStart.data('datepicker'); oDatS = ((!!oDatS)? oDatS.o: false);
                                var oDatE = $dateEnd.data('datepicker'); oDatE = ((!!oDatE)? oDatE.o: false);
                                if(!oDatS || !oDatE) return;
                                var dateS = $dateStart.datepicker('getDate');
                                var dateE = $dateEnd.datepicker('getDate');
                                if(dateS == null && dateE == null) return;
                                return [
                                    (((dateE == null || e.getTime() < dateE.getTime()) && (dateS == null || e.getTime() > dateS.getTime()))? 'range ll_background-100 ll_border-color-100': ''),
                                    (((dateE != null && e.getTime() == dateE.getTime()) || (dateS != null && e.getTime() == dateS.getTime()))? 'll_background-400 ll_border-color-400': ''),
                                    (((oDatE == this && dateE != null && e.getTime() == dateE.getTime()) || (oDatS == this && dateS != null && e.getTime() == dateS.getTime()))? 'll_background-500 ll_border-color-500': '')
                                ].join(' ');
                            }
                        });
                    });

                <?php break; case 'telefone': ?>

                    var SPMaskBehavior = function(val){
                        return (val.replace(/\D/g, '').length === 11 ? '(00) 0.0000-0000' : '(00) 0000-00009');};

                    $('.navigi_list [data-mask="telefone"]').mask(SPMaskBehavior, { onKeyPress: function(val, e, field, options) {
                        field.mask(SPMaskBehavior.apply({}, arguments), options);
                    }});

                <?php break; } ?>

            })(jQuery);
        </script>
    <?php }

    private static function montaSelect($name, $valeu, array $options, $first = false){
        $r = '';
        foreach ($options as $val => $opt){
            $data = ShortTag::Search($val);
            $data = (($data && isset($data[0]))? $data[0]: array());
            $key = (($data)? substr($val, 0, $data['start']): $val);

            $attr = array();
            if(!empty($data))
                foreach ($data['shortTag'] as $k => $v)
                    $attr[((is_numeric($k))? $v: $k)] = $v;

            if (is_array($opt)) {
                $attr = self::montaAttr(array_merge(array(
                    'label' => $key
                ), $attr));
                $r .= "<optgroup{$attr}>" . self::montaSelect($name, $valeu, $opt, $first) . '</optgroup>';
            } else {
                $attr = self::montaAttr(array_merge(
                    ['value' => $key],
                    (((isset($valeu[$name]) && (
                        (!is_array($valeu[$name]) && $key == $valeu[$name]) ||
                        ( is_array($valeu[$name]) && array_key_exists($key, $valeu[$name]))
                    ) && $first = true) || ($first != true && $first = true))? ['selected' => 'selected']: []), $attr
                ));
                $r .= "<option{$attr}>{$opt}</option>";
            }
        }
        return $r;
    }

    private static function montaInput($name, array $dados, $chackbox = false){
        return 'name="' . $name . '"' . ($chackbox === false ? (isset($dados[$name]) ? ' value="' . ((string) $dados[$name]) . '"' : '') : (' value="' . (is_null($chackbox) ? 'null' : $chackbox) . '"' . (array_key_exists($name, $dados) && ($dados[$name] == $chackbox)? ' checked="checked"' : '')));
    }

    private static function montaAttr(array $attrs){
        $r = '';
        foreach($attrs as $k => $v)
            $r .= ' ' . ((is_string($k))? $k: $v) . '="' . ((is_string($v) || is_numeric($v))? $v: ((is_null($v))? $k: ((is_bool($v))? 'true': 'false'))) . '"';
        return ($r);
    }

    private static function buildWhere(array $where){

        function each2(array $_array)
        {
            return next($_array) !== false ?: key($_array) !== null;
        }


        $r = '';
        foreach($where as $v) switch(((is_array($v))? count($v): '')){
            case 3:
                $r .= '`' . $v[0] . '` '. $v[1]. ' "'. $v[2]. '"';

            break;
            case 1:
                foreach ($v as $col => $val) break;
                $r .= '`' . $col . '` = "'. $val. '"';
            break;
            default:
                $r .= ' '. ((string) $v). ' ';

            break;
        }
        return $r;
    }

    private static function buildOrder(array $order){
        $r = '';
        foreach($order as $col => $mod)
            $r .= ((empty($r))? '': ', '). '`'. $col . '` '. $mod;
        return $r;
    }

    /*public static function router(): Router{
        $router = new Router();
        $router->setStrategy(new JsonStrategy);
        $router->any('/*', [Navigi::class, 'controller']);
        return $router;
    }

    public static function controller()
    {
        echo '<pre>' . __FILE__ . ':' . __LINE__ . ': $_GET = '. print_r($_GET, 1). '</pre>' . "\n";
        
        ob_start();
        navigi_rest::start();
        return ob_get_clean();
    }*/

    public static function build(){

        ll::usr('db');
        ll::usr('jfkey');
        \Jquery\Jquery::build();
        \FontAwesome\FontAwesome::build();
        ll::usr('jquery-taphold');
        ll::usr('jquery-maskplugin');
        ll::api('datepicker');
        \Api\Vigile\Vigile::build();

        Persona::style(ll::baseDir() . '/vendor/almasaeed2010/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css', Persona::COMPONENT_PRIORITY);
        Persona::style(__DIR__. '/navigi.css', Persona::COMPONENT_PRIORITY);
        Persona::script(__DIR__. '/navigi.js', Persona::COMPONENT_PRIORITY);
        Persona::call(__NAMESPACE__ . '\Navigi::filterScripts', Persona::COMPONENT_PRIORITY);
    }

    public static function router(): router{
        return static::routerLegacy(new Router);
    }

    public static function appPath(): string{
        return __DIR__;
    }
}






