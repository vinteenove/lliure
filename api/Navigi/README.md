#Navigi

## Iniciando
	
Para iniciar a classe
```php
$navigi = new \Api\Navigi\Navigi();
```

## Tabela e query
Define a tabela da consulta
```php
$navigi->tabela = PREFIXO.'tabela ';
```

Definendo a query para consulta
```php
$navigi->query = 'select * from '.$navigi->tabela;
```

## Rest
Alternativamente a uma query pode ser configura um rest.
```php
$navigi->rest = ll::$data->app->onserver . 'ac=navigi';
```

## Exibição
Define como será a exibição. por ser "lista" ou "icone"
```php
$navigi->exibicao = self::ICONE;
```
**_OU_**
```php
$navigi->exibicao = self::LISTA;
```

## Configurações
Define as cofigurações da navegação, sera detalhada a seguir.
```php
$navigi->config = (array) $config;
```

## Exibir
Para rodar a classe
```php
$navigi->monta();
```


## Opção "config"

Essa é a mais complicada pois é nela que difinimos como vai ser o icone o clique duplo e a divercidade da busca...

Assim ficaria uma configuração mais simples, no caso apenas vamos direcionar o duplo clique  para uma página (visualizar) que tenha o id "X" (por padrão o jfnav pesquisa o campo "id" e adiciona no final da url)
```php
$navigi->config = [
    'link' => '?app=meuapp&pagina=visualizar&id='
];
```

###Icone
Para alterar o icone padrão adicione 'ico' => 'img/meuico.png' onde o endereço começa a contar apartir da raiz do aplicativo, exp:
```php
$navigi->config = [
    'link' => '?app=meuapp&pagina=visualizar&id=',
    'ico' => 'img/meuico.png'
];
```

###Coluna de exbição
Alterando a coluna de exibição (por padrão se chama nome), desta forma vamos definir que a coluna que vamos consultar será "cor" ao invés de "nome"
```php
$navigi->config = [
    'link' => '?app=meuapp&pagina=visualizar&id=',
    'coluna' => 'cor'
];
```

###Tabelas Anexadas
Exemplo de montagem do config com tabelas anexadas, isso é usado para quando a coluna principal estiver em outra tabela (um exemplo é quando utilizamos multidiomas), o "as_id" nada mais é que o id da FK para quando for fazer o rename realizar na tabela correta e com o id correto
```php
$navigi->config = [
    'link' => '?app=meuapp&pagina=visualizar&id=',
    'ico' => 'img/meuico.png',

    'tabela' => PREFIXO.'meuapp_dados'
    'as_id' => 'campo_id'
];
```
	
###Consultando mais de um tipo de registro
para este fim você tera que usar da mesma forma que a de cima porem dentro de arrays, e usar o parametro 'configSel' para definir o campo que diferencia um do outro, e os indices dos array de configuração será a diferença
```php
$navigi->configSel = 'tipo';
$navigi->config['produto'] = [
    'link' => '?app=meuapp&pagina=visualizar&id=',
    'ico' => 'img/meuico.png'
];
	
$navigi->config['categoria'] = [
    'link' => '?app=meuapp&p=categoria&id=',
    'ico' => 'img/outroico.png'
];
```

### Trabalhando com botões auxiliares
use:
- `'ico'` para definir o icone do botao
- `'fa'` use no lugar de `ico` para definir um icone, use a class corespondente ao icone da biblioteca fontawesome
- `'link'` para definir o link ao clicar, Em #ID sera colocado o id do item que foi clicado.
- `'modal'` em caso de abertura de modal, sendo "Largura X Altura" ex: 250x100, para que fique automatico use a palavra "auto"

***ex:***
```php
$navigi->config = [
    ...
    
    'botao' => [
        [
            'ico' => ll::$data->app->pasta.'img/box.png', 
            'link' => ll::$data->app->sen_html.'&apm=produtos>produto&ac=estoque&id=#ID',
            'modal' => '300xauto'
        ]
    ]
];
```

--------

## Apagando registros
Para habilitar a função "apagar" passe como "true" o paramentro 'delete'
```php
$navigi->delete = true;      
```

## Renomeando registros	
Para habilitar a função "renomear" passe como "true" o paramentro 'rename'
```php
$navigi->rename = true;      
```

##	Alterando os nomes das etiquetas

```php
$navigi->etiqueta = [
    'id' => 'Pedido',
    'coluna' => 'Data'
];
```
                        
1. lembrando que essas são as duas padrões utilizadas pelo sistema, caso adicione mais, as mesmo serão carregadas no modo lista com seus respectivos conteudos
2. utilize um array com o arg 0 com o nome e o arg 1 com a medida da coluna caso necessário ex: `'usuario' => ['nome', '50px'];`

## Pesquisa simples

Para instanciar uma pesquisa utilize:
```
{nomeDaColuna}:{tipoDaColuna}[,{nomeDaColuna}:{tipoDaColuna}[,...]]
```

- `{nomeDaColuna}` sera o mode de umas das colunas obtidas na query;
- `{tipoDaColuna}` é o tipo que essa coluna é (int (inteiro) ou str (string));

***EXP:***
```php
$navigi->pesquisa = 'Id:int,Numero:str';
```

Por padrao todos são strings
```php
$navigi->pesquisa = 'Id,Numero';
```

### Pesquisa placeholder
É posivel confirar um placeholder diferente em seu campo de pesquisa.
```php
$navigi->placeholder = 'Numero e nome';
```

--------

##	Exemplo de utilização simples
	
	$navigi = new \Api\Navigi\Navigi();
	$navigi->tabela = PREFIXO.'app';
	$navigi->query = 'select * from '.$navigi->tabela.' order by nome asc' ;
	$navigi->delete = true;
	$navigi->rename = true;
	$navigi->config = array(
		'ico' => ll::$data->app->pasta.'imagens/sys/app.png',
		'link' => '?app=meuapp&ac=editar&id='           
		);								
	$navigi->monta();