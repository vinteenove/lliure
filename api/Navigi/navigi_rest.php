<?php
namespace Api\Navigi;

use Helpers\HttpHelper;
use LliurePanel\ll;
use Opt\SessionFix\SessionFix;

class navigi_rest {

    protected $navigi;

    protected function __construct($navigi){
        $this->navigi = $navigi;
    }

    protected function get(){
        $db = new navigi_db($this->navigi);
        return $db->get();
    }

    protected function tot(){
        $db = new navigi_db($this->navigi);
        return $db->tot();
    }

    protected function put($tabela, array $dados, $where){
        $db = new navigi_db($this->navigi);
        $db->put($tabela, $dados, $where);
    }

    protected function del($tabela, $where){
        $db = new navigi_db($this->navigi);
        $db->del($tabela, $where);
    }

    final public static function start(){
        $rest = get_called_class();
        $rest = new $rest(((isset($_POST['token']))? unserialize(jf_decode(SessionFix::getSessionId(), $_POST['token'])): []));
        $rest->run();
    }

    final private function run(){
        switch(isset($_GET['nvgAC'])? $_GET['nvgAC']: ''){

            case 'pesquisa':

                $pesquisa = ((isset($_POST['pesquisa']) && !empty($_POST['pesquisa']))? $pesquisa = '/' .  HttpHelper::buildQuery(['nvgP' => (base64_encode($_POST['pesquisa']))]): '');

                if(isset($_POST['filter']) && (isset($_POST['nvgS']) || isset($_POST['nvgO']))){

                    $o = $f = [];

                    if(isset($_POST['nvgS'])) foreach($_POST['nvgS'] as $k => $v)
                        if(!empty($v)) $f['nvgS'][$k] = rawurlencode($v);

                    if(isset($_POST['nvgO'])) foreach($_POST['nvgO'] as $k => $v)
                        if(!empty($v) && !!($v = (explode(':', $v)))) $o[$v[0]] = [$k => $v[1]];

                    ksort($o);
                    if(!empty($o)) $f['nvgO'] = [];
                    foreach($o as $k => $v) $f['nvgO'] = array_merge($f['nvgO'], $v);

                    $pesquisa = ((!!($f = HttpHelper::buildQuery($f)))? "/$f": '');
                }

                header('location: ' . rtrim($_POST['url'], '/') . $pesquisa);

            break;
            case 'delete':

                $seletor = 0;
                if($this->navigi['configSel'] != false && isset($_POST['seletor'])) $seletor = $_POST['seletor'];

                if ((key_exists('delete', $this->navigi['config'][$seletor]) && !!$this->navigi['config'][$seletor]['delete'])
                || (!key_exists('delete', $this->navigi['config'][$seletor]) && !!$this->navigi['delete'])){

                    $tabela = $this->navigi['config'][$seletor]['tabela'];
                    $asId = $this->navigi['config'][$seletor]['as_id'];
                    $id = ($_POST['id']);

                    try{
                        $this->del($tabela, '`' . $asId . '` = "'. $id. '"');
                        echo 200;

                    }catch(Exception $e){
                        echo $e->getMessage();
                    }

                }else
                    echo 403;

            break;
            case 'rename':

                $seletor = 0;
                if($this->navigi['configSel'] != false && isset($_POST['seletor'])) $seletor = $_POST['seletor'];

                if ((key_exists('rename', $this->navigi['config'][$seletor]) && !!$this->navigi['config'][$seletor]['rename'])
                || (!key_exists('rename', $this->navigi['config'][$seletor]) && !!$this->navigi['rename'])){

                    $_POST = jf_iconv2($_POST);

                    $tabela = $this->navigi['config'][$seletor]['tabela'];
                    $asId = $this->navigi['config'][$seletor]['as_id'];
                    $asNome = $this->navigi['config'][$seletor]['coluna'];

                    $dados[$asNome] = ($_POST['texto']);
                    $id = ($_POST['id']);

                    try{
                        $this->put($tabela, $dados, '`' . $asId . '` = "'. $id. '"');
                        echo 200;

                    }catch(Exception $e){
                        echo $e->getMessage();
                    }

                }else echo 403;

            break;
            default:

                /* $query = mysql_query($this->navigi['query']);
                if(mysql_error() != false) die('Erro na consulta mysql: <strong>' . $this->navigi['query'] . '</strong>'); */

                $result = $this->get();

                if($this->navigi['paginacao'] !== false)
                    $this->navigi['paginacao']['paginas'] = ceil($this->tot() / $this->navigi['paginacao']['itens']);

                $tratamento = function($dados){

                    $configSel = 0;
                    if($this->navigi['configSel'] != false) $configSel = $dados[$this->navigi['configSel']];

                    /** Configura a coluna e o id que serao exibidos    */
                    //$dados['coluna'] = $dados[$this->navigi['config'][$configSel]['coluna']];
                    //$dados['id'] = $dados[$this->navigi['config'][$configSel]['id']];
                    $dados['coluna'] = \Helpers\HtmlHelper::value($this->navigi['config'][$configSel]['coluna'], $dados, '');
                    $dados['id'] = \Helpers\HtmlHelper::value($this->navigi['config'][$configSel]['id'], $dados, '');

                    /**********        DEFINICAO DO CLICK                        **/
                    $dados['click'] = null;

                    if(isset($this->navigi['config'][$configSel]['link_col']))
                        $dados['click'] = $dados[$this->navigi['config'][$configSel]['link_col']];

                    elseif(isset($this->navigi['config'][$configSel]['link']))
                        $dados['click'] = $this->navigi['config'][$configSel]['link'] . $dados['id'];


                    /** DEFINE SE O CLICK SERA MODAL */
                    if(isset($this->navigi['config'][$configSel]['modal'])){
                        if(strpos('x', $this->navigi['config'][$configSel]['modal']) !== false){
                            $tamanho = explode('x', $this->navigi['config'][$configSel]['modal']);
                            $dados['modal'] = htmlentities(json_encode(['url' => $dados['click'], 'width' => $tamanho[0], 'height' => $tamanho[1]]), ENT_QUOTES, 'UTF-8');
                        }else{
                            $dados['modal'] = htmlentities(json_encode(['url' => $dados['click'], 'width' => $this->navigi['config'][$configSel]['modal']]), ENT_QUOTES, 'UTF-8');
                        }
                    }


                    /**********        DEFINIÇÃO DO ICONE                            **/
                    if(!isset($this->navigi['config'][$configSel]['fa'])){
                        $dados['ico'] = 'api/navigi/img/ico.svg';

                        if(isset($this->navigi['config'][$configSel]['ico']))
                            $dados['ico'] = $this->navigi['config'][$configSel]['ico'];

                        if(isset($this->navigi['config'][$configSel]['ico_col']) && !empty($this->navigi['config'][$configSel]['ico_col']))
                            //$dados['ico'] = $dados[$this->navigi['config'][$configSel]['ico_col']];
                            $dados['ico'] = \Helpers\HtmlHelper::value($this->navigi['config'][$configSel]['ico_col'], $dados, $dados['ico']);

                    }else{
                        $dados['fa'] = $this->navigi['config'][$configSel]['fa'];
                    }
                    /**/

                    $dados['as_id'] = $dados[$this->navigi['config'][$configSel]['as_id']]; // alias para o id

                    /**********        DEFINIÇÃO DAS FUNÇÕES DE RENOMEAR E DELETAR    **/
                    /** Também realiza a codificação de permições, usadas no js    **/

                    $dados['rename'] = (($dados['rename'] === null)?
                        ((isset($this->navigi['config'][$configSel]['rename']))?
                            !!$this->navigi['config'][$configSel]['rename']:
                            !!$this->navigi['rename']): !!$dados['rename']);

                    $dados['delete'] = (($dados['delete'] === null)?
                        ((isset($this->navigi['config'][$configSel]['delete']))?
                            !!$this->navigi['config'][$configSel]['delete']:
                            !!$this->navigi['delete']): !!$dados['delete']);

                    $per_ren = (($this->navigi['rename'])? '1': '0');
                    $per_del = (($this->navigi['delete'])? '1': '0');

                    $dados['permicao'] = $per_ren . $per_del;

                    if(isset($this->navigi['config'][$configSel]['botao']))
                        $dados['botao'] = $this->navigi['config'][$configSel]['botao'];

                    return $dados;
                };

                $this->navigi['rename'] = ($this->navigi['rename']? 1: 0);
                $this->navigi['delete'] = ($this->navigi['delete']? 1: 0);

                $lista = '';

                //// exibindo como icones
                if($this->navigi['exibicao'] == 'icone'){
                    //while($dados = mysql_fetch_assoc($query)){
                    foreach($result as $dados){
                        $dados = $tratamento(array_merge([
                            'rename' => null,
                            'delete' => null,
                        ], $dados));

                        $attr = array_merge([
                            'id'       => 'tem_' . $dados['id'],
                            'as_id'    => $dados['as_id'],
                            'dclick'   => $dados['click'],
                            'permicao' => $dados['permicao'],
                            'nome'     => $dados['coluna'],
                        ], ($this->navigi['configSel'] != false? ['seletor' => $dados[$this->navigi['configSel']]]: []));

                        ob_start(); ?>
                        <div class="navigi_item"<?php echo self::montaAttr($attr); ?>>
                            <div class="navigi_item_main">
                                <div class="navigi_item_padding">
                                    <div class="navigi_item_padding_main">
                                        <div class="navigi_item_content">
                                            <div class="navigi_ico">
                                                <?php
                                                if(isset($dados['ico']) && !strstr($dados['ico'], '.')){
                                                    $dados['fa'] = $dados['ico'];
                                                }

                                                if(isset($dados['fa'])){ ?>
                                                    <i class="navigi_fa fa fas <?php echo $dados['fa']; ?>"></i>
                                                <?php }else{ ?>
                                                    <div class="navig_thumb" style="background-image: url('<?php echo $dados['ico']; ?>'); "></div>
                                                <?php } ?>
                                            </div>
                                            <div id="nome_<?php echo $dados['id']; ?>" class="navigi_nome">
                                                <?php echo $dados['coluna']; ?>
                                            </div>
                                        </div>
                                        <i class="navigi_menuContextoOpen fa fa-exclamation-circle"></i>
                                        <div class="navigi_contextoMenu">
                                            <div class="btn-group-vertical">
                                                <button type="button" class="btn btn-default btn-sm navigi_icone_open">
                                                    Abrir
                                                </button>
                                                <?php
                                                if (!!$dados['rename']) {
                                                    ?>
                                                    <button type="button"
                                                            class="btn btn-default btn-sm navigi_icone_rename">
                                                        Renomear
                                                    </button>
                                                    <?php
                                                }

                                                if (!!$dados['delete']) {
                                                    ?>
                                                    <button type="button"
                                                            class="btn btn-default btn-sm navigi_icone_delete">
                                                        Deletar
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $lista .= ob_get_clean();
                    }

                    //// exibindo como lista
                }else{
                    $ico = false;

                    if(!!$this->navigi['configSel']) foreach($this->navigi['config'] as $conf)
                        $ico = (isset($conf['ico']) || isset($conf['fa'])? true: $ico);

                    $linhas = [];
                    //while($dados = mysql_fetch_array($query)){
                    foreach($result as $dados){

                        $dados = $tratamento(array_merge([
                            'rename' => null,
                            'delete' => null,
                            '__bots' => [],
                            '__cell' => '',
                        ], $dados));

                        //echo '<pre>';
                        //var_dump($ico);
                        //echo '</pre>';
                        //echo '<pre>$dados = '. print_r($dados, 1). '</pre>';

                        /** ICONE */
                        if($ico == true) $dados['__cell'] .= (
                            '<td class="navigi_ico">'
                            . (isset($dados['fa'])
                                ? '<i class="fa ' . $dados['fa'] . '"></i>'
                                : '<img src="' . $dados['ico'] . '" alt="' . $dados['coluna'] . '" style="max-width: 16px;"/>')
                            . '</td>'
                        );

                        /** ID / COD. */
                        $dados['__cell'] .= '<td class="navigi_cod">' . str_pad($dados['as_id'], 7, 0, STR_PAD_LEFT) . '</td>';

                        /** NOME */
                        $dados['__cell'] .= '<td><div class="navigi_nome">' . $dados['coluna'] . '</div></td>';


                        /** ETIQUETAS | puxando os campos que foram setados nas etiquetas    ***/
                        if(!empty($this->navigi['cell']))
                            foreach($this->navigi['cell'] as $key => $valor)
                                //$dados['__cell'] .= '<td>' . $dados[$key] . '</td>';
                                $dados['__cell'] .= '<td>' . \Helpers\HtmlHelper::value($key,  $dados, '') . '</td>';

                        /** BOTOES */
                        if(isset($dados['botao'])) foreach($dados['botao'] as $key => $valor){
                            $bot = [];
                            if(isset($valor['link'])) $bot['href'] = str_replace('#ID', $dados['id'], $valor['link']);
                            if(isset($valor['modal'])) $bot['class'] = ((isset($valor['class']))? $valor['class']. ' ': ''). 'navigi_bmod';
                            $bot['class'] = ((isset($bot['class']))? $bot['class']. ' ': ''). 'btn btn-default';
                            if(isset($valor['modal'])) $bot['rel'] = $valor['modal'];
                            $ic = (isset($valor['fa'])? '<i class="fa ' . $valor['fa'] . '"></i>': ((isset($valor['ico']))? '<img src="' . $valor['ico'] . '">': ''));

                            unset($valor['link'], $valor['modal'], $valor['class'], $valor['fa'], $valor['ico']);
                            $bot = array_merge($bot, $valor);
                            $dados['__bots'][] = '<a ' . \Helpers\HtmlHelper::attr($bot) . '>' . $ic . '</a>';
                        }
                        if(!!$dados['rename']) $dados['__bots'][] = '<button type="button" class="navigi_ren btn btn-default"><i class="fas fa-pencil-alt"></i></button>';
                        if(!!$dados['delete']) $dados['__bots'][] = '<button type="button" class="navigi_del btn btn-default"><i class="fas fa-trash-alt"></i></button>';

                        $dados['__cell'] .= '<td class="navigi_botoes text-right" style="white-space: nowrap;"><div class="btn-group">';
                        foreach($dados['__bots'] as $bot)
                            $dados['__cell'] .= $bot;
                        $dados['__cell'] .= '</td></div>';


                        //$linhas[] = $dados;

                        $lista .=
                            '<tr class="navigi_tr" '
                            . 'id="item_' . $dados['id'] . '" '
                            . 'as_id = "' . $dados['as_id'] . '" '
                            . 'dclick="' . $dados['click'] . '" '
                            . ((isset($dados['modal']))? 'modal="'. $dados['modal'] . '" ': '')
                            . ($this->navigi['configSel'] != false? 'seletor="' . $dados[$this->navigi['configSel']] . '" ': '')
                            . 'permicao="' . $dados['permicao'] . '" '
                            . 'nome="' . $dados['coluna'] . '">'
                            . $dados['__cell']
                            . '</tr>';

                    }
                }


                $pagi = '';
                if($this->navigi['paginacao'] !== false){

                    $links = [];

                    $pg = ($this->navigi['paginacao']['pagina']);
                    $tl = $this->navigi['paginacao']['paginas'];
                    $r = floor($this->navigi['paginacao']['range'] / 2);
                    $min = max(1, $pg - $r);
                    $max = min($tl, $pg + $r);
                    $max = ($min ==   1? min($tl, ($r * 2 + 1)): $max);
                    $min = ($max == $tl? max( 1, $tl - ($r * 2)): $min);

                    $ant = ((($ant = $pg - 1) < $min)? $min: $ant);
                    $pro = ((($pro = $pg + 1) > $tl)? $tl: $pro);

                    if($tl > 1){

                       if(isset($this->navigi['paginacao']['start']) && !!$this->navigi['paginacao']['start'])
                            $links[] = '<li class="paginate_button page-item ' . (($min <= 1)? ' disabled': '') . '" >' . (!($min <= 1)?
                                '<a href="' . (empty($this->navigi['paginacao']['url'])? '': $this->navigi['paginacao']['url'] . '&') . "nvgPG=" . 1 . '"  class="page-link">' . $this->navigi['paginacao']['start'] . '</a>':
                                '<span class="page-link">' . $this->navigi['paginacao']['start'] . '</span>') .
                            '</li>';

                        if(isset($this->navigi['paginacao']['prev']) && !!$this->navigi['paginacao']['prev'])
                            $links[] = '<li class="paginate_button page-item ' . (($pg <= 1)? ' disabled': '') . '" >' . (!($pg <= 1)?
                                '<a href="' . (empty($this->navigi['paginacao']['url'])? '': $this->navigi['paginacao']['url'] . '&') . "nvgPG=" . $ant . '"  class="page-link">' . $this->navigi['paginacao']['prev'] . '</a>':
                                '<span class="page-link">' . $this->navigi['paginacao']['prev'] . '</span>') .
                            '</li>';

                        for($i = $min; $i <= $max; $i++) {
                            $links[] = '<li class="paginate_button page-item ' . ($i == ($pg) ? 'active' : '') . '" ><a class="page-link" href="' . (empty($this->navigi['paginacao']['url']) ? '' : $this->navigi['paginacao']['url'] . '&') . "nvgPG=" . $i . '" tabindex="0">' . $i . '</a></li>';
                        }

                        if(isset($this->navigi['paginacao']['next']) && !!$this->navigi['paginacao']['next'])
                            $links[] = '<li class="paginate_button page-item' . (($pg >= $tl)? ' disabled': '') . '" >' . (!($pg >= $tl)?
                                '<a href="' . (empty($this->navigi['paginacao']['url'])? '': $this->navigi['paginacao']['url'] . '&') . "nvgPG=" . $pro . '" class="page-link">' . $this->navigi['paginacao']['next'] . '</a>':
                                '<span class="page-link">' . $this->navigi['paginacao']['next'] . '</span>') .
                            '</li>';

                        if(isset($this->navigi['paginacao']['end']) && !!$this->navigi['paginacao']['end'])
                            $links[] = '<li class="paginate_button page-item' . (($max >= $tl)? ' disabled': '') . '">' . (!($max >= $tl)?
                                '<a href="' . (empty($this->navigi['paginacao']['url'])? '': $this->navigi['paginacao']['url'] . '&') . "nvgPG=" . $tl . '" class="page-link">' . $this->navigi['paginacao']['end'] . '</a>':
                                '<span class="page-link">' . $this->navigi['paginacao']['end'] . '</span>') .
                            '</li>';
                    }

                    $pagi .= '<div class="dataTables_paginate paging_simple_numbers"><ul class="pagination">' . implode('', $links) . '</ul></div>';
                    /*
                    <ul class="pagination">
                        <li class="paginate_button page-item active">
                            <a href="#"
                               aria-controls="example2"
                               data-dt-idx="1"
                               tabindex="0"
                               class="page-link">1</a>
                        </li>

                        <li class="paginate_button page-item next"
                            id="example2_next">
                            <a href="#"
                               aria-controls="example2"
                               data-dt-idx="7"
                               tabindex="0"
                               class="page-link">Next</a>
                        </li>
                    </ul>
                    */
                }

                echo \Helpers\JsonHelper::encode(['list' => $lista, 'pagi' => $pagi]);
            break;
        }
    }

    private static function montaAttr(array $attrs){
        $r = '';
        foreach($attrs as $k => $v)
            $r .= ' ' . ((is_string($k))? $k: $v) . '="' . ((is_string($v) || is_numeric($v))? $v: ((is_null($v))? $k: ((is_bool($v))? 'true': 'false'))) . '"';
        return ($r);
    }

}