<?php
namespace Api\Navigi;
use DB\DB;

class navigi_db extends DB{

    private $aplimo;

    public function __construct(array $aplimo){
        parent::__construct($aplimo['tabela']);
        $this->aplimo = $aplimo;
    }

    protected function &conectar($basetype = null, $hostName = null, $userName = null, $password = null, $tableName = null){
        $r = parent::conectar($basetype, $hostName, $userName, $password, $tableName);
        $this->exec("SET NAMES 'utf8'");
        $this->exec("SET CHARACTER SET utf8");
        $this->exec("SET COLLATION_CONNECTION = 'utf8_unicode_ci'");
        return $r;
    }

    public function get(){
        return self::select($this->aplimo['queryLista']);
    }

    public function tot(){
        return self::first(self::select($this->aplimo['queryTotal']))['total'];
    }

    public function put($tabela, array $dados, $where){
        self::setTempTab($tabela)->update($dados, $where);
    }

    public function del($tabela, $where){
        self::setTempTab($tabela)->delete([[]], $where);
    }
}