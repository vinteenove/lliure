<?php

namespace Opt\SessionFix;

use Helpers\HttpHelper;
use LliurePanel\Routable;
use LliurePanel\Strategy\JsonStrategy;
use Persona\Persona;
use Persona\PersonaBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Router\Router;

class SessionFix implements Routable, PersonaBuilder, MiddlewareInterface
{

	static private string $sessionId;
	
	public static function router(): Router{
		$router = new Router;

		$router->setStrategy(new JsonStrategy);
		$router->get('/', fn() => ['time' => time()]);

		return $router;
	}
	

	/**
	 * @return bool
	 */
	public static function is_session_started()
	{
		if (php_sapi_name() !== 'cli') {
			if (version_compare(phpversion(), '5.4.0', '>=')) {
				return session_status() === PHP_SESSION_ACTIVE;
			} else {
				return session_name() === '';
			}
		}
		
		return FALSE;
	}
	
	/**
	 * bootstrap
	 */
	public static function bootstrap($sessionId){
		if (self::is_session_started() === FALSE) {
			self::$sessionId = $sessionId;
			session_name($sessionId);
			session_start();
		}
	}
	
	public static function getSessionId(){
		return self::$sessionId;
	}

	/**
	 *
	 */
	public static function build(): void{
		$url = HttpHelper::pathToUri(__DIR__ . '/../../session');
		Persona::call([self::class . '::script', $url], Persona::LOW_PRIORITY);
	}

	/**
	 * @param $link
	 */
	static public function script($link){
		ob_start(); ?><!-- Sessionfix --><script type="text/javascript">window.addEventListener("load", f => setInterval(f => fetch("<?php echo $link; ?>"), 1000*60*10));</script><?php ob_end_flush();
	}

	/**
	 * Sessionfix constructor.
	 * @param string $idSession
	 */
	public function __construct(string $idSession){
		self::$sessionId = $idSession;
	}

	/**
	 * @inheritDoc
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{
		self::bootstrap(self::$sessionId);
		
		self::build();
		
		$response = $handler->handle($request);
		
		return $response;
	}
	
}