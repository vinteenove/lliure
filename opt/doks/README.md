# Doks

## O que é 

Um opt dedicado a busca e listagem dos README.md nos modulos (app, api, opt e usr). 

## Como usa

Crie um arquivo README.md na rais do modulo e coloque as expliçações de uso dele

### sub link

Caso queira criar um link para um outro arquivo use o seguinte padrão

```
index.php?opt=doks&apm=dok&p=dok-[typeModulo]-[chaveModulo]&dok=[caminho]>[fileNome].md
``` 

`[typeModulo]`: o tipo do modulo (app, api, opt e usr)

`[chaveModulo]`: geralmente o nome da pasta do modulo

`[caminho]`: caminho para o arquivo, subistituindo: "/" por ">" e "/../" por "<"

`[fileNome]`: nome do arquivo

### Exemplo de sub Link

Considere o seguinte exemplo:

- tipo do modulo: app
- nome do app: teste
- caminho do arquivo que queremos abrir: app/teste/pagina
- nome do arquivo: LEIA.md

**RESULTADO**:

```
index.php?opt=doks&apm=dok&p=dok-app-teste&dok=app>teste>pagina>LEIA.md
```