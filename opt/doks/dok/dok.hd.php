<?php
use LliurePanel\ll;
use Parsedown;

global $dok, $parsedonw;

\GridSystem\GridSystem::build();
ll::usr('rdscrollspy');

$parsedonw = new Parsedown();

$_GET['dok'] = ((isset($_GET['dok']))? $_GET['dok']: 'README.md');

$dok = file_get_contents(\Helpers\HttpHelper::path(ll::baseDir() . str_replace(['>', '<'], ['/', '/../'], $_GET['dok'])));