<?php global $dok, $parsedonw; ?>

<div id="cmdGeral" class="row">
    <div id="mark-top" class="xs-1-1"></div>
    <div id="cmdTexto" class="md-4-5">
        <?php echo $parsedonw->text($dok); ?>
    </div>
    <div class="md-1-5">
        <div id="cmdMenuBase">
            <div id="cmdMenuFix">
                <ul id="cmdMenu" class="cmd-menu" data-nivel="2"></ul>
            </div>
        </div>
    </div>
    <div id="mark-botton" class="xs-1-1"></div>
</div>

<script>

    (function ($) {

        var $cmdTexto = $("#cmdTexto");
        var $cmdMenu = $("#cmdMenu");
        var pai = $cmdMenu;
        var irmao = $cmdMenu;

        $cmdTexto.find('h2, h3, h4').each(function(i, e){
            $(this).attr('id', 'title-' + i);
            var eu = $('<li id="li_' + i + '"><a href="#title-' + i + '">' + $(this).text() + '</a></li>');

            do{
                
                var vPai = parseInt(pai.data('nivel'));
                var vEu = parseInt(e.localName[1]);

                if(pai.is(':not([data-nivel])')) break;

                if (vPai == vEu){
                    pai.append(eu);
                    break;
                }

                if(vEu > vPai){
                    var nPai = $('<ul data-nivel="' + (e.localName[1]) + '">');
                    irmao.append(nPai);
                    pai = nPai;
                    continue;
                }

                if(vEu < vPai) pai = pai.parents('ul').eq(0);

            }while(true);

            irmao = eu;
        });


        $('.cmd-menu a').click(function () {
            var top = $($(this).attr('href')).offset().top - 5;

            $('html, body').animate({
                scrollTop: top
            }, 500);
            return false;
        });

    })(jQuery);

</script>