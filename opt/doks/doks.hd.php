<?php
use LliurePanel\ll;
use Persona\Persona;
use Api\Aplimo\Aplimo;

global $aplimo, $_ll;

ll::api('aplimo');
ll::usr('highlight');
Persona::add(__DIR__ . '/doks.css', 'css');

$aplimo = new Aplimo(false);
$aplimo->nome = 'Doks';


$loadDoks = function() use (&$aplimo, $_ll){

    $doks = [];

    foreach([
        'app',
        'api',
        'opt',
        'usr',
    ] as $p) foreach(array_diff(scandir(\Helpers\HttpHelper::path(ll::baseDir() . $p)), ['.', '..']) as $mod)
        if(file_exists(ll::baseDir() . ($f = $p . DIRECTORY_SEPARATOR . $mod . DIRECTORY_SEPARATOR . 'README.md')))
            $doks[$p][$mod] = str_replace(DIRECTORY_SEPARATOR, '>', $f);
    
    $itens = [];
    foreach([
        'APPs' => 'app',
        'APIs' => 'api',
        'OPTs' => 'opt',
        'USRs' => 'usr',
    ] as $k => $v) foreach(((isset($doks[$v]))? $doks[$v]: []) as $mod => $dok)
        $itens[$v][] = $aplimo->menuItem(($p = 'dok-' . $v . '-' . $mod), [
            'nome' => $mod,
            'href' => $_ll[$_ll['operation_type']]['home'] . '&apm=dok&p=' . $p . '&dok=' . $dok,
            'mark' => ['p' => $p],
        ]);

    $menu = [];
    foreach([
        'APPs' => 'app',
        'APIs' => 'api',
        'OPTs' => 'opt',
        'USRs' => 'usr',
    ] as $k => $v) if(isset($itens[$v]))
        $menu[] = $aplimo->menuSubGrupo('doks-' . $v, $k, $itens[$v]);
    
    return $menu;
};


$aplimo->menuNovo([
    $aplimo->menuGrupo('doks', 'Documentações',
        array_merge([ $aplimo->menuItem('dok', [
            'nome' => 'lliure',
            'href' => $_ll[$_ll['operation_type']]['home'] . '&apm=dok&p=dok&dok=README.md',
            'mark' => ['p' => 'dok'],
            'home' => 'dok'
        ])], $loadDoks())
    )
]);

$aplimo->header();