<?php


namespace Opt\Teste\Models;


use dibi;
use LliureCore\Model;

class UserFeatures extends Model {

    protected static $primaryKey = 'id';

    protected static ?string $table = 'teste_user_features';

    public static function findByIdUser($idUser): array{
        return dibi::query('SELECT * FROM ' .  static::getTable() . ' WHERE idUser = ? ', $idUser)->fetchAll();
    }

}