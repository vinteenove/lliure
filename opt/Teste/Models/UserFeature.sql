CREATE TABLE `ll_teste_user_features` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`idUser` INT(11) NULL DEFAULT NULL,
	`feature` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `id_feature` (`feature`, `idUser`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;