<?php


namespace Opt\Teste;


use Opt\Singin\SinginLogger;
use Opt\Singin\SinginPermission;

class TesteFeatures {

    use SinginPermission;

    /*public function map(){
        $permissions = new SinginPermission(self::class);

        $permissions->any('verPao');

        $permissions->children('users', [Singin::class, 'createUser'],  function(SinginPermission $permissions){
            $permissions->children('verPao', true, function(SinginPermission $permissions){
                $permissions->feature('verBoi', false);
                $permissions->admin('createUser', [Singin::class, 'createUser']);
                $permissions->user('createUser', [Singin::class, 'createUser']);
            });
            $permissions->children('createPao', [], function(SinginPermission $permissions){
                $permissions->any('verBoi', [self::class, 'verBoi']);
                $permissions->any('createUser', [Singin::class, 'createUser']);
                $permissions->admin('createUser', [Singin::class, 'createUser']);
            });
        });
        return $permissions;
    }

    public static function hasFeature($feature){
        return self::map()->hasPermision($feature);
    }*/

    protected static function getAppKey(): string{
        return Teste::class;
    }

    public static function verAzul(): bool{
        return self::isAdmin()? true: self::hasPermission('verAzul');
    }

    public static function verVermelho(): bool{
        return self::hasPermission('verVermelho');
    }

    public static function verVerde(): bool{
        return self::isAdmin();
    }

    public static function verRoxo(): bool{
        return self::verAzul() && self::verVermelho();
    }
    public static function verAmarelo(): bool{
        return self::verVerde() || self::verVermelho();
    }
}