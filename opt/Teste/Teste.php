<?php

namespace Opt\Teste;

use App\Connect\Models\OwnerFeature;
use LliurePanel\Middleware\NeedUserMiddleware;
use LliurePanel\Routable;
use Opt\Singin\SinginLogger;
use Opt\Teste\Models\UserFeatures;
use Router\Router;

class Teste implements Routable {

    public static function router(): Router{
        $router = new Router;

        $router->get('/', [self::class, 'index'])->middleware(new NeedUserMiddleware(new SinginLogger(self::class, [self::class, 'myUserLogin'])));

        return $router;
    }

    public static function index(){
        ob_start();
        echo '<pre>' . __FILE__ . ':' . __LINE__ . ': self::myUser() = '. print_r(self::myUser(), 1). '</pre>' . "\n";

        if(TesteFeatures::verAzul()){
            echo '<span style="background: #00a; color: #fff">feature(verAzul) - admin pode ver</span><br>';
        }

        if(TesteFeatures::verVermelho()){
            echo '<span style="background: #a00;  color: #fff">feature(verVermelho)</span> <br>';
        }

        if(TesteFeatures::verVerde()){
            echo '<span style="background: #0a0;  color: #fff">feature(verVerde) - apenas admin</span><br>';
        }

        if(TesteFeatures::verRoxo()){
            echo '<span style="background: #a0a;  color: #fff">feature(verRoxo) - se tiver azul e vermelho</span><br>';
        }

        if(TesteFeatures::verAmarelo()){
            echo '<span style="background: #aa0;  color: #fff">feature(verAmarelo) - se tiver vermelho ou verde</span><br>';
        }


        return ob_get_clean();
    }

    public static function myUser(){
        $user = clone SinginLogger::getUser();
        $user->features = SinginLogger::getAppUserFeature(self::class);
        return $user;
    }

    public static function myUserLogin($user){
        $features = array_column(UserFeatures::findByIdUser($user->id), 'feature');
        return $features;
    }

}