<?php


namespace Opt\LoadRoutines;

use LliurePanel\ll;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class LoadRoutines implements
    MiddlewareInterface
{

    private static array $files = [];

    public static function bootstrap()
    {

        if(!self::isDivergente()){
            return;
        }

        $msgs = [];

        foreach  (glob(ll::baseDir() . 'usr/*/sup.*.php.ll') as $entry) {
            $nameSpace = basename(dirname($entry));
            $nameClass = explode('.', basename($entry, '.php.ll'))[1];

            if(!file_exists($p = ll::baseDir() . 'etc/'.$nameSpace.'/'.$nameClass.'.php')){
                if (!file_exists(ll::baseDir() . 'etc/'.$nameSpace) && !@mkdir(ll::baseDir() . 'etc/'.$nameSpace)){
                    $msgs[] = (array('error' => 'Copiar criar o diretório <strong>'. $nameSpace. '</strong>: <strong>ERRO! </strong>'));
                }
                if (!@copy($entry, $p)){
                    $msgs[] = (array('error' => 'Copiar aquivo <strong>'. $p. '</strong>: <strong>ERRO! </strong>'));
                }
            }
        }

        if(!file_exists($FC = 'etc/llconf.ll')){
            //Configurações basicas da instalação
            ll::complila_conf((object) $in = array(
                'versao' => '11 alpha',
                'idiomas' => array(
                    'nativo' => 'pt_br',
                ),
                'tema_default' => 'AdminLTE',
                'temas' => array(
                    'persona' => 'usr\/AdminLTE\/',
                ),
                'grupo' => array(
                    'default' => array(
                        'local'    => 'default',
                        'template' => 'persona',
                        'color'    => '94324B',
                        'execucao' => 'URL_AMIGAVEL',
                        'home_wli' => 'opt=desktop',
                        'home_nli' => 'opt=singin',
                        'user_wli' => 'opt=singin',
                    ),
                ),
            ));
        }

        if(!empty($msgs)){
            echo '<pre>';
            foreach ($msgs as $k => $msg){
                echo $msg['error']."\n";
            }
            echo '</pre>';
            die();
        }

    }

    public static function isDivergente(): bool{

        if(empty(self::$files)){
            self::$files = [ll::baseDir() . 'etc/llconf.ll'];
            foreach(glob(ll::baseDir() . 'usr/*/sup.*.php.ll') as $entry){
                $nameSpace = basename(dirname($entry));
                $nameClass = explode('.', basename($entry, '.php.ll'))[1];
                self::$files[] = ll::baseDir() . 'etc/' . $nameSpace . '/' . $nameClass . '.php';
            }
        }

        foreach(self::$files as $file){
            if(!file_exists($file)){
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{

        self::bootstrap();

        return $handler->handle($request);
    }

}