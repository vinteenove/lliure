<?php

namespace Opt\Desktop\Models;

use LliureCore\Model;

class Shortcut extends Model
{
    protected static $primaryKey = 'id';
    protected static ?string $table = 'desktop_shortcut';
}