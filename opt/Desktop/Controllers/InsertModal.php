<?php

namespace Opt\Desktop\Controllers;

use LliurePanel\ll;
use LliureCore\Controller;
use Opt\Desktop\Desktop;
use Persona\Persona;

class InsertModal implements Controller{

    public static function insert(){

        $url = ll::basicUrlAnalyzer($_POST['url']);
        $url['get'] = ltrim($url['get'], '/');
        $parse = parse_url($url['get']);
        $gets = [];

        if (isset(Persona::$theme->exec)
            && (Persona::$theme->exec == URL_AMIGAVEL)) {
            ll::parse_get(explode(('/'), $parse['path']), $gets);
        } else {
            parse_str($parse['query'], $gets);
        }

        $kys = array_keys($gets);

        if(!isset($kys[0])){
            header('Location: '. ll::$data->app->onclient. '/ac=addDesktopError/error=1');
            return;
        }

        $confgs = ll::confg_app($kys[0], $gets[$kys[0]]);

        if(!isset($_POST['nome']) || empty($_POST['nome'])) {
            $_POST['nome'] = ((isset($confgs->nome)) ? $confgs->nome : 'Sem nome');
        }

        $_POST['link'] = $url['get'];
        $_POST['imagem'] = ((isset($confgs->ico))? $confgs->ico: '');

        Shortcut::build(['name' => $_POST['nome'] ,'link' => $_POST['link'],'icon' => $_POST['imagem']])->insert();

        header('Location: '. ll::$data->app->onclient. 'ac=addDesktopSuccess');
    }

    public static function addDesktop(){

        echo Desktop::engine()->render('modal/addDesktop');
    }

    public static function addDesktopError(){

        echo Desktop::engine()->render('modal/addDesktopError');
    }

    public static function addDesktopSuccess(){

        echo Desktop::engine()->render('modal/addDesktopSuccess');
    }

}