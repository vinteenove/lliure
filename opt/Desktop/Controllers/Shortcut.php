<?php

namespace Opt\Desktop\Controllers;

use LliureCore\Controller;
use Opt\Desktop\Desktop;
use Opt\Desktop\Models\Shortcut as ModelShortcut;

class Shortcut implements Controller {

    public static function controller(): string{
        
        return '';
    }

    public static function desktop(){

        $itens = ModelShortcut::all();

        return Desktop::engine()->render('desktop', ['itens' => $itens]);
    }

    public static function addDesktop(){



    }
}