<?php

namespace Opt\Desktop;

use Etc\Persona\PersonaTheme;
use League\Plates\Engine;
use LliurePanel\Middleware\NeedUserMiddleware;
use LliurePanel\Routable;
use Opt\Desktop\Controllers\InsertModal;
use Opt\Desktop\Controllers\Shortcut;
use Router\Router;

class Desktop implements Routable{

    public static function router(): Router{
        $router = new Router();
        $router->middleware(new NeedUserMiddleware);

        $router->get('/', [Shortcut::class, 'desktop']);

        $router->post('/insert', [InsertModal::class, 'insert']);
        $router->get('/addDesktop', [InsertModal::class, 'addDesktop']);
        $router->get('/addDesktopError', [InsertModal::class, 'addDesktopError']);
        $router->get('/addDesktopSuccess', [InsertModal::class, 'addDesktopSuccess']);

        return $router;
    }

    public static function engine(): Engine{
        return new Engine(__DIR__ . '/Views/' . PersonaTheme::getThemerName());
    }
}