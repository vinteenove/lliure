<?php
use LliurePanel\ll;
$desctopDoors = ll::doors('opt', 'desktop');
$_GET['url'] = ((isset($_GET['url']))? base64_decode(str_replace([' '], ['+'], $_GET['url'])): ''); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title ll_color">Adicionar ao desktop</h4>
</div>
<form class="jfbox" action="<?php echo $desctopDoors['opt']['onserver']; ?>&ac=addDesktop">
    <input type="hidden" <?php echo \Helpers\HtmlHelper::input('url', $_GET); ?>>
    <div class="modal-body">
        <label>Nome</label>
        <input type="text" class="form-control" name="nome">
        <small>Digite o nome desta página para adicionar um atalho ao desktop</small>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary btn-lliure">Criar</button>
    </div>
</form>
