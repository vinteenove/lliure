<?php \Api\Navigi\Navigi::build(); ?>
<br>
<div class="container-fluid">
    <div class="bodyhome">
        <?php
        $navegador = new \Api\Navigi\Navigi();
        $navegador->tabela = PREFIXO . "desktop_shortcut";
        $navegador->query = 'select `name`, TRIM(LEADING "/" FROM link) as link, `icon` from ' . $navegador->tabela;
        $navegador->config = array('link_col' => 'link', 'ico_col' =>  'icon', 'coluna' => 'name');

        $navegador->order = ['name' => 'ASC'];

        if(ll_tsecuryt('user')) {
            $navegador->rename = true;
            $navegador->delete = true;
        }

        $navegador->monta(); ?>
    </div>
</div>