<?php
use LliurePanel\ll;
use Persona\Persona;
global $_ll, $aplimo, $sufUrl, $dados, $dir; /* @var $aplimo aplimo */

Persona::add(__DIR__ . '/../../js/jsoneditor/jsoneditor.min.css', 'css');
Persona::add(__DIR__ . '/../../js/jsoneditor/jsoneditor.min.js', 'js');



$aplimo->hdMenuLeft([
    $aplimo->hdMenuTitle('Formularios'),
]);

$sufUrl = '&fm=' . $_GET['fm'] . '&ap=' . $_GET['ap'];

if(!(isset($_GET['it']) || isset($_GET['new']))) $aplimo->hdMenuRight([
    $aplimo->hdMenuA('Novo', $this->apm->home . $sufUrl . '&new'),
]);

$dir = \Helpers\HttpHelper::path(__DIR__ . '/../../../../../uploads/content/' . $_GET['ap']) . '/formularios';
$dados = [];

if(isset($_GET['it'])){
    if(file_exists($file = ($dir. '/' . $_GET['it'] . '.json'))) $dados = [
        'nome' => $_GET['it'],
        'json' => (file_get_contents($file))
    ];

}else
    if(!!($files = array_diff(scandir($dir), ['.', '..']))) foreach($files as $file)
        $dados[] = basename($file, '.json');