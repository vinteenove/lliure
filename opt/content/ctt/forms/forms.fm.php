<?php
use LliurePanel\ll;
global $_ll, $instalilo, $sufUrl, $dados; /* @var $this aplimo */ ?>

<form id="form-forms" action="<?php echo $this->apm->onserver . $sufUrl; ?>&ac=salvar" method="post">

    <div class="form-group">
        <label>Nome</label>
        <input type="text" class="form-control" <?php echo \Helpers\HtmlHelper::input('nome', $dados); ?>>
    </div>

    <div class="form-group">
        <label>Conteudo</label>
        <input type="hidden" <?php echo \Helpers\HtmlHelper::input('json', $dados); ?>>
        <div id="jsoneditor" style="width: 100%; height: 60vh;"></div>
    </div>

    <div class="text-right">
        <button type="submit" class="btn btn-success">Salvar</button>
        <a href="<?php echo $this->apm->home . $sufUrl; ?>" class="btn btn-default">Voltar</a>
    </div>
</form>

<script>
    (function ($){
        $(function () {

            var $form = $('#form-forms');
            var $json = $('[name="json"]');

            // create the editor
            var container = document.getElementById("jsoneditor");
            var editor = new JSONEditor(container, {
                modes: [
                    'tree',
                    'code'
                ],
                indentation: 4,
                templates: [
                    {
                        text: 'Fields',
                        title: 'Lista dos compos do forumalrio',
                        field: 'fields',
                        value: []
                    },
                    {
                        text: 'Field',
                        value: {
                            'label': 'label',
                            'type': 'text',
                            'name': ''
                        }
                    },
                    {
                        text: 'Lista',
                        title: 'Listagem do itens salvos',
                        field: 'list',
                        value: {
                            'delete' : false,
                            'rename' : true,
                            'exibicao' : 'lista',
                            'paginacao' : 20,
                            'config' : [],
                            'etiqueta' : {}
                        }
                    }
                ],
                onChange: function(e){
                    $json.val(editor.getText());
                }
            });

            // set json
            var json = JSON.parse($json.val());
            editor.set(json);
        });
    })(jQuery);
</script>