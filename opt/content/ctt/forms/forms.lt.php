<?php global $_ll, $instalilo, $sufUrl, $dados; /* @var $this aplimo */ ?>

<table class="table">
    <thead>
    <tr>
        <th>Nome</th>
        <th style="width: 84px;"></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($dados as $file){ ?>
        <?php $url = $this->apm->home . $sufUrl . '&it=' . $file; ?>
        <?php $del = $this->apm->onserver . $sufUrl . '&it=' . $file . '&ac=del'; ?>
        <tr>
            <td>
                <a href="<?php echo $url; ?>" style="display: block;"><?php echo $file; ?></a>
            </td>
            <td>
                <a href="<?php echo $url; ?>" class="btn btn-default btn-sm">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <a href="<?php echo $del; ?>" class="btn btn-default btn-sm">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>

