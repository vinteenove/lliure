<?php global $_ll, $sufUrl, $dir; /* @var $aplimo aplimo */


switch(((isset($_GET['ac']))? $_GET['ac']: '')){
    case 'salvar':
        if(!isset($_POST['nome'], $_POST['json'])) break;

        $_GET['it'] = $_POST['nome'] = preg_replace("/[^a-z0-9-]/", "", str_replace(
            ['á','à','ã','â','ä','é','è','ê','ë','í','ì','î','ï','ó','ò','õ','ô','ö','ú','ù','û','ü','ç',' '],
            ['a','a','a','a','a','e','e','e','e','i','i','i','i','o','o','o','o','o','u','u','u','u','c','-'],
            strtolower($_POST['nome'])
        ));

        file_put_contents(($dir. '/' . $_POST['nome'] . '.json'), $_POST['json']);

        Vigile::success('Formulário salvo');
        
        header('Location: '. ll::$data->url->endereco . $this->apm->home . $sufUrl . '&it='. $_GET['it']);
    break;
    case 'del':
        @unlink("{$dir}/{$_GET['it']}.json");

        Vigile::success('Formulário deletado');

        header('Location: '. ll::$data->url->endereco . $this->apm->home . $sufUrl);
    break;
}