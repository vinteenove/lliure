<?php
use LliurePanel\ll;
global $_ll, $sufUrl, $dir; /* @var $aplimo aplimo */


switch(((isset($_GET['ac']))? $_GET['ac']: '')){
    case 'salvar':
        if(!isset($_POST['label'], $_POST['nome'], $_POST['order'], $_POST['pages'])) break;

        $_GET['it'] = $_POST['nome'] = preg_replace("/[^a-z0-9-]/", "", str_replace(
            ['á','à','ã','â','ä','é','è','ê','ë','í','ì','î','ï','ó','ò','õ','ô','ö','ú','ù','û','ü','ç',' '],
            ['a','a','a','a','a','e','e','e','e','i','i','i','i','o','o','o','o','o','u','u','u','u','c','-'],
            strtolower($_POST['nome'])
        ));

        $_POST['pages'] = json_decode($_POST['pages'], true);

        file_put_contents(($dir. '/' . $_POST['nome'] . '.json'), json_encode($_POST));

        Vigile::success('Formulário salvo');

        header('Location: '. ll::$data->url->endereco . $this->apm->home . $sufUrl . '&it='. $_GET['it']);
    break;
    case 'del':

        $dPag = \Helpers\HttpHelper::path(dirname($dir). '/paginas');
        @unlink("{$dir}/{$_GET['it']}.json");

        function delTree($dir){
            foreach (array_diff(scandir($dir), array('.','..')) as $file)
                (is_dir("$dir/$file"))? delTree("$dir/$file"): @unlink("$dir/$file");
            return rmdir($dir);}
        if(file_exists($p = \Helpers\HttpHelper::path(("{$dPag}/{$_GET['it']}")))) delTree($p);

        Vigile::success('Formulário deletado');
        header('Location: '. ll::$data->url->endereco . $this->apm->home . $sufUrl);
    break;
}