<?php
use LliurePanel\ll;
use Persona\Persona;

global $_ll, $aplimo, $sufUrl, $dados, $forms, $dir; /* @var $aplimo aplimo */

Persona::add(__DIR__ . '/../../js/jsoneditor/jsoneditor.min.css', 'css');
Persona::add(__DIR__ . '/../../js/jsoneditor/jsoneditor.min.js', 'js');



$aplimo->hdMenuLeft([
    $aplimo->hdMenuTitle('Formularios'),
]);

$sufUrl = '&fm=' . $_GET['fm'] . '&ap=' . $_GET['ap'];

if(!(isset($_GET['it']) || isset($_GET['new']))) {
    $aplimo->hdMenuRight([
        $aplimo->hdMenuA('Novo', $this->apm->home . $sufUrl . '&new'),
    ]);
}

$dForm = \Helpers\HttpHelper::path(__DIR__ . '/../../../../../uploads/content/' . $_GET['ap']) . '/formularios';
$dir = \Helpers\HttpHelper::path(__DIR__ . '/../../../../../uploads/content/' . $_GET['ap']) . '/menus';
$dados = $forms = [];

if (isset($_GET['it']) || isset($_GET['new'])) {
    if (!!($files = scandir($dForm))) {
        foreach ($files as $file) {
            if (!($file == '.' || $file == '..')) {
                $forms[] = basename($file, '.json');
            }
        }
    }
}

if(isset($_GET['it'])){
    if(file_exists($file = ($dir. '/' . $_GET['it'] . '.json'))) {
        $dados = array_merge(['nome' => $_GET['it'],], json_decode(file_get_contents($file), true));
    }

} else {
    if (!!($files = array_diff(scandir($dir), ['.', '..']))) {
        foreach ($files as $file) {
            $dados[] = basename($file, '.json');
        }
    }
}