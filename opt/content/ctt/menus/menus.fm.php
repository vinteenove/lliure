<?php
use LliurePanel\ll;
global $_ll, $instalilo, $sufUrl, $dados, $forms; /* @var $this aplimo */ ?>

<?php $json = json_decode(((!empty($dados['json']))? $dados['json']: ('{"order": 10, "label": "", "pages": {}}')), true); ?>
<?php $dados['pages'] = ((isset($dados['pages']))? json_encode($dados['pages']): '{}'); ?>

<?php //echo '<pre>' . __FILE__ . ':' . __LINE__ . ': $dados = '. print_r($dados, 1). '</pre>' . "\n"; ?>

<form id="form-forms" action="<?php echo $this->apm->onserver . $sufUrl; ?>&ac=salvar" method="post">

    <div class="form-group">
        <label>Nome</label>
        <input type="text" class="form-control" <?php echo \Helpers\HtmlHelper::input('label', $dados); ?> required="required" minlength="1">
    </div>

    <div class="form-group">
        <label>Chave</label>
        <input type="text" class="form-control" <?php echo \Helpers\HtmlHelper::input('nome', $dados); ?> required="required" minlength="1">
    </div>

    <div class="form-group">
        <label>Posição</label>
        <input type="number" class="form-control" <?php echo \Helpers\HtmlHelper::input('order', $dados); ?> required="required">
    </div>

    <?php /* <div class="form-group">
        <label>Conteudo</label>
        <?php $dados['json'] = ((!empty($dados['json']))? $dados['json']: ('{"order": 10, "label": "", "pages": {}}')); ?>
        <input type="hidden" <?php echo \Helpers\HtmlHelper::input('json', $dados); ?>>
        <div id="jsoneditor" style="width: 100%; height: 60vh;"></div>
    </div> */ ?>

    <div class="form-group">
        <label>Menu</label>
        <input type="hidden" <?php echo \Helpers\HtmlHelper::input('pages', $dados); ?>>
        <div id="blocklyArea" class="form-control" style="width: 100%; height: 60vh; padding: 0; border-radius: 0;"></div>
    </div>

    <div class="text-right">
        <button type="submit" class="btn btn-success">Salvar</button>
        <a href="<?php echo $this->apm->home . $sufUrl; ?>" class="btn btn-default">Voltar</a>
    </div>
</form>

<xml id="toolbox" style="display: none">
    <category name="Configurações">
        <block type="pages"></block>
        <block type="page"></block>
        <block type="attr"></block>
    </category>
</xml>
<div id="blocklyDiv" style="position: absolute"></div>

<script>
    (function ($){
        $(function () {

            var forms = <?php echo json_encode($forms); ?>;
            var $pages = $('[name="pages"]');
            <?php /* var $json = $('[name="json"]');

            // create the editor
            var container = document.getElementById("jsoneditor");
            var editor = new JSONEditor(container, {
                modes: [
                    'tree',
                    'code'
                ],
                indentation: 4,
                templates: [
                    {
                        text: 'Grupo',
                        title: 'Item que tem sub paginas',
                        field: 'sub-menu',
                        value: {
                            'label': 'Sub menu',
                            'pages': []
                        }
                    },
                    {
                        text: 'Pagina',
                        title: 'Pagina com formulario',
                        field: 'pagina',
                        value: {
                            'label': 'Pagina',
                            'form': ''
                        }
                    }
                ],
                autocomplete: {
                    getOptions: function(text, path){
                        var node = path.pop();
                        if(node !== 'form') return null;
                        return {startFrom: 0, options: forms};
                    }
                },
                onChange: function(e){
                    $json.val(editor.getText());
                }
            });

            // set json
            var json = JSON.parse($json.val());
            editor.set(json); */ ?>





















            var options = [];
            for(var i in forms)if((function (k, v){
                options.push([v, v]);
            })(i, forms[i]) == false) break;

            Blockly.Blocks['pages'] = {
                init: function() {
                    this.appendValueInput("attrs")
                        .setCheck("attr")
                        .appendField("Paginas")
                        .appendField(new Blockly.FieldTextInput("Chave"), "chave")
                        .appendField(new Blockly.FieldTextInput("Nome"), "label");
                    this.appendStatementInput("subPages")
                        .setCheck(["pages", "page"]);
                    this.setPreviousStatement(true, ["pages", "page"]);
                    this.setNextStatement(true, ["pages", "page"]);
                    this.setColour(135);
                    this.setTooltip("");
                    this.setHelpUrl("");
                }
            };

            Blockly.Blocks['page'] = {
                init: function() {
                    this.appendValueInput("attrs")
                        .setCheck("attr")
                        .appendField("Pagina")
                        .appendField(new Blockly.FieldTextInput("Chave"), "chave")
                        .appendField(new Blockly.FieldTextInput("Nome"), "label")
                        .appendField(new Blockly.FieldDropdown(options), "form");
                    this.setPreviousStatement(true, ["pages", "page"]);
                    this.setNextStatement(true, ["pages", "page"]);
                    this.setColour(315);
                    this.setTooltip("");
                    this.setHelpUrl("");
                }
            };

            Blockly.Blocks['attr'] = {
                init: function() {
                    this.appendValueInput("attrs")
                        .setCheck(["pages", "page", "attr"])
                        .appendField(new Blockly.FieldTextInput("name"), "name")
                        .appendField(new Blockly.FieldTextInput("value"), "value");
                    this.setOutput(true, ["pages", "page", "attr"]);
                    this.setColour(45);
                    this.setTooltip("");
                    this.setHelpUrl("");
                }
            };


            Blockly.JavaScript['pages'] = function (block) {
                var text_chave = block.getFieldValue('chave');
                var text_label = block.getFieldValue('label');
                var value_attrs = Blockly.JavaScript.valueToCode(block, 'attrs', Blockly.JavaScript.ORDER_ATOMIC);
                var statements_name = Blockly.JavaScript.statementToCode(block, 'subPages');

                statements_name = statements_name.split(/\|\s/);
                statements_name.pop();
                statements_name = statements_name.join(', ');

                var code = '"' + text_chave + '": {"label": "' + text_label + '", ' + ((value_attrs.length)? value_attrs + ', ': '') + '"pages": {' + statements_name + ' }}| ';
                return code;
            };

            Blockly.JavaScript['page'] = function (block) {
                var text_chave = block.getFieldValue('chave');
                var text_label = block.getFieldValue('label');
                var dropdown_form = block.getFieldValue('form');
                var value_attrs = Blockly.JavaScript.valueToCode(block, 'attrs', Blockly.JavaScript.ORDER_ATOMIC);

                var code = '"' + text_chave + '":{"label": "' + text_label + '", ' + ((value_attrs.length)? value_attrs + ', ': '') + '"form": "' + dropdown_form + '"}| ';
                return code;
            };

            Blockly.JavaScript['attr'] = function(block) {
                var text_name = block.getFieldValue('name');
                var text_value = block.getFieldValue('value');
                var value_name = Blockly.JavaScript.valueToCode(block, 'attrs', Blockly.JavaScript.ORDER_ATOMIC);

                var code = '"' + text_name + '": "' + text_value + '"' + ((value_name.length)? ', ' + value_name: '');

                return [code];
            };

            function reverseObject(object){
                var newObject = {};
                var keys = [];
                for (var key in object)
                    keys.push(key);
                for (var i = keys.length - 1; i >= 0; i--)
                    newObject[keys[i]]= object[keys[i]];
                return newObject;
            }

            /**
             * @param json
             * @returns {string}
             */
            function blocklyJsonToXml(json){
                var xml = '';
                json = reverseObject(json);

                for(var i in json)if((function (k, v){
                    var type = '', form = '', statement = '';
                    var chave = '<field name="chave">' + k + '</field>';
                    var label = '<field name="label">' + v.label + '</field>';
                    
                    delete(v.label);

                    if(typeof v.pages == "undefined"){
                        type = 'page';
                        form  = '<field name="form">' + v.form + '</field>';
                        delete(v.form);
                    }else{
                        type = 'pages';
                        statement = '<statement name="subPages">' + blocklyJsonToXml(v.pages) + '</statement>';
                        delete(v.pages);
                    }

                    var value = blocklyJsonToXmlValue(v);
                    var next = ((xml.length)? '<next>' + xml + '</next>': '');
                    xml = '<block type="' + type + '">' + chave + label + form + value + statement + next + '</block>';

                })(i, json[i]) == false) break;
                return xml;
            }

            /**
             * @param json
             * @returns {string}
             */
            function blocklyJsonToXmlValue(json){
                var xml = '';
                json = reverseObject(json);
                for(var i in json)if((function (k, v){

                    var name = '<field name="name">' + k + '</field>';
                    var value = '<field name="value">' + v +'</field>';
                    xml = '<value name="attrs"><block type="attr">' + name + value + xml + '</block></value>';

                })(i, json[i]) == false) break;
                return xml;
            }

            var json = $pages.val();
            json = JSON.parse(((json.length)? json: '{}'));
            //console.log(json);
            var xml_text = blocklyJsonToXml(json);
            xml_text = '<xml xmlns="http://www.w3.org/1999/xhtml"><variables></variables>' + xml_text + '</xml>';
            //console.log(xml_text);








            var blocklyRef = $('.apm-container .apm-centro');
            var blocklyArea = document.getElementById('blocklyArea');
            var blocklyDiv = document.getElementById('blocklyDiv');
            var workspace = Blockly.inject(blocklyDiv, {
                toolbox: document.getElementById('toolbox'),
                media: 'usr/google-blockly/media/',
                horizontalLayout: true,
                sounds: false
            });

            var xml = Blockly.Xml.textToDom(xml_text);
            Blockly.Xml.domToWorkspace(xml, workspace);


            var onresize = function(e) {
                // Compute the absolute coordinates and dimensions of blocklyArea.
                var element = blocklyArea;
                var x = 0;
                var y = 0;
                do {
                    x += element.offsetLeft;
                    y += element.offsetTop;
                    element = element.offsetParent;
                } while (element);

                var ref = blocklyRef.offset();
                // Position blocklyDiv over blocklyArea.
                blocklyDiv.style.left = (x - ref.left) + 'px';
                blocklyDiv.style.top = (y - ref.top) + 'px';
                blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
                blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
                Blockly.svgResize(workspace);
            };
            window.addEventListener('resize', onresize, false);
            onresize();
            Blockly.svgResize(workspace);


            function myUpdateFunction(event) {
                var code = Blockly.JavaScript.workspaceToCode(workspace);

                code = code.split(/\|\s/);
                code.pop();
                code = code.join(', ');
                code = JSON.parse("{" + code + "}");
                code = JSON.stringify(code, null, '\t');

                $pages.val(code);
            }
            workspace.addChangeListener(myUpdateFunction);

        });
    })(jQuery);
</script>