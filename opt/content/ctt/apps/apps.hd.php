<?php
use LliurePanel\ll;
use DB\DB;

global $_ll, $aplimo, $dados, $dir, $instalilo; /* @var $aplimo aplimo *//* @var $this aplimo */

$aplimo->hdMenuLeft([
    $aplimo->hdMenuTitle('Aplicativos'),
]);

if(!(isset($_GET['ap']) || isset($_GET['new']))) $aplimo->hdMenuRight([
    $aplimo->hdMenuA('Novo', $this->apm->home . '&new'),
]);

$dir = \Helpers\HttpHelper::path(__DIR__ . '/../../../../../uploads/content');

$dados = [];
if(isset($_GET['ap']))
    $dados = DB::first($instalilo->get(['chave' => $_GET['ap']]));