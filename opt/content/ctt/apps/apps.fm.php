<?php
use LliurePanel\ll;
global $dados;
/* @var $this aplimo */
?>

<form action="<?php echo $this->apm->onserver . '&ac=salvar' . ((isset($dados['chave']))? '&ap=' . $dados['chave']: '&new'); ?>" method="post">

    <div class="form-group">
        <label>Nome</label>
        <input class="form-control" type="text" <?php echo \Helpers\HtmlHelper::input('nome', $dados); ?>>
    </div>

    <div class="text-right">
        <button type="submit" class="btn btn-success">Salvar</button>
        <a href="<?php echo $this->apm->home; ?>" class="btn btn-default">Voltar</a>
    </div>
</form>