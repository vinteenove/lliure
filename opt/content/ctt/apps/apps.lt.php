<?php global $_ll, $instalilo; /* @var $this aplimo */ ?>

<table class="table">
    <thead>
        <tr>
            <th>Nome</th>
            <th style="width: 84px;"></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($instalilo->get(['pasta' => 'opt=content', 'chave:!=' => 'usrContent']) as $app){ ?>
            <?php $url = $this->apm->home . '&ap=' . $app['chave']; ?>
            <?php $del = $this->apm->onserver . '&ap=' . $app['chave'] . '&ac=del'; ?>
            <tr>
                <td>
                    <a href="<?php echo $url; ?>" style="display: block;"><?php echo $app['nome']; ?></a>
                </td>
                <td>
                    <a href="<?php echo $url; ?>" class="btn btn-default btn-sm">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    <a href="<?php echo $del; ?>" class="btn btn-default btn-sm">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

