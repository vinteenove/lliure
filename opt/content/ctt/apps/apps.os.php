<?php
use LliurePanel\ll;
global $_ll, $dados, $dir, $instalilo; /* @var $this aplimo */

switch(((isset($_GET['ac']))? $_GET['ac']: '')){
    case 'salvar':

        $_POST['chave'] = preg_replace("/[^a-z0-9]/", "", str_replace(
            ['á','í','ó','ú','é','ä','ï','ö','ü','ë','à','ì','ò','ù','è','ã','õ','â','î','ô','û','ê','ç'],
            ['a','i','o','u','e','a','i','o','u','e','a','i','o','u','e','a','o','a','i','o','u','e','c'],
            strtolower($_POST['nome'])
        ));

        if(empty($dados)){
            $instalilo->set(array_merge([
                'nome'  => 'Novo App',
                'chave' => 'novo-app',
                'pasta' => 'opt=content',
            ], $_POST));

            mkdir(\Helpers\HttpHelper::path($dir . '/' . $_POST['chave']));
            mkdir(\Helpers\HttpHelper::path($dir . '/' . $_POST['chave'] . '/arquivos'));
            mkdir(\Helpers\HttpHelper::path($dir . '/' . $_POST['chave'] . '/formularios'));
            mkdir(\Helpers\HttpHelper::path($dir . '/' . $_POST['chave'] . '/menus'));
            mkdir(\Helpers\HttpHelper::path($dir . '/' . $_POST['chave'] . '/paginas'));
            file_put_contents(\Helpers\HttpHelper::path($dir . '/' . $_POST['chave'] . '/' . $_POST['chave'] . '.json'), '{}');

            Vigile::success('App criado');
        }else{
            $instalilo->upd(array_merge([
                'id'    => '',
                'nome'  => 'Novo App',
                'chave' => 'novo-app',
                'pasta' => 'opt=content',
            ], $dados, $_POST));

            if($dados['chave'] != $_POST['chave']){
                rename(
                    \Helpers\HttpHelper::path($dir . '/' . $dados['chave'] . '/' . $dados['chave'] . '.json'),
                    \Helpers\HttpHelper::path($dir . '/' . $dados['chave'] . '/' . $_POST['chave'] . '.json')
                );
                rename(
                    \Helpers\HttpHelper::path($dir . '/' . $dados['chave']),
                    \Helpers\HttpHelper::path($dir . '/' . $_POST['chave'])
                );
            }

            Vigile::success('App salvo');
        }

        header('Location: '. ll::$data->url->endereco . $this->apm->home . '&ap=' . $_POST['chave']);
    break;

    case 'del':
        $instalilo->del([
            'chave' => $_GET['ap'],
            'pasta' => 'opt=content',
        ]);

        function delTree($dir){
            foreach (array_diff(scandir($dir), array('.','..')) as $file)
                (is_dir("$dir/$file"))? delTree("$dir/$file"): unlink("$dir/$file");
            return rmdir($dir);
        }
        if(file_exists($p = \Helpers\HttpHelper::path($dir . '/' . $_GET['ap']))) delTree($p);

        Vigile::success('App deletado');
        header('Location: '. ll::$data->url->endereco . $this->apm->home);
    break;
}