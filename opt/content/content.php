<?php
use LliurePanel\ll;
use DB\DB;
use UsrContent\UsrContent;
use Opt\Instalilo\Instalilo;

class content extends UsrContent{

    private
        $application = '',
        $applicationData = [],
        $applicationPath = '',
        $applicationURI = '',

        $setMenu = '',
        $setPagina = '',

        $formularios = [],
        $menus = [];

    /** @var $aplimo aplimo */
    private $aplimo;

    /** @var $usrContent usrContent */
    private $usrContent;

    public function __construct(array $confs){
        parent::__construct([], '');

        foreach($confs as $k => $v) $this->{$k} = $v;

        if(empty($this->application) || !$this->path($this->application))
            throw new Exception("Configuração do application não existe ou mal feita", 0);

        $this->applicationData = self::load($this->applicationPath . DIRECTORY_SEPARATOR . $this->application . '.json');

        $menu = [];
        if (!!($ms = array_diff(scandir(($this->applicationPath . '/menus')), ['.', '..'])))
            foreach($ms as $m) $menu[basename($m, '.json')] = $m;

        $load = [];
        natcasesort($menu);
        foreach($menu as $k => $f){
            $data = array_merge(['order' => 10], self::load($this->applicationPath . '/menus/' . $f));
            $load[$data['order']][$k] = $data;}
        ksort($load);
        foreach($load as $p => $ms) foreach($ms as $k => $v) $this->menus[$k] = $v;

        if (!!($ms = array_diff(scandir(($this->applicationPath . '/formularios')), ['.', '..'])))
            foreach($ms as $m) $this->formularios[basename($m, '.json')] = self::load($this->applicationPath . '/formularios/' . $m) ;
    }


    /**
     * @param string $pagina ([menu]:>[pagina]>[sub_pagina...])
     * @return usrContent
     */
    public function __invoke($pagina){

        list($this->setMenu, $this->setPagina) = explode(':>', $pagina, 2);

        $menu = [];
        if(file_exists(($f = $this->applicationPath . '/menus/' . $this->setMenu . '.json'))) $menu = self::load($f);

        $formFields = [];
        if(isset($menu['pages'])){
            $formFile = self::getMenu(explode('>', $this->setPagina), $menu['pages']);
            $formFields = ((file_exists(($f = $this->applicationPath . '/formularios/' . $formFile . '.json')))? self::load($f): []);
        }

        return new parent(
            $formFields = (($formFields)? $formFields: []),
            ($this->applicationPath . implode(DIRECTORY_SEPARATOR, array_merge(['', 'paginas', $this->setMenu], explode('>', $this->setPagina)))),
            ($this->applicationPath . DIRECTORY_SEPARATOR . 'arquivos' . DIRECTORY_SEPARATOR)
        );
    }

    private static function getMenu(array $pagina, array &$menu){
        $alvo = array_shift($pagina);
        if(empty($pagina) && isset($menu[$alvo]['form']))
            return $menu[$alvo]['form'];

        elseif(!empty($pagina) && isset($menu[$alvo]['pages']))
            return self::getMenu($pagina, $menu[$alvo]['pages']);

        return false;
    }

    public function path($application){
        if (file_exists((($path = (\Helpers\HttpHelper::path(__DIR__ . '/../../../'. ($uri = \Helpers\HttpHelper::path('uploads/content/' . $application))))) . DIRECTORY_SEPARATOR . $application . '.json'))){
            $this->applicationPath = $path;
            $this->applicationURI = $uri;
            return true;
        }else return false;
    }

    public function uri(){
        return str_replace(DIRECTORY_SEPARATOR, '/', $this->applicationURI). '/';
    }

    public function URIarquivos(){
        return str_replace(DIRECTORY_SEPARATOR, '/', $this->applicationURI). '/arquivos/';
    }

    public function menu(){
        return $this->menus;
    }

    public function formulario(){
        return $this->formularios;
    }

    public function diretorio(){
        return $this->applicationPath;
    }


    public function header(){
        global $_ll;

        $instalilo = new Instalilo();
        $appData = DB::first($instalilo->get(['pasta' => 'opt=content', 'chave' => $_ll['operation_key']]));

        $this->aplimo = new aplimo(false);
        $this->aplimo->nome = $appData['nome'];

        $page = [];
        if(!isset($_GET['cpg'])) $_GET['cpg'] = false;

        $menuRender = function($mark, $key, $datas) use (&$menuRender, &$_ll, &$page){
            if(isset($datas['pages'])){
                $filhos = [];
                foreach($datas['pages'] as $k => $d)
                    if(!!($fil = $menuRender(((empty($mark))? '': $mark . '>') . $k, $k, $d))) $filhos[] = $fil;
                return $this->aplimo->menuGrupo($key, $datas['label'], $filhos);
            }else{
                if(!isset($datas['form'])) return false;
                if(!$_GET['cpg']) $_GET['cpg'] = $mark;
                if($_GET['cpg'] == $mark) $page = $datas;
                return $this->aplimo->menuItem($mark, ['nome' => $datas['label'], 'href' => $_ll[$_ll['operation_type']]['home'] . '&cpg=' . $mark, 'mark' => ['cpg' => $mark]]);
            }
        };

        $menu = [];
        foreach($this->menu() as $k => $m)
            $menu[] = $menuRender(($k . ':'), $k, $m);
        $this->aplimo->menuNovo($menu);

        $this->aplimo->header();

        $this->usrContent = $this($_GET['cpg']);
        $this->usrContent->header();

        $this->aplimo->hdMenuLeft([
            $this->aplimo->hdMenuTitle($page['label'])
        ]);

        $this->aplimo->hdMenuRigth([
            $this->aplimo->hdMenuA('Novo', $_ll[$_ll['operation_type']]['home'] . '&cpg=' . $_GET['cpg'] . '&new'),
        ]);
    }

    public function monta(){
        $this->aplimo->monta(function(){
            $this->usrContent->monta();
        });
    }

    public function onserver(){
        $this->usrContent->onserver();
    }

    public function onclient(){
        $this->usrContent->onclient();
    }

}