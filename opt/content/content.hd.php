<?php
use LliurePanel\ll;
use Persona\Persona;
use Opt\Instalilo\Instalilo;

global $_ll, $inContent, $aplimo, $content;

$inContent = $_ll['operation_key'] == 'content';

\Api\Fileup\Fileup::build();
\Api\Vigile\Vigile::build();

ll::opt('instalilo');
ll::api('aplimo');

ll::opt('content');
ll::opt('instalilo');

\Bootstrap\Bootstrap::build();
\FontAwesome\FontAwesome::build();
\GridSystem\GridSystem::build();
\GoogleBlockly\GoogleBlockly::build();

Persona::style(__DIR__ . '/content.css');


if($inContent){

    $instalilo = new Instalilo();

    $aplimo = new aplimo(false);
    $aplimo->nome = 'Content';

    $menu = [];
    foreach($instalilo->get(['pasta' => 'opt=content', 'chave:!=' => 'content']) as $app){
        $menu[] = $aplimo->menuGrupo($app['chave'], $app['nome'], [
            $aplimo->menuItem('forms-' . $app['chave'], [
                'nome' => 'Formularios',
                'href' => $_ll[$_ll['operation_type']]['home'] . '&apm=ctt>forms&fm=forms-' . $app['chave'] . '&ap=' . $app['chave'],
                'mark' => ['fm' => 'forms-' . $app['chave']]
            ]),
            $aplimo->menuItem('menus-' . $app['chave'], [
                'nome' => 'Menus',
                'href' => $_ll[$_ll['operation_type']]['home'] . '&apm=ctt>menus&fm=menus-' . $app['chave'] . '&ap=' . $app['chave'],
                'mark' => ['fm' => 'menus-' . $app['chave']]
            ])
        ]);
    }

    $aplimo->menuNovo($m = [
        $aplimo->menuGrupo('ctt', 'Content', [
            $aplimo->menuItem('apps', ['nome' => 'Apps', 'home' => 'ctt>apps'])
        ]),
        $aplimo->menuGrupo($_ll['operation_type'], 'Aplicativos', $menu)
    ]);

    $aplimo->header();

}else{

    $content = new content(["application" => $_ll['operation_key']]);
    $content->header();
}