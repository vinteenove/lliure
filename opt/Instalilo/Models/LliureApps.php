<?php

namespace Opt\Instalilo\Models;

use LliureCore\Model;

class LliureApps extends Model
{
    protected static $primaryKey = 'id';
    protected static ?string $table = 'lliure_apps';

}