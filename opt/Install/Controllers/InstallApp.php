<?php

namespace Opt\Install\Controllers;

use LliurePanel\ll;
use Opt\Install\Install;
use Opt\Install\LoadIni;

class InstallApp {

	/** @Route("/install/app/{appName:alphanum_dash}", methods={"GET"}) */
	public static function main($request, array $vars): string{
		try{
			[$appName, $appInstallFile] = self::validApp($vars);

			$ini = new LoadIni($appInstallFile);
			$appConfig = $ini->config();

			return Install::engine()->render('InstallApp/main', [
				'appName' => $appName,
				'appConfig' => $appConfig
			]);
		}catch(\Exception $e){
			return Install::engine()->render('InstallApp/error', ['error' => $e->getMessage()]);
		}
	}

	/** @Route("/install/app/{appName:alphanum_dash}", methods={"POST"}) */
	public static function install($request, array $vars){
		try{
			[$appName, $appInstallFile] = self::validApp($vars);

			define("PATH_BASE", ll::baseDir());
			define("PATH_ETC", PATH_BASE . 'etc/');
			define("PATH_APP", PATH_BASE . 'app/');
			define("PATH_UPLOAD", PATH_BASE . '../uploads/');

			define("DATABASE_HOSTNAME", $GLOBALS['hostname_conexao']);
			define("DATABASE_USERNAME", $GLOBALS['username_conexao']);
			define("DATABASE_PASSWORD", $GLOBALS['password_conexao']);
			define("DATABASE_BASENAME", $GLOBALS['banco_conexao']);
			define("DATABASE_PREFIX", PREFIXO);

			$install = new Install([$appInstallFile => '']);
			$status = $install->installing();
			$install->buildVersionsFile("; Este arquivo contem as versões do componentes instalados pelo sistema\n; não altere este arquivo se não souber o que estiver fazendo");

			return Install::engine()->render('InstallApp/result', [
				'status' => $status,
				'installLog' => $install->getInstallLog()
			]);
		}catch(\Exception $e){
			return Install::engine()->render('InstallApp/error', ['error' => $e->getMessage()]);
		}
	}

	private static function validApp($vars): array{
		$appName = $vars['appName'] ?? false;

		if($appName === false){
			throw new \Exception('appName visio');
		}

		$appPath = ll::baseDir() . 'app/' . $appName;

		if(!file_exists($appPath)){
			throw new \Exception('App não localizado');
		}

		$appInstallFile = "{$appPath}/lliure.ini";

		if(!file_exists($appInstallFile)){
			throw new \Exception('Arquivo de instalação do app não localizado');
		}

		return [$appName, $appInstallFile];
	}

}