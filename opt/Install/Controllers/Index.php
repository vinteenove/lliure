<?php

namespace Opt\Install\Controllers;

use DB\DB;
use Helpers\HttpHelper;
use LliureCore\Controller;
use LliurePanel\ll;
use Opt\Install\Install;

class Index implements Controller{

    /** @Route("/install", methods={"GET"}) */
    public static function main(): string{
        return Install::engine()->render('index/main');
    }

    /** @Route("/install/isInstall", methods={"POST"}) */
    public static function isInstall(): array{
        return self::statusReturn(ll::$data->install, []);
    }

    /** @Route("/install/folders", methods={"POST"}) */
    public static function folders(): array{

        if(ll::$data->install) return self::statusReturn(false, [['success' => 'O lliure já foi instalado!']]);

        $folders = [
            'etc' => ll::baseDir() . 'etc',
            '../uploads' => ll::baseDir() . '../uploads',
        ];

        $error = false;
        $result = [];

        foreach($folders as $name => $folder){
            if(!file_exists($folder)){
                $error = true;
                $result[] = [ 'danger' =>
                    "<p><strong>Pasta " . $name . "</strong><br></p>".
                    "<i class='fa fa-square' aria-hidden='true'></i> Pasta criada<br>".
                    "<i class='fa fa-square' aria-hidden='true'></i> Permissões necessária".
                    "<hr>".
                    "<p>Crie a pasta <strong>" . $folder . "</strong> e libere ela para leitura e escrita.</p>"
                ];
            } else if(!is_writeable($folder)){
                $error = true;
                $result[] = ['danger' =>
                    "<strong>Pasta " . $name . "</strong><br>".
                    "<i class='fa fa-check-square' aria-hidden='true'></i> Pasta criada<br>".
                    "<i class='fa fa-square' aria-hidden='true'></i> Permissões necessária<br>".
                    "<hr>".
                    "<p>Libere a pasta <strong>" . $folder . "</strong> para leitura e escrita.</p>"
                ];
            } else {
                $result[] = [ 'success' =>
                    "<strong>Pasta " . $name . "</strong><br>".
                    "<i class='fa fa-check-square' aria-hidden='true'></i> Pasta criada<br>".
                    "<i class='fa fa-check-square' aria-hidden='true'></i> Permissões necessária<br>"
                ];
            }
        }

        return self::statusReturn($error, $result);
    }

    /** @Route("/install/databaseAccess", methods={"POST"}) */
    public static function databaseAccess(): array{
        if(ll::$data->install) return self::statusReturn(false, [['success' => 'O lliure já foi instalado!']]);

        if(!Install::testConnection(
            $_POST['host'],
            $_POST['login'],
            $_POST['senha'],
            $_POST['tabela'],
            $_POST['prefixo'],
        )) return self::statusReturn(true, [['danger' => '<strong>DATABASE ERROR:</strong> Erro na conexão com o banco de dados']]);

        return self::statusReturn(false, []);
    }

    /** @Route("/install/installing", methods={"POST"}) */
    public static function installing(): array{

        if(Install::isInstall()) return self::statusReturn(false, Install::getLog());

        if(empty($_POST)) return self::statusReturn(true, [['danger' => 'Erro em sua requisição']]);

        if(!Install::validParens(
            $_POST['host'],
            $_POST['login'],
            $_POST['tabela'],
            $_POST['prefixo'],
            $_POST['user']
        )) return self::statusReturn(true, Install::getLog());

        $GLOBALS['hostname_conexao'] = $_POST['host'];
        $GLOBALS['username_conexao'] = $_POST['login'];
        $GLOBALS['password_conexao'] = $_POST['senha'];
        $GLOBALS['banco_conexao'] = DB::antiInjection($_POST['tabela']);

        define("PATH_BASE", ll::baseDir());
        define("PATH_ETC", PATH_BASE . 'etc/');
        define("PATH_APP", PATH_BASE . 'app/');
        define("PATH_UPLOAD", PATH_BASE . '../uploads/');

        define("DATABASE_HOSTNAME", $GLOBALS['hostname_conexao']);
        define("DATABASE_USERNAME", $GLOBALS['username_conexao']);
        define("DATABASE_PASSWORD", $GLOBALS['password_conexao']);
        define("DATABASE_BASENAME", $GLOBALS['banco_conexao']);
        define("DATABASE_PREFIX", $_POST['prefixo']);

        try{
            Install::testConnection(
                $GLOBALS['hostname_conexao'],
                $GLOBALS['username_conexao'],
                $GLOBALS['password_conexao'],
                $GLOBALS['banco_conexao'],
                $_POST['prefixo'],
            );
        }catch(\Exception $e){
            return self::statusReturn(true, [['danger' => '<strong>DATABASE ERROR: </strong>' . $e->getMessage()]]);
        }

        $filesIni = array_flip([
            ...glob(HttpHelper::path(ll::baseDir() . 'usr/*/lliure.ini')),
            ...glob(HttpHelper::path(ll::baseDir() . 'vendor/*/*/lliure.ini')),
            ...glob(HttpHelper::path(ll::baseDir() . 'opt/*/lliure.ini')),
            ...glob(HttpHelper::path(ll::baseDir() . 'app/*/lliure.ini')),
        ]);

        $install = new Install($filesIni);
        $status = $install->installing();
        $install->defaultUser($_POST['user']['login'], $_POST['user']['senha']);
        $install->buildVersionsFile("; Este arquivo contem as versões do componentes instalados pelo sistema\n; não altere este arquivo se não souber o que estiver fazendo");

        return self::statusReturn(!$status, $install->getInstallLog());
    }

    public static function installingTeste(){
        $_POST = [
            'host' => 'localhost',
            'login' => 'root',
            'senha' => '',
            'tabela' => 'lliure_alpha',
            'prefixo' => 'll_',
            'user' => [
                'login' => 'dev',
                'senha' => 'dev12345',
            ],
        ];

        return self::installing();
    }

    public static function testeIni(){

        /*$_SESSION['ll']['user']['newsmade'] = [
            'editPost',
            'editGaleria'
        ];*/

        echo '<pre>';
        var_dump(ll::$data->user);
        echo '</pre>';

        /*$file = (__DIR__ . '/../Install.instructions.ini');

        //$parsedIni = parse_ini_file($file, true, INI_SCANNER_TYPED);

        $install = new Install($file);

        echo '<pre>';
        var_dump($install);
        echo '</pre>';*/

        return '';
    }


    private static function statusReturn(bool $error, array $messages = []): array{
        return ['error' => $error, 'messages' => $messages];
    }

}