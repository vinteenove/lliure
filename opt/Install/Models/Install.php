<?php

namespace Opt\Install\Models;

use dibi;
use LliureCore\Model;

class Install extends Model{

    public static function connection(string $hostname, string $username, string $password, string $basename, string $prefix): bool{
        try{
            dibi::connect([
                'driver'   => 'pdo',
                'username' => $username,
                'password' => $password,
                'charset'  => 'utf8',
                'dsn'      => 'mysql:host=' . $hostname . ';dbname=' . $basename,
            ], 'pdo:' . $hostname . '@' . $username . '/' . $basename);
            self::setPrefix((empty($prefix) || $prefix == Model::getPrefix()? Model::getPrefix(): $prefix));
            return true;
        }catch(\Dibi\Exception $e){
            return false;
        }
    }

    public static function exec($sql): \Dibi\Result{
        return dibi::query($sql);
    }

    public static function getAffectedRows(): int{
        return dibi::getAffectedRows();
    }

}