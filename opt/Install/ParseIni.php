<?php


namespace Opt\Install;

use function \asort;
use function \arsort;
use function \ksort;
use function \krsort;
use function \natsort;
use function \natcasesort;
use function \rsort;
use function \sort;
use function \usort;
use function \uasort;
use function \uksort;

class ParseIni implements
    \ArrayAccess
{

    private array $data = [];

    private function __construct(array $array){
        $this->data = static::extendedIni($array);
    }

    public static function file(string $filename, bool $process_sections = true, int $scanner_mode = INI_SCANNER_TYPED): ParseIni{
        return static::string(file_get_contents($filename), $process_sections, $scanner_mode);
    }

    public static function string(string $ini_string, bool $process_sections = true, int $scanner_mode = INI_SCANNER_TYPED): ParseIni{
        return static::array(parse_ini_string($ini_string, $process_sections, $scanner_mode));
    }

    public static function array(array $array): ParseIni{
        return new static($array);
    }

    public function implode(): string{
        $output = '';
        foreach($this->data as $name => $data){
            $implodedData = static::implodeDots($data);
            $output .= ((!$output)? "": "\n\n") . "[ {$name} ]";
            foreach($implodedData as $label => $valeu){
                if(preg_match('/[?{}|&~!()^"\n]/m', $valeu)){
                    $valeu = '"' . addslashes($valeu) . '"';
                }
                $output .= "\n{$label} = {$valeu}";
            }
        }
        return $output;
    }

    public function save(string $filename){
        return file_put_contents($filename, $this->implode());
    }

    public function __toString(): string{
        return $this->implode();
    }

    public function toArray(): array{
        return $this->data;
    }

    public function offsetExists($offset): bool{
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset){
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value){
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset){
        unset($this->data[$offset]);
    }


    private static function extendedIni(array $parsedIni): array{
        $newIni = [];
        foreach($parsedIni as $namespace => $properties){
            $current = [];
            $extends = explode(':', $namespace);
            array_walk($extends, fn(&$i) => ($i = trim($i)));
            $extends = array_reverse($extends);

            foreach($extends as $reference){
                $referenceData = $newIni[$reference] ?? [];
                $current = array_replace_recursive($referenceData, $current, $referenceData);
            }

            $properties = static::explodeDots($properties);
            $newIni[$reference] = array_replace_recursive($properties, $current, $properties);
        }
        return $newIni;
    }

    private static function explodeDots($properties): array{
        $newProperties = [];
        foreach($properties as $index => $value){
            $data = [];
            $current =& $data;

            foreach(explode('.', $index) as $key){
                $current[$key] = [];
                $current =& $current[$key];
            }

            $current = $value;
            $newProperties = array_replace_recursive($newProperties, $data);
        }
        return $newProperties;
    }

    private static function implodeDots(array $array): array{
        $itens = [];
        foreach($array as $l1l => $l1v){
            if (is_array($l1v)){
                foreach($l1v as $l2l => $l2v){
                    if (is_array($l2v)){
                        self::implodeDotsRecursive($itens, "{$l1l}.{$l2l}", $l2v);
                    }else{
                        $itens["{$l1l}[{$l2l}]"] = $l2v;
                    }
                }
            }else{
                $itens[$l1l] = $l1v;
            }
        }
        return $itens;
    }

    private static function implodeDotsRecursive(array &$itens, string $label, $value){
        if (is_array($value)){
            foreach($value as $index => $item){
                self::implodeDotsRecursive($itens, "{$label}.{$index}", $item);
            }
        }else{
            $itens[$label] = $value;
        }
    }


    public function asort(){
        asort($this->data);
    }

    public function arsort(){
        arsort($this->data);
    }

    public function ksort(){
        ksort($this->data);
    }

    public function krsort(){
        krsort($this->data);
    }

    public function natsort(){
        natsort($this->data);
    }

    public function natcasesort(){
        natcasesort($this->data);
    }

    public function rsort(){
        rsort($this->data);
    }

    public function sort(){
        sort($this->data);
    }

    public function usort($cmp_function){
        usort($this->data, $cmp_function);
    }

    public function uasort($cmp_function){
        uasort($this->data, $cmp_function);
    }

    public function uksort($cmp_function){
        uksort($this->data, $cmp_function);
    }
}