<?php

namespace Opt\Install;

use Helpers\HttpHelper;
use Leitor_sql\Leitor_sql;
use Opt\Install\Models\Install as InstallModel;

class LoadIni{

    private string $ini;
    private array $config;
    private array $files = [];
    private string $basePath;
    private string $classMethods;
    private array $log = [];

    public function __construct(string $fileIni, string $classMethods = fileMethods::class){

        $this->ini = HttpHelper::path($fileIni);
        $this->basePath = dirname($this->ini) . DIRECTORY_SEPARATOR;
        $this->classMethods = $classMethods;

        $parsedIni = parse_ini_file($this->ini, true, INI_SCANNER_TYPED);

        if($parsedIni === false){
            throw new \Exception('Failed to open file');
        }

        $parsedIni = self::extendedIni($parsedIni);

        $this->config = $parsedIni['config'] ?? [];
        unset($parsedIni['config']);

        $methodsValid = array_flip(get_class_methods($classMethods));
        unset($methodsValid['__construct']);

        foreach($parsedIni as $nameFile => $functions){
            if(file_exists($fileNameComplete = HttpHelper::path($this->basePath . $nameFile))){
                $functions = array_intersect_key(array_merge($methodsValid, $functions), $methodsValid, $functions);
                $this->files[$fileNameComplete] = $functions;
            }
        }
    }

    public function installing(){
        $filesSql = $this->config['sql'] ?? [];

        if(!empty($filesSql)){
            self::normalizeParam($filesSql);
            $this->execSql($filesSql);
        }

        $this->processFiles();
    }

    public function getClearLog(): array{
        $log = $this->log;
        $this->log = [];
        return $log;
    }

    public function config(): array{
        return $this->config;
    }

    public function basePaht(): string{
        return $this->basePath;
    }


    private function setLog(string $status, string $message){
        $this->log[] = [$status => $message];
    }

    private function execSql(array $filesSql){
        foreach($filesSql as $fileName){
            if(file_exists(($filePath = $this->basePath . $fileName))){
                $sqlLog = new Leitor_sql($this->basePath . $fileName, 'll_', DATABASE_PREFIX);

                foreach($sqlLog->getMsgs() as $line){
                    foreach($line as $status => $msg){
                        $this->setLog($status, $msg);
                    }
                }

                /*$fileContent = file_get_contents($this->basePath . $fileName);
                try{
                    InstallModel::exec($fileContent);
                    $this->setLog('success', (($this->config['name'] ?? '--') . ' SQL ' . basename($filePath)));
                }catch(\Dibi\Exception $exception){
                    $this->setLog('error', (($this->config['name'] ?? '--') . ' SQL ' . basename($filePath)));
                }*/
            }
        }
    }

    private function processFiles(){
        foreach($this->files as $fileName => $commands){
            if(file_exists($fileName)){
                $fileContent = file_get_contents($fileName);
                $this->execCommandsFile($commands, new $this->classMethods($fileContent, $this->basePath, $this, fn(string $status, string $message) => $this->setLog($status, $message)));
            }
        }
    }

    /**
     * @param string|string[] $commands
     * @param                 $installFile
     */
    private function execCommandsFile($commands, $installFile){
        foreach($commands as $command => $params){
            $installFile->{$command}($params);
        }
    }

    private static function extendedIni(array $parsedIni): array{
        $newIni = [];
        foreach($parsedIni as $namespace => $properties){
            $current = [];
            $extends = explode(':', $namespace);
            array_walk($extends, fn(&$i) => ($i = trim($i)));
            $extends = array_reverse($extends);

            foreach($extends as $reference){
                $referenceData = $newIni[$reference] ?? [];
                $current = array_replace_recursive($referenceData, $current, $referenceData);
            }

            $properties = self::explodeDots($properties);
            $newIni[$reference] = array_replace_recursive($properties, $current, $properties);
        }
        return $newIni;
    }

    private static function explodeDots($properties): array{
        $newProperties = [];
        foreach($properties as $index => $value){
            $data = [];
            $current =& $data;

            foreach(explode('.', $index) as $key){
                $current[$key] = [];
                $current =& $current[$key];
            }

            $current = $value;
            $newProperties = array_replace_recursive($newProperties, $data);
        }
        return $newProperties;
    }

    /**
     * @param string|string[] &$params
     */
    private static function normalizeParam(&$params){
        if(!is_array($params) && is_string($params)){
            $params = [$params];
        }
    }


    public static function mkDirRecursive(string $dirname): bool{
        if(file_exists($dirname)){
            return true;
        }

        $path = dirname($dirname);
        if(!self::mkDirRecursive($path)){
            return false;
        }
        return mkdir($dirname);
    }


}