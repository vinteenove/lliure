<div class="modal-header">
	<h5 class="modal-title">Instalando aplicativo</h5>
</div>
<div class="modal-body">
	<div class="alert alert-danger" role="alert">
		<?php echo $error; ?>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
</div>