<form action="install/app/<?php echo $appName; ?>" class="jfbox" method="post">
	<div class="modal-header">
		<h5 class="modal-title">Instalando aplicativo</h5>
	</div>

	<div class="modal-body">
        <div class="form-group m-0">
            <label for="FormInstalInputNome">Nome</label>
            <input type="text" name="nome" class="form-control" id="FormInstalInputNome" placeholder="<?php echo $appConfig['name'] ?? ''; ?>">
        </div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
		<button type="submit" class="btn btn-primary">Instalar</button>
	</div>
</form>