<div class="modal-header">
	<h5 class="modal-title">Instalando aplicativo</h5>
</div>
<div class="modal-body pb-0">
    <?php foreach($installLog as $log) foreach($log as $logStatus => $logMessage){ ?>
        <div class="alert alert-<?php echo $logStatus; ?>" role="alert">
            <?php echo $logMessage; ?>
        </div>
    <?php } ?>
    <?php if($status){ ?>
        <div class="alert alert-success" role="alert">
            APP instalado
        </div>
    <?php }else{ ?>
        <div class="alert alert-danger" role="alert">
            Erro ao instalar APP
        </div>
    <?php } ?>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
</div>

<?php if($status){ ?>
    <script>
        jfBox.ready(function (modal) {
            modal.always(function () {
                window.location.reload();
            });
        });
    </script>
<?php } ?>
