<?php

/**
 * lliure WAP
 *
 * @Versão 8.x
 * @Pacote lliure
 * @Entre em contato com o desenvolvedor <lliure@lliure.com.br> http://www.lliure.com.br/
 * @Licença http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

use LliurePanel\ll;
use Persona\Persona;
use GridSystem\GridSystem;
use FontAwesome\FontAwesome;
use Bootstrap\Bootstrap;

Bootstrap::build();
Persona::bootstrap();
GridSystem::build();
FontAwesome::build();

?>

<div class="container">
    <div class="row">
        <div class="sm-5-7 sm-offset-1-7 md-5-9 md-offset-2-9 lg-1-2 lg-offset-1-4">
            <div id="loginBox">

                <br><br>
                <div class="text-center">
                    <div id="lliurelogo" style=" width: 180px; max-width: 100%; "><?php require Persona::$theme->path. 'layout/logo.svg'; ?></div>
                </div>
                <br><br>

                <h1>Instalação</h1>

                <div id="carousel-install-steps" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false" data-touch="false" data-keyboard="false">
                    <div class="carousel-inner" role="listbox">

                        <!-- 0 Load -->
                        <div class="carousel-item active">
                            <div class="text-center">
                                <br>
                                <div>
                                    <i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i>
                                </div>
                                <br>
                            </div>
                        </div>

                        <!-- 1 Instaled -->
                        <div class="carousel-item">
                            <h2>Etapa 0 - Já instalado</h2>
                        </div>

                        <!-- 2 Start -->
                        <div class="carousel-item">

                            <h2><small style="display: block;"><small><small class="text-muted">Introdução</small></small></small>Vamos começar</h2>

                            <p>
                                Olá, obrigado por escolher nosso sistema, essa ferramenta irá te auxiliar na criação de
                                pastas e configurações necessárias para o funcionamento do sistema, tenha cuidado
                                nas próximas etapas, mas não se preocupe, como desenvolvedor você pdoerá alterar qualquer configuração realizada.
                            </p>

                            <div class="form-group text-right">
                                <button type="button" class="btn btn-lliure" data-carousel="3">Próximo</button>
                            </div>
                        </div>

                        <!-- 3 Folders -->
                        <div class="carousel-item">

                            <h2><small style="display: block;"><small><small class="text-muted">Etapa 1</small></small></small>Pastas</h2>

                            <p>
                                O sistema precisará de pastas em determinados locais com configurações de leitura e escrita específicas,
                                verificaremos isso para você, caso alguma divergência for encontrada instruções de como resolver lhe serão sugeridas.
                            </p>

                            <div id="result-install-folders">
                                <div class="text-center">
                                    <br>
                                    <div>
                                        <i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i>
                                    </div>
                                    <br>
                                </div>
                            </div>

                            <div class="form-group text-right">
                                <button type="button" class="btn btn-light" data-function="etapa3">Atualizar</button>
                                <button id="btn-install-folders" type="button" class="btn btn-lliure" data-carousel="4" disabled>Próximo</button>
                            </div>

                        </div>

                        <!-- 4 Data Base -->
                        <div class="carousel-item">

                            <h2><small style="display: block;"><small><small class="text-muted">Etapa 2</small></small></small>Banco de dados</h2>

                            <p>
                                Essas configurações são de acesso ao banco de dados, você precisar ter já ter criado o banco antes de cadastrar o acesso aqui.
                            </p>

                            <form id="form-install-database">
                                <div class="form-group">
                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <label for="formInstalInputHost" class="col-form-label">Host</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" name="host" class="form-control" id="formInstalInputHost" placeholder="EXP: site.com ou 192.168.0.1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="formInstalInputLogin" class="col-form-label">Login</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" name="login" class="form-control" id="formInstalInputLogin" placeholder="EXP: root" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="formInstalInputSenha" class="col-form-label">Senha</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="password" name="senha" class="form-control" id="formInstalInputSenha">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="formInstalInputTabela" class="col-form-label">Banco de dados</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" name="tabela" class="form-control" id="formInstalInputTabela" placeholder="EXP: lliure" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="formInstalInputPrefixo" class="col-form-label">Prefixo</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" name="prefixo" class="form-control" id="formInstalInputPrefixo" value="ll_" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="form-text">Caso tenha outras tabelas cadastradas em seu banco o prefixo te ajudará a separar as que o sistema vai usar das que ja existe no seu banco.</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="return-install-database">

                                </div>

                                <div class="form-group text-right">
                                    <button id="btn-install-db" type="submit" class="btn btn-lliure">Próximo</button>
                                </div>
                            </form>

                        </div>

                        <!-- 5 User -->
                        <div class="carousel-item">

                            <h2><small style="display: block;"><small><small class="text-muted">Etapa 3</small></small></small>Primeiro usuário</h2>

                            <p>
                                Nesse ponto você define o primeiro usuário do sistema,
                                ele terá permições de Desenvolvedor e tera acesso a todos as funções do
                                sistema, tome muito cuidado nesta etapa ela é crucial para segurança do
                                seu sistema, escolha uma senha forte com letras maiúsculas e minúsculas
                                números e símbolos e com no mínimo 8 caracteres, não coloque datas e nem palavras
                                conhecidas, e caso queira aumentar mais a força de sua senha utilize no mínimo
                                16 caracteres. Seguindo esses recomendações sua senha sera forte e a chance
                                de alguém mau intencionado descobrir sua senha diminui bastante.
                            </p>

                            <form id="formInstallUserDev">
                                <h2>Usuário Desenvolvedor</h2>
                                <div class="form-group">
                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <label for="formInstallUserDevInputLogin" class="col-form-label">Login</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" name="user[login]" class="form-control" id="formInstallUserDevInputLogin" placeholder="EXP: root">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row align-items-center">
                                        <div class="col-sm-3">
                                            <label for="formInstallUserDevInputSenha" class="col-form-label">Senha</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="password" name="user[senha]" class="form-control" id="formInstallUserDevInputSenha">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group" id="return-install-userdev">

                                </div>

                                <div class="form-group text-right">
                                    <button id="btn-install-userdev" type="submit" class="btn btn-lliure">Próximo</button>
                                </div>
                            </form>

                        </div>

                        <!-- 6 Installing -->
                        <div class="carousel-item">

                            <h2><small style="display: block;"><small><small class="text-muted">Etapa 4</small></small></small>Instalando</h2>

                            <div class="form-group" id="return-install-installing">
                                <br>
                                <div>
                                    <i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i>
                                </div>
                                <br>
                            </div>

                            <div class="form-group text-right">
                                <button id="btn-installing-folders" type="button" class="btn btn-light" data-carousel="3" disabled>Pastas</button>
                                <button id="btn-installing-basedata" type="button" class="btn btn-light" data-carousel="4" disabled>Banco de dados</button>
                                <button id="btn-installing-userdev" type="button" class="btn btn-light" data-carousel="5" disabled>Usuário</button>
                                <button id="btn-installing-thanks" type="button" class="btn btn-lliure" data-carousel="7" disabled>Encerrar</button>
                            </div>
                        </div>

                        <!-- 7 Thanks -->
                        <div class="carousel-item">

                            <h2><small style="display: block;"><small><small class="text-muted">Etapa 5</small></small></small>Agradecimento</h2>

                            <p>
                                Obrigado por escolher o lliure,
                                ele esta pronto para você aproveitar ao máximo todos recursos, esperamos
                                que lhe seja de muito proveitoso.
                            </p>

                            <div class="form-group text-right">
                                <a href="" class="btn btn-lliure">Login</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    ($=>$(()=>{

        const EtapaInitial = 2;
        const EtapaUser = 5;
        const EtapaInstalling = 6;
        const EtapaThanks = 7;

        const $carousel = $('#carousel-install-steps');
        const $resultInstallFolders = $('#result-install-folders');
        const $btnInstallFolders = $('#btn-install-folders');
        const $formInstallDatabase = $('#form-install-database');
        const $returnInstallDatabase = $('#return-install-database');

        const $formInstallInputHost = $('#formInstalInputHost');
        const $formInstallInputLogin = $('#formInstalInputLogin');
        const $formInstallInputTable = $('#formInstalInputTabela');
        const $formInstallInputPrefixo = $('#formInstalInputPrefixo');

        const $formInstallUserDev = $('#formInstallUserDev');
        const $returnInstallUserDev = $('#return-install-userdev');
        const $formInstallUserDevInputLogin = $('#formInstallUserDevInputLogin');
        const $formInstallUserDevInputSenha = $('#formInstallUserDevInputSenha');

        const $returnInstallInstalling = $('#return-install-installing');
        const $btnInstallingFolders = $('#btn-installing-folders');
        const $btnInstallingBasedata = $('#btn-installing-basedata');
        const $btnInstallingUserDev = $('#btn-installing-userdev');
        const $btnInstallingThanks = $('#btn-installing-thanks');

        $formInstallInputHost.on('input', function (){ validEmptyField($(this))});
        $formInstallInputLogin.on('input', function (){ validEmptyField($(this))});
        $formInstallInputTable.on('input', function (){ validEmptyField($(this))});
        $formInstallInputPrefixo.on('input', function (){ validEmptyField($(this))});

        $formInstallUserDevInputLogin.on('input', function (){ validEmptyField($(this)); validMinLengthField($(this), 3)});
        $formInstallUserDevInputSenha.on('input', function (){ validEmptyField($(this)); validPasswordField($(this)); validMinLengthField($(this), 8) });

        const etapas = {};

        $carousel.on('slid.bs.carousel', function (e){
            if(!!etapas['etapa' + e.to]) etapas['etapa' + e.to]();
        });

        $formInstallDatabase.on('submit', function (e){
            etapas.etapaDB();
            e.stopPropagation();
            e.preventDefault();
            return false;
        });

        $formInstallUserDev.on('submit', function (e){
            etapas.etapaUser();
            e.stopPropagation();
            e.preventDefault();
            return false;
        });

        $('[data-carousel]').click(function (){
            const $this = $(this);
            if($this.is(':disabled')) return;

            const slider = parseInt($this.attr('data-carousel'));
            $carousel.carousel(slider);
        });

        $('[data-function]').click(function (){
            const $this = $(this);
            if($this.is(':disabled')) return;

            const func = $this.attr('data-function');
            etapas[func]();
        });

        function validEmptyField($filed){
            if(!$filed[0].validStart) return;

            const value = $filed.val();
            const result = !!value.length;
            $filed.toggleClass('is-invalid', !result);

            return result;
        }

        function validPasswordField($filed){
            if(!$filed[0].validStart) return;

            const value = $filed.val();
            const letras = value.replace(/[^a-z]/gmi, '');
            const numbers = value.replace(/[^\d]/gmi, '');
            const result = (!!letras.length && !!numbers.length);
            $filed.toggleClass('is-invalid', result);

            return result;
        }

        function validMinLengthField($filed, min){
            if(!$filed[0].validStart) return;

            min = Math.max(min, 0);
            const value = $filed.val();
            const result = value.length >= min;
            $filed.toggleClass('is-invalid', !result);

            return result;
        }

        function validStart($filed){
            $filed[0].validStart = true;
        }

        function dumpMessages($where, messages){
            for (const msg of messages) {
                $where.append(buildMessage(msg));
            }
        }

        function buildMessage(msg){
            for (const type in msg){
                console.log(type);
                const text = msg[type];
                return '<div class="alert alert-' + type + '" role="alert">' + text + '</div>'
            }
        }

        function createMessage(type, text){
            return JSON.parse('[{ "' + type + '" : "' + text + '"}]');
        }

        etapas.etapa0 = async function(){
            fetch('<?php echo substr(ll::pipeline()->getNamedRoute('install.isInstall')->getQuery(), 1); ?>', {method: 'POST'}).then(r => r.blob()).then(b => b.text()).then(function(text){

                const {error: isInstall} = JSON.parse(text);

                if(isInstall){
                    $carousel.carousel(EtapaThanks);
                }else{
                    $carousel.carousel(EtapaInitial);
                }

            });
        }

        etapas.etapa3 = async function(){

            $btnInstallFolders.prop('disabled', true);
            const {error, messages} = JSON.parse(await fetch('<?php echo substr(ll::pipeline()->getNamedRoute('install.folders')->getQuery(), 1); ?>', {method: 'POST'}).then(r => r.blob()).then(b => b.text()))

            $resultInstallFolders.html('');
            dumpMessages($resultInstallFolders, messages);

            if(!error){
                $btnInstallFolders.prop('disabled', false);
            }
        }

        etapas.etapaDB = async function(){
            validStart($formInstallInputHost);
            validStart($formInstallInputLogin);
            validStart($formInstallInputTable);
            validStart($formInstallInputPrefixo);

            let error = [];
            error.push(!validEmptyField($formInstallInputHost));
            error.push(!validEmptyField($formInstallInputLogin));
            error.push(!validEmptyField($formInstallInputTable));
            error.push(!validEmptyField($formInstallInputPrefixo));
            error = error.filter(r => !!r);

            if(!!error.length) return;

            const form = new FormData($formInstallDatabase[0]);
            const data = new URLSearchParams([...form]);

            $returnInstallDatabase.html('<br><div class="text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div><br>');
            const {error: err} = JSON.parse(await fetch('<?php echo substr(ll::pipeline()->getNamedRoute('install.databaseAccess')->getQuery(), 1); ?>', {method: 'POST', body: data}).then(r => r.blob()).then(b => b.text()));
            $returnInstallDatabase.html('');

            if(err){
                dumpMessages($returnInstallDatabase, createMessage('danger', 'Não foi possível se conectar  ao banco de dados com essas inforamções'));
            }else{
                $carousel.carousel(EtapaUser);
            }
        }

        etapas.etapaUser = async function(){
            validStart($formInstallUserDevInputLogin);
            validStart($formInstallUserDevInputSenha);

            let error = [];
            error.push(!validEmptyField($formInstallUserDevInputLogin));
            error.push(!validMinLengthField($formInstallUserDevInputLogin, 3));
            error.push(!validEmptyField($formInstallUserDevInputSenha));
            error.push(!validPasswordField($formInstallUserDevInputSenha));
            error.push(!validMinLengthField($formInstallUserDevInputSenha, 8));
            error = error.filter(r => !!r);


            $returnInstallUserDev.html('');
            if(!!error.length){
                dumpMessages($returnInstallUserDev, createMessage('danger', 'O login e senha informados não tem a segurança mínima necessária'));
            }else{
                $carousel.carousel(EtapaInstalling);
            }
        }

        etapas.etapa6 = async function(){
            $returnInstallInstalling.html('');
            $btnInstallingFolders.prop('disabled', true);
            $btnInstallingBasedata.prop('disabled', true);
            $btnInstallingUserDev.prop('disabled', true);
            $btnInstallingThanks.prop('disabled', true);

            validStart($formInstallInputHost);
            validStart($formInstallInputLogin);
            validStart($formInstallInputTable);
            validStart($formInstallInputPrefixo);
            validStart($formInstallUserDevInputLogin);
            validStart($formInstallUserDevInputSenha);

            let valid = [];

            //validate Database
            valid.push(!validEmptyField($formInstallInputHost));
            valid.push(!validEmptyField($formInstallInputLogin));
            valid.push(!validEmptyField($formInstallInputTable));
            valid.push(!validEmptyField($formInstallInputPrefixo));

            //validate login
            valid.push(!validEmptyField($formInstallUserDevInputLogin));
            valid.push(!validMinLengthField($formInstallUserDevInputLogin, 3));

            //validate password
            valid.push(!validEmptyField($formInstallUserDevInputSenha));
            valid.push(!validPasswordField($formInstallUserDevInputSenha));
            valid.push(!validMinLengthField($formInstallUserDevInputSenha, 8));

            // count errors
            valid = !(valid.filter(r => !!r).length);

            if(!valid){
                dumpMessages($returnInstallInstalling, createMessage('danger', 'Erro de validação dos dados para a instalação'));
                $btnInstallingFolders.prop('disabled', false);
                $btnInstallingBasedata.prop('disabled', false);
                $btnInstallingUserDev.prop('disabled', false);
                return;
            }

            const database = new FormData($formInstallDatabase[0]);
            const userDev = new FormData($formInstallUserDev[0]);
            const form = [...database, ...userDev];
            const data = new URLSearchParams(form);

            $returnInstallInstalling.html('');
            dumpMessages($returnInstallInstalling, createMessage('info', 'Iniciando instalação'));

            const {error, messages} = JSON.parse(await fetch('<?php echo substr(ll::pipeline()->getNamedRoute('install.installing')->getQuery(), 1); ?>', {method: 'POST', body: data}).then(r => r.blob()).then(b => b.text()));

            console.log(error, messages)

            dumpMessages($returnInstallInstalling, messages);

            if(error){
                dumpMessages($returnInstallInstalling, createMessage('danger', 'Erros impedirão a instalação correta do sistema, reveja a configurações e tente novamente'));
                $btnInstallingFolders.prop('disabled', false);
                $btnInstallingBasedata.prop('disabled', false);
                $btnInstallingUserDev.prop('disabled', false);
            }else{
                dumpMessages($returnInstallInstalling, createMessage('success', 'O sistema foi instalado corretamente'));
                $btnInstallingThanks.prop('disabled', false);
            }
        }

        etapas.etapa0();

    }))(jQuery);
</script>