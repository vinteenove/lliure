{
	"versao": "11 alpha",
	"idiomas": {
		"nativo": "pt_br"
	},
	"tema_default": "AdminLTE",
	"temas": {
		"persona": "usr\\\/AdminLTE\\\/"
	},
	"grupo": {
		"default": {
			"local": "default",
			"template": "persona",
			"color": "94324B",
			"execucao": "URL_AMIGAVEL",
			"home_wli": "opt=desktop",
			"home_nli": "opt=singin",
			"user_wli": "opt=singin"
		}
	}
}