<?php

global $hostname_conexao, $username_conexao, $password_conexao, $banco_conexao;

$hostname_conexao = "{{localhost}}";
$username_conexao = "{{root}}";
$password_conexao = "{{senha}}";
$banco_conexao = "{{banco}}";
define("PREFIXO", "{{prefixo}}");

try{
    dibi::connect([
        'driver'   => 'pdo',
        'username' => $username_conexao,
        'password' => $password_conexao,
        'charset'  => 'utf8',
        'dsn'      => 'mysql:host=' . $hostname_conexao . ';dbname=' . $banco_conexao,
    ], 'pdo:' . $hostname_conexao . '@' . $username_conexao . '/' . $banco_conexao);

    dibi::query("SET NAMES utf8");
}catch(\Dibi\Exception $e){
    die('DATABASE ERROR: ' . $e->getMessage());
}

\LliureCore\Model::setPrefix("{{prefixo}}");
