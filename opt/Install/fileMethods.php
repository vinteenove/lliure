<?php

namespace Opt\Install;

use Helpers\HttpHelper;

class fileMethods{

    private string $fileContent;
    private string $basePath;
    private LoadIni $loadIni;
    private \Closure $setLog;

    public function __construct(string &$fileContent, string $basePath, LoadIni $loadIni, \Closure $setLog){
        $this->fileContent = $fileContent;
        $this->basePath = $basePath;
        $this->loadIni = $loadIni;
        $this->setLog = $setLog;
    }

    public function replace(array $replace){
        $replaceFrom = array_keys($replace);
        $replaceTo = array_values($replace);
        array_walk($replaceFrom, fn(&$k) => ($k = "{{{$k}}}"));
        $replaceFrom[] = '\{\{';
        $replaceTo[] = '{{';
        $this->fileContent = str_replace($replaceFrom, $replaceTo, $this->fileContent);
    }

    public function moveTo(string $destiny): bool{
        $destiny = HttpHelper::path($destiny);

        $parsePart = parse_url($destiny);

        if(!(isset($parsePart['host']) || isset($parsePart['scheme']))){
            $destiny = HttpHelper::path($this->basePath . $destiny);
        }

        LoadIni::mkDirRecursive(dirname($destiny));

        if(file_exists($destiny)){
            $this->setLog->call($this->loadIni, 'warning', 'Arquivo já existe ' . basename($destiny));
            return false;
        }

        if(!file_put_contents($destiny, $this->fileContent)){
            $this->setLog->call($this->loadIni, 'danger', 'Erro ao criar ' . basename($destiny));
            return false;
        }

        $this->setLog->call($this->loadIni, 'success', 'Arquivo ' . basename($destiny) . ' criado');
        return true;
    }

}