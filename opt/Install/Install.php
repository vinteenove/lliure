<?php

namespace Opt\Install;

use Helpers\HttpHelper;
use League\Plates\Engine;
use LliurePanel\ll;
use LliurePanel\Middleware\NeedUserMiddleware;
use LliurePanel\Routable;
use LliurePanel\Strategy\JsonStrategy;
use Opt\Install\Controllers\Index;
use Opt\Install\Controllers\InstallApp;
use Opt\Singin\Models\Users;
use Persona\Persona;
use Router\RouteGroup;
use Router\Router;
use Opt\Install\Models\Install as InstallModel;
use Router\Strategy\StrategyStandard;
use Senha\Senha;

class Install implements Routable {

    public static function router(): Router{
        $router = new Router();

        $router->get('/', [Index::class, 'main']);
        $router->get('/testeIni', [Index::class, 'testeIni'])->middleware(new NeedUserMiddleware);

        $router->group('/', function(RouteGroup $router){
            $router->post('/isInstall', [Index::class, 'isInstall'])->setName('install.isInstall');
            $router->post('/folders', [Index::class, 'folders'])->setName('install.folders');
            $router->post('/databaseAccess', [Index::class, 'databaseAccess'])->setName('install.databaseAccess');
            $router->post('/installing', [Index::class, 'installing'])->setName('install.installing');
            $router->get('/installingTeste', [Index::class, 'installingTeste']);
        })->setStrategy(new JsonStrategy);

        $router->group('/app', function(RouteGroup $router){
			$router->get('/{appName:alphanum_dash}', [InstallApp::class, 'main'])->setStrategy(new StrategyStandard);
			$router->post('/{appName:alphanum_dash}', [InstallApp::class, 'install'])->setStrategy(new StrategyStandard);
        })->middleware(new NeedUserMiddleware);

        return $router;
    }

    public static function engine(): Engine{
        return new Engine(__DIR__ . '/Views/' . Persona::getTheme()::getThemerName());
    }

    /* @var $files LoadIni[]  */
    private array $files;
    private static array $log = [];

    public function __construct(array $filesIni){
        foreach($filesIni as $fileIni => $v){
            try{
                $file = new LoadIni($fileIni);
                $this->files[] = $file;
            }catch(\Exception $e){}
        }
    }

    public function installing(): bool{
        foreach($this->files as $fileIni => $loadIni){
            try{
                $loadIni->installing();
            }catch(\Exception $e){
                return false;
            }
        }
        return true;
    }

    public function defaultUser($login, $password){
        try{
            $user = new Users([
                'login' => $login,
                'hash' => Senha::create($password),
                'name' => 'Desenvolvedor',
                'group' => 'dev',
            ]);
            $user->insert();
            self::setLog('success', 'Primeiro usuário criado');
        }catch(\Exception $e){
            self::setLog('danger', 'Erro ao criar usuário');
        }
    }

    public function getInstallLog(): array{
        $log = [];
        foreach($this->files as $fileIni => $loadIni){
            $log = [...$log, ...$loadIni->getClearLog()];
        }
        return $log;
    }

    public static function testConnection(string $hostname, string $username, string $password, string $basename, string $prefix): bool{
        if(InstallModel::connection($hostname, $username, $password, $basename, $prefix)){
            static::setLog('success', 'Sucesso na conexão com o banco de dados');
            return true;
        }else{
            static::setLog('danger', 'Erro na conexão com o banco de dados');
            return false;
        }
    }

    public static function isInstall(): bool{
        if(ll::$data->install){
            self::setLog('success', 'O lliure já foi instalado!');
            return true;
        }
        return false;
    }

    public static function validParens(string $hostname, string $username, string $basename, string $prefix, array $user): bool{
        $erros = [];

        if(!self::validEmptyValue($hostname)) $erros[] = ['danger' => 'Seu host não pode ser vazio'];
        if(!self::validEmptyValue($username)) $erros[] = ['danger' => 'Seu login não pode ser vazio'];
        if(!self::validEmptyValue($basename)) $erros[] = ['danger' => 'Sua tabela não pode ser vazio'];
        if(!self::validEmptyValue($prefix)) $erros[] = ['danger' => 'Seu prefixo não pode ser vazio'];

        if(!self::validEmptyValue($user['login'])) $erros[] = ['danger' => 'O login do usuário não pode ser vazio'];
        if(!self::validMinLengthValeu($user['login'], 3)) $erros[] = ['danger' => 'O login do usuário tem de ter um tamanho mínimo de 3'];

        if(!self::validEmptyValue($user['senha'])) $erros[] = ['danger' => 'A senha do usuário não pode ser vazia'];
        if(!self::validMinLengthValeu($user['senha'], 8)) $erros[] = ['danger' => 'A senha do usuário tem de ter um tamanho mínimo de 8'];
        if(!self::validPasswordValue($user['senha'])) $erros[] = ['danger' => 'A senha do usuário de seguir as recomendações de criação de senha'];

        foreach($erros as $erro) foreach($erro as $type => $label) self::setLog($type, $label);

        return empty($erros);
    }

    private static function setLog(string $status, string $message){
        self::$log[] = [$status => $message];
    }

    public static function getLog(): array{
        return self::$log;
    }

    public function buildVersionsFile(string $introduction){

        $versionsFile = HttpHelper::path(ll::baseDir() . '/etc/installed.ini');
        $parsedVisionsFile = ParseIni::file($versionsFile);

        foreach($this->files as $fileIni => $loadIni){
            $version = $loadIni->config()['version'] ?? false;
            $name = $loadIni->config()['name'] ?? false;

            if($name !== false && $version !== false){
                $parsedVisionsFile[$name] = ['version' => $version];
            }
        }

        $parsedVisionsFile->uksort(fn($a, $b) => strcmp($a, $b));

        file_put_contents($versionsFile, ($introduction . "\n\n" . $parsedVisionsFile));
    }

    private static function validEmptyValue(string $valeu): bool{
        return !empty($valeu);
    }

    private static function validPasswordValue(string $valeu): bool{
        $letras = preg_replace('/[^a-z]/i', '', $valeu);
        $numbers = preg_replace('/[^\d]/i', '', $valeu);
        return (!!strlen($letras) && !!strlen($numbers));
    }

    private static function validMinLengthValeu(string $valeu, int $min): bool{
        return (strlen($valeu) <= $min);
    }



    private function extendedIni(array $parsedIni): array{
        $newIni = [];
        foreach($parsedIni as $namespace => $properties){
            $current = [];
            $extends = explode(':', $namespace);
            array_walk($extends, fn(&$i) => ($i = trim($i)));
            $extends = array_reverse($extends);

            foreach($extends as $reference){
                $referenceData = $newIni[$reference] ?? [];
                $current = array_replace_recursive($referenceData, $current, $referenceData);
            }

            $properties = self::explodeDots($properties);
            $newIni[$reference] = array_replace_recursive($properties, $current, $properties);
        }
        return $newIni;
    }

    private static function explodeDots($properties): array{
        $newProperties = [];
        foreach($properties as $index => $value){
            $data = [];
            $current =& $data;

            foreach(explode('.', $index) as $key){
                $current[$key] = [];
                $current =& $current[$key];
            }

            $current = $value;
            $newProperties = array_replace_recursive($newProperties, $data);
        }
        return $newProperties;
    }

}