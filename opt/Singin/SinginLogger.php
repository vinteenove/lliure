<?php

namespace Opt\Singin;

use LliurePanel\Lliure;
use LliurePanel\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface;

class SinginLogger implements LoggerInterface{

    protected string $app;

    /** @var callable|string */
    private $handler;

    public function __construct(string $app, $handler){
        $this->app = $app;
        $this->handler = $handler;
    }

    public static function getUser(){
        return $_SESSION['ll']['SinginUser'] ?? (object) [];
    }

    /**
     * @param string $app
     * @return array|mixed
     */
    public static function getAppUserFeature(string $app){
        return $_SESSION['ll']['SinginFeatureApp'][$app] ?? [];
    }

    public function hasUser(ServerRequestInterface $request): ServerRequestInterface{
        if(isset($_SESSION['ll']['SinginUser'])){

            if(!isset($_SESSION['ll']['SinginFeatureApp'][$this->app])){
                $_SESSION['ll']['SinginFeatureApp'][$this->app] = $this->getCallable()($_SESSION['ll']['SinginUser']);
            }

            $request = $request->withAttribute('user', $_SESSION['ll']['user']);
            Lliure::$data->user = (object) $_SESSION['ll']['user'];
        }

        return $request;
    }

    public function getCallable(): callable
    {
        $callable = $this->handler;

        if (is_string($callable) && strpos($callable, '::') !== false) {
            $callable = explode('::', $callable);
        }

        if (is_array($callable) && isset($callable[0]) && is_object($callable[0])) {
            $callable = [$callable[0], $callable[1]];
        }

        if (!is_callable($callable)) {
            throw new \Exception('Could not resolve a callable for this route');
        }

        return $callable;
    }

}