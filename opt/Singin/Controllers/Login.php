<?php

namespace Opt\Singin\Controllers;

use Exception;
use Lliure\Http\Message\Response;
use LliurePanel\ll;
use LliurePanel\Lliure;
use Opt\Singin\Singin;
use Senha\Senha;
use Opt\Singin\Models\Users;

class Login implements \LliureCore\Controller{

    public static function index($request, $vars): string{

    	$id = $vars['id'] ?? false;
    	$token = $vars['token'] ?? false;
		$resetPassword = ($id !== false && $token !== false);

        return Singin::engine()->render('login/index', [
        	'configs' => Singin::configs(),
			'resetPassword' => $resetPassword,
			'token' => $token,
			'id' => $id,
		]);
    }

    public static function login(){
    	try{

			if(empty($_POST)){
				throw new Exception('Empty Post', 400);
			}

			if(!isset($_POST['login'])){
				throw new Exception('Nonexistent login key', 400);
			}
			if(empty($_POST['login'])){
				throw new Exception('Empty login key', 400);
			}
			if(strlen($_POST['login']) < 3){
				throw new Exception('Login key with invalid size', 400);
			}

			if(!isset($_POST['senha'])){
				throw new Exception('Nonexistent password', 400);
			}
			if(empty($_POST['senha'])){
				throw new Exception('Empty password', 400);
			}
			if(strlen($_POST['senha']) < 8){
				throw new Exception('password with invalid size', 400);
			}

			['login' => $login, 'senha' => $senha] = $_POST;

			$data = Users::exist($login, $senha);

			if($data === null){
				throw new Exception('Login not found', 401);
			}

			$auth = ll::autentica($data['login'], $data['name'], $data['group']);

			if($auth === false){
				throw new Exception('Unauthorized authentication', 401);
			}

			$_SESSION['ll']['SinginUser'] = (object) $data->toArray();

			$redirectionTo = $_SESSION['ll']['retorno'] ?? ll::$data->url->endereco;
			unset($_SESSION['ll']['retorno']);

			return ['error' => false, 'code' => 200, 'location' => $redirectionTo];

		}catch(Exception $e){
			return ['error' => true, 'code' => $e->getCode(), 'message' => $e->getMessage()];
		}
    }

	public static function loginExist(){
		try{

			if(empty($_POST)){
				throw new Exception('Empty Post', 400);
			}
			if(!isset($_POST['login'])){
				throw new Exception('Nonexistent login key', 400);
			}
			if(empty($_POST['login'])){
				throw new Exception('Empty login key', 400);
			}
			if(strlen($_POST['login']) < 3){
				throw new Exception('Login key with invalid size', 400);
			}

			['login' => $login] = $_POST;

			$data = Users::findByLogin($login);

			if($data === null){
				throw new Exception('Login not found', 401);
			}

			return ['error' => false, 'code' => 200, 'data' => ['login' => $data['login'], 'name' => $data['name'], 'email' => self::hiddenEmail( (string) $data['email'])]];

		}catch(Exception $e){
			return ['error' => true, 'code' => $e->getCode(), 'message' => $e->getMessage()];
		}
    }

	public static function forgotPassword(){
    	global $_ll;

		try{
			$configs = Singin::configs();
			if(!$configs['redefine_password']){
				throw new Exception('Reset password was not authorized', 401);
			}

			if(empty($_POST)){
				throw new Exception('Empty Post', 400);
			}
			if(!isset($_POST['login'])){
				throw new Exception('Nonexistent login key', 400);
			}
			if(empty($_POST['login'])){
				throw new Exception('Empty login key', 400);
			}
			if(strlen($_POST['login']) < 3){
				throw new Exception('Login key with invalid size', 400);
			}

			['login' => $login] = $_POST;

			$user = Users::findByLogin($login);

			if($user === null){
				throw new Exception('Login not found', 401);
			}

			$user = $user->toArray();

			['id' => $userId, 'name' => $userName, 'email' => $email, 'hash' => $hash] = $user;

			$twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader([
				'subject' => $configs['redefine_password_subject'],
				'body' => $configs['redefine_password_template_email'],
			]));

			$to = $email;
			$from = $configs['redefine_password_from'];
			$subject = $twig->render('subject', ['name' => $userName]);

			/*$token = Senha::create($hash);
			$link = "{$_ll['url']->endereco}{$userId}/{$token}";*/

			$link = \Opt\Singin\Tools\User::generateUrlReforgePassord($userId, $hash);

			$body = $twig->render('body', ['name' => $userName, 'link' => $link]);
			$body = chunk_split(base64_encode($body));

			$headers = "MIME-Version: 1.0\n";
			$headers .= "From: " . $from . "\n";
			$headers .= "Return-Path: " . $from . "\n";

			$boundary = "--------------" . date("dmYisdmYisdmYis");
			$message = "--$boundary\n";
			$message .= "Content-Type: text/html; charset=\"UTF-8\"\n";
			$message .= "Content-Transfer-Encoding: base64\n\n";
			$message .= "$body\n\n\n";
			$message .= "--$boundary\n";

			$headers .= "Content-type: multipart/mixed; \n boundary=\"$boundary\"\n";
			$headers .= "$boundary\n";

			if(!@mail($to, $subject, $message, $headers)){
				throw new Exception("Error trying to send email", 500);
			}

			return ['error' => false, 'code' => 200];

		}catch(Exception $e){
			return ['error' => true, 'code' => $e->getCode(), 'message' => $e->getMessage()];
		}
	}

	public static function checkToken(){
		try{
			$configs = Singin::configs();
			if(!$configs['redefine_password']){
				throw new Exception('Reset password was not authorized', 401);
			}

			if(empty($_POST)){
				throw new Exception('Empty Post', 400);
			}

			if(!isset($_POST['id'])){
				throw new Exception('Nonexistent id', 400);
			}
			if(empty($_POST['id'])){
				throw new Exception('Empty id', 400);
			}

			if(!isset($_POST['token'])){
				throw new Exception('Nonexistent token', 400);
			}
			if(empty($_POST['token'])){
				throw new Exception('Empty token', 400);
			}

			['id' => $id, 'token' => $token] = $_POST;

			$data = Users::findById($id);
			if($data === null){
				throw new Exception('user not exist', 401);
			}

			['hash' => $hash] = $data;
			if(!Senha::valid($hash, $token)){
				throw new Exception('unauthorized token', 401);
			}

			return ['error' => false, 'code' => 200, 'data' => ['login' => $data['login'], 'name' => $data['name'], 'email' =>self::hiddenEmail($data['email'])]];

		}catch(Exception $e){
			return ['error' => true, 'code' => $e->getCode(), 'message' => $e->getMessage()];
		}
	}

	public static function resetPassword(){
		try{
            ['error' => $error] = $checkToken = self::checkToken();

		    if($error){
                ['code' => $code, 'message' => $message] = $checkToken;
				throw new Exception($message, $code);
            }

			if(!isset($_POST['senha'])){
				throw new Exception('Nonexistent password', 400);
			}
			if(!is_array($_POST['senha']) || !isset($_POST['senha'][0], $_POST['senha'][1])){
				throw new Exception('There are no passwords to compare', 400);
			}
			if(strlen($_POST['senha'][0]) < 8){
				throw new Exception('Password with invalid size', 400);
			}
			if($_POST['senha'][0] != $_POST['senha'][1]){
				throw new Exception('Different passwords', 400);
			}

			['id' => $id] = $_POST;
			$data = Users::findById($id);
			$data['hash'] = Senha::create($_POST['senha'][0]);
			$data->update();

			return ['error' => false, 'code' => 200];

		}catch(Exception $e){
			return ['error' => true, 'code' => $e->getCode(), 'message' => $e->getMessage()];
		}
	}

	public static function urlTrap(){

		$_SESSION['ll']['retorno'] = Lliure::$data->url->full;
		return new Response(301, ['Location' => Lliure::$data->url->endereco]);

	}

	private static function hiddenEmail(string $email): string{
    	if(strpos($email, '@') === false) return $email;
		[$user, $host] = explode('@', $email, 2);
		$user = str_pad(substr($user, 0, 3), strlen($user), '*');
		return "{$user}@{$host}";
    }

}