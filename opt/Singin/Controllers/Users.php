<?php

namespace Opt\Singin\Controllers;

use dibi;
use Lliure\Http\Message\Response;
use LliureCore\Controller;
use LliurePanel\Lliure;
use Opt\Singin\Singin;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Opt\Singin\Models\Users as UserModal;

class Users implements Controller {

    public static function index(): string{
        return Singin::engine()->render('user/index');
    }

    public static function form(ServerRequestInterface $request, $data): string{

        //$isPermission = Singin::permissios()->hasPermission('createUser', $user);

        $id = $data['id'];
        $user = UserModal::findById($id);
        return Singin::engine()->render('user/form', ['user' => (array) $user->toArray()]);
    }

    public static function minhaConta(ServerRequestInterface $request): string{
        $login = Lliure::$data->user->login;
        $user = UserModal::findByLogin($login);
        return Singin::engine()->render('user/form', ['user' => (array) $user->toArray()]);
    }

    public static function onserver(){

    }

    public static function logoff(): ResponseInterface{
        $response = new Response();
        $response = $response->withStatus(302);
        $response = $response->withHeader('Location',  Lliure::$data->url->endereco);
        Lliure::desautentica();
        unset($_SESSION['ll']['SinginUser'], $_SESSION['ll']['SinginFeatureApp']);
        return $response;
    }

}