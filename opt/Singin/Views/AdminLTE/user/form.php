<?php

use LliurePanel\ll;
use Liberacao\Liberacao;
use Opt\Instalilo\Instalilo;
use Helpers\HtmlHelper;
\Api\Appbar\Appbar::build();
\Api\Fileup\Fileup::build();
global $_ll, $backReal, $backNome;
/* @var array $user */

echo app_bar('Painel de usuários', [['href' => $backReal, 'fa' => 'fa-chevron-left', 'title' => (($backNome)? $backNome: 'Voltar'), 'attr' => [
    'onclick' => 'window.history.back(); return !1;',
]]]); ?>

<form method="post" action="<?php echo substr(ll::pipeline()->getNamedRoute('Singin.user.save')->getQuery(), 1); ?>" enctype="multipart/form-data">
    <input type="hidden" <?php echo HtmlHelper::input('id', $user); ?>>
    <div class="container">
        <br>
        <fieldset>
            <legend>Dados pessoais</legend>

            <div class="form-group">
                <label for="FormUserInputNome">Nome
                    <span>*</span></label>
                <input type="text" class="form-control" id="FormUserInputNome" <?php echo HtmlHelper::input('name', $user); ?> value="<?php echo $name; ?>" name="name">
            </div>

            <div class="form-group">
                <label for="FormUserInputEmail">E-mail</label>
                <input type="email" class="form-control" id="FormUserInputEmail" <?php echo HtmlHelper::input('email', $user); ?>>
            </div>

            <div class="form-group">
                <label>Foto</label>
                <?php echo \Api\Fileup\Fileup::make(array(
                    'name'   => 'photo',
                    'value'  => HtmlHelper::value('photo', $user),
                    'accept' => 'image/*',
                    'button' => 'Selecionar imagem',
                )); ?>
            </div>
        </fieldset>

        <fieldset>
            <legend>Dados de acesso</legend>

            <div class="form-group">
                <?php echo $login = HtmlHelper::value('login', $user); ?>
                <label for="FormUserInputLogin">Login <span>*</span></label>
                <input id="FormUserInputLogin" type="text" name="login" class="form-control"<?php echo !empty($login)? ' readonly value="' . $login . '"': ''; ?>>
            </div>

            <div class="form-group">
                <label for="FormUserInputSenha">Senha</label>
                <input id="FormUserInputSenha" type="password" class="form-control" name="password" placeholder="Deixe em branco para manter a senha atual.">
            </div>

            <?php if(ll::valida('admin') && $login != ll::$data->user->login){ ?>
                <div class="form-group">
                    <label for="FormUserInputGrupo">Grupo de usuário</label>
                    <select id="FormUserInputGrupo" class="form-control" name="group">
                        <?php $options = [];

                        if(isset($_ll['conf']->grupo)){
                            if(!empty($_ll['conf']->grupo)){
                                $subOptions = [];

                                foreach($_ll['conf']->grupo as $ogrupo => $valor){
                                    if(isset($valor->nome)){
                                        $subOptions[$ogrupo] = $valor->nome;
                                    }
                                }

                                if(!empty($subOptions)){
                                    $options['Sub-grupos'] = $subOptions;
                                }
                            }
                        }

                        $options['Grupos principais'] = [];
                        if(ll::valida()) $options['Grupos principais']['dev'] = 'Desenvolvedor';
                        $options['Grupos principais']['admin'] = 'Administrador';
                        $options['Grupos principais']['user'] = 'Usuário';

                        echo HtmlHelper::select('group', $user, $options); ?>
                    </select>
                </div>
                <?php
            } ?>
        </fieldset>

        <?php if(ll::valida()){ ?>
            <fieldset>
                <legend>Liberações</legend>

                <?php $libberacao = new Liberacao(); $libs = true; ?>

                <?php if($user['group'] != 'dev'){
                    $libs = array();
                    foreach($libberacao->get(array('login' => $user['login'])) as $v){
                        $libs["{$v['operation_type']}/{$v['operation_key']}"] = true;
                    }
                }; ?>

                <?php $locais = array(
                    'Sistema' => array(
                        'opt/user'       => 'Usuarios',
                        'opt/desktop'    => 'Desktop',
                        'opt/stirpanelo' => 'Painel de controle',
                        'opt/instalilo'  => 'Instalações de apps',
                        'opt/idiomas'    => 'Idiomas',
                    ),
                );
                $instalilo = new Instalilo();

                foreach($instalilo->get() as $v){
                    $locais['Apps']["app/{$v['chave']}"] = $v['nome'];
                }

                foreach($locais as $local => $apps){ ?>
                    <h4><?php echo $local; ?></h4>
                    <?php foreach($apps as $k => $v){ ?>
                        <div class="checkbox<?php echo(($libs === true)? ' disabled': ''); ?>">
                            <label>
                                <input type="checkbox" name="liberacoes[]" value="<?php echo $k; ?>"<?php echo(($libs === true || (isset($libs[$k]) && $libs[$k]))? ' checked="checked"': ''); ?><?php echo(($libs === true)? ' disabled="disabled"': ''); ?>>
                                <?php echo $v; ?>
                            </label>
                        </div>
                    <?php } ?>
                <?php } ?>
            </fieldset>
        <?php } ?>
        <br>
        <a class="btn btn-default" href="<?php echo $backReal; ?>" role="button">Voltar</a>
        <button class="btn btn-lliure" type="submit">Gravar</button>
    </div>
</form>

<br>
<br>
