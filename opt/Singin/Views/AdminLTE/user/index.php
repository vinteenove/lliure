<?php

use LliurePanel\ll;
\Api\Appbar\Appbar::build();
\Api\Vigile\Vigile::build();
\Api\Navigi\Navigi::build();

global $backReal, $_ll, $backNome;

$botoes[] =['href' => (($backReal)? $backReal: '#'), 'fa' => 'fa-chevron-left', 'title' => (($backNome)? $backNome: 'Voltar'), 'attr' => [
    'onclick' => 'window.history.back(); return !1;'
]];

$botoes[] = array('href' => '#naoImplementado', 'fa' => 'fa-user-plus ', 'title' => 'Criar usuário', 'attr' => array('class' => 'criar'));
echo app_bar('Painel de usuários', $botoes);
?>
<div class="container-fluid">
    <?php
    $navegador = new \Api\Navigi\Navigi();
    $navegador->tabela = PREFIXO . 'singin_users';
    $navegador->query = 'select * from ' . $navegador->tabela . ' where login is null || login != "' . ll::$data->user->login . '"' . (ll::valida() ? '' : ' and group != "dev"') . ' order by name ASC';
    $navegador->delete = true;
    $navegador->rename = true;
    $navegador->config = array(
        'link' => 'singin/user=',
        'fa' => 'fa-user',
        'coluna' => 'name'
    );

    $navegador->monta();
    ?>
</div>
<script type="text/javascript">
    $(function(){
        $(".criar").click(function(){
            $.get($(this).attr('href'), function(){
                Vigile().success('Novo usuário criado com sucesso!');
                navigi_start();
            });
            return false;
        });
    });
</script>