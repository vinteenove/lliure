<?php header("Content-type: text/css; charset: UTF-8");

ob_start();
require_once __DIR__ . '/../../../../../usr/AdminLTE/layout/wli.css.php';
ob_clean();

?>

html{
    display: block;
    height: 100%;
    width: 100%;
}

body{
    position: relative;
    display: block;
    min-height: 100%;
    width: 100%;
    font-family: 'Montserrat', sans-serif;

    background: rgb(38, 62, 36);
    background: 	-moz-radial-gradient(center, ellipse cover,                                          <?php echo $rgbP400; ?> 0%,               <?php echo $rgbP700; ?> 95%);
    background: 	    -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, <?php echo $rgbP400; ?>), color-stop(95%, <?php echo $rgbP700; ?>));
    background:  -webkit-radial-gradient(center, ellipse cover,                                          <?php echo $rgbP400; ?> 0%,               <?php echo $rgbP700; ?> 95%);
    background: 	  -o-radial-gradient(center, ellipse cover,                                          <?php echo $rgbP400; ?> 0%,               <?php echo $rgbP700; ?> 95%);
    background: 	  -o-radial-gradient(center, ellipse cover,                                          <?php echo $rgbP400; ?> 0%,               <?php echo $rgbP700; ?> 95%);
    background: 	 -ms-radial-gradient(center, ellipse cover,                                          <?php echo $rgbP400; ?> 0%,               <?php echo $rgbP700; ?> 95%);
    background: 		 radial-gradient(ellipse at center,                                              <?php echo $rgbP400; ?> 0%,               <?php echo $rgbP700; ?> 95%);

}


#singin.singin-layout-adminlte .centerVertical{
    display: flex;
    flex-direction: column;
    flex-wrap: nowrap;
    justify-content: space-between;
    align-content: stretch;
    align-items: center;

    min-height: 100vh;
}

#singin.singin-layout-adminlte .loginBox{
    position: relative;
}

#singin.singin-layout-adminlte .modal-contact{
    position: relative;
    top: auto;
    right: auto;
    bottom: auto;
    left: auto;
    z-index: 1;
    display: block;
    overflow: visible;
    margin-top: 1rem;
    margin-bottom: 3rem;
}


#singin.singin-layout-adminlte .modal-contact .modal-content{
    border-radius: 0 !important;

    -webkit-box-shadow: none;
    box-shadow: none;

    border: none;
}

#singin.singin-layout-adminlte .modal-contact .modal-content .modal-header,
#singin.singin-layout-adminlte .modal-contact .modal-content .modal-body,
#singin.singin-layout-adminlte .modal-contact .modal-content .modal-footer{
    padding: 30px;
}

#singin.singin-layout-adminlte .modal-contact .modal-content .modal-header{
    padding-top: 30px;
    padding-bottom: 15px;


    word-spacing: -0.528em;
    font-weight: 800;
    font-size: 42px;
    line-height: 0.95em;
    margin: 0;

    border: none !important;
}

#singin.singin-layout-adminlte .modal-contact .modal-content .modal-body{
    padding-top: 15px;
    padding-bottom: 15px;
}

#singin.singin-layout-adminlte .modal-contact .modal-content .modal-footer{
    padding-top: 15px;
    padding-bottom: 30px;

    border: none !important;
}

#singin.singin-layout-adminlte .form-control{
    height: 40px;
    line-height: 40px;
    outline: none !important;
    box-shadow: none !important;
}

#singin.singin-layout-adminlte .modal-contact .btn-lliure{
    padding-left: 25px;
    padding-right: 25px;
}

#singin.singin-layout-adminlte .btn-status-default,
#singin.singin-layout-adminlte .btn-status-disabled{
    display: none;
}

#singin.singin-layout-adminlte button:not(:disabled) .btn-status-default,
#singin.singin-layout-adminlte button:disabled .btn-status-disabled{
    display: inline;
}
