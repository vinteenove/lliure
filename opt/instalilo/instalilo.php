<?php

namespace Opt\Instalilo;

use LliureCore\Controller;
use LliurePanel\Routable;
use Opt\Instalilo\Models\LliureApps;
use Router\Router;

class Instalilo  implements
    Controller,
    Routable
{

    public function __construct(){
        //parent::__construct(PREFIXO. 'lliure_apps');
    }


    public static function router(): Router
    {
        $router = new Router();

        return $router;
    }

    /**
     * @param null $id
     * @return \LliureCore\Collection|LliureApps|null
     */
    public function get($id = null){
        if($id === null){
            return self::load($id);
        } else {
            return self::all();
        }

    }

    /**
     * @param $id
     * @return LliureApps|null
     * @throws \Exception
     */
    public static function load($id){
        return LliureApps::load($id);
    }

    /**
     * @return \LliureCore\Collection
     * @throws \Exception
     */
    public static function all(){
        return LliureApps::all();
    }

    /**
     * @param array $dados
     * @return false
     * @throws \Exception
     */
    public function upd(array $dados){
        $dados = array_intersect_key(array_merge($k = array(
            'id' => null,
            'nome' => '',
            'chave' => '',
            'pasta' => '',
        ), $dados), $k);

        if(isset($dados['id'])){
            LliureApps::build($dados)->update();
        }
        else return false;
    }

    /**
     * @param array $dados
     * @throws \Exception
     */
    public function set(array $dados){
        $dados = array_intersect_key(array_merge($k = array(
            'nome' => '',
            'chave' => '',
            'pasta' => '',
        ), $dados), $k);

        LliureApps::build($dados)->insert();

    }

    /**
     * @param null $id
     * @throws \Exception
     */
    public function del($id = null){
        LliureApps::build(['id' => $id])->delete();
    }
}