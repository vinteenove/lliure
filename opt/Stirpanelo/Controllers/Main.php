<?php

namespace Opt\Stirpanelo\Controllers;

use Helpers\HttpHelper;
use LliureCore\Controller;
use Opt\Install\LoadIni;
use Opt\Install\ParseIni;
use Opt\Stirpanelo\Stirpanelo;
use Opt\Instalilo\Instalilo;
use LliurePanel\ll;

class Main implements Controller{

    public static function index(): string{
        ll::opt('instalilo');

        $installed = $installable = [];

        $componentsInstalled = ParseIni::file(ll::baseDir() . '/etc/installed.ini');

        $filesIni = glob(HttpHelper::path(ll::baseDir() . 'app/*/lliure.ini'));
        $files = [];
        foreach($filesIni as $fileIni){
            try{
                $file = new LoadIni($fileIni);
                $files[$fileIni] = $file;
            }catch(\Exception $e){}
        }

        if(empty($files)){
            return Stirpanelo::engine()->render('index', ['dirError' => true]);
        }

        foreach($files as $filename => $data){
            $chave = $data->config()['name'] ?? false;
            $configs = $data->config();

            $configs['ico'] = HttpHelper::pathToUri(((isset($configs['ico']))? $data->basePaht() . $configs['ico']: __DIR__ . '/../img/icon_defaulto.png'));

            if(!isset($componentsInstalled[$chave])){
                $installable[$chave] = $configs;
            }else{
                $installed[$chave] = $configs;
            }
        }

        uksort($installable, 'strnatcmp');

        return Stirpanelo::engine()->render('index', [
            'dirError' => false,
            'installed' => $installed,
            'installable' => $installable,
        ]);
    }



}
