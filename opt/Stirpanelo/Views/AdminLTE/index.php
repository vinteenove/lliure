<?php
/**
 *
 * lliure
 * WAP
 *
 * @Versão  8.0
 * @Pacote  lliure
 * @Entre   em
 *          contato
 *          com
 *          o
 *          desenvolvedor
 *          <lliure@lliure.com.br>
 *          http://www.lliure.com.br/
 * @Licença //mit-license.org/
 *          MIT
 *          License
 *
 */

use LliurePanel\ll;
use Persona\Persona;

ll::opt('sobre');
\Api\Jfbox\Jfbox::build();
\Api\Appbar\Appbar::build();
\Api\Navigi\Navigi::build();
ll::opt('instalilo');

Persona::add(__DIR__ . '/index.css', 'css');

global $backReal, $backNome;

$botoes[] = array('href' => $backReal, 'fa' => 'fa-chevron-left', 'title' => $backNome);
echo app_bar('Painel de controle', $botoes); ?>

<div class="container-fluid">
    <div class="painelCtrl">
        <div class="bloco">
            <h2 style="margin-top: 0;">Configurações</h2>

            <div class="listp">
                <div class="inter">
                    <a href="singin"><img src="opt/stirpanelo/img/users.png" alt=""/></a>
                    <a href="singin"><span>Usuários</span></a>
                </div>
            </div>

			<?php
			if(ll::valida()){
				?>
                <div class="listp">
                    <div class="inter link_idioma">
                        <a href="idiomas"><img src="opt/stirpanelo/img/language.png" alt=""/></a>
                        <a href="idiomas"><span>Idiomas</span></a>
                    </div>
                </div>
				<?php
			} ?>

            <div class="listp">
                <div class="inter">
                    <a href="sobre" class="llSobre"><img src="opt/stirpanelo/img/info.png" alt=""/></a>
                    <a href="sobre" class="llSobre"><span>Sobre</span></a>
                </div>
            </div>

            <div class="listp">
                <div class="inter">
                    <a href="content"><img src="opt/content/sys/ico.svg" alt="" style="width: 32px;"/></a>
                    <a href="content"><span>Content</span></a>
                </div>
            </div>

            <div class="listp">
                <div class="inter">
                    <a href="opt=doks"><img src="opt/doks/sys/ico.svg" alt="" style="width: 32px;"/></a>
                    <a href="opt=doks"><span>Doks</span></a>
                </div>
            </div>

        </div>

        <div class="bloco">
            <h2>Aplicativos</h2>

            <?php if($dirError){ ?>

                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">ERRO</h3>
                    </div>
                    <div class="panel-body">
                        <p>Não foi localizados apps instalados e/ou para instalar</p>
                    </div>
                </div>

            <?php } else { ?>

                <?php foreach($installed as $chave => $confs){ ?>
                    <div class="listp">
                        <div class="inter">
                            <a href="<?php echo strtolower($chave); ?>"><img src="<?php echo $confs['ico']; ?>" alt="<?php echo $confs['name']; ?>" style="max-width: 32px;"/></a>
                            <a href="<?php echo strtolower($chave); ?>"><span><?php echo $confs['name']; ?></span></a>
                        </div>
                    </div>
                <?php } ?>

                <?php if(ll::valida()) foreach($installable as $chave => $confs){ ?>
                    <div class="listp">
                        <div class="inter">
                            <a href="install/app/<?php echo $chave ?>" class="install install-icone">
                                <i class="fa fa-archive"></i><img src="<?php echo $confs['ico']; ?>" alt="<?php echo $confs['name']; ?>" style="max-width: 32px;"/>
                            </a>
                            <a href="install/app/<?php echo $chave ?>" class="install"><span><?php echo $confs['name']; ?></span></a>
                        </div>
                    </div>
                <?php } ?>

            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.install').jfbox({width: 420});
        $('.link_idioma a').jfbox({width: 420});
        $('.llSobre').jfbox({
            width: 420,
            addClass: 'llSobre_box'
        });
    });
</script>
