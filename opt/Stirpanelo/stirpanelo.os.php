<?php

class organicDashboardNavigi extends navigi_rest{
    protected function get(){

        $organic = new Organic();
        $pages = $organic->uniqueVisits();

        usort($pages, function($a, $b){
            return ($a['uniqueVisits'] < $b['uniqueVisits'])? 1: -1;
        });

        foreach($pages as $k => $v){
            $pages[$k] = array_merge([
                'id' => ($k + 1),
                'url' => '',
                'visits' => '',
                'uniqueVisits' => '',
            ], $v);
        }

        return $pages;
    }
} organicDashboardNavigi::start();