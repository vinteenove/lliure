<?php

namespace Opt\Stirpanelo;

use Etc\Persona\PersonaTheme;
use League\Plates\Engine;
use LliurePanel\Middleware\NeedUserMiddleware;
use LliurePanel\Routable;
use Opt\Stirpanelo\Controllers\Main;
use Router\Router;

class Stirpanelo implements Routable
{

    public static function router(): Router{
        $router = new Router;
        $router->middleware(new NeedUserMiddleware);
        $router->get('/', [Main::class, 'index']);
        return $router;
    }

    public static function engine(): Engine{
        return new Engine(__DIR__ . '/Views/' . PersonaTheme::getThemerName());
    }

}