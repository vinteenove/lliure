<?php

namespace FontAwesome;

use LliurePanel\ll;
use Persona\Persona;

class FontAwesome implements \Persona\PersonaBuilder
{
    public static function build(): void
    {
        \Persona\Persona::style(ll::baseDir() . 'vendor/fortawesome/font-awesome/css/all.css', Persona::HIGH_PRIORITY);
        //\Persona\Persona::script(ll::baseDir() . 'vendor/fortawesome/font-awesome/js/all.js', Persona::HIGH_PRIORITY);
    }
}