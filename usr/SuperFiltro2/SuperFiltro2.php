<?php

namespace SuperFiltro2;

use Persona\Persona;
use Persona\PersonaBuilder;

class SuperFiltro2 implements PersonaBuilder{
	public static function build(): void{
		\Jquery\Jquery::build();
		Persona::add(__DIR__. '/SuperFiltro2.js', 'js', 5);
	}
}