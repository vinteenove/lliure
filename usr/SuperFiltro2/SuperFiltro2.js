;(function ($){

    Array.prototype.cursorPosition = 0;
    Array.prototype.current = function(){
        return this[this.cursorPosition];
    };
    Array.prototype.next = function(){
        this.cursorPosition = Math.min(this.cursorPosition + 1, this.length);
        return this[this.cursorPosition];
    };
    Array.prototype.previous = function(){
        this.cursorPosition = Math.max(this.cursorPosition - 1, -1);
        return this[this.cursorPosition];
    };
    Array.prototype.isEnd = function(){
        return (this.cursorPosition >= this.length);
    };
    Array.prototype.intersect = function(filter){
        if (!Array.isArray(filter)) return this;
        return this.filter(function(n){
            return (filter.indexOf(n) !== -1);
        });
    };

    function getDataInContext ($context){
        const value = [];
        let $inputs = $($context.find([
            ':input[data-sf-input]:not(:radio):not(:checkbox):not(button)',
            ':input[data-sf-input]:radio:checked:not(:disabled)',
            ':input[data-sf-input]:checkbox:checked:not(:disabled)'
        ].join(', ')));
        $inputs.each(function(){
            let $e = $(this);
            value.push({
                name: (($e.attr('data-sf-name')) ? $e.attr('data-sf-name') : $e.attr('name')),
                value: $e.val(),
                disabled: $e.is(':disabled'),
                visible: $e.is(':visible')
            })
        });
        return value;
    }

    function QueryGrupe(query, checkClosing){
        let actions = [];
        let statusPrev = 'denial';
        let statusNext;
        let corrent;

        while (!query.isEnd()){
            corrent = query.current();
            query.next();

            statusNext = defineStaus(corrent);
            if(statusNext === false)
                throw new Error('step not available for query (' + corrent + ')');

            if(([
                'denial>groupClouse',
                'denial>logicalOperator',

                'comparison>denial',
                'comparison>comparison',
                'comparison>groupOpen',

                'groupOpen>groupClouse',
                'groupOpen>logicalOperator',

                'groupClouse>denial',
                'groupClouse>comparison',
                'groupClouse>groupOpen',

                'logicalOperator>groupClouse',
                'logicalOperator>logicalOperator'
            ].indexOf(statusPrev + ">" + statusNext) + 1))
                throw new Error('you can not use (' + statusNext + ') after (' + statusPrev + ')');

            statusPrev = statusNext;

            if(corrent === ')') break;

            else if(corrent === '('){
                const newGrupe = new QueryGrupe(query, true);
                actions.push(newGrupe);

            }else{
                actions.push(corrent);}
        }

        if(([
            'denial',
            'groupOpen',
            'logicalOperator'
        ].indexOf(statusNext) + 1))
            throw new Error('you can not end a query using (' + statusPrev + ')');

        if(checkClosing && corrent !== ')')
            throw new Error('some group is not properly closed');

        this.process = function(data, debug){
            let currResult = false;
            let prevResult = false;
            let prevOperation = 'OR';
            let revert = false;

            for (const item of actions){

                if(typeof item == 'string'){
                    switch (item.toUpperCase()){
                        case 'AND': case '&&':
                            prevOperation = 'AND';
                        break;
                        case 'OR': case '||':
                            prevOperation = 'OR';
                        break;
                        case 'XOR':
                            prevOperation = 'XOR';
                        break;
                        case '!':
                            revert = !revert;
                        break;
                    }
                    continue;
                }

                if(debug) console.log({prevOperation, currResult, prevResult, 'AND': !!(prevResult & currResult), 'OR': !!(prevResult | currResult), 'XOR': !!(prevResult ^ currResult)});

                if(prevOperation === 'AND' && currResult === false){
                    continue;
                }

                if(prevOperation === 'OR' && currResult === true){
                    return true;
                }

                if(item instanceof QueryGrupe) {
                    prevResult = currResult;
                    currResult = item.process(data);

                    if(revert){
                        if(debug) console.log({revert, currResult});
                        revert = false;
                        currResult = !currResult;
                    }
                }else if(Array.isArray(item) && item.length === 3){
                    prevResult = currResult;

                    const [chaveNoData, operator, comparacaoValue] = item;
                    const conparandoValue = getValeu(chaveNoData, data);

                    if(debug) console.log({chaveNoData, conparandoValue, operator, comparacaoValue});

                    switch (operator){
                        case 'IN': case 'NOT_IN': case '!IN':
                            const conparandoArray = ((Array.isArray(conparandoValue))? conparandoValue: [conparandoValue]);
                            const comparacaoArray = ((Array.isArray(comparacaoValue))? comparacaoValue: [comparacaoValue]);
                            const intersectArray = conparandoArray.intersect(comparacaoArray);

                            if(operator == 'IN')
                                currResult = (intersectArray.length == conparandoArray.length);
                            else
                                currResult = (intersectArray.length != conparandoArray.length);

                        break;
                        case '=': case '==':
                            currResult = (conparandoValue == comparacaoValue);
                        break;
                        case '===': case 'IS':
                            currResult = (conparandoValue === comparacaoValue);
                        break;
                        case '!=': case '!IS':
                            currResult = (conparandoValue != comparacaoValue);
                        break;
                        case '!==': case 'NOT_IS':
                            currResult = (conparandoValue !== comparacaoValue);
                        break;
                        case '>=':
                            currResult = (conparandoValue >= comparacaoValue);
                        break;
                        case '<=':
                            currResult = (conparandoValue <= comparacaoValue);
                        break;
                        case '>':
                            currResult = (conparandoValue > comparacaoValue);
                        break;
                        case '<':
                            currResult = (conparandoValue < comparacaoValue);
                        break;
                        case ':':
                            currResult = resolveProp(chaveNoData, comparacaoValue, data);
                        break;
                        case '!:':
                            currResult = !resolveProp(chaveNoData, comparacaoValue, data);
                        break;
                        default: throw new Error('operator ("' + operator + '") is not an accepted comparator');
                    }

                    if(revert){
                        if(debug) console.log({revert, currResult});
                        revert = false;
                        currResult = !currResult;
                    }
                }

                if(prevOperation === 'XOR' && (prevResult ^ currResult)){
                    return true;
                }

                if(prevOperation === 'XOR' && !(prevResult ^ currResult)){
                    currResult = false;
                }
            }

            return currResult;
        };

        return this;

        function getValeu(key, data){
            for(const {name, value} of data) if(name === key) return value;
        }

        function defineStaus(corrent){
            if(Array.isArray(corrent)) return 'comparison';
            if(corrent == '(') return 'groupOpen';
            if(corrent == ')') return 'groupClouse';
            if(corrent == '!') return 'denial';
            if(([
                "AND", "OR", "XOR", "&&", "||"
            ].indexOf(corrent) + 1)) return 'logicalOperator';
            return false;
        }

        function resolveProp(key, value, data){
            const props = getProps(key, data);
            if (value == 'disabled') return props.disabled;
            if (value == 'visible') return props.visible;
            if (value == 'enabled') return !props.disabled;
            if (value == 'hidden') return !props.visible;
            throw new Error('property ("' + value + '") is not accepted as a result');
        }

        function getProps(key, data){
            for(const i in data) if(data.hasOwnProperty(i) && data[i].name == key) return {
                disabled: ((data[i].hasOwnProperty('disabled'))? data[i].disabled: false),
                visible: ((data[i].hasOwnProperty('visible'))? data[i].visible: true)
            };
        }
    }

    function resolveQuery(jsonQuery, data, debug){
        if (!jsonQuery) return;
        const query = JSON.parse(jsonQuery);
        if (debug) console.log(query);

        const objQuery = new QueryGrupe(query);

        if(debug === 2) console.table(data);

        return objQuery.process(data, debug === 2);
    }

    $.superFiltro2 = function($context, data){
        let affecteds;

        const o = {
            'debug': false,
        };

        if (o['debug']) console.group('Filter start');
        if (o['debug']) console.log('context', $context);

        if (typeof data === 'undefined') data = getDataInContext($context);
        if (o['debug']) console.table(data);

        affecteds = $context.find('[data-sf-hide]');
        if (affecteds.length) {
            if (o['debug']) console.group('sf-hide');
            if (o['debug']) console.log(affecteds);
            affecteds.each(function (i, e) {
                const $this = $(e);
                const iDebug = ((o['debug'])? 1: (($(e).attr('data-sf-debug') === 'true')? 2: false));
                if (iDebug) console.groupCollapsed('Item: ' + i);
                if (iDebug) console.log($this);
                const jsonQuery = $this.attr('data-sf-hide');
                const result = resolveQuery(jsonQuery, data, iDebug);
                if (iDebug) console.log('result: ', result);
                if(result) {
                    $this.prop('hidden', true);
                    if (iDebug) console.log('Item: ' + i + ' | hidden');
                }else{
                    $this.prop('hidden', false);
                    if (iDebug) console.log('Item: ' + i + ' | visible');
                }
                if (iDebug) console.groupEnd();
            });
            if (o['debug']) console.groupEnd();
        }

        affecteds = $context.find('[data-sf-show]');
        if (affecteds.length) {
            if (o['debug']) console.group('sf-show');
            if (o['debug']) console.log(affecteds);
            affecteds.each(function (i, e) {
                const $this = $(e);
                const iDebug = ((o['debug'])? 1: (($(e).attr('data-sf-debug') === 'true')? 2: false));
                if (iDebug) console.groupCollapsed('Item: ' + i);
                if (iDebug) console.log($this);
                const jsonQuery = $this.attr('data-sf-show');
                const result = resolveQuery(jsonQuery, data, iDebug);
                if (iDebug) console.log('result: ', result);
                if(result) {
                    $this.prop('hidden', false);
                    if (iDebug) console.log('Item: ' + i + ' | visible');
                }else{
                    $this.prop('hidden', true);
                    if (iDebug) console.log('Item: ' + i + ' | hidden');
                }
                if (iDebug) console.groupEnd();
            });
            if (o['debug']) console.groupEnd();
        }

        affecteds = $context.find('[data-sf-disabled]');
        if (affecteds.length) {
            if (o['debug']) console.group('sf-disabled');
            if (o['debug']) console.log(affecteds);
            affecteds.each(function (i, e) {
                const $this = $(e);
                const iDebug = ((o['debug'])? 1: (($(e).attr('data-sf-debug') === 'true')? 2: false));
                if (iDebug) console.groupCollapsed('Item: ' + i);
                if (iDebug) console.log($this);
                const jsonQuery = $this.attr('data-sf-disabled');
                const result = resolveQuery(jsonQuery, data, iDebug);
                if (iDebug) console.log('result: ', result);
                if(result) {
                    $this.prop('disabled', true);
                    if (iDebug) console.log('Item: ' + i + ' | disabled');
                }else{
                    $this.prop('disabled', false);
                    if (iDebug) console.log('Item: ' + i + ' | enabled');
                }
                if (iDebug) console.groupEnd();
            });
            if (o['debug']) console.groupEnd();
        }

        affecteds = $context.find('[data-sf-enabled]');
        if (affecteds.length) {
            if (o['debug']) console.group('sf-enabled');
            if (o['debug']) console.log(affecteds);
            affecteds.each(function (i, e) {
                const $this = $(e);
                const iDebug = ((o['debug'])? 1: (($this.attr('data-sf-debug') === 'true')? 2: false));
                if (iDebug) console.groupCollapsed('Item: ' + i);
                if (iDebug) console.log($this);
                const jsonQuery = $this.attr('data-sf-enabled');
                const result = resolveQuery(jsonQuery, data, iDebug);
                if (iDebug) console.log('result: ', result);
                if(result) {
                    $this.prop('disabled', false);
                    if (iDebug) console.log('Item: ' + i + ' | enabled');
                }else{
                    $this.prop('disabled', true);
                    if (iDebug) console.log('Item: ' + i + ' | disabled');
                }
                if (iDebug) console.groupEnd();
            });
            if (o['debug']) console.groupEnd();
        }

        if (o['debug']) console.groupEnd();
    };

    $.fn.superFiltro2 = function(data){
        $.superFiltro2(this, data);
        return this;
    };

    $(e => { $('body')
        .on('click',  '[data-sf="input"]',  dispararProcesso)
        .on('click',  '[data-sf="click"]',  dispararProcesso)
        .on('change', '[data-sf="change"]', dispararProcesso);

        $('[data-sf="input"]').trigger('input');
        $('[data-sf="click"]').trigger('click');
        $('[data-sf="change"]').trigger('change');
    });

    function dispararProcesso(){
        const $this = $(this);
        const $context =
            (($this.attr('data-sf-parent')) ? $($this.parents($this.attr('data-sf-parent'))[0]) :
            (($this.attr('data-sf-context')) ? $($this.attr('data-sf-context')[0]) :
            $('body')));
        $.superFiltro2($context);
    }

})(jQuery);