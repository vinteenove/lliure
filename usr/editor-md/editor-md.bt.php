<?php
use Persona\Persona;
Persona::add(__DIR__ . '/css/editormd.min.css', 'css');
Persona::add(__DIR__ . '/editormd.min.js', 'js');
Persona::add(__DIR__ . '/languages/en.js', 'js');