<?php

namespace AdminLTE;

use ClassBody\ClassBody;
use FontAwesome\FontAwesome;
use Helpers\HttpHelper;
use League\Event\ListenerPriority;
use Lliure\Http\Exception\HttpExceptionInterface;
use Lliure\Http\Message\Response;
use Lliure\Http\Message\Stream;
use LliurePanel\ll;
use Persona\Persona;
use Persona\PersonaInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Router\Route;
use Throwable;

class AdminLTE implements
    PersonaInterface
{

	protected bool $needUser = false;

    /**
     * @return string
     */
    public static function getThemerName(): string
    {
        return 'AdminLTE';
    }

    public static function bootstrap()
    {
        global $personaData;

        Persona::bootstrap();

        $personaData = array_replace([
            'color'      => '94324b',
            'logo'       => Persona::$theme->path . 'layout/logo.php',
            'link'       => 'http://www.lliure.com.br',
            'assinatura' => 'lliure '. @ll::$data->conf->versao,
        ], (array) ((isset(Persona::$theme->chave, ll::$data->conf->grupo->{Persona::$theme->chave}))? ll::$data->conf->grupo->{Persona::$theme->chave}: []));

        $personaData['color'] = ((isset(Persona::$theme->chave, ll::$data->conf->grupo->{Persona::$theme->chave}->color))
            ? ['cor' => ll::$data->conf->grupo->{Persona::$theme->chave}->color]
            : ['cor' => $personaData['color']]
        );

        if(ll::autentica()){
            require_once __DIR__ . '/layout/MainMenuLeft.php';
            require_once __DIR__ . '/layout/MainMenuRight.php';
        }

        ll::on('ClassBody', function (ClassBody $e) {
            $e->addClass(['sidebar-collapse']);
        }, ListenerPriority::HIGH);
    }

    public static function build()
    {
        static::bootstrap();

        global $personaData;

        //Inicia o históico
        ll_historico('inicia');

        \Jquery\Jquery::build();
        \Bootstrap\Bootstrap::build();
        ll::api('jfbox');

        // IonIcons
        Persona::style('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css', Persona::COMPONENT_PRIORITY);

        // Google Font: Source Sans Pro
        Persona::style('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700', Persona::COMPONENT_PRIORITY);

        // Theme style
        Persona::style(ll::baseDir() . '/vendor/almasaeed2010/adminlte/dist/css/adminlte.css', Persona::COMPONENT_PRIORITY);

        // Themes script
        Persona::script(ll::baseDir() . '/vendor/almasaeed2010/adminlte/dist/js/adminlte.min.js',Persona::COMPONENT_PRIORITY);

        /** carrega o estilo do layout */
        if (ll::$data->enter_mode == 'wli') {
            if (!empty(Persona::$theme->wli['css'])) {
                Persona::style(Persona::$theme->wli['css'], Persona::COMPONENT_PRIORITY);
            }
        } else {
            if (!empty(Persona::$theme->nli['css'])){
                Persona::style(Persona::$theme->nli['css'], Persona::COMPONENT_PRIORITY);
            }
        }

        Persona::style( ll::baseDir() . HttpHelper::pathToUri(__DIR__. '/layout/wli.css.php', $personaData['color']), Persona::COMPONENT_PRIORITY);
    }

    /**
     * @param $appView
     */
    public static function wli($appView){
        self::build();
        global $_ll, $mainMenuLeft, $mainMenuRight, $personaData; ob_start(); ?>
            <!DOCTYPE html>
            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
                <head>

                    <base href="<?php echo ll::$data->url->endereco; ?>"/>
                    <meta charset="utf-8">
                    <meta name="url" content="<?php echo ll::$data->url->endereco; ?>"/>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link rel="SHORTCUT ICON" href="<?php echo ll::$data->url->endereco; ?>/usr/img/favicon.ico" type="image/x-icon"/>
                    <meta name="DC.creator.address" content="lliure@lliure.com.br"/>

                    <title><?php echo $_ll['titulo'] ?></title>

                    <?php Persona::head(); ?>
                </head>
                <body class="<?php echo implode(' ', ll::trigger(new ClassBody('ClassBody'))->getClass()); ?>">
                    <div class="wrapper">

                        <!-- Navbar -->
                        <nav id="ll_topo" class="main-header navbar navbar-expand navbar-dark">
                            <?php echo $mainMenuLeft; ?>

                            <?php echo $mainMenuRight; ?>
                        </nav>
                        <!-- /.navbar -->

                        <?php ll::trigger(new \AdminLTE\layout\Content('prepend.content')); ?>

                        <div class="content-wrapper">
                            <?php ll::trigger(new \AdminLTE\layout\Content('before.content')); ?>

                            <?php echo $appView; ?>

                            <?php ll::trigger(new \AdminLTE\layout\Content('after.content')); ?>
                        </div>

                        <?php ll::trigger(new \AdminLTE\layout\Content('append.content')); ?>

                        <!-- Main Footer -->
                        <footer class="main-footer">
                            <a href="<?php echo $personaData['link']; ?>" class="ll_color ll_color-hover" target="_blank" ><?php echo $personaData['assinatura'] ?? ''; ?></a>
                        </footer>
                    </div>

                    <script type="text/javascript">
                        (function($){$(function(){
                            $('.btn-add-desktop').click(function(){
                                var url = window.btoa(window.location.href);
                                jfBox('oc/opt=desktop/ac=addDesktop/url=' + url).open();
                            });
                        })})(jQuery);
                    </script>

                    <?php Persona::footer(); ?>
                </body>
            </html>
        <?php return ob_get_clean();
    }

    /**
     * @param $appView
     */
    public static function nli($appView){
        self::build();
        global $_ll;
        ob_start();
        ?>
            <!DOCTYPE html>
            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
                <head>
                    <base href="<?php echo ll::$data->url->endereco; ?>"/>
                    <meta charset="utf-8">
                    <meta name="url" content="<?php echo ll::$data->url->endereco; ?>"/>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link rel="SHORTCUT ICON" href="<?php echo ll::$data->url->endereco; ?>/usr/img/favicon.ico" type="image/x-icon"/>
                    <meta name="DC.creator.address" content="lliure@lliure.com.br"/>

                    <title><?php echo $_ll['titulo'] ?></title>

                    <?php Persona::head(); ?>
                </head>

                <body class="<?php echo implode(' ', ll::trigger(new ClassBody('ClassBody'))->getClass()); ?>">
                    <?php echo $appView;
                    Persona::footer(); ?>
                </body>
            </html>
        <?php return ob_get_clean();
    }

    public static function page_denied(){
        FontAwesome::build(); ob_start(); ?>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Not authorized</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo ll::$data->url->real; ?>">Desktop</a></li>
                            <li class="breadcrumb-item active">Not authorized</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> <i class="fas fa-users-slash"></i></h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Not authorized</h3>

                    <p>
                        You are not authorized to enter this page. <br>
                        Contact an administrator for more details. <br>
                        Meanwhile, you may <a href="<?php echo ll::$data->url->real; ?>">return to desktop</a>.
                    </p>
                </div>
            </div>
        </section>
    <?php return ob_get_clean(); }

    public static function page_404() {
        FontAwesome::build(); ob_start(); ?>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>404 Error Page</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo ll::$data->url->real; ?>">Desktop</a></li>
                            <li class="breadcrumb-item active">404 Error Page</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> <i class="far fa-dizzy"></i></h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>

                    <p>
                        Something wrong is not right <br>
                        Meanwhile, you may <a href="<?php echo ll::$data->url->real; ?>">return to desktop</a>.
                    </p>

                </div>
            </div>
        </section>
    <?php return ob_get_clean(); }

    public static function page_405() {
        FontAwesome::build(); ob_start(); ?>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>405 Method Not Allowed</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo ll::$data->url->real; ?>">Desktop</a></li>
                            <li class="breadcrumb-item active">405 Method Not Allowed</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> <i class="far fa-dizzy"></i></h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Method not allowed.</h3>

                    <p>
                        Something wrong is not right <br>
                        Meanwhile, you may <a href="<?php echo ll::$data->url->real; ?>">return to desktop</a>.
                    </p>
                </div>
            </div>
        </section>
    <?php return ob_get_clean(); }


    /** @inheritDoc */
    public function invokeRouteCallable(Route $route, ServerRequestInterface $request): ResponseInterface{
        $this->needUser = $request->getAttribute('needUser', false);
        return Persona::buildeResponse($route, $request);
    }

    /** @inheritDoc */
    public function getUnsolvedDecorator(Throwable $exception): MiddlewareInterface{
        return new class($exception, static::class) implements MiddlewareInterface
        {
            protected Throwable $error;
            protected $class;

            public function __construct(Throwable $error, $class){
                $this->error = $error;
                $this->class = $class;
            }

            public function process(
                ServerRequestInterface $request,
                RequestHandlerInterface $requestHandler
            ): ResponseInterface{

				$codeError = ($this->error instanceof HttpExceptionInterface)? $this->error->getStatusCode(): $this->error->getCode();

                if(!is_callable([$this->class, "page_{$codeError}"])){
                    throw $this->error;
                }

                $response = new Response();

                if($this->error instanceof HttpExceptionInterface){
					$response = $response->withStatus(
                        $this->error->getStatusCode(),
                        $this->error->getMessage()
                    );
                }else{
					$response = $response->withStatus(500, 'Internal Server Error');
                }

                $body = $this->class::{"page_{$codeError}"}($this->error);

                return $response->withBody(Stream::createFromString($body));
            }
        };
    }

    /** @inheritDoc */
    public function getThrowableHandler(): MiddlewareInterface{
        return new class($this->needUser, $this, $this->logged) implements MiddlewareInterface{

            private bool $needUser;
            private $target;
            private $logged;

            public function __construct(&$needUser, $target, $logged){
                $this->needUser =& $needUser;
                $this->logged = $logged;
                $this->target = $target;
            }

            /** @inheritdoc */
            public function process(
                ServerRequestInterface $request,
                RequestHandlerInterface $requestHandler
            ): ResponseInterface {
                try {
                    return $this->completeBody($requestHandler->handle($request));
                }catch (Throwable $e) {
                    return $this->completeBody($this->target->getUnsolvedDecorator($e)->process($request, $requestHandler));
                }
            }
	
			private function completeBody(ResponseInterface $response): ResponseInterface{
				$body = (string) $response->getBody();
				
				if($this->needUser || $this->logged){
					$body = $this->target::wli($body);
				}else{
					$body = $this->target::nli($body);
				}
				
				return $response->withBody(Stream::createFromString($body));
            }
        };
    }


    protected $logged = false;

    /**
     * AdminLTE constructor.
     * @param bool $logged
     */
    public function __construct(bool $logged = false)
    {
        $this->logged = $logged;
    }

}