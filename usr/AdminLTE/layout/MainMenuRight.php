<?php

use LliurePanel\ll;
use Persona\Persona;

global $mainMenuRight, $_ll;

$mainMenuRight = new \Menu\Itens\Menu(\Menu\Itens\Menu::NavBar, ['class' => 'ml-auto']);

$mainMenuRight->children(new \Menu\Itens\Link('systemHome', 'Home', ll::$data->url->endereco));
$mainMenuRight->children(new \Menu\Itens\Link('systemAccount', 'Minha conta', substr(ll::pipeline()->getNamedRoute('systemAccount')->getQuery(), 1)));
if(ll::valida()) $mainMenuRight->children(new \Menu\Itens\Link('systemPanel', 'Painel de controle', (ll::$data->url->endereco . 'stirpanelo')));
$mainMenuRight->children(new \Menu\Itens\Link('systemLogoff', 'Sair', substr(ll::pipeline()->getNamedRoute('systemLogoff')->getQuery(), 1)));

