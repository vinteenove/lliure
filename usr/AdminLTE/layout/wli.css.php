<?php header("Content-type: text/css; charset: UTF-8");

global $defCor, $disCor, $rgbCor, $rgbaCor, $hexCor, $prosCor;

$defCor = function($cor){
    if(!is_string($cor)) return $cor;
    $rgb = ((strlen($cor) == 3)? preg_replace('/(.)(.)(.)/', '$1$1$2$2$3$3', $cor): $cor);
    $r = (hexdec($rgb) >> 16) & 0xFF;
    $g = (hexdec($rgb) >> 8) & 0xFF;
    $b = hexdec($rgb) & 0xFF;
    return [$r, $g, $b];
};

$disCor = function($de, $ate, $dis){
    $r = [];
    $dis = (($dis < 0)? 0: (($dis > 1)? 1: $dis));
    foreach($de as $k => $v) $r[$k] = ((int)((($ate[$k] - $de[$k]) * $dis) + $de[$k]));
    return $r;
};

$rgbCor = function($cor){
    return 'rgb('. implode(', ', $cor). ')';
};

$rgbaCor = function($cor, $alf){
    return 'rgba('. implode(', ', $cor). ', ' . $alf .  ')';
};

$hexCor = function($cor){
    return '#' . dechex($cor[0]) . dechex($cor[1]) . dechex($cor[2]);
};

$prosCor = function($cor) use (&$rgbCor, &$disCor){
    $f = (1 / 5);
    $branco = [255, 255, 255];
    $preto = [0, 0, 0];

    $c100 = $rgbCor($disCor($cor, $branco, ($f * 4)));
    $c200 = $rgbCor($disCor($cor, $branco, ($f * 3)));
    $c300 = $rgbCor($disCor($cor, $branco, ($f * 2)));
    $c400 = $rgbCor($disCor($cor, $branco, ($f * 1)));
    $c500 = $rgbCor($cor);
    $c600 = $rgbCor($disCor($cor, $preto, ($f * 1)));
    $c700 = $rgbCor($disCor($cor, $preto, ($f * 2)));
    $c800 = $rgbCor($disCor($cor, $preto, ($f * 3)));
    $c900 = $rgbCor($disCor($cor, $preto, ($f * 4)));

    return [$c100, $c200, $c300, $c400, $c500, $c600, $c700, $c800, $c900];
};

if(!isset($_GET['cor'])) $_GET['cor'] = '888888';
$c = ((is_array($_GET['cor']))? $_GET['cor']: []);
if(!is_array($_GET['cor'])) list($c[0], $c[1]) = [$defCor($_GET['cor']), $disCor($defCor($_GET['cor']), [255, 255, 255], .2)];
$_GET['cor'] = $c;

$rgbP = $defCor($_GET['cor'][0]);
$rgbS = $defCor($_GET['cor'][1]);

list($rgbP100, $rgbP200, $rgbP300, $rgbP400, $rgbP500, $rgbP600, $rgbP700, $rgbP800, $rgbP900) = $prosCor($rgbP);
list($rgbS100, $rgbS200, $rgbS300, $rgbS400, $rgbS500, $rgbS600, $rgbS700, $rgbS800, $rgbS900) = $prosCor($rgbS); ?>

.ll_color{ color: <?php echo $rgbP500; ?> !important; }
.ll_color-100{ color: <?php echo $rgbP100; ?> !important; }
.ll_color-200{ color: <?php echo $rgbP200; ?> !important; }
.ll_color-300{ color: <?php echo $rgbP300; ?> !important; }
.ll_color-400{ color: <?php echo $rgbP400; ?> !important; }
.ll_color-500{ color: <?php echo $rgbP500; ?> !important; }
.ll_color-600{ color: <?php echo $rgbP600; ?> !important; }
.ll_color-700{ color: <?php echo $rgbP700; ?> !important; }
.ll_color-800{ color: <?php echo $rgbP800; ?> !important; }
.ll_color-900{ color: <?php echo $rgbP900; ?> !important; }

.ll_color-p{ color: <?php echo $rgbP500; ?> !important; }
.ll_color-p-100{ color: <?php echo $rgbP100; ?> !important; }
.ll_color-p-200{ color: <?php echo $rgbP200; ?> !important; }
.ll_color-p-300{ color: <?php echo $rgbP300; ?> !important; }
.ll_color-p-400{ color: <?php echo $rgbP400; ?> !important; }
.ll_color-p-500{ color: <?php echo $rgbP500; ?> !important; }
.ll_color-p-600{ color: <?php echo $rgbP600; ?> !important; }
.ll_color-p-700{ color: <?php echo $rgbP700; ?> !important; }
.ll_color-p-800{ color: <?php echo $rgbP800; ?> !important; }
.ll_color-p-900{ color: <?php echo $rgbP900; ?> !important; }

.ll_color-s{ color: <?php echo $rgbS500; ?> !important; }
.ll_color-s-100{ color: <?php echo $rgbS100; ?> !important; }
.ll_color-s-200{ color: <?php echo $rgbS200; ?> !important; }
.ll_color-s-300{ color: <?php echo $rgbS300; ?> !important; }
.ll_color-s-400{ color: <?php echo $rgbS400; ?> !important; }
.ll_color-s-500{ color: <?php echo $rgbS500; ?> !important; }
.ll_color-s-600{ color: <?php echo $rgbS600; ?> !important; }
.ll_color-s-700{ color: <?php echo $rgbS700; ?> !important; }
.ll_color-s-800{ color: <?php echo $rgbS800; ?> !important; }
.ll_color-s-900{ color: <?php echo $rgbS900; ?> !important; }

.ll_color-hover:hover{ color: <?php echo $rgbP500; ?> !important; }
.ll_color-100-hover:hover{ color: <?php echo $rgbP100; ?> !important; }
.ll_color-200-hover:hover{ color: <?php echo $rgbP200; ?> !important; }
.ll_color-300-hover:hover{ color: <?php echo $rgbP300; ?> !important; }
.ll_color-400-hover:hover{ color: <?php echo $rgbP400; ?> !important; }
.ll_color-500-hover:hover{ color: <?php echo $rgbP500; ?> !important; }
.ll_color-600-hover:hover{ color: <?php echo $rgbP600; ?> !important; }
.ll_color-700-hover:hover{ color: <?php echo $rgbP700; ?> !important; }
.ll_color-800-hover:hover{ color: <?php echo $rgbP800; ?> !important; }
.ll_color-900-hover:hover{ color: <?php echo $rgbP900; ?> !important; }

.ll_color-p-hover:hover{ color: <?php echo $rgbP500; ?> !important; }
.ll_color-p-100-hover:hover{ color: <?php echo $rgbP100; ?> !important; }
.ll_color-p-200-hover:hover{ color: <?php echo $rgbP200; ?> !important; }
.ll_color-p-300-hover:hover{ color: <?php echo $rgbP300; ?> !important; }
.ll_color-p-400-hover:hover{ color: <?php echo $rgbP400; ?> !important; }
.ll_color-p-500-hover:hover{ color: <?php echo $rgbP500; ?> !important; }
.ll_color-p-600-hover:hover{ color: <?php echo $rgbP600; ?> !important; }
.ll_color-p-700-hover:hover{ color: <?php echo $rgbP700; ?> !important; }
.ll_color-p-800-hover:hover{ color: <?php echo $rgbP800; ?> !important; }
.ll_color-p-900-hover:hover{ color: <?php echo $rgbP900; ?> !important; }

.ll_color-s-hover:hover{ color: <?php echo $rgbS500; ?> !important; }
.ll_color-s-100-hover:hover{ color: <?php echo $rgbS100; ?> !important; }
.ll_color-s-200-hover:hover{ color: <?php echo $rgbS200; ?> !important; }
.ll_color-s-300-hover:hover{ color: <?php echo $rgbS300; ?> !important; }
.ll_color-s-400-hover:hover{ color: <?php echo $rgbS400; ?> !important; }
.ll_color-s-500-hover:hover{ color: <?php echo $rgbS500; ?> !important; }
.ll_color-s-600-hover:hover{ color: <?php echo $rgbS600; ?> !important; }
.ll_color-s-700-hover:hover{ color: <?php echo $rgbS700; ?> !important; }
.ll_color-s-800-hover:hover{ color: <?php echo $rgbS800; ?> !important; }
.ll_color-s-900-hover:hover{ color: <?php echo $rgbS900; ?> !important; }

.ll_border-color{ border-color: <?php echo $rgbP500; ?> !important; }
.ll_border-color-100{ border-color: <?php echo $rgbP100; ?> !important; }
.ll_border-color-200{ border-color: <?php echo $rgbP200; ?> !important; }
.ll_border-color-300{ border-color: <?php echo $rgbP300; ?> !important; }
.ll_border-color-400{ border-color: <?php echo $rgbP400; ?> !important; }
.ll_border-color-500{ border-color: <?php echo $rgbP500; ?> !important; }
.ll_border-color-600{ border-color: <?php echo $rgbP600; ?> !important; }
.ll_border-color-700{ border-color: <?php echo $rgbP700; ?> !important; }
.ll_border-color-800{ border-color: <?php echo $rgbP800; ?> !important; }
.ll_border-color-900{ border-color: <?php echo $rgbP900; ?> !important; }

.ll_border-color-p{ border-color: <?php echo $rgbP500; ?> !important; }
.ll_border-color-p-100{ border-color: <?php echo $rgbP100; ?> !important; }
.ll_border-color-p-200{ border-color: <?php echo $rgbP200; ?> !important; }
.ll_border-color-p-300{ border-color: <?php echo $rgbP300; ?> !important; }
.ll_border-color-p-400{ border-color: <?php echo $rgbP400; ?> !important; }
.ll_border-color-p-500{ border-color: <?php echo $rgbP500; ?> !important; }
.ll_border-color-p-600{ border-color: <?php echo $rgbP600; ?> !important; }
.ll_border-color-p-700{ border-color: <?php echo $rgbP700; ?> !important; }
.ll_border-color-p-800{ border-color: <?php echo $rgbP800; ?> !important; }
.ll_border-color-p-900{ border-color: <?php echo $rgbP900; ?> !important; }

.ll_border-color-s{ border-color: <?php echo $rgbS500; ?> !important; }
.ll_border-color-s-100{ border-color: <?php echo $rgbS100; ?> !important; }
.ll_border-color-s-200{ border-color: <?php echo $rgbS200; ?> !important; }
.ll_border-color-s-300{ border-color: <?php echo $rgbS300; ?> !important; }
.ll_border-color-s-400{ border-color: <?php echo $rgbS400; ?> !important; }
.ll_border-color-s-500{ border-color: <?php echo $rgbS500; ?> !important; }
.ll_border-color-s-600{ border-color: <?php echo $rgbS600; ?> !important; }
.ll_border-color-s-700{ border-color: <?php echo $rgbS700; ?> !important; }
.ll_border-color-s-800{ border-color: <?php echo $rgbS800; ?> !important; }
.ll_border-color-s-900{ border-color: <?php echo $rgbS900; ?> !important; }

.ll_border-color-hover:hover{ border-color: <?php echo $rgbP500; ?> !important; }
.ll_border-color-100-hover:hover{ border-color: <?php echo $rgbP100; ?> !important; }
.ll_border-color-200-hover:hover{ border-color: <?php echo $rgbP200; ?> !important; }
.ll_border-color-300-hover:hover{ border-color: <?php echo $rgbP300; ?> !important; }
.ll_border-color-400-hover:hover{ border-color: <?php echo $rgbP400; ?> !important; }
.ll_border-color-500-hover:hover{ border-color: <?php echo $rgbP500; ?> !important; }
.ll_border-color-600-hover:hover{ border-color: <?php echo $rgbP600; ?> !important; }
.ll_border-color-700-hover:hover{ border-color: <?php echo $rgbP700; ?> !important; }
.ll_border-color-800-hover:hover{ border-color: <?php echo $rgbP800; ?> !important; }
.ll_border-color-900-hover:hover{ border-color: <?php echo $rgbP900; ?> !important; }

.ll_border-color-p-hover:hover{ border-color: <?php echo $rgbP500; ?> !important; }
.ll_border-color-p-100-hover:hover{ border-color: <?php echo $rgbP100; ?> !important; }
.ll_border-color-p-200-hover:hover{ border-color: <?php echo $rgbP200; ?> !important; }
.ll_border-color-p-300-hover:hover{ border-color: <?php echo $rgbP300; ?> !important; }
.ll_border-color-p-400-hover:hover{ border-color: <?php echo $rgbP400; ?> !important; }
.ll_border-color-p-500-hover:hover{ border-color: <?php echo $rgbP500; ?> !important; }
.ll_border-color-p-600-hover:hover{ border-color: <?php echo $rgbP600; ?> !important; }
.ll_border-color-p-700-hover:hover{ border-color: <?php echo $rgbP700; ?> !important; }
.ll_border-color-p-800-hover:hover{ border-color: <?php echo $rgbP800; ?> !important; }
.ll_border-color-p-900-hover:hover{ border-color: <?php echo $rgbP900; ?> !important; }

.ll_border-color-s-hover:hover{ border-color: <?php echo $rgbS500; ?> !important; }
.ll_border-color-s-100-hover:hover{ border-color: <?php echo $rgbS100; ?> !important; }
.ll_border-color-s-200-hover:hover{ border-color: <?php echo $rgbS200; ?> !important; }
.ll_border-color-s-300-hover:hover{ border-color: <?php echo $rgbS300; ?> !important; }
.ll_border-color-s-400-hover:hover{ border-color: <?php echo $rgbS400; ?> !important; }
.ll_border-color-s-500-hover:hover{ border-color: <?php echo $rgbS500; ?> !important; }
.ll_border-color-s-600-hover:hover{ border-color: <?php echo $rgbS600; ?> !important; }
.ll_border-color-s-700-hover:hover{ border-color: <?php echo $rgbS700; ?> !important; }
.ll_border-color-s-800-hover:hover{ border-color: <?php echo $rgbS800; ?> !important; }
.ll_border-color-s-900-hover:hover{ border-color: <?php echo $rgbS900; ?> !important; }

.ll_background{ background-color: <?php echo $rgbP500; ?> !important; }
.ll_background-100{ background-color: <?php echo $rgbP100; ?> !important; }
.ll_background-200{ background-color: <?php echo $rgbP200; ?> !important; }
.ll_background-300{ background-color: <?php echo $rgbP300; ?> !important; }
.ll_background-400{ background-color: <?php echo $rgbP400; ?> !important; }
.ll_background-500{ background-color: <?php echo $rgbP500; ?> !important; }
.ll_background-600{ background-color: <?php echo $rgbP600; ?> !important; }
.ll_background-700{ background-color: <?php echo $rgbP700; ?> !important; }
.ll_background-800{ background-color: <?php echo $rgbP800; ?> !important; }
.ll_background-900{ background-color: <?php echo $rgbP900; ?> !important; }

.ll_background-p{ background-color: <?php echo $rgbP500; ?> !important; }
.ll_background-p-100{ background-color: <?php echo $rgbP100; ?> !important; }
.ll_background-p-200{ background-color: <?php echo $rgbP200; ?> !important; }
.ll_background-p-300{ background-color: <?php echo $rgbP300; ?> !important; }
.ll_background-p-400{ background-color: <?php echo $rgbP400; ?> !important; }
.ll_background-p-500{ background-color: <?php echo $rgbP500; ?> !important; }
.ll_background-p-600{ background-color: <?php echo $rgbP600; ?> !important; }
.ll_background-p-700{ background-color: <?php echo $rgbP700; ?> !important; }
.ll_background-p-800{ background-color: <?php echo $rgbP800; ?> !important; }
.ll_background-p-900{ background-color: <?php echo $rgbP900; ?> !important; }

.ll_background-s{ background-color: <?php echo $rgbS500; ?> !important; }
.ll_background-s-100{ background-color: <?php echo $rgbS100; ?> !important; }
.ll_background-s-200{ background-color: <?php echo $rgbS200; ?> !important; }
.ll_background-s-300{ background-color: <?php echo $rgbS300; ?> !important; }
.ll_background-s-400{ background-color: <?php echo $rgbS400; ?> !important; }
.ll_background-s-500{ background-color: <?php echo $rgbS500; ?> !important; }
.ll_background-s-600{ background-color: <?php echo $rgbS600; ?> !important; }
.ll_background-s-700{ background-color: <?php echo $rgbS700; ?> !important; }
.ll_background-s-800{ background-color: <?php echo $rgbS800; ?> !important; }
.ll_background-s-900{ background-color: <?php echo $rgbS900; ?> !important; }

.ll_background-hover:hover{ background-color: <?php echo $rgbP500; ?> !important; }
.ll_background-100-hover:hover{ background-color: <?php echo $rgbP100; ?> !important; }
.ll_background-200-hover:hover{ background-color: <?php echo $rgbP200; ?> !important; }
.ll_background-300-hover:hover{ background-color: <?php echo $rgbP300; ?> !important; }
.ll_background-400-hover:hover{ background-color: <?php echo $rgbP400; ?> !important; }
.ll_background-500-hover:hover{ background-color: <?php echo $rgbP500; ?> !important; }
.ll_background-600-hover:hover{ background-color: <?php echo $rgbP600; ?> !important; }
.ll_background-700-hover:hover{ background-color: <?php echo $rgbP700; ?> !important; }
.ll_background-800-hover:hover{ background-color: <?php echo $rgbP800; ?> !important; }
.ll_background-900-hover:hover{ background-color: <?php echo $rgbP900; ?> !important; }

.ll_background-p-hover:hover{ background-color: <?php echo $rgbP500; ?> !important; }
.ll_background-p-100-hover:hover{ background-color: <?php echo $rgbP100; ?> !important; }
.ll_background-p-200-hover:hover{ background-color: <?php echo $rgbP200; ?> !important; }
.ll_background-p-300-hover:hover{ background-color: <?php echo $rgbP300; ?> !important; }
.ll_background-p-400-hover:hover{ background-color: <?php echo $rgbP400; ?> !important; }
.ll_background-p-500-hover:hover{ background-color: <?php echo $rgbP500; ?> !important; }
.ll_background-p-600-hover:hover{ background-color: <?php echo $rgbP600; ?> !important; }
.ll_background-p-700-hover:hover{ background-color: <?php echo $rgbP700; ?> !important; }
.ll_background-p-800-hover:hover{ background-color: <?php echo $rgbP800; ?> !important; }
.ll_background-p-900-hover:hover{ background-color: <?php echo $rgbP900; ?> !important; }

.ll_background-s-hover:hover{ background-color: <?php echo $rgbS500; ?> !important; }
.ll_background-s-100-hover:hover{ background-color: <?php echo $rgbS100; ?> !important; }
.ll_background-s-200-hover:hover{ background-color: <?php echo $rgbS200; ?> !important; }
.ll_background-s-300-hover:hover{ background-color: <?php echo $rgbS300; ?> !important; }
.ll_background-s-400-hover:hover{ background-color: <?php echo $rgbS400; ?> !important; }
.ll_background-s-500-hover:hover{ background-color: <?php echo $rgbS500; ?> !important; }
.ll_background-s-600-hover:hover{ background-color: <?php echo $rgbS600; ?> !important; }
.ll_background-s-700-hover:hover{ background-color: <?php echo $rgbS700; ?> !important; }
.ll_background-s-800-hover:hover{ background-color: <?php echo $rgbS800; ?> !important; }
.ll_background-s-900-hover:hover{ background-color: <?php echo $rgbS900; ?> !important; }



/* Botão padão */
.btn-lliure{
    color: #fff !important;
    background-color: <?php echo $rgbP500; ?> !important;
    border-color: <?php echo $rgbP600; ?> !important;
}

.btn-lliure:focus,
.btn-lliure.focus {
    color: #fff !important;
    background-color: <?php echo $rgbS600; ?> !important;
    border-color: <?php echo $rgbS700; ?> !important;
}

.btn-lliure:hover {
    color: #fff !important;
    background-color: <?php echo $rgbS600; ?> !important;
    border-color: <?php echo $rgbS700; ?> !important;
}

.btn-lliure:active,
.btn-lliure.active,
.open > .dropdown-toggle.btn-lliure {
    color: #fff !important;
    background-color: <?php echo $rgbP600; ?> !important;
    border-color: <?php echo $rgbS700; ?> !important;
}

.btn-lliure:active:hover,
.btn-lliure.active:hover,
.open > .dropdown-toggle.btn-lliure:hover,
.btn-lliure:active:focus,
.btn-lliure.active:focus,
.open > .dropdown-toggle.btn-lliure:focus,
.btn-lliure:active.focus,
.btn-lliure.active.focus,
.open > .dropdown-toggle.btn-lliure.focus {
    color: #fff !important;
    background-color: <?php echo $rgbS700; ?> !important;
    border-color: <?php echo $rgbS800; ?> !important;
}

.btn-lliure:active,
.btn-lliure.active,
.open > .dropdown-toggle.btn-lliure {
    background-image: none; !important
}

.btn-lliure.disabled:hover,
.btn-lliure[disabled]:hover,
fieldset[disabled] .btn-lliure:hover,
.btn-lliure.disabled:focus,
.btn-lliure[disabled]:focus,
fieldset[disabled] .btn-lliure:focus,
.btn-lliure.disabled.focus,
.btn-lliure[disabled].focus,
fieldset[disabled] .btn-lliure.focus {
    background-color: <?php echo $rgbP500; ?> !important;
    border-color: <?php echo $rgbP600; ?> !important;
}

.btn-lliure .badge {
    color: <?php echo $rgbS100; ?> !important;
    background-color: <?php echo $rgbS800; ?> !important;
}



/* Botão primario padão */
.btn-lliure-p{
    color: #fff;
    background-color: <?php echo $rgbP500; ?>;
    border-color: <?php echo $rgbP600; ?>;
}

.btn-lliure-p:focus,
.btn-lliure-p.focus {
    color: #fff;
    background-color: <?php echo $rgbP600; ?>;
    border-color: <?php echo $rgbP700; ?>;
}

.btn-lliure-p:hover {
    color: #fff;
    background-color: <?php echo $rgbP600; ?>;
    border-color: <?php echo $rgbP700; ?>;
}

.btn-lliure-p:active,
.btn-lliure-p.active,
.open > .dropdown-toggle.btn-lliure-p {
    color: #fff;
    background-color: <?php echo $rgbP600; ?>;
    border-color: <?php echo $rgbP700; ?>;
}

.btn-lliure-p:active:hover,
.btn-lliure-p.active:hover,
.open > .dropdown-toggle.btn-lliure-p:hover,
.btn-lliure-p:active:focus,
.btn-lliure-p.active:focus,
.open > .dropdown-toggle.btn-lliure-p:focus,
.btn-lliure-p:active.focus,
.btn-lliure-p.active.focus,
.open > .dropdown-toggle.btn-lliure-p.focus {
    color: #fff;
    background-color: <?php echo $rgbP700; ?>;
    border-color: <?php echo $rgbP800; ?>;
}

.btn-lliure-p:active,
.btn-lliure-p.active,
.open > .dropdown-toggle.btn-lliure-p {
    background-image: none;
}

.btn-lliure-p.disabled:hover,
.btn-lliure-p[disabled]:hover,
fieldset[disabled] .btn-lliure-p:hover,
.btn-lliure-p.disabled:focus,
.btn-lliure-p[disabled]:focus,
fieldset[disabled] .btn-lliure-p:focus,
.btn-lliure-p.disabled.focus,
.btn-lliure-p[disabled].focus,
fieldset[disabled] .btn-lliure-p.focus {
    background-color: <?php echo $rgbP500; ?>;
    border-color: <?php echo $rgbP600; ?>;
}

.btn-lliure-p .badge {
    color: <?php echo $rgbP100; ?>;
    background-color: <?php echo $rgbP800; ?>;
}



/* Botão secundario padão */
.btn-lliure-s{
    color: #fff;
    background-color: <?php echo $rgbS500; ?>;
    border-color: <?php echo $rgbS600; ?>;
}

.btn-lliure-s:focus,
.btn-lliure-s.focus {
    color: #fff;
    background-color: <?php echo $rgbS600; ?>;
    border-color: <?php echo $rgbS700; ?>;
}

.btn-lliure-s:hover {
    color: #fff;
    background-color: <?php echo $rgbS600; ?>;
    border-color: <?php echo $rgbS700; ?>;
}

.btn-lliure-s:active,
.btn-lliure-s.active,
.open > .dropdown-toggle.btn-lliure-s {
    color: #fff;
    background-color: <?php echo $rgbS600; ?>;
    border-color: <?php echo $rgbS700; ?>;
}

.btn-lliure-s:active:hover,
.btn-lliure-s.active:hover,
.open > .dropdown-toggle.btn-lliure-s:hover,
.btn-lliure-s:active:focus,
.btn-lliure-s.active:focus,
.open > .dropdown-toggle.btn-lliure-s:focus,
.btn-lliure-s:active.focus,
.btn-lliure-s.active.focus,
.open > .dropdown-toggle.btn-lliure-s.focus {
    color: #fff;
    background-color: <?php echo $rgbS700; ?>;
    border-color: <?php echo $rgbS800; ?>;
}

.btn-lliure-s:active,
.btn-lliure-s.active,
.open > .dropdown-toggle.btn-lliure-s {
    background-image: none;
}

.btn-lliure-s.disabled:hover,
.btn-lliure-s[disabled]:hover,
fieldset[disabled] .btn-lliure-s:hover,
.btn-lliure-s.disabled:focus,
.btn-lliure-s[disabled]:focus,
fieldset[disabled] .btn-lliure-s:focus,
.btn-lliure-s.disabled.focus,
.btn-lliure-s[disabled].focus,
fieldset[disabled] .btn-lliure-s.focus {
    background-color: <?php echo $rgbS500; ?>;
    border-color: <?php echo $rgbS600; ?>;
}

.btn-lliure-s .badge {
    color: <?php echo $rgbS100; ?>;
    background-color: <?php echo $rgbS800; ?>;
}




html{
    display: block;
    height: 100% !important;
    width: 100% !important;
}

body{
    position: relative;
    display: block;
    min-height: 100%  !important;
    width: 100% !important;
}

textarea{
    resize: vertical;
}

#lliurelogo{}

#lliurelogo circle,
#lliurelogo ellipse,
#lliurelogo image,
#lliurelogo line,
#lliurelogo mesh,
#lliurelogo path,
#lliurelogo polygon,
#lliurelogo polyline,
#lliurelogo rect,
#lliurelogo text,
#lliurelogo tspan,
#lliurelogo g{ fill: <?php echo $rgbP500; ?>; }

#lliurelogo.color-white circle,
#lliurelogo.color-white ellipse,
#lliurelogo.color-white image,
#lliurelogo.color-white line,
#lliurelogo.color-white mesh,
#lliurelogo.color-white path,
#lliurelogo.color-white polygon,
#lliurelogo.color-white polyline,
#lliurelogo.color-white rect,
#lliurelogo.color-white text,
#lliurelogo.color-white tspan,
#lliurelogo.color-white g{ fill: #fff; }

#lliurelogo.color-black circle,
#lliurelogo.color-black ellipse,
#lliurelogo.color-black image,
#lliurelogo.color-black line,
#lliurelogo.color-black mesh,
#lliurelogo.color-black path,
#lliurelogo.color-black polygon,
#lliurelogo.color-black polyline,
#lliurelogo.color-black rect,
#lliurelogo.color-black text,
#lliurelogo.color-black tspan,
#lliurelogo.color-black g{ fill: #000; }

#ll_topo {

    /* background:
        radial-gradient(100% 100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        radial-gradient(  0  100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        linear-gradient(to bottom, <?php echo $rgbP500; ?> 0%,<?php echo $rgbP500; ?> 100%);

    background:
        -ms-radial-gradient(100% 100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        -ms-radial-gradient(  0  100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        linear-gradient(to bottom, <?php echo $rgbP500; ?> 0%,<?php echo $rgbP500; ?> 100%);

    background:
        -moz-radial-gradient(100% 100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        -moz-radial-gradient(  0  100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        linear-gradient(to bottom, <?php echo $rgbP500; ?> 0%,<?php echo $rgbP500; ?> 100%);

    background:
        -o-radial-gradient(100% 100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        -o-radial-gradient(  0  100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        linear-gradient(to bottom, <?php echo $rgbP500; ?> 0%,<?php echo $rgbP500; ?> 100%);

    background:
        -webkit-radial-gradient(100% 100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        -webkit-radial-gradient(  0  100%, rgba(0,0,0,0) 68%, <?php echo $rgbP500; ?> 73%),
        linear-gradient(to bottom, <?php echo $rgbP500; ?> 0%,<?php echo $rgbP500; ?> 100%);

    background-position:        left bottom, right bottom, left top;
    -webkit--background-size:   10px 6px, 10px 6px, 100% calc(100% - 6px);
    -moz-background-size:       10px 6px, 10px 6px, 100% calc(100% - 6px);
    -ms-background-size:        10px 6px, 10px 6px, 100% calc(100% - 6px);
    -o-background-size:         10px 6px, 10px 6px, 100% calc(100% - 6px);
    background-size:            10px 6px, 10px 6px, 100% calc(100% - 6px);
    background-repeat:          no-repeat; */

    background-color: <?php echo $rgbP500; ?>;
    margin: 0 !important;
    border: none;
    position: relative;
    z-index: 1039;
}


#ll_topo > .navbar{
    border: none;
    border-radius: 0;
    background: none;
    min-height: auto;
}

/*
#ll_topo > .navbar > .navbar-header{
    position: relative;
}

#ll_topo > .navbar > .navbar-header > .navbar-brand{
    height: 34px;
    padding: 8px 10px;
    position: relative;
    margin-left: 0;
    color: #fff;
}


#ll_topo > .navbar > .navbar-header > .navbar-toggle{
    margin: 0;
    padding: 0 15px;
    position: absolute;
    top: 0; bottom: 0;
    left: 0; right: 0;

    background: none !important;
    border: none !important;
    width: 100%;
    color: #fff;
}

!*#ll_topo .btn-add-desktop{
    padding: 5px;
    margin: 5px;
}*!*/







#ll_rodape_widht{
    display: none;
}

#ll_rodape{
    height: 25px;
    display: block;
    z-index: 400;
}

@media (min-width: 768px) {
    #ll_rodape_widht{
        display: block;
        width: 100%;
        height: 25px;
        opacity: 0;
        z-index: -1;
    }
    #ll_rodape{
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
    }
}

#ll_rodape > div > a {
    line-height: 25px;
}