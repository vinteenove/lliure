<?php
namespace AdminLTE\layout;

use League\Event\HasEventName;

class Content implements HasEventName{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function eventName(): string
    {
        return $this->name;
    }
}