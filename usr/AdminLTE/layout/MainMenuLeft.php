<?php
global $_ll, $personaData, $mainMenuLeft;
use LliurePanel\ll;

$mainMenuLeft = new \Menu\Itens\Menu(\Menu\Itens\Menu::NavBar);

//$mainMenu->children(new \Menu\Itens\PushMenu('PushMenu', 'PushMenu'));

if(ll::valida() && $_ll['operation_type'] == 'app'){
    $mainMenuLeft->children(new \Menu\Itens\Text('mainMenuAddDesktop', 'addDesktop', '<button type="button" class="btn btn-navbar btn-sm btn-lliure btn-add-desktop"><i class="fa fa-desktop" aria-hidden="true"></i></button>'));
}

ob_start(); ?>
<div id="lliurelogo" class="color-white" style="width: 60px;"><?php require ll::baseDir() . $personaData['logo']; ?></div>
<?php $personaLogo = ob_get_clean();
$mainMenuLeft->children(new \Menu\Itens\Brand('mainMneuBradImage', 'lliure', $personaLogo));
