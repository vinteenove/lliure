<?php


namespace Cropperjs;


class Cropperjs implements \Persona\PersonaBuilder
{

    public static function build(): void
    {
        // TODO: Implement build() method.
        \Persona\Persona::style(__DIR__ . '/cropper.css');
        \Persona\Persona::script(__DIR__ . '/cropper.js');
    }
}