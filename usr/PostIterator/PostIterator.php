<?php

namespace PostIterator;

use ArrayIterator;

class PostIterator extends ArrayIterator{
    public function __construct(){
        parent::__construct($_POST, ArrayIterator::STD_PROP_LIST);
    }

    public function isError(): bool{
        return (isset($_SERVER["CONTENT_LENGTH"]) && $_SERVER["CONTENT_LENGTH"] > (1024 * 1024 * ((int) ini_get("post_max_size"))));
     }
}