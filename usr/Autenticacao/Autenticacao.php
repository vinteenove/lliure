<?php
namespace Autenticacao;
use DB\DB;

class Autenticacao extends DB {

    public function __construct(){
        parent::__construct(PREFIXO . 'lliure_autenticacao');
    }

    public function get($id = null, $ord = [], $pg = null, $pp = null){
        list($w, $o, $l) = self::WhereOrderLimit($id, $ord, $pg, $pp);
        return ($this->select("SELECT * FROM {$this}{$w}{$o}{$l}"));
    }

    public function set($dados){
        $this->insert($this->fil($dados));
        return $this->insert_id();
    }

    public function upd($dados, $where){
        $this->update($this->fil($dados), $where);
        return ((isset($where['id']))? $where['id']: null);
    }

    public function del($dados, $where){
        $this->delete($this->fil($dados), $where);
        return ((isset($where['id']))? $where['id']: null);
    }

    private function fil($dados){
        return array_intersect_key(array_merge(($k = array_intersect_key([

            'id'           => '',
            'login'        => '',
            'nome'         => '',
            'grupo'        => '',
            'tema'         => '',
            'ultimoacesso' => '',
            'cadastro'     => '',

        ], $dados)), $dados), $k);
    }

    /**
     * @param null|string|array|double $id  query para o WHERE caso um numero intemde que é o id
     * @param array                    $ord contem as colunas para ordernar
     * @param null                     $pg  PaGina
     * @param null                     $pp  Por Pagina
     * @return array array($where, $limit, $order)
     */
    private static function WhereOrderLimit($id = null, $ord = [], $pg = null, $pp = null){
        $id = (($id === null || is_string($id) || is_array($id))? $id: ['id' => $id]);
        $where = [];
        if(is_array($id)){
            foreach(parent::antiInjection($id) as $col => $val){
                $val = ((is_array($val))? $val: [$val]);
                foreach($val as $comp => $value){
                    $comp = ((is_numeric($comp))? '==': $comp);
                    $dados[$col] = $value;
                    $where[] = parent::prepare("{$col} [{$comp} {$col}]", $dados);
                }
            }
            $where = implode(' AND ', $where);
        }else
            $where = (string) $id;
        if(!empty($where)) $where = " WHERE ({$where})";

        $order = [];
        foreach($ord as $col => $sent) $order [] = ((is_numeric($col))? '': $col . ' ') . $sent;
        $order = ((!empty($order))? ' ORDER BY ' . implode($order, ', '): '');

        $pg = ($pg !== null? max(1, $pg): $pg);
        $pp = ($pp !== null? max(1, $pp): $pp);
        $limit = ($pg !== null && $pp !== null? ' LIMIT ' . ($pp * ($pg - 1)) . ', ' . ($pp): '');

        return [$where, $order, $limit];
    }
}