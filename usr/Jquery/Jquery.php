<?php

namespace Jquery;


use Persona\Persona;

class Jquery implements \Persona\PersonaBuilder
{

    public static function build(): void
    {
        // TODO: Implement build() method.
        Persona::script(__DIR__ . '/jquery-1.12.0.js', 0, Persona::HEADER_LOCATION);
    }
}