<?php
namespace ClassBody;

use League\Event\HasEventName;

class ClassBody implements HasEventName
{
    private array $classArray = [];
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function eventName(): string
    {
        return $this->name;
    }

    public function addClass(array $class){
        $this->classArray = array_unique(array_merge($this->classArray, $class));
    }

    public function removeClass(array $class){
        $this->classArray = array_diff($this->classArray, $class);
    }

    public function getClass(){
        return $this->classArray;
    }
}