<?php
namespace Color;

class Color{

    private $color;

    public function __construct(array $arrayRGB){
        if(!isset($arrayRGB[0], $arrayRGB[1], $arrayRGB[2]))
            trigger_error('Cor mau formada. Correto [{vernmelho decimal}, {verde decimal}, {azul decimal}]', E_USER_ERROR);
        $this->color = $arrayRGB;
    }

    public static function hex($stringHex){
        if($e = preg_match('/^#?([0-9abcdef]{2})([0-9abcdef]{2})([0-9abcdef]{2})$|^#?([0-9abcdef])([0-9abcdef])([0-9abcdef])$/i', trim($stringHex), $m))
            return new self([
                hexdec($m[1] . @$m[4] . @$m[4]),
                hexdec($m[2] . @$m[5] . @$m[5]),
                hexdec($m[3] . @$m[6] . @$m[6]),
            ]);
        return false;
    }

    public function get(){
        return $this->color;
    }

    public function luminancia(){
        $cor = $this->get();
        return ((
            ($cor[0] * 0.30) +
            ($cor[1] * 0.59) +
            ($cor[2] * 0.11)
        ));
    }

    public function luminanciaCor(){
        $l = $this->luminancia();
        return new self([$l, $l, $l]);
    }

    public function transpor(Color $corDestino, $porcentagem){
        $r = [];
        $porcentagem = min(1, max(0, $porcentagem));
        $de = $this->get();
        $ate = $corDestino->get();
        foreach($ate as $k => $v) $r[$k] = ((int) ((($ate[$k] - $de[$k]) * $porcentagem) + $de[$k]));
        return new self($r);
    }

    public function toRGB(){
        return 'rgb('. implode(', ', $this->get()). ')';
    }

    function toRGBA($alfa){
        $alfa = max(1, min(0, $alfa));
        return 'rgba('. implode(', ', $this->get()). ', ' . $alfa .  ')';
    }

    function toHEX($fence = true){
        $cor = $this->get();
        return ((!!$fence)? '#': '') .
            str_pad(dechex($cor[0]), 2, '0', STR_PAD_LEFT) .
            str_pad(dechex($cor[1]), 2, '0', STR_PAD_LEFT) .
            str_pad(dechex($cor[2]), 2, '0', STR_PAD_LEFT);
    }

}