<?php


namespace GoogleBlockly;


class GoogleBlockly implements \Persona\PersonaBuilder
{

    public static function build(): void
    {
        // TODO: Implement build() method.
        \Persona\Persona::script(__DIR__ . '/blockly_compressed.js');
        \Persona\Persona::script(__DIR__ . '/blocks_compressed.js');
        \Persona\Persona::script(__DIR__ . '/javascript_compressed.js');
        \Persona\Persona::script(__DIR__ . '/msg/js/pt-br.js');
    }
}