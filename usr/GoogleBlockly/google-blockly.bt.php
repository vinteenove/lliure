<?php
use Persona\Persona;

Persona::add(__DIR__ . '/blockly_compressed.js');
Persona::add(__DIR__ . '/blocks_compressed.js');
Persona::add(__DIR__ . '/javascript_compressed.js');
Persona::add(__DIR__ . '/msg/js/pt-br.js');