<?php

namespace Menu;

use OutOfBoundsException;

trait MenuIterator {

    /**
     * @var MenuParentInterface|null
     */
    private ?MenuParentInterface $parent = null;

    /**
     * @var MenuIteratorInterface|null
     */
    private ?MenuData $before = null;

    /**
     * @var MenuIteratorInterface|null
     */
    private ?MenuData $after = null;

    /**
     * @var MenuIteratorInterface|null
     */
    private ?MenuData $current = null;



    /**
     * @param MenuParentInterface|null $newParent
     * @return MenuParentInterface
     */
    public function parent(?MenuParentInterface $newParent = null): MenuParentInterface{
        if($newParent !== null)
            $this->setParent($newParent);
        return $this->getParent();
    }

    /**
     * @param MenuData $new
     * @return MenuData
     */
    public function prepend(MenuData $new): MenuData{
        $first = $this->first();

        $new->setParent($this->getParent());
        $new->setAfter($first);
        $first->setBefore($new);

        return $this;
    }

    /**
     * @param MenuData $new
     * @return MenuData
     */
    public function append(MenuData $new): MenuData{
        $last = $this->last();

        $new->setParent($this->getParent());
        $new->setBefore($last);
        $last->setAfter($new);

        return $this;
    }

    /**
     * @param MenuData|null $new
     * @return MenuData|null
     */
    public function before(?MenuData $new = null): ?MenuData{
        if($new instanceof MenuData){
            $prev = $this->getBefore();

            if($prev) $prev->after($new);
            $this->setBefore($new);

            $new->setBefore($prev);
            $new->setAfter($this);

            $new->setParent($this->getParent());

            return $this;
        }
        return $this->getBefore();
    }

    /**
     * @param MenuData|null $new
     * @return MenuIteratorInterface|null
     */
    public function after(?MenuData $new = null): ?MenuData{
        if($new instanceof MenuData){
            $next = $this->getAfter();

            if($next) $next->before($new);
            $this->setAfter($new);

            $new->before($this);
            $new->after($next);

            $new->parent($this->getParent());

            return $this;
        }
        return $this->getAfter();
    }

    /**
     * @param string $key
     * @return MenuIteratorInterface
     */
    public function findByKey(string $key): ?MenuData{
        $current = $this->first();

        do{
            if($current->getKey() == $key) return $current;
        }while(($current = $current->getAfter()) !== null);

        return null;
    }

    /**
     * @return $this|MenuIteratorInterface
     */
    public function first(): MenuData{
        if($this->getBefore() !== null)
            return $this->getBefore()->first();
        return $this;
    }

    /**
     * @return $this|MenuIteratorInterface
     */
    public function last(): MenuData{
        if($this->getAfter() !== null)
            return $this->getAfter()->last();
        return $this;
    }

    /**
     * @return MenuIteratorInterface|null
     */
    public function remove(): ?MenuData{
        $prev = $this->getBefore();
        $next = $this->getAfter();

        if($prev) $prev->setAfter($next);
        if($next) $next->setBefore($prev);

        $this->setParent();
        $this->setBefore();
        $this->setAfter();

        if($prev) return $prev->first();
        if($next) return $next->first();
        return null;
    }

    /**
     * @param MenuIteratorInterface $destiny
     * @return MenuIteratorInterface
     */
    public function exchange(MenuData $destiny): MenuData{
        $origin = $this;

        $originParent = $origin->getParent();
        $destinyParent = $destiny->getParent();
        $origin->setParent($destinyParent);
        $destiny->setParent($originParent);

        $originBefore = $origin->getBefore();
        $originAfter = $origin->getAfter();

        $destinyBefore = $destiny->getBefore();
        $destinyAfter = $destiny->getAfter();

        if($origin->getAfter() === $destiny){
            $destinyBefore = $destiny;
            $originAfter = $origin;
        }

        if($origin->getBefore() === $destiny){
            $destinyAfter = $destiny;
            $originBefore = $origin;
        }

        $origin->setBefore($destinyBefore);
        $origin->setAfter($destinyAfter);
        if($originAfter) $originAfter->setBefore($destiny);
        if($originBefore) $originBefore->setAfter($destiny);

        $destiny->setBefore($originBefore);
        $destiny->setAfter($originAfter);
        if($destinyAfter) $destinyAfter->setBefore($origin);
        if($destinyBefore) $destinyBefore->setAfter($origin);

        return $this;
    }





    /**
     * @param MenuParentInterface|null $parent
     */
    protected function setParent(MenuParentInterface &$parent = null): void{
        foreach($this as $current) $current->parent = $parent;
    }

    /**
     * @return MenuParentInterface|null
     */
    protected function &getParent(): ?MenuParentInterface{
        return $this->parent;
    }

    /**
     * @param MenuData|null $after
     */
    protected function setAfter(?MenuData $after = null): void{
        $this->after = $after;
    }

    /**
     * @return MenuData|null
     */
    protected function getAfter(): ?MenuData{
        return $this->after;
    }

    /**
     * @param MenuData|null $before
     */
    protected function setBefore(?MenuData $before = null): void{
        $this->before = $before;
    }

    /**
     * @return MenuData|null
     */
    protected function getBefore(): ?MenuData{
        return $this->before;
    }





    public function asort(){
        $this->uasort(function(string $target, string $compare): int{
            return strcmp($target, $compare);
        });
    }

    public function ksort(){
        $this->uksort(function(string $target, string $compare): int{
            return strcmp($target, $compare);
        });
    }

    public function natcasesort(){
        $this->uasort(function(string $target, string $compare): int{
            return strnatcasecmp($target, $compare);
        });
    }

    public function natsort(){
        $this->uasort(function(string $target, string $compare): int{
            return strnatcmp($target, $compare);
        });
    }

    public function uasort($cmp_function){
        $this->usort(function(MenuData $target, MenuData $compare) use ($cmp_function): int{
            return $cmp_function($target->label, $compare->label);
        });
    }

    public function uksort($cmp_function){
        $this->usort(function(MenuData $target, MenuData $compare) use ($cmp_function): int{
            return $cmp_function($target->key, $compare->key);
        });
    }

    public function usort(callable $cmp_function){
        $current = $this->getFirst();
        while(($current !== null) && ($target = $current->getAfter()) !== null){
            $moved = false;
            while(($compare = $target->getBefore()) !== null){
                $result = $cmp_function($target, $compare);
                if($result < 0){
                    $moved = true;
                    $target->exchange($compare);
                }else{
                    if(!$moved) $current = $current->getAfter();
                    continue 2;
                }
            }
        }
    }



    public function rewind(){
        $this->current = $this->first();
    }

    public function valid(){
        return !!($this->current);
    }

    public function current(){
        return $this->current;
    }

    public function key(){
        return $this->current->getKey();
    }

    public function next(){
        $this->current = (($this->current)? $this->current->after(): null);
    }

    public function seek($position){
        if($pos = $this->findByKey($position)){
            $this->current = $pos;
        }else{
            throw new OutOfBoundsException("invalid seek position ($position)");
        }
    }

    public function count(){
        $current = $this->getFirst();
        $count = 0;
        do{
            $count++;
        }while(($current = $current->getAfter()) !== null);
        return $count;
    }



    public function __clone(){
        $this->current = null;
    }

    public function __destruct(){
        $this->remove();
    }


}