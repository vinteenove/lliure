<?php

namespace Menu;

use League\Plates\Engine;

trait MenuTemplate{
    private Engine $template;

    protected function setTemplate(string $namespace){
        $this->template = new Engine($namespace);
    }

    public function getTemplate(): Engine{
        return $this->template;
    }
}