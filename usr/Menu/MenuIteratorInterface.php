<?php

namespace Menu;

use Iterator;
use Countable;
use SeekableIterator;

interface MenuIteratorInterface extends Iterator, Countable, SeekableIterator {

    public function getKey(): string;

    public function parent(?MenuParentInterface $newParent = null): MenuParentInterface;

    public function prepend(MenuData $new): MenuData;

    public function append(MenuData $new): MenuData;

    public function before(?MenuData $new = null): ?MenuData;

    public function after(?MenuData $new = null): ?MenuData;

    public function findByKey(string $key): ?MenuData;

    public function first(): MenuData;

    public function last(): MenuData;

    public function remove(): ?MenuData;

    public function exchange(MenuData $destiny): MenuData;


    public function asort();

    public function ksort();

    public function natcasesort();

    public function natsort();

    public function uasort(callable $cmp_function);

    public function uksort(callable $cmp_function);

    public function usort(callable $cmp_function);

}