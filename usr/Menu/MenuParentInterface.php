<?php

namespace Menu;

interface MenuParentInterface{

    /**
     * @param MenuIteratorInterface|null $newChild
     * @return MenuIteratorInterface|null
     */
    public function children(?MenuIteratorInterface $newChild = null): ?MenuIteratorInterface;
}