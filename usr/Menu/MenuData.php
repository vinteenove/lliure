<?php

namespace Menu;

use Error;
use League\Plates\Engine;

/**
 * Class
 * MenuData
 * @package Menu
 */
abstract class MenuData implements MenuIteratorInterface {
    use MenuIterator;

    protected static int $index = 0;
    protected string $key;
    protected string $label;
    protected array $attrs;
    protected bool $active = false;

    /**
     * MenuData
     * constructor.
     * @param string $key
     * @param string $label
     * @param array  $attrs
     */
    public function __construct(string $key, string $label, array $attrs = []){
        $this->key = $key;
        $this->label = $label;
        $this->attrs = $attrs;
    }

    public function active(bool $ascend = false){
        $this->setActive(true, $ascend);
    }

    public function deactivate(bool $ascend = false){
        $this->setActive(false, $ascend);
    }

	public function isActive(): bool{
		return $this->active;
	}

    private function setActive(bool $status, bool $ascend = false){
        $this->active = $status;
        if($ascend){
            $parent = $this->parent();
            if($parent instanceof MenuData){
				$status? $parent->active(true): $parent->deactivate(true);
            }
        }
    }

    protected function template(): Engine{
        if($this instanceof MenuTemplateInterface){
            return $this->getTemplate();
        }

        $parent = $this->parent();

        if($parent instanceof MenuTemplateInterface){
            return $parent->getTemplate();
        }

        if($parent instanceof MenuData){
            return $parent->template();
        }

        throw new MenuException('Template not defined');
    }

    /**
     * @return string
     */
    public function getKey(): string{
        return $this->key;
    }

    abstract public function __toString();
}