<?php

namespace Menu;

use League\Plates\Engine;

interface MenuTemplateInterface{
    function getTemplate(): Engine;
}