<?php

namespace Menu\Itens;

use Menu\MenuParent;
use Menu\MenuParentInterface;
use Menu\MenuTemplate;
use Menu\MenuTemplateInterface;
use LliurePanel\ll;

class Sidebar implements MenuParentInterface, MenuTemplateInterface{
    use MenuParent;
    use MenuTemplate;

    const NavBar = __DIR__.'/../View/NavBar';

    private string $subItens = '';
    private array $attrs;
    private string $attributes;

    public function __construct(string $view, array $attrs = []){
        $this->setTemplate($view);
        $this->attrs = $attrs;
        $this->attributes = '';
    }

    public function __toString(): string{
        $this->attributes = \Helpers\HtmlHelper::attr($this->attrs);
        if($this->children() !== null) foreach($this->children() as $child) $this->subItens .= $child;
        return $this->getTemplate()->render('Sidebar', get_object_vars($this));
    }
}