<?php

namespace Menu\Itens;

use LliurePanel\ll;
use Menu\MenuData;

class Brand extends MenuData{

    private string $content = '';
    private string $href = '';

    public function __construct(string $key, string $label, string $content, string $href = '', array $attrs = []){
        parent::__construct($key, $label, $attrs);
        $this->content = $content;
        $this->href = $href;
    }

    public function __toString(): string{
        return $this->template()->render('Brand', get_object_vars($this));
    }

}