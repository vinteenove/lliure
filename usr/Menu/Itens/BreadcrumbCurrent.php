<?php

namespace Menu\Itens;

use Menu\MenuData;
use Menu\MenuParent;
use Menu\MenuParentInterface;

class BreadcrumbCurrent extends MenuData{

    public function __construct(string $key, string $label, array $attrs = []){
        parent::__construct($key, $label, $attrs);
    }

    public function __toString(): string{
        return $this->template()->render('BreadcrumbCurrent', get_object_vars($this));
    }

}