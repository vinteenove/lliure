<?php

namespace Menu\Itens;

use LliurePanel\ll;
use Menu\MenuData;
use Menu\MenuIterator;
use Menu\MenuIteratorInterface;

class Link extends MenuData implements MenuIteratorInterface{
    use MenuIterator;

    protected string $href;
    private string $attributes;

    public function __construct(string $key, string $label, string $href, array $attrs = []){
        parent::__construct($key, $label, $attrs);
        $this->attributes = '';
        $this->href = $href;
    }

    public function __toString(): string{
        $attributes = array_merge(['href' => $this->href, 'class' => ''], $this->attrs, ['href' => $this->href]);
        $attributes['class'] = 'nav-link' . (($this->active)? ' active': ''). ((empty($attributes['class']))? '': ' ' . $attributes['class']);
        $this->attributes = \Helpers\HtmlHelper::attr($attributes);
        return $this->template()->render('Link', get_object_vars($this));
    }

}