<?php

namespace Menu\Itens;

use Menu\MenuData;

class Search extends MenuData{

    private string $placeholder;
    private string $name;
    private string $method;

    public function __construct(string $key, string $label, string $action, array $attrs = []){
        $this->placeholder = ((isset($attrs['placeholder']))? $attrs['placeholder']: $label);
        $this->name = ((isset($attrs['name']))? $attrs['name']: $key);
        $this->method = ((isset($attrs['method']))? $attrs['method']: 'post');
        $attrs = array_diff_key($attrs, ['placeholder' => '', 'name' => '', 'post' => '', 'action' => '']);
        $attrs['action'] = $action;
        parent::__construct($key, $label, $attrs);
    }

    public function __toString(): string{
        return $this->template()->render('Search', get_object_vars($this));
    }

}