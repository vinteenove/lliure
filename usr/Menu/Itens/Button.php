<?php

namespace Menu\Itens;

use LliurePanel\ll;
use Menu\MenuData;

class Button extends MenuData{

    private string $attributes;

    public function __construct(string $key, string $label, string $type = 'button', array $attrs = []){
        parent::__construct($key, $label, $attrs);

        $attributes = array_merge([
            'type'  => $type,
            'class' => '',
        ], $attrs, [
            'type'  => $type,
        ]);
        $attributes['class'] = 'btn' . ((empty($image['class']))? '': ' ' . $attributes['class']);

        $this->attributes = \Helpers\HtmlHelper::attr($attributes);
    }

    public function __toString(): string{
        return $this->template()->render('Button', get_object_vars($this));
    }

}