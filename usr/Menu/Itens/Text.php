<?php

namespace Menu\Itens;

use Menu\MenuData;

class Text extends MenuData{

    private string $text;

    public function __construct(string $key, string $label, string $text, array $attrs = []){
        parent::__construct($key, $label, $attrs);
        $this->text = $text;
    }

    public function __toString(): string{
        return $this->template()->render('Text', get_object_vars($this));
    }

}