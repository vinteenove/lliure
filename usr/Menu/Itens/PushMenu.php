<?php

namespace Menu\Itens;

use Menu\MenuData;

class PushMenu extends MenuData{

    public function __construct(string $key, string $label, array $attrs = []){
        parent::__construct($key, $label, $attrs);
    }

    public function __toString(): string{
        return $this->template()->render('PushMenu', get_object_vars($this));
    }

}