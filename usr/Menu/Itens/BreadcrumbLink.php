<?php

namespace Menu\Itens;

use Menu\MenuData;
use Menu\MenuParent;
use Menu\MenuParentInterface;

class BreadcrumbLink extends MenuData{

    private string $href;

    public function __construct(string $key, string $label, string $href, array $attrs = []){
        parent::__construct($key, $label, $attrs);
        $this->href = $href;
    }

    public function __toString(): string{
        return $this->template()->render('BreadcrumbLink', get_object_vars($this));
    }

}