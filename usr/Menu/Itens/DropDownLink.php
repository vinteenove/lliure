<?php

namespace Menu\Itens;

use Menu\MenuData;

class DropDownLink extends MenuData{

    private string $href;

    public function __construct(string $key, string $label, string $href, array $attrs = []){
        parent::__construct($key, $label, $attrs);
        $this->href = $href;
    }

    public function __toString(): string{
        return $this->template()->render('DropDownLink', get_object_vars($this));
    }

}