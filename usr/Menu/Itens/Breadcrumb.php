<?php

namespace Menu\Itens;

use Menu\MenuData;
use Menu\MenuParent;
use Menu\MenuParentInterface;

class Breadcrumb extends MenuData implements MenuParentInterface{
    use MenuParent;

    private string $itens = '';

    public function __construct(string $key, string $label, array $attrs = []){
        parent::__construct($key, $label, $attrs);
    }

    public function __toString(): string{
        if($this->children()) foreach($this->children() as $child) $this->itens .= $child;
        return $this->template()->render('Breadcrumb', get_object_vars($this));
    }

}