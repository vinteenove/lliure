<?php

namespace Menu\Itens;

use Menu\MenuData;

class DropDownDivider extends MenuData{
    private string $href;

    public function __construct(string $key, string $label, array $attrs = []){
        parent::__construct($key, $label, $attrs);
    }

    public function __toString(): string{
        return $this->template()->render('DropDownDivider', get_object_vars($this));
    }

}