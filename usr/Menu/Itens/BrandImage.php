<?php

namespace Menu\Itens;

use LliurePanel\ll;
use Menu\MenuData;

class BrandImage extends MenuData{

    private string $image = '';
    private string $href = '';

    public function __construct(string $key, string $label, string $image, string $href = '', array $attrs = []){
        parent::__construct($key, $label, $attrs);
        $this->href = $href;
        $image = array_merge([
            'src'   => $image,
            'alt'   => $label,
            'class' => '',
        ], $attrs, [
            'src' => $image,
        ]);
        $image['class'] = 'brand-image' . ((empty($image['class']))? '': ' ' . $image['class']);
        $this->image = \Helpers\HtmlHelper::attr($image);
    }

    public function __toString(): string{
        return $this->template()->render('BrandImage', get_object_vars($this));
    }

}