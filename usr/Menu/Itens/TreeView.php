<?php

namespace Menu\Itens;

use Menu\MenuData;
use Menu\MenuParent;
use Menu\MenuParentInterface;

class TreeView extends MenuData implements MenuParentInterface{
    use MenuParent;
    protected string $subMenu = '';

    public function __toString(){
        if($this->children()) foreach($this->children() as $child) $this->subMenu .= $child;
        return $this->template()->render('TreeView', get_object_vars($this));
    }
}