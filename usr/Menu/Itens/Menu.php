<?php

namespace Menu\Itens;

use Helpers\HtmlHelper;
use Menu\MenuParent;
use Menu\MenuParentInterface;
use Menu\MenuTemplate;
use Menu\MenuTemplateInterface;

class Menu implements MenuParentInterface, MenuTemplateInterface{
    use MenuParent;
    use MenuTemplate;

    const NavBar = __DIR__.'/../View/NavBar';

    private string $subItens = '';
    private array $attrs;
    private string $attributes;

    public function __construct(string $view, array $attrs = []){
        $this->setTemplate($view);
        $this->attrs = $attrs;
        $this->attributes = '';
    }

    public function __toString(): string{
        $this->attrs['class'] = 'navbar-nav' . ((isset($this->attrs['class']))? ' ' . $this->attrs['class']: '');
        $this->attributes = HtmlHelper::attr($this->attrs);
        if($this->children()) foreach($this->children() as $child) $this->subItens .= $child;
        return $this->getTemplate()->render('Menu', get_object_vars($this));
    }
}