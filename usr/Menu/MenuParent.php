<?php

namespace Menu;

use Menu\MenuData;

/**
 * Trait MenuParent
 * @package Menu
 */
trait MenuParent{

    /** @var ?MenuIteratorInterface $children */
    private ?MenuIteratorInterface $children = null;

    /**
     * @return MenuIteratorInterface|null
     */
    protected function getChildren(): ?MenuIteratorInterface{
        return $this->children;
    }

    /**
     * @param MenuIteratorInterface $children
     */
    protected function setChildren(MenuIteratorInterface $children): void{
        $this->children = $children;
    }


    public function children(?MenuIteratorInterface $newChild = null): ?MenuIteratorInterface{
        if($newChild instanceof MenuIteratorInterface){

            if($this->getChildren() instanceof MenuIteratorInterface)
                $this->getChildren()->append($newChild);
            else
                $this->setChildren($newChild);

            $newChild->parent($this);

            return null;
        } return $this->getChildren();
    }

}