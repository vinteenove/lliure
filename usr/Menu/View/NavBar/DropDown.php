<li class="nav-item dropdown" data-menu-key="<?php echo $key ; ?>">
    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $label; ?></a>
    <div class="dropdown-menu"><?php echo $subMenu; ?></div>
</li>