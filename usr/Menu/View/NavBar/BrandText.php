<li class="nav-item" data-menu-key="<?php echo $key ; ?>">
    <?php if(!empty($href)){ ?>
        <a href="<?php echo $href; ?>" class="navbar-brand nav-link">
            <span class="brand-text"><?php echo $label; ?></span>
        </a>
    <?php }else{ ?>
        <span class="navbar-brand nav-link">
            <span class="brand-text"><?php echo $label; ?></span>
        </span>
    <?php } ?>
</li>