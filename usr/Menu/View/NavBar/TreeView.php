<li class="nav-item has-treeview<?php echo (($active)? ' menu-open': ''); ?>" data-menu-key="<?php echo $key ; ?>">
    <a href="#" class="nav-link<?php echo (($active)? ' active': ''); ?>"><?php echo $label; ?></a>
    <?php if(!empty($subMenu)){ ?>
        <ul class="nav nav-treeview"><?php echo $subMenu; ?></ul>
    <?php } ?>
</li>