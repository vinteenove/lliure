<?php

use LliurePanel\ll;

?>

<li class="nav-item" data-menu-key="<?php echo $key ; ?>">
    <div class="px-3 py-1">
        <form class="form-inline" <?php echo \Helpers\HtmlHelper::attr($attrs); ?>>
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" name="<?php echo $name; ?>" placeholder="<?php echo $placeholder; ?>" aria-label="<?php echo $placeholder; ?>">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</li>