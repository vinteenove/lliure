<?php
use LliurePanel\ll;
use Persona\Persona;

\Jquery\Jquery::build();

Persona::add(__DIR__. '/jquery-ui.css', 'css', 1);
Persona::add(__DIR__. '/jquery-ui.structure.css', 'css', 1);
Persona::add(__DIR__. '/jquery-ui.theme.css', 'css', 1);

Persona::add(__DIR__. '/jquery-ui.js', 'js', 1);
Persona::add(__DIR__. '/jquery.ui.touch-punch.min.js', 'js', 1);