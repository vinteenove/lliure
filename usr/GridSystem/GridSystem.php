<?php


namespace GridSystem;


class GridSystem implements \Persona\PersonaBuilder
{

    public static function build(): void
    {
        \Persona\Persona::style(__DIR__. '/GridSystem.css',  1);
    }
}