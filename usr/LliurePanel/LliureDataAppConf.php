<?php

namespace LliurePanel;

use ArrayAccess;
use Helpers\HttpHelper;
use Opt\Install\LoadIni;

class LliureDataAppConf implements ArrayAccess{
	use PropertyArrayAccessTrait;

	/**
	 * @var string
	 */
	public string $nome;

	/**
	 * @var string|null
	 */
	public ?string $ico = null;

	/**
	 * @param string $type
	 * @param string $load
	 * @return LliureDataAppConf
	 * @throws \Exception
	 */
	public static function load(string $type, string $load): LliureDataAppConf{
		$configs = ['name' => $load, 'ico' => ((file_exists(HttpHelper::path(ll::baseDir() . ($f = "$type/$load/sys/ico.svg"))))? $f:((file_exists(HttpHelper::path(ll::baseDir() . ($f = "$type/$load/sys/ico.png"))))? $f: ('fa-cog')))];

		if (file_exists($appDataFile = (HttpHelper::path(ll::baseDir() . "$type/$load/lliure.ini")))){
			$ini = new LoadIni($appDataFile);
			$configs = array_merge($configs, $ini->config());
		}

        $conf = new self;
        foreach($configs as $k => $v){
            $conf->{$k} = $v;
        }

        return $conf;
    }
}