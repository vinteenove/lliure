<?php

namespace LliurePanel;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class LliureMiddleware implements \Psr\Http\Server\MiddlewareInterface {

    private string $baseDir;

    public function __construct(string $baseDir){
        $this->baseDir = $baseDir;
    }

    /** @inheritDoc */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{

        Lliure::bootstrap($this->baseDir);

        $reponse = $handler->handle($request);

        Lliure::build();

        return $reponse;

    }
}