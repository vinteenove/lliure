<?php
namespace LliurePanel;

class ll_modulo{
    private $type, $load;

    function __construct($type, $load){
        $this->type = $type;
        $this->load = $load;
    }

    public function doors($chave = null){
        $chave = ((is_null($chave))? $this->load: $chave);
        return ll::doors($this->type, $chave, $this->load);
    }
}