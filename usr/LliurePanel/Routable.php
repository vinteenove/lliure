<?php

namespace LliurePanel;

use Router\Router;

interface Routable
{
    public static function router(): Router;
}