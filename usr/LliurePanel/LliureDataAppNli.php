<?php

namespace LliurePanel;

use Helpers\HttpHelper;

class LliureDataAppNli implements \ArrayAccess
{
	use PropertyArrayAccessTrait;

    public string $pasta;
    public string $home;
    public string $onserver;
    public string $onclient;

    public static function doors(string $type, string $key, ?string $load = null): LliureDataAppNli{
        $load = ((is_null($load))? $key: $load);
        $nli = new self;

        $base = $type;
        if(strpos($load, '=') !== false){
            list($base, $load) = explode('=', $load, 2);
        }

        $load = ucfirst($load);
        $nli->pasta = "{$base}/{$load}/nli/";

        $base = HttpHelper::pathToUri(ll::baseDir());
        $base = ((empty($base))? '': "$base/");
        $k = strtolower($key);

        $nli->home = "{$base}{$k}/nli/";
        $nli->onserver = "{$base}{$k}/nli/os/";
        $nli->onclient = "{$base}{$k}/nli/oc/";

        return $nli;
    }

}









