<?php

namespace LliurePanel\Middleware;

use Helpers\HttpHelper;
use LliurePanel\Lliure;
use LliurePanel\LliureDataAppWli;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class StartAppLegacyMiddleware implements MiddlewareInterface{

	private string $raisClass;

	public function __construct(string $raisClass){
		$this->raisClass = $raisClass;
	}

	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{

		Lliure::$data->desktop = is_subclass_of(('\\Etc\\LliurePanel\\' . ((Lliure::autentica())? 'LliureIndexWLI': 'LliureIndexNLI')), $this->raisClass);

		HttpHelper::parse_get(Lliure::$data->url->query, $_GET);

		if(!Lliure::$data->desktop){
			array_shift($_GET);
		}

		Lliure::$data->setOperation(Lliure::operation($_GET));

		[$type, $key, $load] = explode('\\', $this->raisClass, 3);

		if (in_array(strtolower($type), Lliure::$data->operation_types)) {
			Lliure::$data->operation_type = strtolower($type);
			Lliure::$data->operation_key = strtolower($key);
		}

		Lliure::$data->app = LliureDataAppWli::doors($type, $key, $load);

		return $handler->handle($request);
	}
}