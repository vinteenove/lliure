<?php

namespace LliurePanel\Middleware;

use LliurePanel\Lliure;
use Lliure\Http\Message\Response;
use LliurePanel\LoggerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class NeedUserMiddleware implements MiddlewareInterface{

    private ?LoggerInterface $logger;

    public function __construct(?LoggerInterface $logger = null){
        $this->logger = $logger;
    }


    /** @inheritDoc */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{

        if (!Lliure::autentica()){
            $response = new Response();
            $response = $response->withStatus(401);
            $response = $response->withHeader('Location', Lliure::pipeline()->getNamedRoute('IndexNLI')->getQuery());
            return $response;
        }

        if($this->logger !== null){
            $request = $this->logger->hasUser($request);
        }

        $request = $request->withAttribute('needUser', true);

        // isso esta aqui para ser apagado depois
        Lliure::$data->user = (object) ($_SESSION['ll']['user'] ?? []);

        return $handler->handle($request);
    }
}