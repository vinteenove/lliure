<?php

namespace LliurePanel;

use Helpers\HttpHelper;

class LliureDataAppWli implements \ArrayAccess
{
	use PropertyArrayAccessTrait;

    public string $controller;
    public string $pasta;
    public string $home;
    public string $onserver;
    public string $onclient;
    public ?LliureDataAppNli $nli = null;
    public ?LliureDataAppConf $conf = null;

    public static function doors(string $type, string $key, ?string $load = null){
        $load = ((is_null($load))? $key: $load);
        $wli = new self;

        $wli->controller = '\\' . ($type) . '\\' . ($key) . '\\' . ($key);

        $base = $type;
        if(strpos($load, '=') !== false){
            list($base, $load) = explode('=', $load, 2);
        }

        //$load = ucfirst($load);
        $wli->pasta = "{$base}/{$load}/";

        $base = HttpHelper::pathToUri(ll::baseDir());
        $base .= ((empty($base))? '': '/');
        $k = strtolower($key);

        $wli->home = "{$base}{$k}/";
        $wli->onserver = "{$base}{$k}/os/";
        $wli->onclient = "{$base}{$k}/oc/";

        $wli->nli = LliureDataAppNli::doors($type, $key, $load);
        $wli->conf = LliureDataAppConf::load($type, $load);

        return $wli;
    }
}









