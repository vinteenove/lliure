<?php

namespace Etc\LliureHosts;

use LliurePanel\LliureHostInterface;
use LliurePanel\LliureHostTrait;

class LliureDefault implements
    LliureHostInterface
{
    use LliureHostTrait;
}