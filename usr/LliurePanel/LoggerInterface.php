<?php

namespace LliurePanel;

use Psr\Http\Message\ServerRequestInterface;

interface LoggerInterface{

    public function hasUser(ServerRequestInterface $request): ServerRequestInterface;

}