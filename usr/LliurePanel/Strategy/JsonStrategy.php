<?php

namespace LliurePanel\Strategy;

use Lliure\Http\Message\Response;
use Lliure\Http\Exception\HttpException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Router\Route;
use Router\Strategy\AbstractStrategy;
use Router\Strategy\StrategyInterface;
use Throwable;

class JsonStrategy extends AbstractStrategy implements
    StrategyInterface
{

    protected int $jsonFlags;

    public function __construct(int $jsonFlags = 0){
        $this->jsonFlags = $jsonFlags;
        $this->addDefaultResponseHeader('content-type', 'application/json');
    }

    /** @inheritDoc */
    public function invokeRouteCallable(Route $route, ServerRequestInterface $request): ResponseInterface{

        $controller = $route->getCallable();
        $response = $controller($request, $route->getVars());

        if($response instanceof ResponseInterface){
            return $response;
        }

        if ($this->isJsonEncodable($response)) {
            $response = json_encode($response, $this->jsonFlags);
        }

        $response = new Response(200, [], $response);
        $response = $this->applyDefaultResponseHeaders($response);
        return $response;
    }

    /** @inheritDoc */
    public function getUnsolvedDecorator(Throwable $exception): MiddlewareInterface{
        return new class($this, $exception) implements MiddlewareInterface
        {
            protected JsonStrategy $strategy;
            protected HttpException $exception;

            public function __construct(
                JsonStrategy $strategy,
                HttpException $exception
            ){
                $this->strategy = $strategy;
                $this->exception = $exception;
            }

            public function process(
                ServerRequestInterface $request,
                RequestHandlerInterface $handler
            ): ResponseInterface {
                return $this->strategy->buildJsonResponse(new Response(), $this->exception);
            }
        };
    }

    /** @inheritDoc */
    public function getThrowableHandler(): MiddlewareInterface{
        return new class($this) implements MiddlewareInterface
        {
            protected JsonStrategy $strategy;

            public function __construct(
                JsonStrategy $strategy
            ){
                $this->strategy = $strategy;
            }

            public function process(
                ServerRequestInterface $request,
                RequestHandlerInterface $requestHandler
            ): ResponseInterface {
                try {
                    return $requestHandler->handle($request);
                } catch (Throwable $exception){
                    $response = new Response();

                    if ($exception instanceof HttpException) {
                        return $this->strategy->buildJsonResponse($response, $exception);
                    }

                    $response->getBody()->write(json_encode([
                        'status_code'   => 500,
                        'reason_phrase' => $exception->getMessage()
                    ]));

                    $response = $response->withAddedHeader('content-type', 'application/json');
                    return $response->withStatus(500, strtok($exception->getMessage(), "\n"));
                }
            }
        };
    }




    protected function isJsonEncodable($response): bool {

        if ($response instanceof ResponseInterface){
            return false;
        }

        return (is_array($response) || is_object($response));
    }

    public function buildJsonResponse(ResponseInterface $response, HttpException $exception): ResponseInterface{

        $this->addDefaultResponseHeaders($exception->getHeaders());

        $response = $this->applyDefaultResponseHeaders($response);

        if ($response->getBody()->isWritable()) {
            $response->getBody()->write(json_encode([
                'status_code'   => $exception->getStatusCode(),
                'reason_phrase' => $exception->getMessage()
            ]));
        }

        return $response->withStatus($exception->getStatusCode(), $exception->getMessage());
    }

}