<?php

namespace LliurePanel;

use Helpers\HttpHelper;
use Helpers\JsonHelper;
use stdClass;

class LliureData implements \ArrayAccess
{
	use PropertyArrayAccessTrait;

    public string $dir;
    public ?stdClass $conf = null;
    public ?stdClass $url = null;
    public bool $install = false;
    public ?bool $insystem = null;
    public string $enter_mode;
    public string $operation_mode;
    public ?string $operation_type;
    public ?string $operation_key;
    public ?string $operation_load;
    public array $operation_types;
    public ?string $ling;
    public string $titulo;
    public stdClass $user;
    public bool $desktop = false;
    public bool $liberado;
    public LliureDataAppWli $app;

    public function __construct(string $entryPath){
        $this->dir = HttpHelper::path($entryPath) . DIRECTORY_SEPARATOR;

        $this->install = $this->getBDConf();

        $this->conf = $this->getConf();
        $this->url = $this->getUrl();
        $this->insystem = $this->getInsystem();

        $this->user = new stdClass;

        /** Define a base para selecionar o app */
        $this->enter_mode = 'wli';
        $this->operation_mode = 'x';

        $this->operation_type = null;
        $this->operation_key = null;
        $this->operation_load = null;

        $this->operation_types = ['opt', 'api', 'app'];
        $this->ling = $this->getLing();
        $this->titulo = 'lliure Wap';

        $this->app = new LliureDataAppWli;
    }

    /** verifica o status de instalação e carrega bdconf se existir */
    public function getBDConf(): bool{
        if($this->install === true){
            return true;
        }
        if(($install = file_exists($f = realpath($this->dir . 'etc/bdconf.php'))) == true){
            require_once $f;
        }
        return $install;
    }

    /**
     * Carrega as configurações do sistema en llconf.ll
     * @return object
     */
    public function getConf(): ?object{
        if($this->conf !== null){
            return $this->conf;
        }

        if($this->conf === null && file_exists($f = ($this->dir . 'etc/llconf.ll'))){
            return $this->conf = json_decode(file_get_contents($f));
        }

        return null;
    }

    /**
     * @param object $conf
     * @return object|null
     */
    public function setConf(?object $conf): ?object{

        if($conf === null){
            $conf = $this->conf;
        }

        if(file_put_contents(($this->dir . 'etc/llconf.ll'), JsonHelper::encode($conf, false)) === false){
            return null;
        }

        $this->conf = $conf;
        return self::getConf();
    }

    /**
     * Define as bases da url
     * @return stdClass
     */
    public function getUrl(): stdClass{
        if($this->url === null) $this->url = (object) (ll::basicUrlAnalyzer((
            (isset($_SERVER['REQUEST_SCHEME'])? $_SERVER['REQUEST_SCHEME']: (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])? 'https': 'http')) . '://' . $_SERVER['HTTP_HOST'] . '/' . $_SERVER['REQUEST_URI']
        )));
        return $this->url;
    }

    /**
     * Testa se o o diretorio destino da requisição é o mesmo do sistema,
     * Se sim esta no sistema.
     * Se não não esta
     * @return bool
     */
    public function getInsystem(): bool{
        if($this->insystem === null){
            $this->insystem = ((dirname(realpath($_SERVER["SCRIPT_FILENAME"])) . DIRECTORY_SEPARATOR) == $this->dir);
        }
        return $this->insystem;
    }

    /**
     * função que retorna a linguagem nativa, caso não tenha nenhuma retorna false
     * @return string|null
     */
    public function getLing(){
        $retorno = false;

        if(isset($this->conf->idiomas, $this->conf->idiomas->nativo) && !empty($this->conf->idiomas)){
            $retorno = (string) $this->conf->idiomas->nativo;
        }

        return $retorno;
    }

    /**
     * Configura so parametos de operação
     * @param object $operation
     */
    public function setOperation(object $operation){
        if(isset($operation->enter_mode)) $this->enter_mode = $operation->enter_mode;
        if(isset($operation->operation_mode)) $this->operation_mode = $operation->operation_mode;
        if(isset($operation->operation_type)) $this->operation_type = $operation->operation_type;
        if(isset($operation->operation_key)) $this->operation_key = $operation->operation_key;
    }


}