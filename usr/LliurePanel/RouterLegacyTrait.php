<?php

namespace LliurePanel;

use Helpers\HttpHelper;
use LliurePanel\Middleware\NeedUserMiddleware;
use LliurePanel\Middleware\StartAppLegacyMiddleware;
use LliurePanel\Strategy\JsonStrategy;
use Opt\Singin\SinginLogger;
use Router\RouteGroup;
use Router\Router;
use Router\Strategy\StrategyStandard;

trait RouterLegacyTrait{

    private static string $appName = '';

    abstract public static function router(): router;

    abstract public static function appPath(): string;

	public static function getUserFeatures(): array{
		return [];
	}

    final public static function routerLegacy(Router $router): Router{
		$router->middleware(new StartAppLegacyMiddleware(static::class));

		$router->group('/', function(RouteGroup $router){
			$router->any('/os', [self::class, 'legacyControllerOS'])->setStrategy(new JsonStrategy);
			$router->any('/os/*', [self::class, 'legacyControllerOS'])->setStrategy(new JsonStrategy);
			$router->any('/oc', [self::class, 'legacyControllerOC'])->setStrategy(new StrategyStandard);
			$router->any('/oc/*', [self::class, 'legacyControllerOC'])->setStrategy(new StrategyStandard);
			$router->any('/', [self::class, 'legacyControllerX']);
			$router->any('/*', [self::class, 'legacyControllerX']);
		})->middleware(new NeedUserMiddleware(new SinginLogger(static::class, [static::class, 'getUserFeatures'])));

		$router->group('/nli', function(RouteGroup $router){
			$router->any('/os', [self::class, 'legacyControllerOS'])->setStrategy(new JsonStrategy);
			$router->any('/os/*', [self::class, 'legacyControllerOS'])->setStrategy(new JsonStrategy);
			$router->any('/oc', [self::class, 'legacyControllerOC'])->setStrategy(new StrategyStandard);
			$router->any('/oc/*', [self::class, 'legacyControllerOC'])->setStrategy(new StrategyStandard);
			$router->any('/', [self::class, 'legacyControllerX']);
			$router->any('/*', [self::class, 'legacyControllerX']);
		});

        return $router;
    }


    public static function getFileHD(): string{
        return HttpHelper::path( static::appPath() . DIRECTORY_SEPARATOR . (!ll::autentica()? 'nli' . DIRECTORY_SEPARATOR: '') . self::$appName . '.hd.php');
    }

    public static function getFileOC(): string{
        return HttpHelper::path( static::appPath() . DIRECTORY_SEPARATOR . (!ll::autentica()? 'nli' . DIRECTORY_SEPARATOR: '') . self::$appName . '.oc.php');
    }

    public static function getFileOS(): string{
        return HttpHelper::path( static::appPath() . DIRECTORY_SEPARATOR . (!ll::autentica()? 'nli' . DIRECTORY_SEPARATOR: '') . self::$appName . '.os.php');
    }

    public static function getFileX(): string{
        return HttpHelper::path( static::appPath() . DIRECTORY_SEPARATOR . (!ll::autentica()? 'nli' . DIRECTORY_SEPARATOR: '') . self::$appName . '.x.php');
    }


    final private static function legacyControllerHD($request, $vars){
		return self::loadFile(static::getFileHD(), $request, $vars);
    }

    final public static function legacyControllerOS($request, $vars){
		self::legacyControllerLoadName();
		$body = self::legacyControllerHD($request, $vars) . self::loadFile(static::getFileOS(), $request, $vars);
        if(!empty($body)) return $body;
        die();
    }

    final public static function legacyControllerOC($request, $vars){
		self::legacyControllerLoadName();
		return self::legacyControllerHD($request, $vars) . self::loadFile(static::getFileOC(), $request, $vars);
    }

    final public static function legacyControllerX($request, $vars){
		self::legacyControllerLoadName();
		
		return self::legacyControllerHD($request, $vars) . self::loadFile(static::getFileX(), $request, $vars);
	}


	final private static function legacyControllerLoadName(){
		self::$appName = strtolower(basename( HttpHelper::path(static::appPath())));
	}

	final private static function loadFile($file, $request, $vars){
		ob_start();
		if(file_exists($file)){
			(function($file, $request, $vars){
				require_once $file;
			})($file, $request, $vars);
		}
		return ob_get_clean();
	}

}