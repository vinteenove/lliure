<?php

namespace LliurePanel;

use AdminLTE\AdminLTE;
use Helpers\HttpHelper;
use League\Event\EventDispatcher;
use Liberacao\Liberacao;
use Lliure\Http\Message\Response;
use LliurePanel\Models\Autenticacao;
use Persona\Persona;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Router\Dispatcher;
use Router\Router;
use Router\Strategy\StrategyInterface;
use stdClass;
use Token\Token;

/**
 *
 * Classe de implementação do lliure
 *
 * @Versão do lliure 8.0
 * @Pacote lliure
 * @contato Entre em contato com o desenvolvedor <lliure@lliure.com.br> http://www.lliure.com.br/
 * @Licença //mit-license.org/ MIT License
 *
 */

class Lliure{

	public static array $apis = [];
	private static ?EventDispatcher $dispatcher = null;
    public static LliureData $data;
    public static ?Router $router = null;

    protected static bool $installed = false;

    /**
     * @return mixed|stdClass
     */
	public static function carrega_conf(): ?object{
	    return self::$data->getConf();
	}

    /**
     * @param null|object $newConfs
     * @return false|stdClass
     */
	public static function complila_conf(?object $newConfs = null){
        return self::$data->setConf($newConfs);
	}

	/**
	 * Carrega as configurações do modulo.
	 * lliure 9.0 toma-la como padrão confg.ll como nome padrão para o arquivo
	 * e seu conteuto seja um json.
	 *
	 * ainda é compativel com xml (mas não recomendado)
	 *
	 * @param string $operation_type tipo do modulo
	 * @param string $operation_load modulo
	 * @param bool $load_confs [true: registra, false: não registra] no $_ll
	 * @return LliureDataAppConf  as congurações do modulo
	 */
	public static function confg_app(string $operation_type, string $operation_load, bool $load_confs = false): LliureDataAppConf{
	    $conf = LliureDataAppConf::load($operation_type, $operation_load);

	    if($load_confs){
            self::$data->app->conf = $conf;
            return self::$data->app->conf;
        }else{
	        return $conf;
        }
	}

	/**
	 * Valida se o usuario é de um grupo
	 * @param string|array|null $grupo
	 * @return bool
	 */
	public static function valida($grupo = null){

		if(func_num_args() > 1){
			$grupo = func_get_args();
		}elseif(!is_array($grupo)){
			$grupo = explode(',', ((string) $grupo));
		}

		$grupo = array_filter($grupo);
		
		$grupo_user = $_SESSION['ll']['user']['group'] ?? null;
		if(($grupo_user == 'dev') || (in_array($grupo_user, $grupo))) return true;
		return false;
	}

	/**
	 * Faz autentição do usáio no sistema
	 *
	 * @param null   $login
	 * @param null   $name
	 * @param string $group
	 * @param string $theme @deprecated
	 * @return bool
	 */
	public static function autentica($login = null, $name = null, $group = 'user', $theme = 'default'){
	    if($login === null){
			if(isset($_SESSION['ll']['user']) && !empty($_SESSION['ll']['user']['login'])){
			    return true;
            }else{
			    return false;
            }
		}

        $userDT = Autenticacao::findByLogin($login);

		if(!empty($userDT)){
            $user = $userDT['id'];
        }else{
		    Autenticacao::build(['login' => $login, 'nome' => $name, 'grupo' => $group])->save();
            $userDT = Autenticacao::findByLogin($login);
            $user = $userDT['id'];
        }

		if(!isset($user) || empty($user)) return false;

		$_SESSION['ll']['user'] = array(
			'id' => $user,
			'login' => $login,
			'name' => $name,
			'group' => $group,
			'token' => Token::create()
		);

		return true;
	}

    /**
     * Revoga a autenticação do usário no sistema
     * @return bool
     */
	public static function desautentica(){
		unset($_SESSION['ll']['user']);
		return true;
	}

    /**
     * @param $name
     * @param bool $load
     * @return false|ll_modulo
     */
	public static function api($name, $load = true){
        return self::loadComponent('api', $name, $load);
	}

    /**
     * @param $name
     * @param bool $load
     * @return false|ll_modulo
     */
	public static function app($name, $load = true){
        return self::loadComponent('app', $name, $load);
	}

    /**
     * @param $name
     * @param bool $load
     * @return false|ll_modulo
     */
	public static function usr($name, $load = true){
        return self::loadComponent('usr', $name, $load);
	}

    /**
     * @param $name
     * @param bool $load
     * @return false|ll_modulo
     */
	public static function opt($name, $load = true){
		return self::loadComponent('opt', $name, $load);
	}

    /**
     * @param $type
     * @param $name
     * @param bool $load
     * @return false|ll_modulo
     */
	protected static function loadComponent($type, $name, $load = true){
		global $_ll;

        if (file_exists($f = (self::$data->dir . $type . '/' . $name . '/' . ($a = $name . '.bt') . '.php'))
            || (file_exists($f = (self::$data->dir . $type . '/' . $name . '/' . ($a = 'boot'       ) . '.php')))
            || (file_exists($f = (self::$data->dir . $type . '/' . $name . '/' . ($a = $name        ) . '.php')))
            || (file_exists($f = (self::$data->dir . $type . '/' . $name . '/' . ($a = 'inicio'     ) . '.php')))){

		    if($load){
                self::$data->components[$type][$name] = true;
                if($a === $name . '.bt' || $a === 'boot') require_once $f;

                if ((!isset(self::$data->install) || !self::$data->install)
                && (file_exists($f = (self::$data->dir. $type. '/'. $name. '/' .('inicio'). '.php'))
                || (file_exists($f = (self::$data->dir. $type. '/'. $name. '/' .($name   ). '.php')))))
                    require_once $f;
            }

			return new ll_modulo($type, $name);

		} else
			return (self::$data->components[$type][$name] = false);
	}

    /**
     * @param $name
     * @return mixed|null
     */
	private static function loadedComponent($name){
		global $_ll;
		foreach(self::$data->components as $type => $names)
		foreach($names as $n => $status)
			if($name == $n) return $status;
		return null;
	}

    /**
     * @param $url
     * @return mixed
     */
    public static function basicUrlAnalyzer($url){
        $b['path']      = (trim(dirname($_SERVER['PHP_SELF']), '\\/'));
        $b['path']     .= (!empty($b['path'])? '/' : '');
        $b['request']   = (rawurldecode(ltrim($_SERVER['REQUEST_URI'], '\\/')));
        $b['request']   = (explode('?', $b['request'], 2));
        $b['get']       = (isset($b['request'][1]) ? '?' . $b['request'][1] : '');
        $b['request']   = (!empty($b['request'][0]) ? $b['request'][0] : '');
        $b['request']   = (!empty($b['request']) ? explode('/', $b['request']) : array());
        $b['query']     = (array_splice($b['request'], (count((!empty($b['path']) ? explode('/', trim($b['path'], '\\/')) : array())))));
        $b['request']   = (implode('/', $b['request']));
        $b['request']  .= (!empty($b['request']) ? '/' : '');
        $b['host']      = (isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http')) . '://'. ($_SERVER['HTTP_HOST'] . '/'). $b['request'];

        $real = parse_url($b['host']);
        $comp = parse_url($url);
        if(!isset($comp['host'])) $comp = array_merge($comp, [
            'scheme' => $real['scheme'],
            'host' => $real['host'],
            'path' => self::path(($real['path'] . ((isset($comp['path']))? $comp['path']: '')), '/')
        ], (isset($comp['query']) ? ['query' => $comp['query']]: []));

        $r['local'] = ($comp['host'] . '/');
        $r['host'] = ($comp['scheme'] . '://');
        $r['path'] = (trim($real['path'], '\\/'));
        $r['request'] = rawurldecode(ltrim(($comp['path'] . (isset($comp['query'])? '?' . $comp['query']: '')), '\\/'));
        $r['request'] = (explode('?', $r['request'], 2));
        $r['get'] = (isset($r['request'][1])? '?' . $r['request'][1]: '');
        $r['request'] = (!empty($r['request'][0])? $r['request'][0]: '');
        $r['request'] = (!empty($r['request'])? explode('/', $r['request']): []);
        $r['query'] = array_splice($r['request'], (count((!empty($r['path'])? explode('/', $r['path']): []))));
        $r['fragment'] = ((isset($comp['fragment']))? $comp['fragment']: '');
        $r['request'] = implode('/', $r['request']);
        $r['get'] = ((!empty($q = implode('/', $r['query']))? '/': '') . $q . $r['get']);
        $r['endereco'] = $r['host'];
        $r['host'] .= $r['local'];
        $r['real'] = $r['host'] . $r['path'];
        $r['local'] .= (!empty($r['request'])? $r['request']: '');
        $r['endereco'] .= $r['local'] . (!empty($r['request'])? '/': '');
        $r['full'] = $r['real'] . $r['get'] . ((empty($r['fragment']))? '': '#' . $r['fragment']);

        return $r;
	}

    /**
     * Padrão da url amigavel
     *
     * sistema/[wli/|nli/][os/|onserver/|oc/|onclient/]opt|api|app=$MODULO$
     *
     * Começa com sistema/; obrigatório (quando dentro da pasta sistema)
     * Segunda parte wli/ ou nli/ com padrão em wli; opcional
     * Terceira pate os/ ou onserver/ ou oc/ ou onclient/ com padrão em x; opcional
     * Quarta parte em opt ou api ou app, com obrigação de passa alguns destes valroes e
     * $MODULO$ que é a chave do modulo que vc quer abrir; obrigatório
     *
     * @param array $get
     * @param array|null $operation_types
     * @return object
     */
    public static function operation(array &$get, ?array $operation_types = null): object{
        $operation_types = ((is_array($operation_types))? $operation_types: self::$data->operation_types);

        $ks = array_keys($get);
        $r = (object) [
            'enter_mode' => 'wli',
            'operation_mode' => 'x',
            'operation_type' => 'app',
            'operation_key' => null,
        ];

        // Define o enter mode
        if(isset($ks[0]) && in_array($get[$ks[0]], ['wli', 'nli'])){
            $r->enter_mode = $get[$ks[0]];
            unset($get[$ks[0]]);
            $ks = array_keys($get);
        }

        // define o modo de operação do sistema [onserver, onclient];
        if(isset($ks[0]) && in_array($get[$ks[0]], ['os', 'onserver', 'oc', 'onclient'])){
            $r->operation_mode = ((in_array($get[$ks[0]], ['os', 'onserver']))? 'os': 'oc');
            unset($get[$ks[0]]);
            $ks = array_keys($get);
        }

        // define o tipo e o modulo a serem carregados;
        if (isset($ks[0]) && in_array((string) $ks[0], $operation_types)){
            $r->operation_type = $ks[0];
            $r->operation_key = $get[$ks[0]];
            unset($get[$ks[0]]);
        }elseif(isset($ks[0]) && is_numeric($ks[0])){
            $r->operation_type = 'app';
            $r->operation_key = $get[$ks[0]];
            unset($get[$ks[0]]);
        }

        return $r;
    }

    /**
     * @param string      $type
     * @param string      $key
     * @param string|null $load
     * @return mixed
     */
    public static function doors(string $type, string $key, ?string $load = null): LliureDataAppWli{
        return LliureDataAppWli::doors($type, $key, $load);
    }

    /**
     * @param $file
     * @return object
     */
	public static function xmlToObject($file){
		if(($file = @simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA)) != false){
			$file = self::ota($file);
			array_walk_recursive($file, function(&$item){
				$item = utf8_decode($item);
			});
		}

		$file = self::ato($file);

		//var_dump($file); die();
		return $file;
	}

    /**
     * converte um array para Objeto
     *
     * @param $array
     * @return object
     */
	public static function ato($array){
		if(!is_array($array))
			return $array;

		foreach ($array as $k => $v)
			$array[$k] = self::ato($v);
		return (object) $array;
	}

    /**
     * @param $obj
     * @return array
     */
	public static function ota($obj){
        return (array) json_decode(json_encode($obj), true);
	}



    /**
     * @param $name
     * @param $listener
     * @param int $priority
     */
    public static function on($name, $listener, $priority = 0){
        if(self::$dispatcher === null) self::$dispatcher = new EventDispatcher;
        self::$dispatcher->subscribeTo($name, $listener, $priority);
    }

    /**
     * @param mixed $event
     * @param mixed  ...$params
     * @return object
     */
    public static function trigger($event, ...$params){
        if(self::$dispatcher === null) self::$dispatcher = new EventDispatcher;
        return self::$dispatcher->dispatch(...[$event, ...$params]);
    }

    /**
     * @return string
     */
	public static function baseDir(): string{
	    return self::$data->dir;
    }

    /**
     * @param string $entryPath
     * @return null
     */
    public static function bootstrap(string $entryPath){
        return new class($entryPath) implements MiddlewareInterface{

            private string $entryPath;

            public function __construct($entryPath){
                $this->entryPath = $entryPath;
            }

            public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{
                global $_ll;

                if(class_exists('\Etc\LliurePanel\LliureData', false)){
                    Lliure::$data = new \Etc\LliurePanel\LliureData($this->entryPath);
                }else{
                    Lliure::$data = new LliureData($this->entryPath);
                }

                $_ll = Lliure::$data;

                return $handler->handle($request);
            }
        };
    }

    public static function emit(ResponseInterface $response){

        self::emitHeaders($response);
        self::emitStatusLine($response);
        self::emitBody($response);

        return true;
    }

    private static function emitHeaders(ResponseInterface $response) : void{
        $statusCode = $response->getStatusCode();
        foreach ($response->getHeaders() as $header => $values) {
            $name  = self::filterHeader($header);
            $first = $name !== 'Set-Cookie';
            foreach ($values as $value) {
                header(sprintf( '%s: %s', $name, $value), $first, $statusCode);
                $first = false;
            }
        }
    }

    private static function filterHeader(string $header) : string{
        return ucwords($header, '-');
    }


    private static function emitStatusLine(ResponseInterface $response) : void{
        $protocolVersion = $response->getProtocolVersion();
        $statusCode = $response->getStatusCode();
        $reasonPhrase = $response->getReasonPhrase();

        header(sprintf('HTTP/%s %d%s', $protocolVersion, $statusCode, ($reasonPhrase ? ' ' . $reasonPhrase : '')), true, $statusCode);
    }

    private static function emitBody(ResponseInterface $response) : void{
        echo $response->getBody();
    }


    public static function build(){
        HttpHelper::parse_get(self::$data->url->query, $_GET);

        self::$data->setOperation(self::operation($_GET));

        self::testInstallation();

        self::testUser();

        self::testHome();

        self::popularRoute();
        
        //self::userIsReleased();

        //self::defineLoadByKey();
    }



    public static function pipeline(): Router{
        if(!self::$router){
            self::$router = new Router(new Dispatcher);
        }

        return self::$router;
    }

    /**
     * @return MiddlewareInterface
     */
    public static function dispatcher(): MiddlewareInterface{
        return new class implements MiddlewareInterface{
            public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{

                /* @var $hostClass LliureHostInterface  */

                $uri = $request->getUri();
                $requestHost = $uri->getHost() . $uri->getPath() . $uri->getQuery();
                $hosts = [];

                $base = (HttpHelper::path(ll::baseDir() . 'etc/LliureHosts') . DIRECTORY_SEPARATOR);
                if(file_exists($base)){
                    foreach(scandir($base) as $name){

                        if(in_array($name, ['.', '..'])){
                            continue;
                        }

                        require_once($base . $name);
                        $class = basename($base . $name, '.php');
                        $host = '\\Etc\\LliureHosts\\' . $class;

                        if(class_exists($host)){
                            $hostClass = new $host($requestHost);

                            if($hostClass instanceof LliureHostInterface && $hostClass->combine()){
                                $hosts[$host] = $hostClass;
                            }
                        }
                    }

                    usort($hosts, function(LliureHostInterface $a, LliureHostInterface $b){
                        return $a->score() > $b->score()? -1: ($a->score() < $b->score()? 1: 0);
                    });

                    usort($hosts, function(LliureHostInterface $a, LliureHostInterface $b){
                        return $a->priority() > $b->priority()? 1: ($a->priority() < $b->priority()? -1: 0);
                    });
                }

                $hostClass = array_shift($hosts);
                $hostClass = $hostClass ?? new LliureHostDefault($requestHost);

                Lliure::$router = $hostClass->router();
                return Lliure::$router->dispatch($request);
            }
        };
    }

    public static function getRouterByApp($app): ?array {
        if(!class_exists($app)){
            return null;
        }

        $implements = @class_implements($app);
        $implements = $implements ?? [];

        if(in_array('Lliure\Routable', $implements) || method_exists($app, 'router')) {
            return [$app, "router"];
        }

        return null;
    }


    /** caso esteja no modo de instalação, mas a url não seja a de instalação, redireciona para ela. */
    protected static function testInstallation(){
        if(self::$data->install && !(self::$data->operation_type == 'opt' && self::$data->operation_key == 'install')){
            $_SESSION['ll']['retorno'] = self::$data->url->full;
            header('Location: ' . ll::doors('opt', 'install')->home);
        }
    }

    public static function isInstalled(): MiddlewareInterface{
        return new class implements MiddlewareInterface {
            /** @inheritDoc */
            public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{

                if(!ll::$data->install && '/install' !== substr($request->getUri()->getQuery(), 0, 8)){
                    $_SESSION['ll']['retorno'] = $request->getUri()->getQuery();
                    header('Location: ' . ll::$data->url->endereco . 'install');
                    die();
                }

                return $handler->handle($request);
            }
        };
    }


    /** Verifica a autenticação para o usuario atual ou cria um usuario anonimo caso nao existir e precisar de um usuario. */
    private static function testUser(){
        if (self::$data->enter_mode == 'wli') {
            if (!ll::autentica()) {
                $_SESSION['ll']['retorno'] = self::$data->url->full;
                header('location: nli');
            }
        } else {
            if (!isset($_SESSION['ll']['user']) || empty($_SESSION['ll']['user'])) {
                $_SESSION['ll']['user'] = array(
                    'id' => null,
                    'login' => null,
                    'name' => 'Anonimo',
                    'group' => 'nli',
                    'themer' => Persona::$theme->name,
                    'hash' => Token::create()
                );
            }
        }
        self::$data->user = (object) $_SESSION['ll']['user'];
    }

    /** verifica se voce esta na home e carrega a operação se estiver */
    private static function testHome(){

        if (!!self::$data->operation_key === false) {

            $desk = explode('=', Persona::$theme->home_wli);

            if (self::$data->enter_mode == 'nli'){
                $desk = explode('=', Persona::$theme->home_nli);
            }

            if (isset(self::$data->conf->grupo->{self::$data->user->group}->{"home_".self::$data->enter_mode})){
                $desk = explode('=', self::$data->conf->grupo->{self::$data->user->grupo}->{"home_".self::$data->enter_mode});
            }

            unset($_GET[$desk[0]]);
            $_GET = array_merge([self::$data->enter_mode, $desk[0] => $desk[1]], $_GET);

            self::$data->desktop = true;
            self::$data->setOperation(self::operation($_GET));
        }
    }

    /** Verifica se o usuario atual esta liberado apra a pagina */
    private static function userIsReleased(){
        // Valida se o ususario vai ter liberação para o app em questão
        if (!(self::$data->liberado = (

            // Libera se o enter_mode for nli, isto é, se for uma requisação que não precisa estar logado.
            (self::$data->enter_mode == 'nli') ||

            // Libera se esiver entrando em uma api
            (self::$data->operation_type == 'api') ||

            // Libra se for uma opt livre
            (self::$data->operation_type == 'opt' && in_array(self::$data->operation_key, [
                'rotinas',
                'mensagens',
                'sessionfix',
            ])) ||

            // Libera se o usuario atual esta altorizado para este modulo
            Liberacao::test(self::$data->operation_type, self::$data->operation_key)))
        ){

            // Se não for liberado muda para o mudulo de mensagens.
            self::$data->operation_mode = 'x';
            self::$data->operation_type = 'opt';
            self::$data->operation_key = 'denied';
            self::$data->operation_load = 'message';
        }
    }

    /** Define o operation_load a partir do operation_key */
    private static function defineLoadByKey(){

        if (self::$data->operation_type != 'app') {
            self::$data->operation_load = ((self::$data->operation_load)? self::$data->operation_load: self::$data->operation_key);

        } else {
            $app = LliureCoreLliure\Models\Apps::findByChave(self::$data->operation_key);
            $app =  (($app)? $app->toArray(): []);

            if (isset($app['pasta'])) {
                self::$data->operation_load = $app['pasta'];
            } else {
                self::$data->operation_load = self::$data->operation_key;
            }
        }

        self::$data->app = ll::doors(self::$data->operation_type, self::$data->operation_key, self::$data->operation_load);
    }


    public static function getStrategy(): StrategyInterface{
        if(class_exists('\Etc\Persona\PersonaTheme')){
            return new \Etc\Persona\PersonaTheme;
        }else{
            return new AdminLTE;
        }

    }

}
