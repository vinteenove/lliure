CREATE TABLE `ll_lliure_liberacao` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`operation_type` VARCHAR(255) NOT NULL,
	`operation_key` VARCHAR(255) NOT NULL,
	`login` VARCHAR(255) NOT NULL,
	`hash` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `operation_type_operation_load_login` (`operation_type`, `operation_key`, `login`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
