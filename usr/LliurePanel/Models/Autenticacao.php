<?php

namespace LliurePanel\Models;

use LliureCore\Model;

class Autenticacao extends Model
{
    protected static $primaryKey = 'id';
    protected static ?string $table = 'lliure_autenticacao';

    public static function findByLogin($login){
        $table = static::getTable();
        return static::findOne('select * from ' . $table . ' where `login` = ?', $login);
    }
}