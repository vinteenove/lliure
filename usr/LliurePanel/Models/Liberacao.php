<?php

namespace LliurePanel\Models;

use LliureCore\Model;

class Liberacao extends Model
{
    protected static $primaryKey = 'id';
    protected static ?string $table = 'lliure_liberacao';

}