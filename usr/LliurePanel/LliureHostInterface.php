<?php

namespace LliurePanel;

use Router\Router;

interface LliureHostInterface
{
    const HIGH_PRIORITY = 0;
    const MEDIUM_PRIORITY = 5;
    const LOW_PRIORITY = 10;

    public function __construct(string $requestHost);

    public function host():string;

    public function priority(): int;

    public function combine(): bool;

    public function score(): int;

    public function router(): Router;
}