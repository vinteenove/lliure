<?php


namespace LliurePanel;


use Helpers\HttpHelper;
use Router\Router;

trait LliureHostTrait
{

    protected string $requestHost;

    protected int $score;

    public function __construct(string $requestHost)
    {
        $this->requestHost = $requestHost;
    }

    public function host(): string
    {
        return '*';
    }

    public function priority(): int
    {
        return self::MEDIUM_PRIORITY;
    }

    /**
     * @return bool
     */
    public function combine(): bool{
        $regExp = str_replace(['.', '/', '*'], ['\.', '\\/', '(.*)'], $this->host().'*');
        $regExp = "/^{$regExp}$/";

        $result = preg_match($regExp, $this->requestHost, $matchers);

        array_shift($matchers);
        $dynamicCombination = array_reduce($matchers, function($carry, $item){
            return $carry + strlen($item);
        }, 0);

        $this->score = (strlen($this->requestHost) - $dynamicCombination) - (substr_count($this->host(), '*') + $dynamicCombination);

        return !!$result;
    }

    public function score(): int{
        return $this->score;
    }

    public function router(): Router{

        $mainRouter = Lliure::pipeline();
        $mainRouter->setStrategy(Lliure::getStrategy());


        $opM = (Lliure::autentica())? 'WLI': 'NLI';

        foreach(['opt' => 'Opt', 'api' => 'Api', 'app' => 'App'] as $dir => $ns){
            foreach(scandir($base = (HttpHelper::path(ll::baseDir() . $dir) . DIRECTORY_SEPARATOR)) as $name){

                if(in_array($name, ['.', '..']) || !is_dir($base . $name)){
                    continue;
                }

                $path = strtolower($name);
                $app = "\\{$ns}\\{$name}\\{$name}";

                if($appRouter = ll::getRouterByApp($app)){
                    /* @var Router $router */
					$router = $appRouter();
					$mainRouter->addGroup($router->toRouteGroup("/{$path}", $mainRouter));
                    if(is_subclass_of(('\\Etc\\LliurePanel\\' . 'LliureIndex'.$opM), $app)){
                        $mainRouter->addGroup($router->toRouteGroup("/", $mainRouter));
                    }
                }
            }
        }

        //echo '<pre>' . __FILE__ . ' [' . __LINE__ . ']: '. print_r($mainRouter, 1). '</pre>' . "\n";

        return $mainRouter;
    }

}