<?php
namespace Exporter;

class Exporter{
    private $nameFile;
    private $folderShared;

    /**
     * @param mixed $folderShared
     */
    public function setFolderShared($folderShared)
    {
        $this->folderShared = $folderShared;
    }

    /**
     * @param mixed $nameFile
     */
    public function setNameFile($nameFile)
    {
        $this->nameFile = $nameFile;
    }

    /**
     * @param $query
     * @return bool
     * @throws \Dibi\Exception
     */
    public function exportQuery($query)
    {
        $outFile = $this->folderShared .'/'. $this->nameFile . '.csv';

        $fp = fopen($outFile, 'w');

        $consulta = \dibi::query($query);
        $data = $consulta->fetchAll();

        foreach ($data as $id => $linha) {
            $linha = array_values($linha->toArray());
            fputcsv($fp, $linha);
        }

        fclose($fp);

        return true;
    }
}