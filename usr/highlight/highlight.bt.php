<?php
use Persona\Persona;

/*Persona::add(__DIR__ . '/styles/phpstorm-light.css', 'css');
Persona::add(__DIR__ . '/highlight.pack.js', 'js');*/

/*<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/styles/default.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/highlight.min.js"></script>
<!-- and it's easy to individually load additional languages -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/languages/go.min.js"></script>*/

Persona::style('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.7.0/styles/default.min.css', Persona::COMPONENT_PRIORITY);
Persona::script('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.7.0/highlight.min.js', Persona::COMPONENT_PRIORITY);

Persona::add('<script>hljs.highlightAll();</script>', 'integral:footer', Persona::COMPONENT_PRIORITY);