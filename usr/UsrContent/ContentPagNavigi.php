<?php
namespace UsrContent;
use Api\Navigi\navigi_rest;


class ContentPagNavigi extends navigi_rest{

    /** @var $content usrContent */
    public static $content;
    /** @var $navigiConf array */
    public static $navigiConf;

    /** @var $itens UsrContentIterator */
    private $itens;

    protected function get(){

        foreach($this->navigi['whereBy'] as $k => $v)
            if (is_array($v) && isset($v[1]) && $v[1] == 'LIKE')
                $this->navigi['whereBy'][$k][2] = trim($this->navigi['whereBy'][$k][2],'%');

        $this->itens = self::$content->filter(
            $this->navigi['whereBy'],
            $this->navigi['orderBy'],
            $this->navigi['paginacao']['itens'],
            $this->navigi['paginacao']['pagina']
        );
        $r = []; foreach($this->itens as $k => $v) $r[] = (array) $v;
        return $r;
    }

    protected function tot(){
        return $this->itens->total();
    }

    protected function del($t, $w){
        return self::$content->del(((isset($_POST['id']))? $_POST['id']: null));
    }

    protected function put($t, array $d, $w){
        $id = ((isset($_POST['id']))? $_POST['id']: null);
        $dado = self::$content->get($id);
        $dado[self::$navigiConf['config']['coluna']] = $_POST['texto'];
        return self::$content->set($id, $dado);
    }

}