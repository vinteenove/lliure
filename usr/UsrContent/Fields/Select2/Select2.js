(function ($){
    $(function () {

        function initSelect2(){
            $(this).find('.fields-select2-input').each(function(e){
                const $this = $(this);
                $this.select2({
                    tags: (!!($this.attr('multiple')))
                });
            })
        }

        const $b = $('body');
        initSelect2.call($b);

        $b.on('start.panel.fields', '.panel', function (){
            initSelect2.call(this);
        });

    });
})(jQuery);