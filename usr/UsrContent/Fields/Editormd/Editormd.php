<?php
namespace UsrContent\Fields\Editormd;

use UsrContent\Field\Field_abstract;
use LliurePanel\ll;
use Persona\Persona;

class Editormd extends field_abstract{

    private static $inputs = [];
    private $index;
    private $attrs;

    public function __construct(array $attrs){
        $this->index = count(self::$inputs);
        //$attrs['class'] = (((isset($attrs['class']))? $attrs['class']. ' ': ''). 'content-fields-tinymce form-control');
        self::$inputs[] = $this->attrs = array_merge([
            'label' => 'Text Area',
            'group' => true,
            'name'  => "editormd[{$this->index}]",
            'value' => '',
        ], $attrs);
    }

    public static function header(){
        ll::usr('editor-md');
        Persona::add('fields_editormd::script', 'call:footer');
    }

    public function input($dados, $prefixo = null){
        $attr = array_diff_key($this->attrs, [
            'label' => '',
            'value' => '',
            'type' => '',
        ]);
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $attr['name'] = self::nameResolve($prefixo, $attr['name']);
        $value =& self::getValue(self::nameExplode($this->attrs['name']), $dados);

        try{
            if(isset($this->attrs['value']) && is_string($this->attrs['value']) && !empty($this->attrs['value']) && preg_match('/^function/', $this->attrs['value'])){
                @eval('$this->attrs[\'value\'] = ' . $this->attrs['value']);
            }
        }catch(Exception $e){}catch(\Error $e){}
        if(isset($this->attrs['value']) && gettype($this->attrs['value']) == 'object' && get_class($this->attrs['value']) == 'Closure'){
            $value = $this->attrs['value'] = $this->attrs['value']($value, $this->attrs, $dados);
        }

        $value = (($value === null && isset($this->attrs['value']))? $this->attrs['value']: $value);
        list($prev, $next) = self::base($label, $group); echo $prev; ?>
        <div id="content-editormd-textarea-<?php echo $this->index; ?>">
            <textarea <?php echo self::montaAttr($attr); ?> style="display: none;"><?php echo $value; ?></textarea>
        </div>
        <?php echo $next;
    }

    public static function script(){ ob_start(); ?>
        <script>
            (function ($){
                $(function () {

                    <?php foreach(self::$inputs as $k => $v){ ?>

                    (function(){
                        editormd('content-editormd-textarea-<?php echo $k ?>', {
                            height              : 600,
                            width               : '100%',
                            gotoLine            : false,
                            dialogMaskBgColor   : '#000',
                            watch               : false,
                            path                : "<?php echo \Helpers\HttpHelper::pathToUri(\Helpers\HttpHelper::path(__DIR__ . '/../../../editor-md/lib/')) ?>/",

                            toolbarIcons : function (){
                                var width = $(document).width();
                                if(width >= 992){
                                    return [
                                        "undo", "redo", "|",
                                        "bold", "del", "italic", "quote", "ucwords", "uppercase", "lowercase", "|",
                                        "h2", "h3", "h4", "|",
                                        "list-ul", "list-ol", "hr", "|",
                                        "link", "reference-link", "code", "code-block", "table", "||",
                                        "search", "help"
                                    ];
                                }else{
                                    return [
                                        "undo", "redo", "|",
                                        "bold", "italic", "|",
                                        "list-ul", "link", "||",
                                        "search", "help"
                                    ];
                                }
                            },

                            onload : function(){
                                this.setToolbarAutoFixed(false);
                            }
                        });
                    })();
                    <?php } ?>

                });
            })(jQuery);
        </script>
    <?php echo trim(ob_get_clean());}
}