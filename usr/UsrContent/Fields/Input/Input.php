<?php

namespace UsrContent\Fields\Input;

use Error;
use Exception;
use UsrContent\Field\Field_abstract;
use LliurePanel\ll;
use Persona\Persona;

class Input extends field_abstract{

    private static $inputs = [];
    private static $script = false;
    private $attrs, $id;

    public function __construct(array $attrs){
        $this->id = count(self::$inputs);
        $this->attrs = array_merge([
            'type'      => 'text',
            'group'     => true,
            'label'     => 'Input',
            'name'      => "input[{$this->id}]",
            'btnGroup'  => false,
            'helpBlock' => false,
            'checkbox'  => false,
            'radio'     => false,
            'sortable'  => false,
            'multiple'  => false,
            'inline'    => false,
        ], $attrs);
        self::$script = self::$script || ($this->attrs['sortable'] || $this->attrs['multiple']);
        if(!!$this->attrs['sortable']) $this->attrs['multiple'] = true;
        self::$inputs[] = $this;
    }

    public static function header(){
        if (self::$script){
            ll::usr('jquery-ui');
            Persona::add(__DIR__ . '/Input.js', 'js');
        }
    }

    public function salve(array &$dados){
        if(isset($this->attrs['value'])){
            $name = self::nameExplode($this->attrs['name']);
            $value =& self::getValue($name, $dados);
            try{
                if(isset($this->attrs['value']) && is_string($this->attrs['value']) && !empty($this->attrs['value']) && preg_match('/^function/', $this->attrs['value'])){
                    @eval('$this->attrs[\'value\'] = ' . $this->attrs['value']);
                }
            }catch(Exception $e){}catch(Error $e){}
            if(isset($this->attrs['value']) && gettype($this->attrs['value']) == 'object' && get_class($this->attrs['value']) == 'Closure'){
                $value = $this->attrs['value'] = $this->attrs['value']($value, $this->attrs, $dados);
            }
        }
    }

    public function input($dados, $prefixo = null){
        $id = 'fields-input-id-' . $this->id;
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $btnGroup = ((!$this->attrs['btnGroup'])? false: (($this->attrs['btnGroup'] !== true)? 'btn-group btn-group-' . $this->attrs['btnGroup']: 'btn-group'));
        $helpBlock = $this->attrs['helpBlock'];
        $radio = $this->attrs['radio'];
        $checkbox = $this->attrs['checkbox'];
        $check = ((!!$radio)? $radio: $checkbox);
        $multiples = $this->attrs['multiple'];
        $sortable = $this->attrs['sortable'];
        $inline = $this->attrs['inline'];
        $btn = array_search($this->attrs['type'], ['submit', 'button', 'reset']) !== false;
        $hidden = $this->attrs['type'] == 'hidden';
        $content = ((isset($this->attrs[1]))? $this->attrs[1]: ((isset($this->attrs[0]))? $this->attrs[0]: ''));
        $this->attrs['class'] = self::addClass((($btn)? '': (($check)? 'form-check-input': 'form-control')), ((isset($this->attrs['class']))? $this->attrs['class']: ''));
        $name = self::nameResolve($prefixo, $this->attrs['name']);
        $value = self::getValue(self::nameExplode($name), $dados);
        try{
            if(isset($this->attrs['value']) && is_string($this->attrs['value']) && !empty($this->attrs['value']) && preg_match('/^function/', $this->attrs['value'])){
                @eval('$this->attrs[\'value\'] = ' . $this->attrs['value']);
            }
        }catch(Exception $e){}catch(Error $e){}
        if(isset($this->attrs['value']) && gettype($this->attrs['value']) == 'object' && get_class($this->attrs['value']) == 'Closure'){
            $value = $this->attrs['value'] = $this->attrs['value']($value, $this->attrs, $dados);
        }
        $value = (($value === null && isset($this->attrs['value']))? $this->attrs['value']: $value);
        $data = self::nameExplode($name);
        self::setValue($data, $value);
        $attrs = array_diff_key($this->attrs, [
            'label'     => '',
            'group'     => '',
            'name'      => '',
            'btnGroup'  => '',
            'helpBlock' => '',
            'radio'     => '',
            'checkbox'  => '',
            'sortable'  => '',
            'multiple'  => '',
            'value'     => '',
            'inline'    => '',
            0           => '',
            1           => '',
        ]);
        if($hidden){
            self::ModeloInputInput($attrs, $name, $data);
        } elseif($btn){
            ?><button <?php echo self::montaAttr(array_merge($attrs, (($value !== null)? ['name' => $name, 'value' => $value]: []))); ?>><?php echo $content; ?></button><?php
        } else {
            [$prev, $next] = self::base($label, $group); echo $prev;
            if(!$check && ($sortable || $multiples)){
                ?><div id="<?php echo $id; ?>" class="fields-input-input-context" data-input="<?php echo $name; ?>">
                    <div class="fields-input-sortable">
                        <?php $i = -1; foreach((($value === null)? []: $value) as $k => $v){ $i = max($i, (int) $k);
                            self::ModeloInputInput($attrs, "{$name}[{$k}]", $data, $checkbox, $multiples, $sortable);
                        } ?>
                    </div>
                    <template class="ModeloInputInput" data-k="<?php echo ++$i; ?>">
                        <?php self::ModeloInputInput($attrs, "{$name}[{{{$id}-id}}]", [], $checkbox, $multiples, $sortable); ?>
                    </template>
                    <button class="btn btn-default btn-block" data-toggle="fields-input-add" type="button">
                        <i class="fa fa-plus" style="line-height: inherit;"></i>
                    </button>
                </div><?php
            } elseif(!!$check){
                if(!!$btnGroup) echo "<div class=\"$btnGroup\" data-toggle=\"buttons\">";
                foreach(((is_array($check))? $check: [1 => $check]) as $k => $v){
                    if($btnGroup){
                        ?><label class="btn btn-default<?php echo ((!!($val = \Helpers\HtmlHelper::value($name, $data)) && ((is_array($val) && in_array($k, $val)) || ($val == $k)))? ' active': ''); ?>"><?php self::ModeloInputInput($attrs, [$name . ((!!$checkbox)? '[]': ''), $name], $data, $k); echo $v; ?></label><?php
                    }else{
                        ?><div class="form-check<?php echo (($inline)? ' form-check-inline': ''); ?>"><label class="form-check-label"><?php self::ModeloInputInput($attrs, [$name . ((!!$checkbox)? '[]': ''), $name], $data, $k);  ?><?php echo $v; ?></label></div><?php
                    }
                }
                if(!!$btnGroup) echo "</div>";
            } else self::ModeloInputInput($attrs, $name, $data);

             if(!!$helpBlock) echo "<div class=\"help-block\">{$helpBlock}</div>";
            echo $next;
        }
    }

    private static function ModeloInputInput($attrs, $name, $value, $checkbox = false, $multiples = false, $sortable = false){
        if(!$multiples){
            ?><input <?php echo self::montaAttr($attrs); ?> <?php echo self::montaInput($name, $value, $checkbox); ?>><?php
        }else{
            ?><div class="form-group">
                <div class="input-group">
                    <?php if($sortable){ ?>
                        <span class="input-group-btn"><span class="btn btn-default btn-sortable-handle"><i class="fa fa-arrows-v" style="line-height: inherit;"></i></span></span>
                    <?php } ?>
                    <input <?php echo self::montaAttr($attrs); ?> <?php echo self::montaInput($name, $value, $checkbox); ?>>
                    <span class="input-group-btn"><button class="btn btn-default" data-toggle="delete-input" type="button"><i class="fa fa-trash-o" style="line-height: inherit;"></i></button></span>
                </div>
            </div><?php
        }
    }

}