;(function ($){
    $(function () {

        var $inputItens = $('.fields-input-input-context');
        $inputItens.each(function(i, e){
            var $sortable = $(e).find(".fields-input-sortable");
            $sortable.sortable({
                axis: "y",
                cursor: "move",
                helper: "clone",
                handle: ".btn-sortable-handle"
            });
        });

        $('body').on('click', '[data-toggle="fields-input-add"]', function(){
            var $context = $(this).closest('.fields-input-input-context');
            var $sortable = $context.find('.fields-input-sortable');
            var id = $context.attr('id');
            var $template = $context.find('>template.ModeloInputInput');
            var k = parseInt($template.attr('data-k'));
            var panel = $($template.html().trim().split("{{" + id + "-id}}").join(k));
            $template.attr('data-k', ++k);
            $sortable.append(panel);
            $sortable.sortable({
                axis: "y",
                cursor: "move",
                helper: "clone",
                handle: ".btn-sortable-handle"
            });
            panel.trigger('start.input.fields');
        })

        .on("click", '[data-toggle="delete-input"]', function(){
            $(this).closest(".form-group").remove();
        });

    });
})(jQuery);