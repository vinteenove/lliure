<?php
namespace UsrContent\Fields\Panel;
use UsrContent\Field\Field_abstract;
use LliurePanel\ll;
use Persona\Persona;
use UsrContent\Field\Fields;

class Panel extends field_abstract{

    private static $instances = [];
    private $poss, $attrs;

    public function __construct(array  $attrs){
        $this->poss = count(self::$instances);
        self::$instances[] = &$this;
        $this->attrs = array_merge([
            'label'       => 'Panel',
            'group'       => true,
            'name'        => "panel[{$this->poss}]",
            'collapse'    => true,
            'sortable'    => true,
            'button'      => 'Adicionar',

            'title'       => 'title',
            'fields'      => [],

            'value'       => '',
            'class'       => false,
            'placeholder' => false,
            'readonly'    => false,

            'models'      => [],
            'modelBy'     => 'model',
        ], $attrs);

        if(is_string($this->attrs['title'])){
            $this->attrs['title'] = ['name' => $this->attrs['title']];
            $this->attrs['title']['label'] = false;
            $this->attrs['title']['group'] = false;
            $this->attrs['title']['type'] = ((is_array($this->attrs['value']))? 'select': 'text');

            if($this->attrs['title']['type'] == 'select')
                $this->attrs['title']['options'] = $this->attrs['value'];
            else
                $this->attrs['title']['value'] = $this->attrs['value'];

            foreach([
                'class', 'placeholder', 'readonly',
            ] as $k) if($this->attrs[$k]) $this->attrs['title'][$k] = $this->attrs[$k];

            $this->attrs['title'] = [$this->attrs['title']];
        }

        $defalt = array_intersect_key($this->attrs, [
            'label'  => '',
            'title'  => '',
            'fields' => '',
        ]);

        if(empty($this->attrs['models'])) $this->attrs['models'] = ['defalt' => $defalt];

        foreach($this->attrs['models'] as $k => $model){
            $this->attrs['models'][$k] = array_merge($defalt, $this->attrs['models'][$k]);

            $this->attrs['models'][$k]['title'] = new Fields($this->attrs['models'][$k]['title']);
            $this->attrs['models'][$k]['fields'] = new Fields($this->attrs['models'][$k]['fields']);
        }
    }

    public static function header(){
        ll::usr('jquery-ui');
        Persona::add(__DIR__ . '/Panel.css', 'css');
        Persona::add(__DIR__ . '/Panel.js', 'js');
    }

    public function input($dados, $prefixo = null){
        $id = "fields-panel-id-{$this->poss}";
        $nome = self::nameResolve($prefixo, $this->attrs['name']);
        $panels = self::getValue(self::nameExplode($nome), $dados);
        $panels = ((!empty($panels))? $panels: []);
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $button = $this->attrs['button'];
        $collapse = $this->attrs['collapse'];
        $sortable = $this->attrs['sortable'];
        $modelBy = $this->attrs['modelBy'];
        $models = $this->attrs['models'];
        $dafalt = ((isset($models['defalt']))? $models['defalt']: ((isset($models[0]))? $models[0]: []));
        list($prev, $next) = self::base($label, $group); echo $prev; ?>
        <div class="fields-panel-context">
            <div class="panel-group" id="<?php echo $id; ?>" style="margin-bottom: 5px;">
                <?php $i = self::ModalPanelModalInput($dafalt, $models, $modelBy, $panels, $nome, $collapse, $sortable, $dados); ?>
            </div>
            <div class="fields-panel-templates" data-k="<?php echo ++$i; ?>" style="display: none !important;">
                <?php foreach($models as $k => $model){ ?>
                    <template data-template="<?php echo $k; ?>">
                        <?php self::ModalPanelModalInput($dafalt, $models, $modelBy, ['{{' . $id . '-id}}' => [$modelBy => $k]], $nome, $collapse, $sortable, []); ?>
                    </template>
                <?php } ?>
            </div>
            <div class="fields-panel-add-context btn btn-default btn-block">
                <button class="fields-panel-add-header btn btn-default btn-block" data-toggle="fields-panel-add-collapse" data-target="#<?php echo $id; ?>-add-colapse" type="button">
                    <i class="pull-right fa fa-plus" style="line-height: inherit;"></i> <?php echo $button; ?>
                </button>
                <div id="<?php echo $id; ?>-add-colapse" class="fields-panel-add-colapse collapse">
                    <ul class="fields-panel-add-list btn btn-default btn-block">
                        <?php foreach($models as $k => $model){ ?>
                            <li><button type="button" class="btn btn-default btn-block" data-toggle="fields-panel-btn-add" data-template="<?php echo $k; ?>"><?php echo $model['label']; ?></button></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php echo $next; ?>
    <?php }

    private static function ModalPanelModalInput($dafalt, $models, $modelBy = 'model', $panels = [], $prefixo = '', $collapse = true, $sortable = true, $dados = []){
        $i = -1;
        foreach($panels as $k => $panelDados){
            $i = max($i, (int) $k);

            $panel = array_replace($dafalt, ((isset($panelDados[$modelBy], $models[$panelDados[$modelBy]]))? $models[$panelDados[$modelBy]]: []));
            $panelName = self::nameImplode(self::nameExplode($prefixo), [$k => '']);

            self::ModeloPanelInput($panel['title'], $panel['fields'], $panelName, $collapse, $sortable, $dados);

        }return $i;
    }

    private static function ModeloPanelInput(fields $input, fields $fields, $prefixo, $collapse, $sortable, $dados = []){
		ob_start(); $fields->form($dados, $prefixo); $content = ob_get_clean();
		$collapse = (empty($content)? false: $collapse); ?>
        <div class="card">
            <div class="card-header">
                <div class="fields-panel-card-header-padding">
                    <div class="fields-panel-input-group">
                        <div class="fields-panel-input-group-item">
                            <?php if($collapse){ ?>
                                <div class="fields-panel-btn-tab">
                                    <?php if($sortable){ ?>
                                        <samp class="btn btn-default btn-sortable-handle" href="#"><i class="fas fa-arrows-alt-v" style="line-height: inherit;"></i></samp>
                                    <?php } ?>
                                    <button type="button" class="btn btn-default" data-toggle="del-panel"><i class="fas fa-trash-alt" style="line-height: inherit;"></i></button>
                                </div>
                            <?php }else{ ?>
                                <?php if($sortable){ ?>
                                    <div class="fields-panel-input-group-item">
                                        <samp class="btn btn-default btn-sortable-handle" href="#"><i class="fas fa-arrows-alt-v" style="line-height: inherit;"></i></samp>
                                    </div>
								<?php } ?>
                            <?php } ?>
                        </div>
                        <div class="fields-panel-input-group-item">
                            <?php $input->form($dados, $prefixo); ?>
                        </div>
                        <?php if($collapse){ ?>
                            <div class="fields-panel-input-group-item">
                                <button class="btn btn-default" type="button" data-toggle="collapse-panel">
                                    <div class="fields-panel-clouse"></div>
                                </button>
                            </div>
                        <?php }else{ ?>
                            <div class="fields-panel-input-group-item">
                                <button type="button" class="btn btn-default" data-toggle="del-panel"><i class="fas fa-trash-alt" style="line-height: inherit;"></i></button>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
			<?php if(!empty($content)){ ?>
                <div class="panel-collapse<?php echo (($collapse)? ' collapse': ''); ?>">
                    <div class="card-body"><?php echo $content; ?></div>
                </div>
			<?php } ?>
        </div>
    <?php }

}