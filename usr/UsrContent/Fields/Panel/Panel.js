;(function ($){
    $(function () {

        const $panelItens = $(".fields-panel-context");

        $panelItens.each(function(i, e){
            const $context = $(e);
            const $sortable = $context.find(".panel-group");
            $sortable.sortable({
                axis: "y",
                cursor: "move",
                helper: "clone",
                handle: ".btn-sortable-handle"
            });
        });

        $("body").on("click", '.fields-panel-context [data-toggle="collapse-panel"]', function(){
            const $this = $(this);
            const $context = $this.parents(".card").eq(0);
            const $panelItens = $this.parents(".panel-group").eq(0);

            if(!$this.hasClass('collapsed')){
                $panelItens.find("> .card").not($context).removeClass('collapsed');
                $context.addClass('collapsed');

                $panelItens.find("> .card").not($context).find("> .panel-collapse").collapse("hide");
                $context.find("> .panel-collapse").collapse("show");

                $panelItens.find('> .card [data-toggle="collapse-panel"]').not($this).removeClass("collapsed");
                $this.addClass("collapsed");
            }else{
                $context.find("> .panel-collapse").collapse("hide");
                $this.removeClass("collapsed");
                $context.removeClass("collapsed");
            }
        })

        .on("click", '.fields-panel-context [data-toggle="fields-panel-add-collapse"]', function(){
            const $this = $(this);
            const $context = $this.closest('.fields-panel-add-context');
            const $list = $context.find('.fields-panel-add-list > li');

            if($list.length <= 0) return;
            if($list.length == 1) $list.eq(0).find('[data-toggle="fields-panel-btn-add"]').click();
            else $context.find($(this).attr('data-target')).collapse('toggle');
        })

        .on("show.bs.collapse hide.bs.collapse", '.fields-panel-context .fields-panel-add-colapse', function(){
            $('[data-target="#' + $(this).attr('id') + '"]').toggleClass('collapsed');
        })

        .on("click", '.fields-panel-context [data-toggle="fields-panel-btn-add"]', function(){
            const $this = $(this);
            const $context = $this.closest('.fields-panel-context');
            const $sortable = $context.find(" > .panel-group");
            const template = $this.attr('data-template');
            const $templates = $context.find('> .fields-panel-templates');
            const $template = $templates.find('template[data-template="' + template + '"]');
            const id = $sortable.attr('id');
            const k = parseInt($templates.attr('data-k'));
            const $panel = $($template.html().trim().split("{{" + id + "-id}}").join(k));
            $templates.attr('data-k', (k + 1));
            $sortable.append($panel).sortable({
                axis: "y",
                cursor: "move",
                helper: "clone",
                handle: ".btn-sortable-handle"
            });
            $panel.trigger('start.panel.fields');
        })

        .on("click", '.fields-panel-context [data-toggle="del-panel"]', function(){
            const $this = $(this);
            const $context = $this.closest(".card");
            const $panelGroup = $this.closest(".panel-group").eq(0);
            $context.remove();
            $panelGroup.trigger('delete.panel.fields')
        });

    });
})(jQuery);