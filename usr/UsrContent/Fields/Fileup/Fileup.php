<?php
namespace UsrContent\Fields\Fileup;

use UsrContent\Field\Field_abstract;
use LliurePanel\ll;

class Fileup extends field_abstract{

    private static $pathUpload;
    private static $instancias = [];
    private $attrs, $id;

    public function __construct(array $attrs){
        $this->id = count(self::$instancias);
        self::$instancias[] = $this->attrs = array_merge([
            'label'  => 'FileUp',
            'group' => true,
            'name'   => "galeria[{$this->id}]",
            'value'  => '',
        ], $attrs);
    }

    public static function header($pathUpload = ''){
        self::$pathUpload = \Helpers\HttpHelper::pathToUri($pathUpload);
		self::$pathUpload = ((empty(self::$pathUpload))? '': self::$pathUpload . '/');
        \Api\Fileup\Fileup::build();
    }

    public function input($dados, $prefixo = null){
        $attrs = array_diff_key($this->attrs, [
            'label' => '',
        ]);
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $attrs['directory'] =  self::$pathUpload;
        $attrs['name'] = self::nameResolve($prefixo, $attrs['name']);
        $value = self::getValue(self::nameExplode($attrs['name']), $dados);
        $value = ((!empty($value))? $value: $attrs['value']);
        $attrs = array_merge($attrs, [
            'value' => $value,
        ]);
        list($prev, $next) = self::base($label, $group); echo $prev;
        $fileup = new \Api\Fileup\Fileup($attrs);
        echo $fileup;
        echo $next;
    }
}