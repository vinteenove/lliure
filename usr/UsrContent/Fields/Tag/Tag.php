<?php
namespace UsrContent\Fields\Tag;
use UsrContent\Field\Field_abstract;
use UsrContent\Field\Fields;

class Tag extends field_abstract{

    private $tag = 'div', $attrs = [], $content = [];

    public function __construct(array $attrs){

        if(isset($attrs['tag'], $attrs[0])){
            $this->content = ((is_array($attrs[0]))? $attrs[0]: [$attrs[0]]);
            unset($attrs[0]);
        }

        foreach([0, 'tag'] as $k)if(isset($attrs[$k]) && is_string($attrs[$k])){
            $this->tag = $attrs[$k];
            unset($attrs[$k]);
            break;
        }

        foreach([0, 1, 'fields', 'content'] as $k) if(isset($attrs[$k])){
            $this->content = ((is_array($attrs[$k]))? $attrs[$k]: [$attrs[$k]]);
            unset($attrs[$k]);
            break;
        }
        
        $this->attrs = $attrs;
        $this->content = new Fields($this->content);
    }

    public function input($dados, $prefixo = null){
        $name = ((isset($this->attrs['name']) && $this->attrs['name'] !== null)? $this->attrs['name']: '');
        $name = self::nameResolve($prefixo, $name);
        $attrs = $this->attrs;
        $attrs = self::montaAttr($attrs);
        ?><<?php echo $this->tag; ?> <?php echo $attrs; ?>><?php $this->content->form($dados, $name); ?></<?php echo $this->tag; ?>><?php
    }

}