<?php
namespace UsrContent\Fields\Select;
use UsrContent\Field\Field_abstract;

class Select extends field_abstract{

    private static $instancias = [];
    private $attrs, $id;

    public function __construct(array $attrs){
        $this->id = count(self::$instancias);
        self::$instancias[] = $this->attrs = array_merge([
            'label'   => 'Selecinar',
            'group'   => true,
            'name'    => "galeria[{$this->id}]",
            'value'   => '',
            'options' => [],
        ], $attrs);
    }

    public function input($dados, $prefixo = null){
        $attrs = array_diff_key($this->attrs, [
            'label'   => '',
            'value'   => '',
            'options' => '',
            'class'   => '',
        ]);
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $options = $this->attrs['options'];
        $attrs['class'] = self::addClass('form-control', ((isset($this->attrs['class']))? $this->attrs['class']: ''));
        $attrs['name'] = self::nameResolve($prefixo, $attrs['name']);
        $value = self::getValue(self::nameExplode($attrs['name']), $dados);
        $value = ((!is_null($value))? $value: $this->attrs['value']);
        $dado = self::nameExplode($attrs['name']); self::setValue($dado, $value);
        if(is_string($this->attrs['options'])) @eval('$this->attrs[\'options\'] = ' . $this->attrs['options']);
        if(is_callable($this->attrs['options'])) $options = $this->attrs['options'] = $this->attrs['options']($this, $value, $this->attrs, $dados);
        list($prev, $next) = self::base($label, $group); echo $prev; ?>
        <select <?php echo self::montaAttr($attrs); ?>>
            <?php echo self::montaSelect($attrs['name'], $dado, $options, true); ?>
        </select>
        <?php echo $next;
    }
}