<?php
namespace UsrContent\Fields\Edittable;
use UsrContent\Field\Field_abstract;
use LliurePanel\ll;
use Persona\Persona;

class Edittable extends field_abstract{

    private static $instancias = [];
    private $attrs, $id;

    public function __construct(array $attrs){
        $this->id = count(self::$instancias);
        self::$instancias[] = $this->attrs = array_merge([
            'label'  => 'Selecinar',
            'group'  => true,
            'name'   => "excel[{$this->id}]",
            'value'  => '',
            'coluns' => [
                'variavel' => ['label' => 'Variável', 'width' => '30%'],
                'valor'    => ['label' => 'Valor', 'width' => '70%'],
            ],
        ], $attrs);
    }

    public static function header(){
        Persona::add(__DIR__ . '/hammer/hammer.min.js', 'js');
        Persona::add(__DIR__ . '/Edittable.css', 'css');
        Persona::add(__DIR__ . '/Edittable.js', 'js');
    }

    public function input($dados, $prefixo = null){
        $attrs = array_diff_key($this->attrs, [
            'label'   => '',
            'value'   => '',
            'options' => '',
            'class'   => '',
            'coluns'  => '',
        ]);
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $attrs['class'] = self::addClass('form-control', ((isset($this->attrs['class']))? $this->attrs['class']: ''));
        $attrs['name'] = self::nameResolve($prefixo, $attrs['name']);
        $value = self::getValue(self::nameExplode($attrs['name']), $dados);
        $value = ((!is_null($value))? $value: $this->attrs['value']);
        $id = 'fields-edittable-id';
        $dado = self::nameExplode($attrs['name']); self::setValue($dado, $value);
        $max = -1;
        $chave = null; foreach($this->attrs['coluns'] as $chave => $colun) break;
        list($prev, $next) = self::base($label, $group); echo $prev; ?>
        <div id="<?php echo $id; ?>" class="fields-edittable-resize">
            <div class="table table-bordered fields-edittable-table">
                <div class="fields-edittable-tr">
                    <div class="fields-edittable-th" <?php echo self::montaAttr(['style' => ['width' => '20px']]); ?>></div>
                    <?php foreach($this->attrs['coluns'] as $colun){ ?>
                        <div class="fields-edittable-th" <?php echo self::montaAttr(((isset($colun['width']))? ['style' => ['width' => $colun['width']]]: [])); ?>>
                            <?php echo $colun['label']; ?>
                        </div>
                    <?php } ?>
                </div>
                <?php if(isset($value[$chave]) && is_array($value[$chave]) && !empty($value[$chave])) foreach($value[$chave] as $k => $line){ $max = max($max, $k);
                    self::tempalteRow($k, $this->attrs['coluns'], $attrs['name'], $dado);
                } ?>
            </div>
            <div class="fields-edittable-resize-handler table-bordered"></div>
            <template data-k="<?php echo ++$max; ?>">
                <?php foreach(['{{' . $id . '-id}}' => ''] as $k => $line){
                    self::tempalteRow($k, $this->attrs['coluns'], $attrs['name'], []);
                } ?>
            </template>
        </div>
        <?php echo $next;
    }

    private static function tempalteRow($id, $colunas, $prefixo, $dados){ ?>
        <div class="fields-edittable-tr">
            <div class="fields-edittable-td">
                <button type="button" tabindex="-1" class="btn btn-block btn-default" data-toggle="fields-edittable-btn-line"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
            </div>
            <?php foreach($colunas as $name => $atts){ ?>
                <?php self::tempalteData($atts, (($search = self::nameResolve($prefixo, $name)) . '[]'), (self::nameResolve($search, $id)), $dados); ?>
            <?php } ?>
        </div>
    <?php }

    private static function tempalteData($atts, $name, $search, $dados){ ?>
        <label class="fields-edittable-td">
            <div class="textareaAutoHeigth">
                <textarea name="<?php echo $name; ?>" class="form-control"><?php echo \Helpers\HtmlHelper::value($search, $dados); ?></textarea>
                <div class="form-control"></div>
            </div>
        </label>
    <?php }

}