;(function ($){
    $(function(){

        var $body = $('body');
        $body.on('input', '.textareaAutoHeigth > textarea', function(){

            var $this = $(this);
            var $context = $this.parents('.textareaAutoHeigth').eq(0);
            var $content = $context.find('> div')[0];

            //console.log($context, $content);
            $content.innerHTML = this.value + '\n';

        }); $('.textareaAutoHeigth > textarea').trigger('input');


        $body.on('mouseenter', '.fields-edittable-resize-handler', function(){
            if(!this.hammer) {

                var $this = $(this);
                $this.on('mousedown', function (e){ e.preventDefault(); });
                var $context = $this.parents('.fields-edittable-resize').eq(0);
                var id = $context.attr('id');
                var $table = $context.find('> .fields-edittable-table');
                var $template = $context.find('> template');
                var k = parseInt($template.attr('data-k'));

                this.hammer = new Hammer($this[0], { recognizers: [[Hammer.Pan, {threshold: 0}]] });

                var startHeight;
                var rowns;

                this.hammer.on('panstart', function(e){
                    startHeight = $context[0].clientHeight;
                    rowns = $context.find('.fields-edittable-tr');
                    $context.addClass('fields-edittable-resize-hover');
                });

                this.hammer.on('panmove', function (e) {
                    var height = (startHeight + e.deltaY);
                    var index = 0; max = 0;

                    rowns.each(function(){
                        var $this = $(this);
                        index += $this.height();

                        if(index > (height - 15)){
                            $this.addClass('fields-edittable-resize-disabled').find(':input, .form-control').prop('disabled', true).attr({disabled: 'disabled'});
                        }else{
                            $this.removeClass('fields-edittable-resize-disabled').find(':input, .form-control').prop('disabled', false).removeAttr('disabled');
                        }
                    });

                    if(height > index){
                        var panel = $template.html().trim().replace().split("{{" + id + "-id}}").join(k);
                        $template.attr('data-k', ++k);
                        $table.append(panel);
                        rowns = $context.find('.fields-edittable-tr');
                    }

                    $context[0].style.height = height + 'px';
                });

                this.hammer.on('panend', function(e){
                    $context.removeClass('fields-edittable-resize-hover');
                });

            }
        });

        $body.on('click', '[data-toggle="fields-edittable-btn-line"]', function(){
            let $this = $(this);

            if(!$this.data('popover.active'))
                $this.popover({
                    html: true,
                    trigger: 'focus',
                    container: 'body',
                    content: function(){

                        var fieldEditTableRowDelete =  $('<li>').append([$('<a>', {href: '#'}).html('Deletar')]);
                        fieldEditTableRowDelete.click(function(e){
                            $this.closest('.fields-edittable-tr').eq(0).remove();
                            return !!e.stopPropagation();
                        });

                        var fieldEditTableRowAddToUp =  $('<li>').append([$('<a>', {href: '#'}).html('Nova linha a cima')]);
                        fieldEditTableRowAddToUp.click(function(e){

                            var $myLine = $this.closest('.fields-edittable-tr').eq(0);
                            var $context = $this.closest('.fields-edittable-resize').eq(0);
                            var id = $context.attr('id');
                            var $template = $context.find('> template');
                            var k = parseInt($template.attr('data-k'));

                            var panel = $template.html().trim().replace().split("{{" + id + "-id}}").join(k);
                            $template.attr('data-k', ++k);
                            $myLine.before(panel);

                            return !!e.stopPropagation();
                        });

                        var fieldEditTableRowAddToDown =  $('<li>').append([$('<a>', {href: '#'}).html('Nova linha a baixo')]);
                        fieldEditTableRowAddToDown.click(function(e){

                            var $myLine = $this.closest('.fields-edittable-tr').eq(0);
                            var $context = $this.closest('.fields-edittable-resize').eq(0);
                            var id = $context.attr('id');
                            var $template = $context.find('> template');
                            var k = parseInt($template.attr('data-k'));

                            var panel = $template.html().trim().replace().split("{{" + id + "-id}}").join(k);
                            $template.attr('data-k', ++k);
                            $myLine.after(panel);

                            return !!e.stopPropagation();
                        });

                        var fieldEditTableRowSwitchToTheTop =  $('<li>').append([$('<a>', {href: '#'}).html('Mover para cima')]);
                        fieldEditTableRowSwitchToTheTop.click(function(e){

                            let $myLine = $this.closest('.fields-edittable-tr').eq(0);
                            let $context = $this.closest('.fields-edittable-resize').eq(0);
                            let $table = $context.find('> .fields-edittable-table');
                            let $rows = $table.find('> .fields-edittable-tr:not(.fields-edittable-resize-disabled)');
                            let pos = $rows.index($myLine);

                            if(pos > 1) $myLine.prev().before($myLine);
                            return !!e.stopPropagation();
                        });

                        var fieldEditTableRowSwitchToTheBass =  $('<li>').append([$('<a>', {href: '#'}).html('Mover para baixo')]);
                        fieldEditTableRowSwitchToTheBass.click(function(e){

                            let $myLine = $this.closest('.fields-edittable-tr').eq(0);
                            let $context = $this.closest('.fields-edittable-resize').eq(0);
                            let $table = $context.find('> .fields-edittable-table');
                            let $rows = $table.find('> .fields-edittable-tr:not(.fields-edittable-resize-disabled)');
                            let pos = $rows.index($myLine);

                            if(pos < ($rows.length - 1)) $myLine.next().after($myLine);
                            return !!e.stopPropagation();
                        });

                        return $('<li>', {class: 'dropdown-menu fields-edittable-dropdown-menu'}).append([
                            fieldEditTableRowAddToUp,
                            fieldEditTableRowAddToDown,
                            $('<li>', {role: 'separator', class: 'divider'}),
                            fieldEditTableRowSwitchToTheTop,
                            fieldEditTableRowSwitchToTheBass,
                            $('<li>', {role: 'separator', class: 'divider'}),
                            fieldEditTableRowDelete,
                        ]);
                    },
                    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content fields-edittable-popover-content"></div></div>'
                }).data('popover.active', true);
            $this.popover('toggle');
        });

        function fieldEditTableRowDelete(){

        }

    });
})(jQuery);