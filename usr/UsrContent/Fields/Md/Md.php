<?php
namespace UsrContent\Fields\Md;
use UsrContent\Field\Field_abstract;
use LliurePanel\ll;
use Persona\Persona;

class Md extends field_abstract{

    const TYPES = ["1c", "abnf", "accesslog", "actionscript", "ada", "angelscript", "apache", "applescript", "arcade", "arduino", "armasm", "asciidoc", "aspectj", "autohotkey", "autoit", "avrasm", "awk", "axapta", "bash", "basic", "bnf", "brainfuck", "c", "cal", "capnproto", "ceylon", "clean", "clojure-repl", "clojure", "cmake", "coffeescript", "coq", "cos", "cpp", "crmsh", "crystal", "csharp", "csp", "css", "d", "dart", "delphi", "diff", "django", "dns", "dockerfile", "dos", "dsconfig", "dts", "dust", "ebnf", "elixir", "elm", "erb", "erlang-repl", "erlang", "excel", "fix", "flix", "fortran", "fsharp", "gams", "gauss", "gcode", "gherkin", "glsl", "gml", "go", "golo", "gradle", "graphql", "groovy", "haml", "handlebars", "haskell", "haxe", "hsp", "http", "hy", "inform7", "ini", "irpf90", "isbl", "java", "javascript", "jboss-cli", "json", "julia-repl", "julia", "kotlin", "lasso", "latex", "ldif", "leaf", "less", "lisp", "livecodeserver", "livescript", "llvm", "lsl", "lua", "makefile", "markdown", "mathematica", "matlab", "maxima", "mel", "mercury", "mipsasm", "mizar", "mojolicious", "monkey", "moonscript", "n1ql", "nestedtext", "nginx", "nim", "nix", "node-repl", "nsis", "objectivec", "ocaml", "openscad", "oxygene", "parser3", "perl", "pf", "pgsql", "php-template", "php", "plaintext", "pony", "powershell", "processing", "profile", "prolog", "properties", "protobuf", "puppet", "purebasic", "python-repl", "python", "q", "qml", "r", "reasonml", "rib", "roboconf", "routeros", "rsl", "ruby", "ruleslanguage", "rust", "sas", "scala", "scheme", "scilab", "scss", "shell", "smali", "smalltalk", "sml", "sqf", "sql", "stan", "stata", "step21", "stylus", "subunit", "swift", "taggerscript", "tap", "tcl", "thrift", "tp", "twig", "typescript", "vala", "vbnet", "vbscript-html", "vbscript", "verilog", "vhdl", "vim", "wasm", "wren", "x86asm", "xl", "xml", "xquery", "yaml", "zephir"];
    private static $inputs = [];
    private $index;
    private $attrs;

    public function __construct(array $attrs){
        $this->index = count(self::$inputs);
        self::$inputs[] = $this->attrs = array_merge([
            'label'    => 'Text Area',
            'group'   => true,
            'name'     => "editormd[{$this->index}]",
            'linguage' => 'markdown',
            'value'    => '',
        ], $attrs);
    }

    public static function header(){
        ll::usr('highlight');

        $types = array_column(self::$inputs, 'linguage');
        array_map('strtolower', $types);
		$types = array_unique($types);
		$types = array_intersect($types, self::TYPES);
	
		foreach($types as $type){
			Persona::script("https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.7.0/languages/{$type}.min.js", Persona::COMPONENT_PRIORITY);
		}

        \FontAwesome\FontAwesome::build();
        Persona::add(__DIR__ . '/Md.css', 'css', 5);
        Persona::add(__DIR__ . '/Md.js', 'js', 5);
    }

    public function salve(array &$dados){
        if(isset($this->attrs['value'])){
            $name = self::nameExplode($this->attrs['name']);
            $value =& self::getValue($name, $dados);
            try{
                if(isset($this->attrs['value']) && is_string($this->attrs['value']) && !empty($this->attrs['value']) && preg_match('/^function/', $this->attrs['value'])){
                    @eval('$this->attrs[\'value\'] = ' . $this->attrs['value']);
                }
            }catch(Exception $e){}catch(Error $e){}
            if(isset($this->attrs['value']) && gettype($this->attrs['value']) == 'object' && get_class($this->attrs['value']) == 'Closure'){
                $value = $this->attrs['value'] = $this->attrs['value']($value, $this->attrs, $dados);
            }
        }
    }

    public function input($dados, $prefixo = null){
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $nome = self::nameResolve($prefixo, $this->attrs['name']);
        $value =& self::getValue( self::nameExplode($nome), $dados);
        try{
            if(isset($this->attrs['value']) && is_string($this->attrs['value']) && !empty($this->attrs['value']) && preg_match('/^function/', $this->attrs['value'])){
                @eval('$this->attrs[\'value\'] = ' . $this->attrs['value']);
            }
        }catch(Exception $e){}catch(Error $e){}
        if(isset($this->attrs['value']) && gettype($this->attrs['value']) == 'object' && get_class($this->attrs['value']) == 'Closure'){
            $value = $this->attrs['value'] = $this->attrs['value']($value, $this->attrs, $dados);
        }
        $value = (($value === null && isset($this->attrs['value']))? $this->attrs['value']: $value);
        $attr = array_diff_key($this->attrs, [
            'label' => '',
            'value' => '',
            'type' => '',
        ]);
        list($prev, $next) = self::base($label, $group); echo $prev; ?>
            <div class="fields-md-context">
                <div class="fields-md-editor form-control panel-default">
                    <?php if($attr['linguage'] == 'markdown'){ ?>
                        <div class="fields-md-menu panel-heading">
                            <div class="btn-toolbar" role="toolbar">
                                <div class="btn-group btn-group-sm mr-1" role="group">
                                    <button type="button" class="btn btn-default" tabindex="-1" data-toggle="fields-md-toggle" data-func="bold">
                                        <i class="fa fa-bold" aria-hidden="true"></i>
                                    </button>

                                    <button type="button" class="btn btn-default" tabindex="-1" data-toggle="fields-md-toggle" data-func="italic">
                                        <i class="fa fa-italic" aria-hidden="true"></i>
                                    </button>

                                    <button type="button" class="btn btn-default" tabindex="-1" data-toggle="fields-md-toggle" data-func="strikethrough">
                                        <i class="fa fa-strikethrough" aria-hidden="true"></i>
                                    </button>
                                </div>

                                <div class="btn-group btn-group-sm mr-1" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default btn-sm" tabindex="-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-heading" aria-hidden="true"></i> <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#" data-toggle="fields-md-toggle" tabindex="-1" data-func="h1"><small class="fa fa-heading" aria-hidden="true"></small> <strong>1</strong></a></li>
                                            <li><a class="dropdown-item" href="#" data-toggle="fields-md-toggle" tabindex="-1" data-func="h2"><small class="fa fa-heading" aria-hidden="true"></small> <strong>2</strong></a></li>
                                            <li><a class="dropdown-item" href="#" data-toggle="fields-md-toggle" tabindex="-1" data-func="h3"><small class="fa fa-heading" aria-hidden="true"></small> <strong>3</strong></a></li>
                                            <li><a class="dropdown-item" href="#" data-toggle="fields-md-toggle" tabindex="-1" data-func="h4"><small class="fa fa-heading" aria-hidden="true"></small> <strong>4</strong></a></li>
                                            <li><a class="dropdown-item" href="#" data-toggle="fields-md-toggle" tabindex="-1" data-func="h5"><small class="fa fa-heading" aria-hidden="true"></small> <strong>5</strong></a></li>
                                            <li><a class="dropdown-item" href="#" data-toggle="fields-md-toggle" tabindex="-1" data-func="h6"><small class="fa fa-heading" aria-hidden="true"></small> <strong>6</strong></a></li>
                                        </ul>
                                    </div>
                                    <button type="button" class="btn btn-default" tabindex="-1" data-toggle="fields-md-toggle" data-func="hr">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </button>
                                </div>

                                <div class="btn-group btn-group-sm" role="group">
                                    <button type="button" class="btn btn-default" tabindex="-1" data-toggle="fields-md-toggle" data-func="ul">
                                        <i class="fa fa-list-ul" aria-hidden="true"></i>
                                    </button>

                                    <button type="button" class="btn btn-default" tabindex="-1" data-toggle="fields-md-toggle" data-func="ol">
                                        <i class="fa fa-list-ol" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="fields-md-autoheight">
                        <textarea class="fields-md-editable form-control" name="<?php echo $nome; ?>" data-linguage="<?php echo $attr['linguage']; ?>"><?php echo $value; ?></textarea>
                        <div class="fields-md-amostra form-control"><?php echo htmlspecialchars($value); ?></div>
                    </div>
                </div>
            </div>
        <?php echo $next;
    }
}