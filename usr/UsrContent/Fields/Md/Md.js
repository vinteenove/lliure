;(function ($){
    $(function () {

        var $body = $('body');

        $body.on('input', '.fields-md-context > .fields-md-editor > .fields-md-autoheight > .fields-md-editable', function(){

            var $this = $(this);
            var language = $this.attr('data-linguage');
            var $context = $this.closest('.fields-md-autoheight');
            var $content = $context.find('> .fields-md-amostra')[0];
            var text = language === 'text'? this.value : (hljs.highlight(this.value , {language})).value;
            $content.innerHTML = text + '\n';

        }); $('.fields-md-context > .fields-md-editor > .fields-md-autoheight > .fields-md-editable').trigger('input');

        function activeFunction(e){
            var $this = $(this);
            var func = $this.attr('data-func');
            var $context = $this.parents('.fields-md-context').eq(0);
            var $textarea = $context.find('.fields-md-editable');
            var editable = $textarea[0];

            var split = editable.value.split(/\n/ig);
            var lineStart = false, lineEnd = false;

            var position = 0;
            for(var i = 0; i < split.length; ++i){
                var p = ((split.length == i)? 0: 1);
                position = position + split[i].length + ((split.length == i)? 0: 1);

                if(lineStart === false && (editable.selectionStart + 1) <= position) lineStart = i;

                if(lineEnd === false && (editable.selectionEnd + p) <= position) lineEnd = i;
            }

            var dados = {
                value: editable.value,

                selectionStart: editable.selectionStart,
                selectionEnd: editable.selectionEnd,

                lines: split,

                lineStart: lineStart,
                lineEnd: lineEnd
            };

            if($this.is('[type="file"]')){
                var reader = new FileReader();
                reader.onload = function(f){

                    dados.file = f.target.result;
                    $textarea.trigger(func + '.toggle.md.fields', [dados]);

                }; reader.readAsDataURL(this.files[0]);

                var novo = $this.clone();
                $this.after(novo);
                $this.remove();
            }else $textarea.trigger(func + '.toggle.md.fields', [dados]);

            e.stopPropagation();
            return false;
        }

        $body.on('click',  '[data-toggle="fields-md-toggle"]', activeFunction);
        $body.on('change', '[data-change="fields-md-toggle"]', activeFunction);

        String.prototype.splice = function(strar, end, str){
            return this.slice(0, strar) + str + this.slice(end);
        };

        $body.on('bold.toggle.md.fields', 'textarea', function(e, dados){
            $(this).val(dados.value.splice(dados.selectionStart, dados.selectionEnd, "**" + dados.value.slice(dados.selectionStart, dados.selectionEnd) + "**")).trigger('input');
        });

        $body.on('italic.toggle.md.fields', 'textarea', function(e, dados){
            $(this).val(dados.value.splice(dados.selectionStart, dados.selectionEnd, "_" + dados.value.slice(dados.selectionStart, dados.selectionEnd) + "_")).trigger('input');
        });

        $body.on('strikethrough.toggle.md.fields', 'textarea', function(e, dados){
            $(this).val(dados.value.splice(dados.selectionStart, dados.selectionEnd, "~~" + dados.value.slice(dados.selectionStart, dados.selectionEnd) + "~~")).trigger('input');
        });

        $body.on('ul.toggle.md.fields', 'textarea', function(e, dados){
            var add = !(dados.lines[dados.lineStart].length >= 2 && dados.lines[dados.lineStart].match(/^([-+*] )/));

            for(var i = dados.lineStart; i <= dados.lineEnd; i++){
                var tem = dados.lines[i].match(/^([-+*] |\d+\. )/);
                tem = ((!tem)? 0: tem[1].length);
                if(add) dados.lines[i] = dados.lines[i].splice(0, tem, "- ");
                else dados.lines[i] = dados.lines[i].splice(0, tem, '');

            }$(this).val(dados.lines.join("\n")).trigger('input');
        });

        $body.on('ol.toggle.md.fields', 'textarea', function(e, dados){
            var add = !(dados.lines[dados.lineStart].length >= 2 && dados.lines[dados.lineStart].match(/^(\d+\. )/));

            for(var i = dados.lineStart, a = 1; i <= dados.lineEnd; i++, a++){
                var tem = dados.lines[i].match(/^([-+*] |\d+. )/);
                tem = ((!tem)? 0: tem[1].length);
                if(add) dados.lines[i] = dados.lines[i].splice(0, tem, a + ". ");
                else dados.lines[i] = dados.lines[i].splice(0, tem, '');

            }$(this).val(dados.lines.join("\n")).trigger('input');
        });

        $body.on([
            'h1.toggle.md.fields',
            'h2.toggle.md.fields',
            'h3.toggle.md.fields',
            'h4.toggle.md.fields',
            'h5.toggle.md.fields',
            'h6.toggle.md.fields'
        ].join(' '), 'textarea', function(e, dados){
            var i = parseInt(e.type[1]);
            var b = dados.lines[dados.lineStart].match(/^(#{1,6})/);
            b = ((!b)? 0: b[1].length);

            if(b == i) dados.lines[dados.lineStart] = dados.lines[dados.lineStart].splice(0, b, '');
            else dados.lines[dados.lineStart] = dados.lines[dados.lineStart].splice(0, b, ("#".repeat(i)));

            $(this).val(dados.lines.join("\n")).trigger('input');
        });

        $body.on('hr.toggle.md.fields', 'textarea', function(e, dados){

            if(dados.lines[dados.lineStart].match(/^\-{3,}$|^\*{3,}$|^\_{3,}$/))
                dados.lines.splice(dados.lineStart, 1);
            else
                dados.lines.splice(dados.lineStart + 1, 0, '', '---', '');

            $(this).val(dados.lines.join("\n")).trigger('input');
        });

        $body.on('img.toggle.md.fields', 'textarea', function(e, dados){
            $(this).val(dados.value.splice(dados.selectionStart, 0, '![](' + dados.file + ')')).trigger('input');
        });

    });
})(jQuery);


