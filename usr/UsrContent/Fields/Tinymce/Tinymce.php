<?php
namespace UsrContent\Fields\Tinymce;
use LliurePanel\ll;
use UsrContent\Field\Field_abstract;
use Persona\Persona;

class Tinymce extends Field_abstract{

    private static $inputs = [];
    private $index;
    private $attrs;

    public function __construct(array $attrs){
        $this->index = count(self::$inputs);
        $attrs['class'] = (((isset($attrs['class']))? $attrs['class'] . ' ': '') . 'content-fields-tinymce form-control');
        self::$inputs[] = $this->attrs = array_merge([
            'label' => 'Text Area',
            'group' => true,
            'name'  => "textarea[{$this->index}]",
            'value' => '',
        ], $attrs);
    }

    public static function header(){
        ll::api('tinymce');
        Persona::add(__DIR__ . '/Tinymce.js', 'js');
    }

    public function input($dados, $prefixo = null){
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $attr = array_diff_key($this->attrs, [
            'label' => '',
            'value' => '',
            'type' => '',
        ]);
        $this->attrs['name'] = self::nameResolve($prefixo, $this->attrs['name']);
        $value =& self::getValue(self::nameExplode($this->attrs['name']), $dados);
        $value = (($value === null && isset($this->attrs['value']))? $this->attrs['value']: $value);
        list($prev, $next) = self::base($label, $group); echo $prev; ?>
        <textarea <?php echo self::montaAttr($attr); ?>><?php echo $value; ?></textarea>
        <?php echo $next;
    }
}