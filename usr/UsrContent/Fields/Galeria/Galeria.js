;(function ($){
    $(function () {

        var $galeriaItens = $(".fields-galeria-contexto");
        $galeriaItens.each(function(i, e){
            var $sortable = $(e).find(".fields-galeria-itens");
            $sortable.sortable({
                axis: "y",
                cursor: "move",
                helper: "clone",
                handle: ".btn-galeria-sortable"
            });
        });

        $('body').on('click', '[data-toggle="galeria-btn-add"]', function(){
            var $context = $(this).closest('.fields-galeria-contexto');
            var $sortable = $context.find('.fields-galeria-itens');

            var id = $context.attr('id');
            var $template = $context.find('>template.ModeloGaleriaInput');
            var k = parseInt($template.attr('data-k'));
            var panel = $template.html().trim().replace().split("{{" + id + "-id}}").join(k);
            $template.attr('data-k', ++k);

            $sortable.append(panel);
            $sortable.sortable({
                axis: "y",
                cursor: "move",
                helper: "clone",
                handle: ".btn-galeria-sortable"
            });
        });
        
    });
})(jQuery);