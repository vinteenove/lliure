<?php
namespace UsrContent\Fields\Galeria;
use UsrContent\Field\Field_abstract;
use LliurePanel\ll;
use Persona\Persona;

class Galeria extends field_abstract{

    private static $pathUpload;
    private static $galerias = [];
    private $attrs, $id;

    public function __construct(array $attrs){
        \Api\Fileup\Fileup::build();
        $this->id = count(self::$galerias);
        self::$galerias[] = $this->attrs = array_replace([
            'label'  => 'Galeria',
            'group'  => true,
            'name'   => "galeria[{$this->id}]",
            'accept' => 'png jpg',
            'button' => 'Imagem',
            'mode'   => fileup::IMAGE_SM,
            'crop'   => false,
        ], $attrs);
    }

    public static function header($pathUpload = ''){
        self::$pathUpload = \Helpers\HttpHelper::pathToUri($pathUpload) . '/';
        \Api\Fileup\Fileup::build();
        ll::usr('jquery-ui');
        Persona::add(__DIR__ . '/Galeria.css', 'css');
        Persona::add(__DIR__ . '/Galeria.js', 'js');
    }

    public function input ($dados, $prefixo = null) {
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $name = self::nameResolve($prefixo, $this->attrs['name']);
        $imgs = self::getValue(self::nameExplode($name), $dados);
        $imgs = ((is_array($imgs))? $imgs: ((!empty($imgs))? [$imgs]: []));
        $id = 'fields-galeria-input-' . $this->id;
        list($prev, $next) = self::base($label, $group); echo $prev; ?>
            <div id="<?php echo $id; ?>" class="fields-galeria-contexto" data-galeria="<?php echo $name; ?>">
                <div class="fields-galeria-itens" style="position: relative;">
                    <?php $i = -1; foreach($imgs as $k => $img){ $i = max($i, (int) $k); ?>
                        <?php self::ModeloGaleriaInput($name . '[' . $k . ']', $img, $this->attrs['accept'], $this->attrs['button'], $this->attrs['mode'], $this->attrs['crop']); ?>
                    <?php } ?>
                </div>
                <template class="ModeloGaleriaInput" data-k="<?php echo ++$i; ?>">
                    <?php self::ModeloGaleriaInput($name . '[{{' . $id . '-id}}]', '', $this->attrs['accept'], $this->attrs['button'], $this->attrs['mode'], $this->attrs['crop']); ?>
                </template>
                <button class="btn btn-default btn-block" data-toggle="galeria-btn-add" type="button">
                    <i class="fa fa-plus" style="line-height: inherit;"></i> Adicionar
                </button>
                <?php /* <div class="text-right">
                    <div class="btn-group dropup">
                        <button type="button" class="btn btn-default galeria-btn-add">
                            <i class="fa fa-plus" style="line-height: inherit;"></i> Adicionar imagem
                        </button>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-caret-up" style="line-height: inherit"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><i class="fa fa-picture-o"></i> Imagem</a></li>
                            <li><a href="#"><i class="fa fa-file-text-o"></i> Arquivo</a></li>
                            <li><a href="#"><i class="fa fa-youtube-play"></i> You tube</a></li>
                        </ul>
                    </div>
                </div> */ ?>
            </div>
        <?php echo $next;
    }

    public function salve(array &$dados){
        $imgs =& self::getValue(self::nameExplode($this->attrs['name']), $dados);
        foreach(((is_array($imgs))? $imgs: []) as $k => $i) if(empty($i)) unset($imgs[$k]);
    }

    private static function ModeloGaleriaInput($campo, $foto = '', $accept = '*', $button = 'Imagem', $mode = fileup::IMAGE_SM, $crop = false){ ?>
        <div class="form-group field-galeria-form-group">
            <div class="btn btn-default btn-galeria-sortable">
                <i class="fa fa-arrows-v"></i>
            </div>
            <?php $inputFoto = new fileup(array_merge([
                'name'      => $campo,
                'value'     => $foto,
                'accept'    => $accept,
                'button'    => $button,
                'mode'      => $mode,
                'directory' => self::$pathUpload,],
                ((!!$crop)? ['crop' => $crop]: [])
            )); echo $inputFoto; ?>
        </div>
    <?php }
}