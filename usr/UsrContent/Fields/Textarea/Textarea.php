<?php
namespace UsrContent\Fields\Textarea;
use UsrContent\Field\Field_abstract;

class Textarea extends field_abstract{

    private static $inputs = [];
    private $attrs;

    public function __construct(array $attrs){
        $i = count(self::$inputs);
        self::$inputs[] = $this->attrs = array_merge([
            'label' => 'Text Area',
            'group' => true,
            'name'  => "textarea[{$i}]",
        ], $attrs);
    }

    public function input($dados, $prefixo = null){
        $label = $this->attrs['label'];
        $group = $this->attrs['group'];
        $attrs = $this->attrs;
        $attrs['name'] = $name = self::nameResolve($prefixo, $this->attrs['name']);
        $value =& self::getValue(self::nameExplode($name), $dados);
        $value = (($value === null && isset($this->attrs['value']))? $this->attrs['value']: $value);
        $attrs['class'] = ((isset($this->attrs['class']))? $this->attrs['class']. ' ': ''). 'form-control';
        list($prev, $next) = self::base($label, $group); echo $prev; ?>
            <textarea <?php echo self::montaAttr($attrs); ?>><?php echo $value; ?></textarea>
        <?php echo $next;
    }
}