<?php
namespace UsrContent\Fields\Tab;
use UsrContent\Field\Field_abstract;
use UsrContent\Field\Fields;

class Tab extends field_abstract{

    private static $instances = [];
    private $poss = 0;
    private $attrs;

    public function __construct(array  $attrs){
        $this->poss = count(self::$instances);
        self::$instances[] =& $this;
        $this->attrs = array_merge([
            'active' => 0,
            'group'   => true,
            'tabs' => [],
        ], $attrs);
        if(is_array($this->attrs['tabs']) && !empty($this->attrs['tabs'])) foreach($this->attrs['tabs'] as $k => $tab){
            $this->attrs['tabs'][$k] = array_intersect_key(array_replace($ks = [
                'label' => '',
                'name' => '',
                'fields' => [],
            ], $this->attrs['tabs'][$k]), $ks);
            if(isset($this->attrs['tabs'][$k]['fields']) && is_array($this->attrs['tabs'][$k]['fields']))
                $this->attrs['tabs'][$k]['fields'] = new Fields($this->attrs['tabs'][$k]['fields']);
        }
    }

    public function input($dados, $prefixo = null){
        $group = $this->attrs['group'];
        $this->attrs['active'] = min(0, max((count($this->attrs['tabs']) - 1), $this->attrs['active']));
        $id = "fields-tab-{$this->poss}-tab-";
        list($prev, $next) = self::base(false, $group); ?>
        <?php echo $prev; ?>
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <?php $i = 0; foreach($this->attrs['tabs'] as $k => $tab){ ?>
                        <li class="nav-item" role="presentation"><a class="nav-link<?php echo (($this->attrs['active'] == $i)? ' active': ''); ?>" data-toggle="tab" href="#<?php echo $id . $i; ?>" role="tab"><?php echo $tab['label']; ?></a></li>
                    <?php $i++;} ?>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <?php $i = 0; foreach($this->attrs['tabs'] as $k => $tab){ ?>
                        <div role="tabpanel" class="tab-pane<?php echo (($this->attrs['active'] == $i)? ' active': ''); ?>" id="<?php echo $id . $i; ?>"><?php echo $tab['fields']->form($dados, self::nameResolve($prefixo, $tab['name'])); ?></div>
                    <?php $i++;} ?>
                </div>
            </div>
        </div>
        <?php echo $next;
    }

}