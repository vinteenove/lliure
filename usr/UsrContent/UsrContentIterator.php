<?php
namespace UsrContent;

use LliurePanel\ll;
use ArrayIterator;

class UsrContentIterator extends ArrayIterator{
    private $diretorioData, $files = [], $filtered = [], $paged = [];

    public function __construct($diretorioData){
        $this->diretorioData = $diretorioData;
        $files = @scandir($this->diretorioData);
        if(is_array($files)) natsort($files);
        $this->paged = $this->filtered = $this->files = array_filter(array_values(array_diff(((is_array($files))? $files: []), ['.', '..'])), function($v){
            return preg_match('/\.json$/i', $v);
        });
    }

    /**
     * @param array $where
     * @param array $order
     * @param int   $pp
     * @param int   $pg
     * @return UsrContentIterator
     */
    public function get(array $where = [], array $order = [], $pp = null, $pg = 1){

        if(!!$where){
            $this->comparing($where);
        }

        $this->filtered = ((empty($this->filtered))? []: $this->filtered);

        if(!!$order){
            $this->ordering($order);
        }

        $pagination = [$this->filtered];

        if($pp !== null && $pp > 0 && $pg > 0){
            $pagination = array_chunk($this->filtered, $pp);
        }

        $this->paged = ((isset($pagination[($pg - 1)]))? $pagination[($pg - 1)]: []);

        if($this->valid()){
            $this->current();
        }

        return $this;
    }

    public function getById($id = null){
        $id = (($id !== null)? $id: basename($this->diretorioData));
        return $this->seek($id);
    }

    public function total(){
        return count($this->filtered);
    }

    public function totalGeneral(){
        return count($this->files);
    }

    public function nextId(){
        $files = $this->files;
        end($files);
        $file = (int) basename(current($files), '.json');
        $index = (int) key($files);
        return (max($file, $index, 0) + 1);
    }


    /** @return UsrContentIterator */
    public function current(){
        foreach((array) $this as $k => $v) $this->offsetUnset($k);
        foreach(self::load($this->diretorioData, current($this->paged)) as $k => $v) $this->offsetSet($k, $v);
        return $this;
    }

    public function rewind(){
        return reset($this->paged);
    }

    public function valid(){
        return key($this->paged) !== null;
    }

    public function key(){
        return key($this->paged);
    }

    public function next(){
        return next($this->paged);
    }

    public function seek($ID){
        $prev = @$this->paged[$this->key()];
        for($this->rewind(); $this->valid() && $this->paged[$this->key()] != "$ID.json"; $this->next()) continue;
        if($this->valid()) return $this->current();
        elseif(!is_null($prev)) $this->seek($prev);
        trigger_error('Não existe o ID [' . $ID . ']', E_USER_WARNING);
        return false;
    }


    public function __get($name){
        return $this->offsetGet($name);
    }

    public function __set($name, $value){
        $this->offsetSet($name, $value);
    }

    public function __isset($name){
        return $this->offsetExists($name);
    }

    public function __unset($name){
        $this->offsetUnset($name);
    }

    public function __debugInfo(){
        return [
            'data' => (array) $this,
            'total' => $this->total(),
            'general' => $this->totalGeneral(),
            'key' => $this->key(),
            'file' => @$this->paged[$this->key()],
            'files' => $this->files,
            'filtered' => $this->filtered,
            'paged' => $this->paged,
            'diretorioData' => $this->diretorioData,
        ];
    }


    private function ordering(array $order = []){
        if(!!$order) foreach(array_reverse($order) as $c => $m){

            // define a direção da ordenação ASC = 1, DESC = -1
            $dir = (($m == 'ASC')? 1: -1);

            uasort($this->filtered, function($a, $b) use (&$c, &$dir){

                // carrega dos dados dos itens;
                $a = self::load($this->diretorioData, $a);
                $b = self::load($this->diretorioData, $b);

                // verifica se nos dados dos registros exite a coluna para comparação
                switch(\Helpers\HtmlHelper::value($c, $a) . \Helpers\HtmlHelper::value($c, $b)){
                    case '10':
                        // retorna 1 * a direção se o primiro array tiver a coluna
                        return 1 * $dir;
                        break;
                    case '01':
                        // retorna -1 * a direção se o segundo array tiver a colina
                        return -1 * $dir;
                        break;
                    case '11':
                        // retorma a ordenação natural * a direção
                        return strnatcmp((@(string) \Helpers\HtmlHelper::value($c,$a)), (@(string) \Helpers\HtmlHelper::value($c, $b))) * $dir;
                        break;
                    default:
                        // retorna 0 se os dos dois não existirem
                        return 0;
                        break;
                }
            });
        }
    }

    private function comparing(array $where = []){
        if(!$where) return;

        $bWhere = self::buildWhere($where);
        if(empty($bWhere)) return;

        $bWhere = 'return (' . $bWhere . ');';
        $this->filtered = [];
        foreach($this->files as $k => $file){
            $dados = self::load($this->diretorioData, $file);
            if(eval($bWhere)) $this->filtered[] = $file;
        }
    }

    private static function compare($chave, $comp, $value){
        $comp = strtoupper($comp);

        if (!in_array($comp, [
            "=", "==", "===",
            ">=", "<=",
            "!=", "!==", "NOT",
            ">", "<",
            "IS", "NOT_IS",
            "IN", "NOT_IN",
            'LIKE', 'LIKE_LEFT', 'LIKE_RIGHT', 'LIKE_CLEAR',
            'NOT_LIKE', 'NOT_LIKE_LEFT', 'NOT_LIKE_RIGHT', 'NOT_LIKE_CLEAR'
        ])) return null;

        $chave = ((is_object($chave))? (array) $chave: $chave);
        $value = ((is_object($value))? (array) $value: $value);

        $in = in_array($comp, ['IN', 'NOT_IN']);
        $is = in_array($comp, ['IS', 'NOT_IS']);

        $like =
            (in_array($comp, array('LIKE_CLEAR', 'NOT_LIKE_CLEAR'))? null:
                (in_array($comp, array('LIKE',       'NOT_LIKE'      ))? 0:
                    (in_array($comp, array('LIKE_LEFT',  'NOT_LIKE_LEFT' ))? -1:
                        (in_array($comp, array('LIKE_RIGHT', 'NOT_LIKE_RIGHT'))? 1: false))));

        $comp =  (in_array($comp, ['=', 'IN', 'IS'])? '==' : $comp);
        $comp =  (in_array($comp, ['NOT', 'NOT_IN', 'NOT_IS'])? '!=' : $comp);
        $nega = !(in_array($comp, ['==', '===', '<=', '>=', 'LIKE', 'LIKE_LEFT', 'LIKE_RIGHT', 'LIKE_CLEAR']));
        $comp =  (in_array($comp, ['LIKE_CLEAR', 'NOT_LIKE_CLEAR', 'LIKE', 'NOT_LIKE', 'LIKE_LEFT', 'NOT_LIKE_LEFT', 'LIKE_RIGHT', 'NOT_LIKE_RIGHT'])? 'LIKE': $comp);

        $r = false;
        switch($t = ((int) is_array($chave)) . ((int) is_array($value))){
            default:
                if($comp == 'LIKE'){
                    $r = preg_match(($m = ('/' . (($like === null || $like ===  1)? '^': ''). preg_quote((string) $value). (($like === null || $like === -1)? '$': '') . '/im')), (string) $chave);
                    $r = (($nega)? !$r: $r);

                }else eval('$r = $chave ' .  $comp . ' $value;');
                break;
            case '10':
                list($chave, $value) = [$value, $chave];
            case '01':
                $chave = [$chave];
            case '11':
                if($in){
                    $r = (count(array_intersect(array_unique($chave), array_unique($value))) > 0);
                    $r = (($nega)? !$r: $r);

                }elseif($is){
                    $r = ((count($chave = array_unique($chave))) == (count($value = array_unique($value))) &&  count(array_intersect($chave, $value)) == count($chave));
                    $r = (($nega)? !$r: $r);

                }elseif($like !== false){
                    $r = (($t = count(array_intersect(array_unique($chave), array_unique($value)))) <= count(array_unique($chave)) && $t > 0);
                    $r = (($nega)? !$r: $r);

                }else eval('$r = $chave ' .  $comp . ' $value;');
                break;
        }
        return $r;
    }

    private static function buildWhere(array $where){
        $r = '';
        $pu = true;
        foreach($where as $k => $v) switch(((is_array($v))? count($v): '')){
            case 3:
                if($pu === false) $r .= ' && '; $pu = false;
                $r .= 'self::compare(\\Helpers\\HtmlHelper::value($where[' . $k . '][0], $dados), $where[' . $k . '][1], $where[' . $k . '][2])';

                break;
            case 2:
                if($pu === false) $r .= ' && '; $pu = false;
                $r .= 'self::compare(\\Helpers\\HtmlHelper::value($where[' . $k . '][0], $dados), "==", $where[' . $k . '][1])';

                break;
            case 1:
                if($pu === false) $r .= ' && '; $pu = false;
                $r .= 'self::compare(\\Helpers\\HtmlHelper::value(array_keys($where[' . $k . '])[0], $dados), "==", array_values($where[' . $k . '])[0])';

                break;
            default:
                $v = ((in_array(strtoupper($v), ['AND', 'OR', 'XOR', '||', '&&', '(', ')']))? $v: '');
                $v = (($k = array_search(strtoupper($v), ['&&' => 'AND', '||' => 'OR', ]))? $k: $v);

                $pu = (($v == ')')? false: ((in_array($v, ['&&', '||', 'XOR']))? true: $pu));
                if(($v == '(') && $pu === false) {$r .= ' && '; $pu = true;}
                if(in_array($v, ['&&', '||', 'XOR'])) $v = " {$v} ";

                $r .= ((!empty($v))? ((string) $v): '');

                break;
        }
        return $r;
    }

    private static function load($directory, $file){
        return array_merge(['id' => ''], json_decode(file_get_contents(($directory . $file)), true), ['id' => basename($file, '.json')]);
    }
}