<?php

namespace UsrContent\Field;
use LliurePanel\ll;
use Persona\Persona;

Persona::add('\UsrContent\Field\Fields::footer', 'call:footer', -1);

class Fields{

    private array $tFields = [];
    private static array $sFilds = [];
    private static array $types = [];

    function __construct(array $campos){
        foreach($campos as $field){
            if(is_array($field)){
                if(isset($field['type'])){
                    $type = $field['type'];
                    $class = ucfirst(strtolower($type));
                    $class = "\\UsrContent\\Fields\\{$class}\\{$class}";
                    $status = class_exists($class);
                    if(!$status){
                        $class = "\\UsrContent\\Fields\\Input\\Input";
                        $status = class_exists($class);
                    }
                    if(!$status){
                        $status = class_exists($type);
                    }
                }else{
                    $class = "\\UsrContent\\Fields\\Tag\\Tag";
                    $status = class_exists($class);
                }
                if($status){
                    self::$types[$class] = $status;
                    $fieldClass = new $class($field);
                    self::$sFilds[] =& $fieldClass;
                    $this->tFields[] =& $fieldClass;
                    unset($fieldClass);
                }
            }else{
                $this->tFields[] = (string) $field;
            }
        }
    }

    /**
     * @param array|UsrContentIterator $dados
     */
    public function form($dados, $prefixo = null){
        foreach($this->tFields as $k => $field) if($field instanceof field_abstract)
            $field->input($dados, $prefixo);
        else
            echo $field;
    }

    public static function header($pathUpload = ''){
        foreach(self::$types as $type => $load)
            if(!!$load && method_exists($type, 'header'))
                call_user_func_array([$type, 'header'], [$pathUpload]);
    }

    public static function salve(array &$post){
        foreach(self::$sFilds as $k => $field) $field->salve($post);
    }

    public static function footer(){ ?>
        <!-- Templates dos campos do formulario -->
        <?php foreach(self::$types as $type => $load) if(!!$load && method_exists($type, 'template')){ call_user_func([$type, 'template']); } ?>

        <!-- Scripts dos campos do formulario -->
        <script type="text/javascript">
            (function($){ $(function(){

                <?php foreach(self::$types as $type => $load) if(!!$load && method_exists($type, 'script')) call_user_func([$type, 'script']); ?>

            })})(jQuery);
        </script>
    <?php }
}