<?php
namespace UsrContent\Field;

interface Field_interface{

    public function __construct(array $field);

    public static function header();

    public function input($dados, $prefixo = null);

    public function salve(array &$post);

    public static function template();

    public static function script();

}