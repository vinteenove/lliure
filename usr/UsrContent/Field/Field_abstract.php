<?php
namespace UsrContent\Field;
use LliurePanel\ll;
abstract class Field_abstract implements Field_interface{

    /**
     * Construtro do campo com os dados do campo que sera construido.
     * @param array $field
     */
    public function __construct(array $field){}

    /**
     * metodo executado no header da pagina onde é carregado o formulario.
     */
    public static function header(){}

    /**
     * medoto onde é desenhado o template.
     * recebe os dados que já estão salvos (se existir) de todos os campos
     * use localizador para localisar os dados referente ao seu campo.
     * @param array $dados
     * @param string|null $prefixo
     */
    public function input($dados, $prefixo = null){
        echo '<pre>' . get_called_class() . '::this = '. print_r($this, 1). '</pre>';
    }

    /**
     * este metodo retorna um prefixo e sufixo padrão para o input
     * @param bool|static $group
     * @param bool|static $label
     * @return array
     */
    final protected static function base($label = false, $group = false){
        $r = ['before' => '', 'after' => ''];
        if($group) $r['before'] .= '<div class="form-group">';
        if($label) $r['before'] .= "<label>{$label}</label>";
        if($group) $r['after'] .= '</div>';
        return array_values($r);
    }

    /**
     * metodo para desenha posiveis templates que seu campo possa usar
     * [metodo roda no rodapé do sistema]
     */
    public static function template(){
        echo '';
    }

    /**
     * metodo para desenha posiveis scripts que seu campo possa usar
     * [metodo roda no rodapé do sistema]
     */
    public static function script(){
        echo '';
    }

    /**
     * metodo usado para tratar dados no post do formulario
     * @param array $post
     */
    public function salve(array &$post){}


    protected static function montaSelect($name, $datas, $options, $first = false){
        return \Helpers\HtmlHelper::select($name, (array) $datas, $options, $first);
    }
    protected static function montaInput($name, $datas, $chackbox = false){
        return \Helpers\HtmlHelper::input($name, (array) $datas, $chackbox);
    }
    protected static function montaAttr(array $attrs){
        return \Helpers\HtmlHelper::attr($attrs);
    }
    /**
     * localiza um $needle em um $haystack, parecido com array_search
     * mas $needle é um array com uma chave recursivamente, iste é,
     * um array com uma chave com uma array com uma chave, etc.
     * terminando com um array com uma chave e de valor NULL.
     *
     * retorna o valor localizado como um ponteiro de $haystack ou NULL.
     *
     * *CUIDADO*: NULL pode ser o valor localizado ou o valor padrão de retorno
     *
     * @param array $needle
     * @param array $haystack
     * @return mixed|null
     */
    protected static function &getValue(array $needle, &$haystack){
        $return = null;
        foreach ($needle as $k => $v) break;
        if(!is_array($v) && isset($haystack[$k])){
            $r =& $haystack[$k]; return $r;
        }elseif(is_array($v) && isset($haystack[$k]))
            return self::getValue($v, $haystack[$k]);
        return $return;
    }
    protected static function setValue(array &$nameArray, $value){
        if(empty($nameArray)){
            $nameArray = $value;
            return;}

        foreach ($nameArray as $k => $v) break;
        if(is_array($v))
            self::setValue($nameArray[$k], $value);
        else
            $nameArray[$k] = $value;
    }
    /**
     * @param string $name
     * @return array
     */
    protected static function nameExplode($name){
        parse_str($name. '=', $array);
        return $array;
    }
    /**
     * Implode os nomes em array cassata em uma string
     * @param array $name1 <p>
     * o primeiro nome arrayCascata a ser implodido
     * </p>
     * @param array $name2 [optional]
     * @param array $_     [optional]
     * @return string String resultado a imploção dos arrays.
     */
    protected static function nameImplode(array $name1, array $name2 = null, array $_ = null){
        $name = [];
        foreach(func_get_args() as $arg) if(!empty($arg)) self::setValue($name, (array) $arg);
        return rawurldecode(substr(($k = http_build_query($name)),0, (strpos($k, '='))));
    }
    /**
     * @param string $prefixo
     * @param string $name
     * @return string
     */
    protected static function nameResolve($prefixo, $name){
        return (($prefixo !== null)? self::nameImplode(self::nameExplode($prefixo), self::nameExplode($name)): $name);
    }
    /**
     * @param string $class
     * @return string
     */
    protected static function addClass(){
        $class = [];
        foreach(func_get_args() as $arg){
            $arg = explode(' ', (string) $arg);
            $arg = array_filter($arg);
            $class = array_merge($class, $arg);
        }
        $class = array_unique($class);
        return implode(' ', $class);
    }
    /**
     * @param string $class
     * @return string
     */
    protected static function removeClass(){
        $class = false;
        foreach(func_get_args() as $arg){
            $arg = explode(' ', (string) $arg);
            $arg = array_filter($arg);
            $class = (($class === false)? $arg: array_diff((array) $class, $arg));
        }
        $class = array_unique((array) $class);
        return implode(' ', $class);
    }
}