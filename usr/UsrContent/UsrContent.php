<?php
namespace UsrContent;

use Api\Vigile\Vigile;
use Helpers\HttpHelper;
use LliurePanel\ll;
use Api\Navigi\Navigi;
use Persona\PersonaBuilder;
use UsrContent\Field\Fields;

class UsrContent implements PersonaBuilder{

    public static function build():void{
        \Api\Navigi\Navigi::build();
        \Api\Vigile\Vigile::build();

        require_once __DIR__ . '/Field/fields.php';
    }

    /** @var $fields fields */
    private
        $form,
        $diretorioData,
        $diretorioFile,
        $pageType,
        $pageFrom,
        $fields,
        $dado = [],
        $navigiConf,
        $navigi;

    public function __construct(array $form, string $diretorioData = '', ?string $diretorioFile = null){
        $this->form = $form;
        $this->diretorioData = rtrim(HttpHelper::path((($diretorioData)? $diretorioData: '')), '\/') . DIRECTORY_SEPARATOR;
        $this->diretorioFile = rtrim(HttpHelper::path((($diretorioFile)? $diretorioFile: '')), '\/') . DIRECTORY_SEPARATOR;
    }

    /**
     * @param array $where
     * @param array $order
     * @param int   $pp
     * @param int   $pg
     * @return UsrContentIterator
     */
    public function filter(array $where = [], array $order = [], $pp = null, $pg = 1){
        $lista = new UsrContentIterator($this->diretorioData);
        return $lista->get($where, $order, $pp, $pg);
    }

    /**
     * @param null|string|int $id
     * @return UsrContentIterator
     */
    public function get($id = null){
        $lista = new UsrContentIterator($this->diretorioData);
        return $lista->getById($id);
    }

    /**
     * @param string $menu
     * @param string $pagina
     * @param null|int|string $id
     * @param mixed  $content
     * @return bool|int
     */
    public function set($id = null, $content = []){

        $file = (\Helpers\HttpHelper::path(self::file($this->diretorioData, $id)));
        if(!file_exists($file)) self::path_exite(dirname($file));

        $content = json_encode($content, JSON_PRETTY_PRINT);
        return file_put_contents($file, $content);
    }

    /**
     * @param string $menu
     * @param string $pagina
     * @param null|int|string $id
     * @return bool|int
     */
    public function del($id = null){
        $file = (\Helpers\HttpHelper::path(self::file($this->diretorioData, $id)));
        if(file_exists($file)) return @unlink($file);
        return false;
    }

    /**
     * @uses usrContent::filter([], [], $pp, $pg)
     * @deprecated
     */
    public function listing($pp = null, $pg = 1){
        trigger_error('use usrContent->filter([], [], $pp, $pg);', E_USER_DEPRECATED);
        return $this->filter([], [], $pp, $pg);
    }

    public function nextId(){
        return $this->filter()->nextId();
    }



    public function header(){
        global $_ll;

        $form = $this->form;

        $this->pageType = ((isset($form['list']))? 'post': 'conf');
        $this->pageFrom = (($_ll['operation_mode'] == 'os' || $_ll['operation_mode'] == 'oc' || $this->pageType == 'conf' || ($this->pageType == "post" && (isset($_GET['id']) || isset($_GET['new']))))? true: false);


        $this->navigiConf = [];
        if($this->pageType == 'post'){
            $this->navigiConf = array_merge($k = [
                'rest'      => null,
                'delete'    => false,
                'rename'    => false,
                'exibicao'  => \Api\Navigi\Navigi::LISTA,
                'paginacao' => false,
                'config'    => [
                    'id'     => 'id',
                    'coluna' => 'nome',
                ],
                'etiqueta'  => [
                    'id'     => ['label' => 'Cod.'],
                    'coluna' => ['label' => 'Nome'],
                ],
                'order'     => [],
                'debug'     => false,

            ], array_intersect_key($form['list'], $k));
            if(!isset($this->navigiConf['config']['link'])) $this->navigiConf['config']['link'] = $_ll[$_ll['operation_type']]['home'] . '/cpg=' . $_GET['cpg'] . '/id=';
        }

        if($this->pageFrom){

            if($this->pageType == 'post' && isset($_GET['id'])) $this->dado = $this->filter([['id' => $_GET['id']]]);

            if($this->pageType == 'conf') $this->dado = $this->filter();

            $useForm = $form['fields'];

            if($this->pageType == 'post' && ((isset($_GET['id']) && empty($this->dado)) || isset($_GET['new'])))
                $useForm = ((isset($form['fieldsToNew']))? $form['fieldsToNew']: $useForm);

            if($this->pageType == 'post' && isset($_GET['id']) && !empty($this->dado))
                $useForm = ((isset($form['fieldsToUpdate']))? $form['fieldsToUpdate']: $useForm);

            $this->fields = new fields($useForm);
            fields::header($this->diretorioFile);

        }else{

            $this->navigi = new Navigi();
            $this->navigi->rest = ((isset($this->navigiConf['rest']))? $this->navigiConf['rest']: $_ll[$_ll['operation_type']]['onserver'] . '/cpg=' . $_GET['cpg'] . '/ac=contentNavigi');
            //$navigi->debug = true;

            $this->navigi->delete = $this->navigiConf['delete'];
            $this->navigi->rename = $this->navigiConf['rename'];
            $this->navigi->exibicao = $this->navigiConf['exibicao'];
            $this->navigi->paginacao = $this->navigiConf['paginacao'];
            $this->navigi->config = $this->navigiConf['config'];
            $this->navigi->etiqueta = $this->navigiConf['etiqueta'];
            $this->navigi->order = $this->navigiConf['order'];
            $this->navigi->debug = ((ll_tsecuryt())? $this->navigiConf['debug']: false);
        }
    }

    public function monta(){
        global $_ll;
        if($this->pageFrom){ ?>
            <form id="formSitePagina" action="<?php echo \Helpers\HttpHelper::pathToUri(ll::baseDir()  . ll::$data->app->onserver, $_GET, ['id', 'new', 'apm'], '/', '/') . ((isset($_GET['id']))? 'id=' . $_GET['id'] . '/': '') ?>ac=contentSalvar/" method="post" enctype="multipart/form-data">
                <?php $this->fields->form(((isset($this->dado) && !empty($this->dado))? $this->dado: [])); ?>
                <div class="text-right">
                    <a href="<?php echo \Helpers\HttpHelper::pathToUri(ll_dir . ll::$data->app->home, $_GET, ['id', 'new']); ?>" class="btn btn-default">Cancelar</a>
                    <button type="submit" class="btn btn-lliure">Salvar</button>
                </div>
            </form>
        <?php }else $this->navigi->monta();
    }

    public function onserver(){
        global $_ll;

        switch(((isset($_GET['ac']))? $_GET['ac']: null)){
            case 'contentSalvar':

                if(!!$this->diretorioFile){
                    $fileup = new \Api\Fileup\Fileup();
                    $fileup->directory = $this->diretorioFile;
                    $fileup->up();
                }

                fields::salve($_POST);

                if($this->pageType == 'conf') $id = null;
                else $id = $_GET['id'] = ((isset($_GET['id']))? $_GET['id']: $this->nextId());

                $this->set($id, $_POST);

                Vigile::success('Dados salvos');

                header('Location: '. ll::$data->url->endereco . \Helpers\HttpHelper::pathToUri(ll::baseDir() . ll::$data->app->home, array_filter($_GET), ['id', 'new', 'apm', 'ac'], '/', '/'));
            break;
            case 'contentNavigi':
                ContentPagNavigi::$content = $this;
                ContentPagNavigi::$navigiConf = $this->navigiConf;
                ContentPagNavigi::start();
            break;
        }
    }

    public function onclient(){

    }



    /**
     * @param string $file
     * @return array
     */
    protected static function load($file){
        return json_decode(file_get_contents($file), true);
    }

    /**
     * @param string $menu
     * @param string $pagina
     * @param null|int|string $id
     * @return string
     */
    private static function file($diretorio, $id = null){
        $path = explode(DIRECTORY_SEPARATOR, rtrim($diretorio,DIRECTORY_SEPARATOR));
        $file = array_pop($path);
        return implode(DIRECTORY_SEPARATOR, array_merge($path, [$file, (((!is_null($id))? $id: $file) . '.json')]));
    }

    /**
     * @param string $path
     */
    protected static function path_exite($path){
        if(!file_exists($path)){
            self::path_exite(dirname($path));
            mkdir($path);
        }
    }

}
