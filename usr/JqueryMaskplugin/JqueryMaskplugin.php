<?php


namespace JqueryMaskplugin;


use Persona\Persona;

class JqueryMaskplugin implements \Persona\PersonaBuilder
{
    public static function build(): void
    {
        Persona::script(__DIR__. '/jquery.mask.min.js', Persona::MEDIUM_PRIORITY);
    }
}