<?php


namespace Persona;


interface PersonaBuilder
{
    public static function build():void;
}