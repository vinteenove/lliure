<?php
namespace Persona;

use Router\Strategy\StrategyInterface;

interface PersonaInterface extends StrategyInterface{
    public static function getThemerName(): string;

    public static function bootstrap();
    public static function build();

    public static function wli($appView);
    public static function nli($appView);

    public static function page_denied();
    public static function page_404();
}