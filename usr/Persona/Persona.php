<?php

namespace Persona;

use Helpers\HttpHelper;
use Lliure\Http\Message\Response;
use Lliure\Http\Message\ServerRequest;
use Lliure\Http\Message\Uri;
use LliurePanel\ll;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Router\Route;

class Persona
{
    use PersonaAssets;

    const HIGH_PRIORITY = 1;
    const COMPONENT_PRIORITY = 5;
    const MEDIUM_PRIORITY = 10;
    const LOW_PRIORITY = 20;

    const HEADER_LOCATION = 'header';
    const FOOTER_LOCATION = 'footer';

    private string $viewRender;

    public static PersonaData $theme;
    public static string $themeDefault = \AdminLTE\AdminLTE::class;

    public static function getTheme(): string{
        if(class_exists('\Etc\Persona\PersonaTheme', false)){
            return '\Etc\Persona\PersonaTheme';
        }else{
            return self::$themeDefault;
        }
    }


    public static function build()
    {
        static::getTheme()::bootstrap();

        /* $uri = HttpHelper::parseFriendlyUri(HttpHelper::getPageUri());
        $query = $uri['query'] ?? '/'; */

        $ServerRequest = new ServerRequest($_SERVER['REQUEST_METHOD'], Uri::fromParts(HttpHelper::parseFriendlyUri(HttpHelper::getPageUri())));

        $controller = ll::$router->dispatch($ServerRequest);

        if($controller !== null){
            static::getTheme()::build();

            if(method_exists($controller, 'view')){
                static::getTheme()::{ll::$data->enter_mode}($controller->view());
            } else {
                static::getTheme()::{ll::$data->enter_mode}($controller);
            }
        }

    }

    public static function bootstrap()
    {
        self::$theme = new PersonaData(ll::$data);
    }



    /**
     * Discarrega od assets do cabeçalho
     */
    public static function head()
    {
        self::unloadAssets(self::HEADER_LOCATION);
    }

    /**
     * Discarrega od assets do rodapé
     */
    public static function footer()
    {
        self::unloadAssets(self::FOOTER_LOCATION);
    }

    public static function buildeResponse(Route $route, ServerRequestInterface $request): ResponseInterface{

        $controller = $route->getCallable();

        $controllerResponse = $controller($request, $route->getVars());

        if($controllerResponse instanceof ResponseInterface){
            return $controllerResponse;
        }

        if(is_object($controllerResponse) && method_exists($controllerResponse, 'view')){
            $body = $controllerResponse->view();
        } else {
            $body = (string) $controllerResponse;
        }

        $response = new Response();

        if(is_string($body)){
            $response->getBody()->write($body);
        }

        return $response;
    }

    public function __construct()
    {

    }

    public function view($view = null, ?array $data = null)
    {
        if($view === null){
            return $this->getViewRender();
        } else {
            $this->setViewRender($view, $data);
        }

        return $this;
    }

    /**
     * @param string $view
     * @param array $data
     */
    public function setViewRender(string $view, array $data): void
    {
        $template = new \League\Plates\Engine(ll::baseDir() . ll::$data->operation_type .'/'. ll::$data->operation_key .'/Views/' . static::getTheme()::getThemerName());

        ob_start();
        echo $template->render($view, $data);
        $this->viewRender = ob_get_clean();
    }

    /**
     * @return string
     */
    public function getViewRender()
    {
        return $this->viewRender;
    }
}