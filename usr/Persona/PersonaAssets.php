<?php

namespace Persona;

use LliurePanel\ll;

trait PersonaAssets{

    private static array $assets = [];

    /**
     * @param string $script
     * @param int    $priorit
     * @param string $location
     */
    public static function script(string $script, int $priorit = 10, string $location = self::FOOTER_LOCATION): void
    {
        $location = ((in_array($location, [self::HEADER_LOCATION, self::FOOTER_LOCATION]))? ":{$location}": '');
        self::add($script,"js{$location}", $priorit);
    }

    /**
     * @param        $style
     * @param int    $priorit
     * @param string $location
     */
    public static function style(string $style, $priorit = 10, string $location = self::HEADER_LOCATION)
    {
        $location = ((in_array($location, [self::HEADER_LOCATION, self::FOOTER_LOCATION]))? ":{$location}": '');
        self::add($style,"css{$location}" ,$priorit);
    }

    /**
     * @param        $call_function
     * @param int    $priorit
     * @param string $location
     */
    public static function call($call_function, $priorit = 10, string $location = self::HEADER_LOCATION)
    {
        $location = ((in_array($location, [self::HEADER_LOCATION, self::FOOTER_LOCATION]))? ":{$location}": '');
        self::add($call_function,"call{$location}" ,$priorit);
    }

    /**
     * @param        $content
     * @param int    $priorit
     * @param string $location
     */
    public static function integral($content, $priorit = 10, string $location = self::HEADER_LOCATION)
    {
        $location = ((in_array($location, [self::HEADER_LOCATION, self::FOOTER_LOCATION]))? ":{$location}": '');
        self::add($content,"integral{$location}" ,$priorit);
    }

    /**
     * carrera scripts, estilos, metas tags, chamadas ou componentes para o sistema
     *
     * carregando scripts e estilos.
     * Persona::add('app/teste/estilo.css'); // carrega meu estilo
     * Persona::add('app/teste/script.js'); // carrega meu script
     *
     * carregando scripts e estilos, marcando o tipo.
     * Persona::add('app/teste/estilo.css', 'css'); // carrega meu estilo
     * Persona::add('app/teste/script.js', 'js'); // carrega meu script
     * Persona::add('app/teste/estilo.css.php', 'css'); // carrega um arquivo php como um estilo
     *
     * carregando scripts e estilos, marcando o tipo e mudando a prioridade.
     * Persona::add('app/teste/estilo.css.php', 'css', 15);
     *
     * @OBS.: as prioridades serven para determinar quando seu arquivo aparecera. a prioridade padrão é 10,
     * e quanrto menor este numero, mais para o inicio do documento seu arquivo aparecera. Procure sempre
     * usar prioridades de valor maior que 10 pos abaixo disto é reservado para o sistema.
     *
     * fixando tags personalisados no heder.
     * Persona::add(array('http-equiv' => 'Content-Type', 'content' => 'text/html; charset=iso-8859-1'), 'meta');
     *
     * carregando um arquivo .php.
     * Persona::add('app/teste/teste.php'); //faz um require no arquivo no começo do documento (requere)
     *
     * carregando uma call (chamado a uma funcao ou metodo estatico).
     * Persona::add('func_teste', 'call'); //carrega uma funcao especifica
     * Persona::add('class_teste::func_teste', 'call'); //carrega um metodo especifica
     * Persona::add(array('class_teste::func_teste', $var1, $var2), 'call'); //carrega um metodo especifica passando parametros para ele
     *
     * @param string|array $file
     * @param string $parans
     * @param int $priorit
     * @return bool
     */
    public static function add($file, $parans = '', $priorit = 10)
    {

        $loc = 'header';
        $async = false;
        $type = null;
        $types = ['js', 'css', 'php', 'meta', 'bese', 'link', 'call', 'integral'];

        $parans = array_filter(explode(':', $parans));

        $searchParan = function (array $parans, array $search) {
            foreach ($search as $term) if (($k = array_search($term, $parans)) !== false) return $k;
            return false;
        };

        if (false !== ($k = $searchParan($parans, ['header', 'footer']))) {
            $loc = $parans[$k];
            unset($parans[$k]);
        }

        if (false !== ($k = $searchParan($parans, ['async']))) {
            $async = true;
            unset($parans[$k]);
        }

        if (false !== ($k = $searchParan($parans, $types))) {
            $type = $parans[$k];
            unset($parans[$k]);
        }

        if ($type === null && is_array($file)) {
            $type = 'meta';
        }

        if ($type === null) {
            $f = parse_url($file);
            $e = explode(".", $f['path']);
            $ext = strtolower(array_pop($e));
            $type = $ext;
        }

        if (null === array_search($type, $types)){
            return false;
        }

        if (is_string($file) && ($type == 'js' || $type == 'css')) {
            $f = parse_url($file);

            if (!(isset($f['host']))){
                $file = rtrim(ll::$data->url->real, '/') . '/' . \Helpers\HttpHelper::pathToUri($file);
            }
        }

        if ($type == 'call' && is_array($file) && isset($file[0]) && strpos($file[0], '::') !== false) {
            $file[0] = explode('::', $file[0]);
        }

        if ($type == 'css') {
            $type = 'link';
            $file = (!is_array($file) ? array_merge(['type' => 'text/css', 'rel' => 'stylesheet', 'href' => $file], (($async) ? ['async'] : [])) : $file);
        }

        if ($type == 'js') {
            $file = (!is_array($file) ? ['src' => $file] : $file);
            $file = array_merge(['type' => 'text/javascript'], $file);
            if ($async && false === array_search('async', $file)) {
                $file = array_merge($file, ['async']);
            }
        }

        foreach (self::$assets as $l => $ps) {
            foreach ($ps as $p => $is) {
                foreach ($is as $i => $ts) {
                    foreach ($ts as $t => $f) {
                        if ($f == $file) {
                            return false;
                        }
                    }
                }
            }
        }

        $reord = !(isset(self::$assets[$loc][$priorit]));
        self::$assets[$loc][$priorit][][$type] = $file;

        if ($reord) {
            ksort(self::$assets[$loc]);
        }

        return true;
    }

    /**
     * @param string $location
     */
    protected static function unloadAssets(string $location){
        if(!isset(self::$assets[$location]) || empty(self::$assets[$location])){
            return;
        }

        $ds = self::$assets[$location];
        $tab = "\t"; $bl = "\n"; $pre = $bl. $tab;

        foreach($ds as $p => $is){ // Documentos, posição, índices
            foreach($is as $i => $ts){ // índices, índice, Tipos
                foreach($ts as $t => $f){ // Tipos, Tipos, função
                    switch($t){
                        case 'meta':
                        case 'base':
                        case 'link':
                            echo $pre . '<' . $t . \Helpers\HtmlHelper::attr($f) . '/>';

                        break;
                        case 'css':
                            echo $pre . '<link type="text/css" href="' . $f . '">';

                        break;
                        case 'js':
                            echo $pre . '<script ' . \Helpers\HtmlHelper::attr($f) . '></script>';

                        break;
                        case 'integral':
                            if(is_array($f)) foreach($f as $i)
                                echo $pre . (string) $i;
                            else
                                echo $pre . $f;

                        break;
                        case 'php':
                            echo $bl;
                            require $f;

                        break;
                        case 'call':
                            echo $bl;
                            if(is_array($f))
                                @call_user_func_array(array_shift($f), $f);
                            else
                                @call_user_func($f);
                        break;
                    }
                }
            }
        }
    }



    abstract public static function head();

    abstract public static function footer();
}