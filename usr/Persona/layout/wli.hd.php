<?php
use LliurePanel\ll;
use Persona\Persona;

global $personaData;
\Api\Jfbox\Jfbox::build();

$personaData = array_replace([
    'color'      => '94324b',
    'logo'       => Persona::$theme->path. 'layout/logo.php',
    'link'       => 'http://www.lliure.com.br',
    'assinatura' => 'lliure '. @$_ll['conf']->versao,
], (array) ((isset(Persona::$theme->chave, $_ll['conf']->grupo->{Persona::$theme->chave}))? $_ll['conf']->grupo->{Persona::$theme->chave}: []));

$personaData['color'] = ((isset(Persona::$theme->chave, $_ll['conf']->grupo->{Persona::$theme->chave}->color))
    ? ['cor' => $_ll['conf']->grupo->{Persona::$theme->chave}->color]
    : ['cor' => $personaData['color']]
);

Persona::add(__DIR__. '/wli.css.php?' . http_build_query($personaData['color']) , 'css', 5);

