<?php
use LliurePanel\ll;
use Persona\PersonaLayout;

global $personaData;
?>
<header id="ll_topo">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button id="persona-navbar-collapse-button" type="button" class="navbar-toggle collapsed text-left" data-toggle="collapse" data-target="#persona-navbar-collapse" aria-expanded="false">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
                <div id="lliurelogoMargen" class="hidden-md hidden-sm hidden-lg"></div>
                <?php if(ll::valida() && $_ll['operation_type'] == 'app'){ ?>
                    <button type="button" class="btn btn-sm btn-add-desktop btn-lliure" style="padding: 5px; float: left; margin: 5px 0; position: relative; ">
                        <i class="fa fa-desktop" aria-hidden="true"></i>
                    </button>
                <?php } ?>
                <a href="<?php echo ll::$data->url->endereco;?>" class="navbar-brand logoSistema navbar-brand" >
                    <div id="lliurelogo" class="color-white" style="white-space: nowrap; max-width: 60px;"><?php require $personaData['logo']; ?></div>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="persona-navbar-collapse">
                <?php PersonaLayout::montaMenu($_ll['mainMenu']); ?>
            </div>
        </div>
    </nav>
</header>

<?php require_once ll::content(); ?>

<div id="ll_rodape_widht"></div>
<footer id="ll_rodape">
    <div class="container-fluid text-right">
        <a href="<?php echo $personaData['link']; ?>" class="ll_color-100 ll_color-100-hover" target="_blank" ><?php echo $personaData['assinatura']; ?></a>
    </div>
</footer>

<script type="text/javascript">
    (function($){$(function(){
        $('.btn-add-desktop').click(function(){
            var url = window.btoa(window.location.href);
            jfBox('onclient.php?opt=desktop&ac=addDesktop&url=' + url).open();
        });
    })})(jQuery);
</script>