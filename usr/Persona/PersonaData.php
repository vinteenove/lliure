<?php

namespace Persona;

use Helpers\HttpHelper;
use LliurePanel\LliureData;

class PersonaData
{
    public string $name;
    public string $path;
    public string $chave;
    public string $local;
    public string $exec;
    public string $home_wli;
    public string $home_nli;
    public string $user_wli;
    public array $wli = [];
    public array $nli = [];
    
    public function __construct(LliureData $data){

        /** Define o tema do sistema */
        $tema_chav = 'default';
        $tema_loca = 'default';
        $tema_name = 'persona';
        $tema_path = 'usr/persona/';
        $tema_exec = URL_NORMAL;
        $tema_homW = 'opt=desktop';
        $tema_homN = 'opt=singin';
        $tema_useW = 'opt=user';

        $this->name = $tema_name = (string) ((isset($data->conf->tema_default)) ? $data->conf->tema_default : $tema_name);
        $this->path = $tema_path = (string) ((isset($data->conf->tema_default, $data->conf->temas->{$data->conf->tema_default})) ? $data->conf->temas->{$data->conf->tema_default} : $tema_path);

        if (isset($data->conf->grupo->default)) {
            $this->chave = $tema_chav = 'default';
            $this->local = $tema_loca = $data->conf->grupo->default->local;
            if (isset($data->conf->grupo->default->template)) {
                if (isset($data->conf->temas->{$data->conf->grupo->default->template})) {
                    $this->name = $tema_name = (string)$data->conf->grupo->default->local;
                    $this->path = $tema_path = (string)$data->conf->temas->{$data->conf->grupo->default->template};
                } else {
                    $this->name = $tema_name = (string)$data->conf->grupo->default->local;
                    $this->path = $tema_path = (string)$data->conf->grupo->default->template;
                }
            }
            $this->exec = $tema_exec = (string)(isset($data->conf->grupo->default->execucao) ? $data->conf->grupo->default->execucao : $tema_exec);
            $this->home_wli = $tema_homW = (string)(isset($data->conf->grupo->default->home_wli) ? $data->conf->grupo->default->home_wli : $tema_homW);
            $this->home_nli = $tema_homN = (string)(isset($data->conf->grupo->default->home_nli) ? $data->conf->grupo->default->home_nli : $tema_homN);
            $this->user_wli = $tema_useW = (string)(isset($data->conf->grupo->default->user_wli) ? $data->conf->grupo->default->user_wli : $tema_useW);
        } else {
            $this->chave = $tema_chav;
            $this->local = $tema_loca;
            $this->name = $tema_name;
            $this->path = $tema_path;
            $this->exec = $tema_exec;
            $this->home_wli = $tema_homW;
            $this->home_nli = $tema_homN;
            $this->user_wli = $tema_useW;
        }


        if (isset($data->conf->grupo)) {
            foreach ($data->conf->grupo as $g => $d) {
                if ($d->local == $data->url->local) {
                    $this->chave = $g;
                    $this->local = $d->local;
                    if (isset($data->conf->temas->{$d->template})) {
                        $this->name = (string)$d->template;
                        $this->path = (string)$data->conf->temas->{$d->template};
                    } else {
                        $this->name = (string)$g;
                        $this->path = (string)$d->template;
                    }
                    $this->exec = (string)(isset($d->execucao) ? $d->execucao : $tema_exec);
                    $this->home_wli = (string)(isset($d->home_wli) ? $d->home_wli : $tema_homW);
                    $this->home_nli = (string)(isset($d->home_nli) ? $d->home_nli : $tema_homN);
                    $this->user_wli = (string)(isset($d->user_wli) ? $d->user_wli : $tema_useW);
                    break;
                }
            }
        }

        $this->path = HttpHelper::path($data->dir . $this->path) . DIRECTORY_SEPARATOR;
        $this->wli['x'] = HttpHelper::path($tema_path . 'layout/wli.x.php');
        $this->nli['x'] = HttpHelper::path($tema_path . 'layout/nli.x.php');
        $this->wli['hd'] = (file_exists($f = HttpHelper::path($tema_path . 'layout/wli.hd.php')) ? $f : '');
        $this->nli['hd'] = (file_exists($f = HttpHelper::path($tema_path . 'layout/nli.hd.php')) ? $f : '');
        $this->wli['css'] = (file_exists($f = HttpHelper::path($tema_path . 'layout/wli.css')) ? $f : '');
        $this->nli['css'] = (file_exists($f = HttpHelper::path($tema_path . 'layout/nli.css')) ? $f : '');


        if (file_exists($f = HttpHelper::path($this->path . 'layout/wli.x.php'))) {
            $this->wli['x'] = $f;
            if (file_exists($f = HttpHelper::path($this->path . 'layout/wli.hd.php'))) $this->wli['hd'] = $f;
            if (file_exists($f = HttpHelper::path($this->path . 'layout/wli.css'))) $this->wli['css'] = $f;
        }

        if (file_exists($f = HttpHelper::path($this->path . 'layout/nli.x.php'))) {
            $this->nli['x'] = $f;
            if (file_exists($f = HttpHelper::path($this->path . 'layout/nli.hd.php'))) $this->nli['hd'] = $f; else $this->nli['hd'] = '';
            if (file_exists($f = HttpHelper::path($this->path . 'layout/nli.css'))) $this->nli['css'] = $f; else $this->nli['css'] = '';
        }
    }
}