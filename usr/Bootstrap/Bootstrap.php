<?php

namespace Bootstrap;

use LliurePanel\ll;
use Persona\Persona;

class Bootstrap implements \Persona\PersonaBuilder
{

    public static function build(): void
    {
        \Jquery\Jquery::build();

        /*\Persona\Persona::style(ll::baseDir() . '/vendor/twbs/bootstrap/dist/css/bootstrap-reboot.css', Persona::HIGH_PRIORITY);
        \Persona\Persona::style(ll::baseDir(). '/vendor/twbs/bootstrap/dist/css/bootstrap.min.css', Persona::HIGH_PRIORITY);
        \Persona\Persona::style(ll::baseDir(). '/vendor/twbs/bootstrap/dist/css/bootstrap-grid.min.css', Persona::HIGH_PRIORITY);*/
		\Persona\Persona::style('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css', Persona::HIGH_PRIORITY);

        //\Persona\Persona::script(ll::baseDir(). 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js',   Persona::HIGH_PRIORITY);
		\Persona\Persona::script('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js',   Persona::HIGH_PRIORITY);

        \Persona\Persona::integral(array(
            '<!-- bootstrap -->',
            '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">',
        ), Persona::LOW_PRIORITY);
    }
}