<?php

namespace GoogleMaterialIcons;
use Persona\Persona;

class GoogleMaterialIcons  implements \Persona\PersonaBuilder
{
    public static function build(): void
    {
        \Persona\Persona::style(__DIR__ . '/css/google-material-icons.css', Persona::HIGH_PRIORITY);
    }
}