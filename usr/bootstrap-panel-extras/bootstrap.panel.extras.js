;(function($){
    $(function(){
        $('body')

        .on('click', '[data-toggle="panel-collapse"]', function (){
            const $card = $(this).parents('.panel, .card')[0];
            $($card).trigger('panel-toggle');
        })

        .on('click', '[data-toggle="panel-delete"]', function (){
            const $group = $(this).closest('.panel-group, .accordion');
            $(this).parents('.panel, .card').eq(0).remove();
            $group.trigger('panel-delete');
        })

        .on('panel-toggle', '.panel, .card', function (e){
            const $target = $(this).find('.panel-collapse, .collapse').eq(0);
            $target.collapse('toggle');
            e.stopPropagation();
            return false;
        })

        .on('panel-show', '.panel', function (e){
            const $target = $(this).find('.panel-collapse, .collapse').eq(0);
            $target.collapse('show');
            e.stopPropagation();
            return false;
        })

        .on('panel-hide', '.panel', function (e){
            const $target = $(this).find('.panel-collapse, .collapse').eq(0);
            $target.collapse('hide');
            e.stopPropagation();
            return false;
        });

    });
})(jQuery);