<?php
use LliurePanel\ll;
use Persona\Persona;

\Jquery\Jquery::build();

Persona::add(__DIR__. '/superfiltro.css', 'css', 5);
Persona::add(__DIR__. '/superfiltro.js', 'js', 5);