;superFiltro = ((typeof superFiltro !== 'undefined') ? superFiltro : {});
(function ($) {

    $.fn.suberFiltro = function (settings) {

        var o = {
            'debug': false,
            'attrKey': 'data-sf',

            'class-visible': 'sf-visible',
            'class-hidden': 'sf-hidden',
            'trigger-visible': 'sf-visible',
            'trigger-hidden': 'sf-hidden',

            'class-disabled': 'sf-disabled',
            'class-enabled': 'sf-enabled',
            'trigger-disabled': 'sf-disabled',
            'trigger-enabled': 'sf-enabled'
        };
        if (settings) $.extend(o, settings);

        return function () {
            return $(this).each(function () {
                if (o['debug']) console.group('Filter start');

                var value = [];
                var $this = $(this);
                var key = $this.attr(o['attrKey']);
                var context = (($this.attr('data-sf-context')) ? $($this.parents($this.attr('data-sf-context'))[0]) : $('body'));

                $([
                    ':input[data-sf="' + key + '"]:not(:radio):not(:checkbox):not(button)',
                    ':input[data-sf="' + key + '"]:radio:checked:not(:disabled)',
                    ':input[data-sf="' + key + '"]:checkbox:checked:not(:disabled)'
                ].join(', '), context).each(function (e) {
                    value.push({
                        name: (($(this).attr('data-sf-name')) ? $(this).attr('data-sf-name') : $(this).attr('name')),
                        value: $(this).val(),
                        disabled: $(this).is(':disabled'),
                        visible: $(this).is(':visible')
                    })
                });

                if (o['debug']) console.log('context', context);
                if (o['debug']) console.log('key', key);
                if (o['debug']) console.table(value);

                var process = function (c, t, f){
                    if (!c) return;
                    c = JSON.parse(c);
                    if (!c[key]) return;
                    if (o['debug']) console.log(c);
                    var show = null;

                    for (var i = 0; i < value.length; i++) {
                        var v = value[i];

                        var val, disabled = false, visible = null;

                        switch (typeof c[key][v.name]) {
                            case 'undefined':
                                continue;

                            case 'object':
                                if (Array.isArray(c[key][v.name]))
                                    val = c[key][v.name];

                                else if (typeof c[key][v.name].value !== "undefined")
                                    val = c[key][v.name].value;

                                else
                                    val = c[key][v.name];

                                if (typeof c[key][v.name]['disabled'] !== "undefined")
                                    disabled = c[key][v.name]['disabled'];

                                if (typeof c[key][v.name].visible !== "undefined")
                                    visible = c[key][v.name].visible;

                            break;
                            default:
                                val = c[key][v.name];

                            break;
                        }

                        if (o['debug']) console.table([
                            {'label': 'v.name', 'value': v.name},
                            {'label': 'v.value', 'value': v.value},
                            {'label': 'val', 'value': val},
                            {'label': 'disabled', 'value': disabled},
                            {'label': 'visible', 'value': visible}
                        ]);

                        if (disabled !== null && ((Array.isArray(disabled) && !(disabled.indexOf(v.disabled) + 1)) || (!Array.isArray(disabled) && disabled !== v.disabled)))
                            continue;

                        if (visible !== null && ((Array.isArray(visible) && !(visible.indexOf(v.visible) + 1)) || (visible !== v.visible)))
                            continue;

                        if (typeof val === "undefined") {
                            //console.log('continue', show);
                            continue;

                        } else if (Array.isArray(val)) {
                            show = !!(val.indexOf(v.value) + 1);
                            if (o['debug']) console.log('val is array: ', show);

                        } else if ((typeof c[key][v.name]) === 'object'){
                            show = !!(val[v.value]);
                            if (o['debug']) console.log('val is object: ', show);

                        } else {
                            show = (val == v.value);
                            if (o['debug']) console.log('val generic: ', show);
                        }

                        //console.log(show);
                        if (!show) break;
                    }

                    //console.log(show);

                    if (show === true) t.call();
                    if (show === false) f.call();
                };

                var target = $('[data-sf-hide]', context);
                if (target.length) {
                    if (o['debug']) console.group('sf-hide');
                    if (o['debug']) console.log(target);
                    target.each(function (i, e) {
                        if (o['debug']) console.groupCollapsed('Item: ' + i);
                        process($(e).attr('data-sf-hide'), function () {
                            $(e).removeClass(o['class-visible']).addClass(o['class-hidden']).trigger(o['trigger-hidden']);
                            if (o['debug']) console.log('Item: ' + i + ' | hidden');
                        }, function () {
                            $(e).removeClass(o['class-hidden']).addClass(o['class-visible']).trigger(o['trigger-visible']);
                            if (o['debug']) console.log('Item: ' + i + ' | visible');
                        });
                        if (o['debug']) console.groupEnd();
                    });
                    if (o['debug']) console.groupEnd();
                }

                target = $('[data-sf-show]', context);
                if (target.length) {
                    if (o['debug']) console.group('sf-show');
                    if (o['debug']) console.log(target);
                    target.each(function (i, e) {
                        if (o['debug']) console.groupCollapsed('Item: ' + i);
                        process($(e).attr('data-sf-show'), function () {
                            $(e).removeClass(o['class-hidden']).addClass(o['class-visible']).trigger(o['trigger-visible']);
                            if (o['debug']) console.log('Item: ' + i + ' | visible');
                        }, function () {
                            $(e).removeClass(o['class-visible']).addClass(o['class-hidden']).trigger(o['trigger-hidden']);
                            if (o['debug']) console.log('Item: ' + i + ' | hidden');
                        });
                        if (o['debug']) console.groupEnd();
                    });
                    if (o['debug']) console.groupEnd();
                }

                target = $('[data-sf-disabled]', context);
                if (target.length) {
                    if (o['debug']) console.group('sf-disabled');
                    if (o['debug']) console.log(target);
                    target.each(function (i, e) {
                        if (o['debug']) console.groupCollapsed('Item: ' + i);
                        process($(e).attr('data-sf-disabled'), function () {
                            $(e).prop('disabled', true).removeClass(o['class-enabled']).addClass(o['class-disabled']).trigger(o['trigger-disabled']);
                            if (o['debug']) console.log('Item: ' + i + ' | disabled');
                        }, function () {
                            $(e).prop('disabled', false).removeClass(o['class-disabled']).addClass(o['class-enabled']).trigger(o['trigger-enabled']);
                            if (o['debug']) console.log('Item: ' + i + ' | enabled');
                        });
                        if (o['debug']) console.groupEnd();
                    });
                    if (o['debug']) console.groupEnd();
                }

                target = $('[data-sf-enabled]', context);
                if (target.length) {
                    if (o['debug']) console.group('sf-enabled');
                    if (o['debug']) console.log(target);
                    target.each(function (i, e) {
                        if (o['debug']) console.groupCollapsed('Item: ' + i);
                        process($(e).attr('data-sf-enabled'), function () {
                            $(e).prop('disabled', false).removeClass(o['class-disabled']).addClass(o['class-enabled']).trigger(o['trigger-enabled']);
                            if (o['debug']) console.log('Item: ' + i + ' | enabled');
                        }, function () {
                            $(e).prop('disabled', true).removeClass(o['class-enabled']).addClass(o['class-disabled']).trigger(o['trigger-disabled']);
                            if (o['debug']) console.log('Item: ' + i + ' | disabled');
                        });
                        if (o['debug']) console.groupEnd();
                    });
                    if (o['debug']) console.groupEnd();
                }

                if (o['debug']) console.groupEnd();
            });
        }
    };

    superFiltro = $.superFiltro = $.fn.suberFiltro;
    $(function () {
        $('body')
            .on('click', '[data-sf-click]', superFiltro({'attrKey': 'data-sf-click'}))
            .on('change', '[data-sf-change]', superFiltro({'attrKey': 'data-sf-change'}));
    });

})(jQuery);