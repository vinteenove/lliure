<?php

namespace Lliure\Supplements;

use Iterator;

class Supplements implements
    Iterator
{

    private string $baseDir;

    private array $supplement;

    public function __construct($filename, $process_sections = false, $scanner_mode = INI_SCANNER_NORMAL){
        if(!file_exists($filename)){
            throw new \Exception('File not found supplements');
        }

        $this->setBaseDir(dirname($filename) . DIRECTORY_SEPARATOR);

        $this->supplement = parse_ini_file($filename, $process_sections, $scanner_mode);
    }

    /**
     * @param string $baseDir
     */
    public function setBaseDir(string $baseDir): void{
        $this->baseDir = $baseDir;
    }

    /**
     * @inheritDoc
     */
    public function current(){
        // TODO: Implement current() method.
    }

    /**
     * @inheritDoc
     */
    public function next(){
        // TODO: Implement next() method.
    }

    /**
     * @inheritDoc
     */
    public function key(){
        // TODO: Implement key() method.
    }

    /**
     * @inheritDoc
     */
    public function valid(){
        // TODO: Implement valid() method.
    }

    /**
     * @inheritDoc
     */
    public function rewind(){
        // TODO: Implement rewind() method.
    }
}